#!/usr/bin/env python3

import os.path
import cv2
import numpy as np
import math
import subprocess


if __name__ == '__main__':
    exec_dir = os.path.dirname(os.path.realpath(__file__)) + "/../"
    exec_file = exec_dir + "/build/KinovisReconstruction" #path to the executable

    scansDTU = [122, 37, 24, 65, 106, 110, 114]
    scansDTUDir =  "../DTU/scan{}"

    scenesNerf = ["chair", "mic", "ship", "materials", "lego", "hotdog", "ficus", "drums"]  
    scenesNerfDir = "../nerf_synthetic/{}"

    testType = "NERF" # NERF or DTU

    dir = None
    list = None
    if testType == "DTU":
        dir = scansDTUDir
        list = scansDTU
    elif testType == "NERF":
        dir = scenesNerfDir
        list = scenesNerf


    os.chdir(exec_dir)
    for l in list:
        SceneConfigPath = dir.format(l) + "/scene_config.txt"

        args = [
            "Window#AutoSave bool 1",  # saves the parameters of the ui
            "Window#AutoLoad bool 1",  # loads the scene automatically
            "Window#SceneConfigPath string {}".format(SceneConfigPath),
            "Sequence#FullAuto bool 1",# makes the reconstruction automatic, no need to click on things
            "Sequence#AutoEval bool 1",# render the mesh in all views and export the images
            "Window#AutoQuit bool 1",  # close the window once finished
            "Window#SHBands int 0"     # number of spherical harmonic bands, can be 0, 1 or 2.
        ]

        output = dir.format(l) + "/output.txt"
        output = open(output, "w")

        popen = subprocess.Popen([exec_file, *args], stdout=subprocess.PIPE, universal_newlines=True)
        for stdout_line in iter(popen.stdout.readline, ""):
            print(stdout_line, end='') 
            output.write(stdout_line)
        output.close()
        popen.stdout.close()
        return_code = popen.wait()

