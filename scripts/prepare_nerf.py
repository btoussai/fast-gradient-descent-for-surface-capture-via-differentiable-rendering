#!/usr/bin/env python3

import math
import numpy as np
import cv2
import xml.etree.cElementTree as ET
from xml.dom import minidom
import os
import json

print("This script prepares the nerf synthetic dataset.")
nerfDir = "/usr/src/nerf_synthetic"

np.set_printoptions(suppress=True)

width = 800
height = 800

def writeCalibNerf(path, transforms, dest):

    data = json.load(open(path + "/" + transforms))
    nbCam = len(data["frames"])

    camera_angle_x = data["camera_angle_x"]

    K = np.eye(3)
    fx = width / (2.0 * math.tan(camera_angle_x / 2.0))
    fy = height / (2.0 * math.tan(camera_angle_x / 2.0))

    K[0, 0] = fx
    K[1, 1] = fy
    K[0, 2] = width / 2.0
    K[1, 2] = height / 2.0

    CalibrationResult = ET.Element("CalibrationResult")
    Information = ET.SubElement(CalibrationResult, "Information")
    Information.set('nbCamera', str(nbCam))
    Information.set('units', "mm")
    Information.set('up', "+z")

    for i in range(0, nbCam):
        cameraNode = ET.SubElement(CalibrationResult, "Camera")
        cameraNode.set("id", str(i))
        cameraNode.set("width", str(width))
        cameraNode.set("height", str(height))

        frame = data["frames"][i]
        P = np.array(frame["transform_matrix"])
        P = np.linalg.inv(P)
        P = P[:3]

        row0 = np.copy(P[0])
        row1 = np.copy(P[1])
        row2 = np.copy(P[2])

        P[1] = -row1
        P[2] = -row2

        R = P[:, :3]
        T = P[:, 3]



        K_Node = ET.SubElement(cameraNode, "K")
        K_Node.text = ' '.join(str(k) for k in np.ravel(K))
        Distortion_Node = ET.SubElement(cameraNode, "Distortion")
        Distortion_Node.set("model", "opencv")
        Distortion_Node.set("K1", str(0.0))
        Distortion_Node.set("K2", str(0.0))
        Distortion_Node.set("P1", str(0.0))
        Distortion_Node.set("P2", str(0.0))
        Distortion_Node.set("K3", str(0.0))

        R_Node = ET.SubElement(cameraNode, "R")
        R_Node.text = ' '.join(str(r) for r in np.ravel(R))
        T_Node = ET.SubElement(cameraNode, "T")
        T_Node.text = ' '.join(str(t) for t in np.ravel(T))


    xmlstr = minidom.parseString(ET.tostring(CalibrationResult)).toprettyxml(indent="   ")
    f = open(path + "/" + dest, "w")
    f.write(xmlstr)
    f.close()


def writeSequence(path):
    data = json.load(open(path + "/transforms_train.json"))
    nbCam = len(data["frames"])

    Video = ET.Element("Video")
    Information = ET.SubElement(Video, "Information")
    Information.set('nbCamera', str(nbCam))
    Information.set('fps', "30")
    Information.set('beginId', "0")
    Information.set('endId', "0")

    for i in range(0, nbCam):
        frame = data["frames"][i]
        cameraNode = ET.SubElement(Video, "Camera")
        cameraNode.set("id", str(i))
        cameraNode.set("path", path + "/train")
        cameraNode.set("fileNameFormat", "r_{}.png".format(i))

    xmlstr = minidom.parseString(ET.tostring(Video)).toprettyxml(indent="   ")
    f = open(path + "/sequence.xml", "w")
    f.write(xmlstr)
    f.close()

def makeBackgrounds(path):
    os.makedirs(path + "/Backgrounds", exist_ok=True)
    data = json.load(open(path + "/transforms_train.json"))
    nbCam = len(data["frames"])

    for i in range(0, nbCam):
        img = cv2.imread(path + "/train/r_{}.png".format(i), cv2.IMREAD_UNCHANGED)
        fg = img[:, :, 3] > 30

        img2 = np.full((height, width, 4), 0, dtype=np.uint8)
        img2[fg] = np.array([0, 0, 255, 255])
        p = path + "/Backgrounds/cam-{}.png".format(i)
        cv2.imwrite(p, img2)

def createSceneSettings(scene, outDir):
    config_file = os.path.dirname(os.path.realpath(__file__)) + "/../examples/nerf/{}_config.txt".format(scene)
    dest_path = "{}/scene_config.txt".format(outDir)
    print("Writing " + dest_path + "...")
    dest_file = open(dest_path, 'w')

    for l in open(config_file, mode='r').readlines():
        if l.startswith("Window#BackgroundsDir"):
            l = "Window#BackgroundsDir string {}\n".format(outDir + "/Backgrounds")
        elif l.startswith("Window#CalibrationFile"):
            l = "Window#CalibrationFile string {}/calib.xml\n".format(outDir)
        elif l.startswith("Window#ExportDir"):
            l = "Window#ExportDir string {}\n".format(outDir)
        elif l.startswith("Window#SequenceFile"):
            l = "Window#SequenceFile string {}/sequence.xml\n".format(outDir)
        dest_file.write(l)



scenes = ["chair", "drums", "ficus", "hotdog", "lego", "materials", "mic", "ship"]
for scene in scenes:
    print("Preparing scene: " + scene)
    path = nerfDir + "/" + scene
    writeCalibNerf(path, "transforms_train.json", "calib.xml")
    writeCalibNerf(path, "transforms_test.json", "calib_test.xml")
    writeSequence(path)
    makeBackgrounds(path)
    createSceneSettings(scene, path) 


print("Done.")
print("You can now run the main program, ")
print("then select a reconstruction by selecting 'file>import scene config' in the menu bar and navigating to the desired config file.")