#!/usr/bin/env python3

import os.path
import cv2
import numpy as np
import math
import multiprocessing

def psnr_once(images):
    renderImg, gtImg, maskImg = images

    mask = cv2.imread(maskImg, cv2.IMREAD_UNCHANGED)
    if len(mask.shape) > 2 and mask.shape[2] == 4:
        # get the alpha component
        mask = mask[:, :, 3]

        mask = np.transpose(np.array([mask, mask, mask]), (1, 2, 0))
    else:#the mask is binary: black or white
        pass

    img1 = cv2.imread(renderImg, cv2.IMREAD_UNCHANGED)[:, :, :3]
    img2 = cv2.imread(gtImg, cv2.IMREAD_UNCHANGED)[:, :, :3]

    mask = mask.astype(np.float64) / 255
    img1 = img1.astype(np.float64) / 255
    img2 = img2.astype(np.float64) / 255


    #diff = np.abs(img1 - img2)
    #cv2.imshow('diff', cv2.resize(diff, (1024, 1024)))
    #cv2.imshow('raster', cv2.resize(img1, (1024, 1024)))
    #cv2.imshow('gt', cv2.resize(img2, (1024, 1024)))
    #cv2.imshow('mask', cv2.resize(mask, (1024, 1024)))
    #cv2.waitKey(0)

    mse = np.mean((img1 - img2)**2)
    psnr = 20 * math.log10(1.0 / math.sqrt(mse))

    # print(gtImg)
    # print("psnr = ", psnr)

    return psnr

def psnr_all(groundTruthFormat, renderFormat, backgroundsFormat, title, camFirstIndex):
    i = 0
    averagePSNR = 0

    args = []
    while True:
        renderImg = renderFormat.format(i) 
        if not os.path.isfile(renderImg):
            break
        gtImg = groundTruthFormat.format(i + camFirstIndex)
        maskImg = backgroundsFormat.format(i + camFirstIndex)
        args.append((renderImg, gtImg, maskImg))
        i = i + 1

    averagePSNR = 0

    if True:
        with multiprocessing.Pool(processes=48) as pool:
            results = pool.map(psnr_once, args)
            averagePSNR = np.mean(results)
    else:
        for arg in args:
            psnr_once(arg)

    print("Average PSNR = ", averagePSNR, title)

def eval_dtu(scan):
    print("Evaluating DTU scan " + str(scan))
    scanDir = "../DTU/scan" + str(scan)
    groundTruthFormat = scanDir + "/image/{:06d}.png"
    rasterDir = scanDir + "/eval_rasterization/r_{}.png"
    volumeRenderDir = scanDir + "/eval_volumeRendering/r_{}.png"
    masksFormat = scanDir + "/mask/{:03d}.png"
    psnr_all(groundTruthFormat, rasterDir, masksFormat, " dtu scan {} with rasterization".format(scan), 0)
    psnr_all(groundTruthFormat, volumeRenderDir, masksFormat, " dtu scan {} with volume rendering".format(scan), 0)


def eval_nerfsynthetic(scene):
    scanDir = "../nerf_synthetic/" + scene
    groundTruthFormat = scanDir + "/test/r_{}.png"
    rasterDir = scanDir + "/eval_rasterization/r_{}.png"
    volumeRenderDir = scanDir + "/eval_volumeRendering/r_{}.png"
    backgroundsFormat = scanDir + "/test/r_{}.png"
    psnr_all(groundTruthFormat, rasterDir, backgroundsFormat, " nerf scene {} with rasterization".format(scene), 0)
    psnr_all(groundTruthFormat, volumeRenderDir, backgroundsFormat, " nerf scene {} with volume rendering".format(scene), 0)

if __name__ == '__main__':
    # scans = [122, 24, 37, 65, 106, 110, 114]
    # for scan in scans:
    #     eval_dtu(scan)

    scenes = ["chair", "drums", "ficus", "hotdog", "lego", "mic", "ship", "materials"]
    for scene in scenes:
        eval_nerfsynthetic(scene)




