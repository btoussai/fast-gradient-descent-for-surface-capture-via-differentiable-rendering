#!/usr/bin/env python3

import numpy as np
import xml.etree.cElementTree as ET
from xml.dom import minidom
import os
import cv2

print("This script prepares the DTU dataset.")
dtu_dir = "/usr/src/DTU"

np.set_printoptions(suppress=True)

width = 1600
height = 1200

def decomposeP(P):
    import numpy as np
    M = P[0:3,0:3]
    Q = np.eye(3)[::-1]
    P_b = Q @ M @ M.T @ Q
    K_h = Q @ np.linalg.cholesky(P_b) @ Q
    K = K_h / K_h[2,2]
    A = np.linalg.inv(K) @ M
    l = (1/np.linalg.det(A)) ** (1/3)
    R = l * A
    t = l * np.linalg.inv(K) @ P[0:3,3]
    return K, R, t

def writeCalibDTU(scanID, nbCam, outDir):
    CalibrationResult = ET.Element("CalibrationResult")
    Information = ET.SubElement(CalibrationResult, "Information")
    Information.set('nbCamera', str(nbCam))
    Information.set('units', "mm")
    Information.set('up', "+z")

    for i in range(0, nbCam):
        cameraNode = ET.SubElement(CalibrationResult, "Camera")
        cameraNode.set("id", str(i))
        cameraNode.set("width", str(width))
        cameraNode.set("height", str(height))
        with open("/scratch/briac/DTU/SampleSet/MVS Data/Calibration/cal18/pos_{:03d}.txt".format(i+1)) as f:
            l = f.read().split()

            P = np.array([float(s) for s in l]).reshape((3, 4))

            K, R, T = decomposeP(P)

            # print("Skew: ",  K[0, 1])
            K[0, 1] = 0 # Skew is not allowed!

            print(K)
            print(R)
            print(T)

            K_Node = ET.SubElement(cameraNode, "K")
            K_Node.text = ' '.join(str(k) for k in np.ravel(K))
            Distortion_Node = ET.SubElement(cameraNode, "Distortion")
            Distortion_Node.set("model", "opencv")
            Distortion_Node.set("K1", str(0.0))
            Distortion_Node.set("K2", str(0.0))
            Distortion_Node.set("P1", str(0.0))
            Distortion_Node.set("P2", str(0.0))
            Distortion_Node.set("K3", str(0.0))

            R_Node = ET.SubElement(cameraNode, "R")
            R_Node.text = ' '.join(str(r) for r in np.ravel(R))
            T_Node = ET.SubElement(cameraNode, "T")
            T_Node.text = ' '.join(str(t) for t in np.ravel(T))


    xmlstr = minidom.parseString(ET.tostring(CalibrationResult)).toprettyxml(indent="   ")
    f = open(outDir + "/calib{}.xml".format(scanID), "w")
    f.write(xmlstr)
    f.close()


def writeCalibDVR(nbCam, outDir, dest):
    CalibrationResult = ET.Element("CalibrationResult")
    Information = ET.SubElement(CalibrationResult, "Information")
    Information.set('nbCamera', str(nbCam))
    Information.set('units', "mm")
    Information.set('up', "+z")

    scaleMatrices = []

    matrices = np.load(outDir + "/cameras.npz")
    for i in range(0, nbCam):
        cameraNode = ET.SubElement(CalibrationResult, "Camera")
        cameraNode.set("id", str(i))
        cameraNode.set("width", str(width))
        cameraNode.set("height", str(height))

        world_mat = matrices["world_mat_{}".format(i)]
        scale_mat = matrices["scale_mat_{}".format(i)]
        scaleMatrices.append(scale_mat)

        P = (world_mat @ scale_mat)[:3]
        K, R, T = decomposeP(P)

        # print("Skew: ",  K[0, 1])
        K[0, 1] = 0 # Skew is not allowed!

        # print(K)
        # print(R)
        # print(T)

        K_Node = ET.SubElement(cameraNode, "K")
        K_Node.text = ' '.join(str(k) for k in np.ravel(K))
        Distortion_Node = ET.SubElement(cameraNode, "Distortion")
        Distortion_Node.set("model", "opencv")
        Distortion_Node.set("K1", str(0.0))
        Distortion_Node.set("K2", str(0.0))
        Distortion_Node.set("P1", str(0.0))
        Distortion_Node.set("P2", str(0.0))
        Distortion_Node.set("K3", str(0.0))

        R_Node = ET.SubElement(cameraNode, "R")
        R_Node.text = ' '.join(str(r) for r in np.ravel(R))
        T_Node = ET.SubElement(cameraNode, "T")
        T_Node.text = ' '.join(str(t) for t in np.ravel(T))


    xmlstr = minidom.parseString(ET.tostring(CalibrationResult)).toprettyxml(indent="   ")
    f = open(dest, "w")
    f.write(xmlstr)
    f.close()
    return scaleMatrices

def writeSequence(scanID, nbCam, outDir):
    Video = ET.Element("Video")
    Information = ET.SubElement(Video, "Information")
    Information.set('nbCamera', str(nbCam))
    Information.set('fps', "30")
    Information.set('beginId', "0")
    Information.set('endId', "0")

    for i in range(0, nbCam):
        cameraNode = ET.SubElement(Video, "Camera")
        cameraNode.set("id", str(i))
        cameraNode.set("path", os.path.abspath(outDir + "/image"))
        cameraNode.set("fileNameFormat", "{:06d}.png".format(i))

    xmlstr = minidom.parseString(ET.tostring(Video)).toprettyxml(indent="   ")
    f = open(outDir + "/sequence{}.xml".format(scanID), "w")
    f.write(xmlstr)
    f.close()

def makeBackgrounds(nbCams, outDir):
    os.makedirs(outDir + "/Backgrounds", exist_ok=True)

    for i in range(0, nbCams):
        file = outDir + "/mask/{:03d}.png".format(i)
        img = cv2.imread(file, cv2.IMREAD_UNCHANGED)

        fg = img[:, :, 0] > 30
        img2 = np.full((height, width, 4), 0, dtype=np.uint8)
        img2[fg] = np.array([0, 0, 255, 255]) #opencv uses bgra
        path = outDir + "/Backgrounds/cam-{}.png".format(i)
        cv2.imwrite(path, img2)

def createSceneSettings(scanID, outDir, scaleMatrices):
    config_file = os.path.dirname(os.path.realpath(__file__)) + "/../examples/dtu/scan{}_config.txt".format(scanID)
    dest_path = "{}/scene_config.txt".format(outDir)
    print("Writing " + dest_path + "...")
    dest_file = open(dest_path, 'w')

    for l in open(config_file, mode='r').readlines():
        if l.startswith("Window#BackgroundsDir"):
            l = "Window#BackgroundsDir string {}\n".format(outDir + "/Backgrounds")
        elif l.startswith("Window#CalibrationFile"):
            l = "Window#CalibrationFile string {}/calib{}.xml\n".format(outDir, scanID)
        elif l.startswith("Window#ExportDir"):
            l = "Window#ExportDir string {}\n".format(outDir)
        elif l.startswith("Window#SequenceFile"):
            l = "Window#SequenceFile string {}/sequence{}.xml\n".format(outDir, scanID)
        dest_file.write(l)

    for i in range(4):
        s = "SceneReferenceFrame#ScaleMatrixRow{} vec4 {} {} {} {}\n".format(i, *scaleMatrices[0][i].flatten())
        dest_file.write(s)


scans = [24, 37, 40, 55, 63, 65, 69, 83, 97, 105, 106, 110, 114, 118, 122]
nbCams = [49, 49, 49, 49, 49, 49, 49, 64, 64, 64, 64, 64, 64, 64, 64]
for scanID, n in zip(scans, nbCams):
    print("Preparing scan {}...".format(scanID))
    outDir = dtu_dir + "/scan{}".format(scanID, scanID)
    scaleMatrices = writeCalibDVR(n, outDir, outDir + "/calib{}.xml".format(scanID))
    writeCalibDVR(n, outDir, outDir + "/calib_test.xml".format(scanID))
    writeSequence(scanID, n, outDir)
    makeBackgrounds(n, outDir)
    createSceneSettings(scanID, outDir, scaleMatrices)


print("Done.")
print("You can now run the main program, ")
print("then select a reconstruction by selecting 'file>import scene config' in the menu bar and navigating to the desired config file.")