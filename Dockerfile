FROM gcc:11.1.0

RUN apt-get update
RUN apt-get install -y xorg-dev cmake python3-pip
RUN python3 -m pip install numpy opencv-python
RUN rm -rf /var/lib/apt/lists/*

# Dependencies for glvnd and X11.
RUN apt-get update \
  && apt-get install -y -qq --no-install-recommends \
    libglvnd0 \
    libgl1 \
    libglx0 \
    libegl1 \
    libxext6 \
    libx11-6 \
  && rm -rf /var/lib/apt/lists/* # Env vars for the nvidia-container-runtime.
ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES graphics,utility,compute


# Install GLFW
WORKDIR /usr/src/
RUN git clone --depth 1 --branch 3.3.8 https://github.com/glfw/glfw.git glfw
WORKDIR /usr/src/glfw
RUN mkdir build
WORKDIR /usr/src/glfw/build
#RUN cmake -S .. -B . -DBUILD_SHARED_LIBS=ON -DGLFW_BUILD_EXAMPLES=ON -DGLFW_BUILD_TESTS=OFF -DGLFW_BUILD_DOCS=OFF -DCMAKE_INSTALL_PREFIX:PATH=/usr/src/install/glfw
RUN cmake -S .. -B . -DBUILD_SHARED_LIBS=ON -DGLFW_BUILD_EXAMPLES=ON -DGLFW_BUILD_TESTS=OFF -DGLFW_BUILD_DOCS=OFF
RUN make -j48
RUN make install

# Install Assimp
WORKDIR /usr/src/
RUN git clone --depth 1 --branch v5.2.5 https://github.com/assimp/assimp.git assimp
WORKDIR /usr/src/assimp
RUN mkdir build
WORKDIR /usr/src/assimp/build
#RUN cmake -S .. -B . -DASSIMP_BUILD_TESTS=OFF -DCMAKE_INSTALL_PREFIX:PATH=/usr/src/install/assimp
RUN cmake -S .. -B . -DASSIMP_BUILD_TESTS=OFF
RUN make -j48
RUN make install

# Install Eigen
WORKDIR /usr/src/
RUN git clone --depth 1 --branch 3.4 https://gitlab.com/libeigen/eigen.git eigen
WORKDIR /usr/src/eigen
RUN mkdir build
WORKDIR /usr/src/eigen/build
#RUN cmake -S .. -B . -DBUILD_TESTING=OFF -DEIGEN_BUILD_DOC=OFF -DCMAKE_INSTALL_PREFIX:PATH=/usr/src/install/eigen
RUN cmake -S .. -B . -DBUILD_TESTING=OFF -DEIGEN_BUILD_DOC=OFF
RUN make -j48
RUN make install

# Install the program
WORKDIR /usr/src/

# Copy the repo folder inside the container
COPY . /usr/src/repo

# Build the program
WORKDIR /usr/src/repo
RUN mkdir build
WORKDIR /usr/src/repo/build
RUN cmake -S .. -B . -DCMAKE_BUILD_TYPE=Release
RUN make -j48
WORKDIR /usr/src/repo
