/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require
#extension GL_NV_gpu_shader5 : 					enable
#extension GL_KHR_shader_subgroup_ballot:		enable
#extension GL_KHR_shader_subgroup_vote : 		enable
#extension GL_KHR_shader_subgroup_arithmetic :  enable
#extension GL_ARB_bindless_texture :			enable
#extension GL_NV_gpu_shader_fp64 : 				enable
#extension GL_NV_shader_atomic_float64 : 		enable


const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

const int WORK_VOLUME_SIDE = VOXELS_PER_SIDE+2;
const int VOXELS_IN_GROUP = WORK_VOLUME_SIDE * WORK_VOLUME_SIDE * WORK_VOLUME_SIDE;

layout(local_size_x = VOXELS_PER_SIDE, local_size_y = VOXELS_PER_SIDE, local_size_z = VOXELS_PER_SIDE) in;
const int GROUP_SIZE = VOXELS_PER_TILE;


struct RayDescriptor{
	int intervalsPtr;
	int intervalsCount;
	int texelsPtr;
	int texelsCount;
	int depth_tile_x;
	int depth_tile_y;
};

struct Interval{
	int16_t start;//inclusive
	int16_t end;//exclusive
};

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

layout(std430, binding = 0) restrict readonly buffer RayDescriptorsBlock{
	RayDescriptor ray_descriptors[];
};

layout(std430, binding = 1) restrict readonly buffer IntervalsBlock{
	Interval intervals[];
};

layout(std430, binding = 2) restrict buffer TileDescriptorsBlock{
	TileDesc tile_descriptors[];
};

layout(std430, binding = 3) restrict buffer GradientBlock{
	u8vec4 gradients[];
};

layout(std430, binding = 4) restrict buffer GradientSquaredBlock{
	u8vec4 gradients_squared[];
};

layout(std430, binding = 5) restrict buffer AtomicBlock{
	float64_t atomics[];
};

#include </common/projection.txt>
#include </common/robust_norm.txt>
#include </common/float_encoding.txt>

layout(std140, binding = 1) uniform ImageHandleBlock
{
	uint64_t RayTexelImageHandles[1024];
};

#define #SHBands
#define #VoxelsTexturesCount

layout(binding = 0) uniform sampler3D VoxelsTexture[VoxelsTexturesCount];
layout(binding = 7) uniform isampler2DArray occupancyImage;
layout(binding = 8) uniform isampler3D TileOccupancy;

uniform vec3 TSDFminCorner;
uniform ivec3 TSDFTilesCount;
uniform float tile_size;
uniform float STEP_SIZE;
uniform int NumCameras;
uniform int NumTiles;
uniform float opacityLaplacianReg;
uniform float baseColorLaplacianReg;
uniform float SHLaplacianReg;
uniform float SHParcimonyReg;
uniform float ballooningTerm;
uniform vec2 beta;


shared f16vec4 workVolume[VOXELS_IN_GROUP];
shared vec4 partial_sums[gl_NumSubgroups];


//https://en.wikipedia.org/wiki/Table_of_spherical_harmonics#Real_spherical_harmonics
const float baseCoeff = 0.28209479177387814;
const float SHcoeffs[9] = {
	1.0f,   						   // 0.5 * sqrt(1/pi)
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	0.31539156525252005 / baseCoeff,   // 0.25 * sqrt(5 / pi)
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	1.0925484305920792  / baseCoeff    // 0.5 * sqrt(15 / pi)
};

ivec3 getRayTexelCoords(uint slice, ivec2 localCoords){
	uint x = bitfieldExtract(slice, 0, 8);
	uint y = bitfieldExtract(slice, 8, 8);
	uint z = bitfieldExtract(slice, 16, 16);
	return ivec3(x * DEPTHMAP_TILE_WIDTH + localCoords.x, y * DEPTHMAP_TILE_WIDTH + localCoords.y, z);
}

vec4 groupAdd(vec4 v){
	
	vec4 w = subgroupAdd(v);
	if(gl_SubgroupInvocationID == gl_SubgroupSize-1){
		partial_sums[gl_SubgroupID] = w;
	}
	memoryBarrierShared();
	barrier();
	
	if(gl_SubgroupID == 0){
		vec4 v2 = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : vec4(0.0f);
		v2 = subgroupAdd(v2);
		if(subgroupElect()){
			partial_sums[0] = v2;
		}
	}
	memoryBarrierShared();
	barrier();
	
	return partial_sums[0];
}

ivec3 unwind(int index, ivec3 s){
	int z = index / (s.x*s.y);
	int remainder = index - z * (s.x*s.y);
	int y = remainder / s.x;
	int x = remainder - y * s.x;
	return ivec3(x, y, z);
}

ivec3 computeTileIndexAtlasCoord(ivec3 tile_coords, out int tile_index) {
	if(any(lessThan(tile_coords, ivec3(0))) || any(greaterThanEqual(tile_coords, TSDFTilesCount))){
		tile_index = -1;
		return ivec3(0);
	}

	tile_index = texelFetch(TileOccupancy, tile_coords, 0).x;

	ivec3 atlas_dims = textureSize(VoxelsTexture[0], 0) / VOXELS_PER_SIDE;

	ivec3 atlas_coords = unwind(tile_index, atlas_dims);
	atlas_coords *= VOXELS_PER_SIDE;

	return atlas_coords;
}

void readVoxels(ivec3 center_tile, int N) {
    //grab the tsdf values and store them in a work volume in shared memory
	for(int k=int(gl_LocalInvocationIndex); k<VOXELS_IN_GROUP; k+=GROUP_SIZE){
		const int sz = WORK_VOLUME_SIDE * WORK_VOLUME_SIDE;
		const int sy = WORK_VOLUME_SIDE;
		int local_z = k / sz;
		int remainder = k - local_z * sz;
		int local_y = remainder / sy;
		int local_x = remainder - local_y * sy;
	
		const int tile_z = (local_z + VOXELS_PER_SIDE-1) / VOXELS_PER_SIDE;
		const int tile_y = (local_y + VOXELS_PER_SIDE-1) / VOXELS_PER_SIDE;
		const int tile_x = (local_x + VOXELS_PER_SIDE-1) / VOXELS_PER_SIDE;
		ivec3 tile = ivec3(tile_x, tile_y, tile_z) - ivec3(1);
		

		local_z = (local_z + VOXELS_PER_SIDE-1) % VOXELS_PER_SIDE;
		local_y = (local_y + VOXELS_PER_SIDE-1) % VOXELS_PER_SIDE;
		local_x = (local_x + VOXELS_PER_SIDE-1) % VOXELS_PER_SIDE;
		
		int tile_index = -1;
		ivec3 atlas_coords = computeTileIndexAtlasCoord(center_tile + tile, tile_index);
		
		if(tile_index == -1){
			workVolume[k] = f16vec4(N==0 ? 1.0f : 0.0f, 0.0f, 0.0f, 0.0f);
		}else if(tile_index == -2){
			workVolume[k] = f16vec4(N==0 ? 0.1f : 0.0f, 0.0f, 0.0f, 0.0f);
		} else {
			ivec3 readCoords = atlas_coords + ivec3(local_x, local_y, local_z);
			vec4 v = vec4(texelFetch(VoxelsTexture[N], readCoords, 0));
			workVolume[k] = f16vec4((N==0) ? v : (2.0f * v - 1.0f));
		}
		
	}
    memoryBarrierShared();
    barrier();
}

vec4 getValueFromSharedMem(ivec3 offset){
	ivec3 coords = ivec3(gl_LocalInvocationID) + offset + ivec3(1);
	const int k = coords.x + WORK_VOLUME_SIDE * (coords.y + WORK_VOLUME_SIDE * coords.z);
	return workVolume[k];
}

void getSample(ivec2 coordsPixel, 
		int t, 
		float depthWeight,
		float rayWeight, 
		int camIndex, 
		inout vec4 gradient) {

	const int depthSliceLocalIndex = (coordsPixel.y % DEPTHMAP_TILE_WIDTH) * DEPTHMAP_TILE_WIDTH
							+ (coordsPixel.x % DEPTHMAP_TILE_WIDTH);				
	const int depthTileIndex = texelFetch(occupancyImage, ivec3(coordsPixel / DEPTHMAP_TILE_WIDTH, camIndex), 0).x;
	
	if(depthTileIndex == -1){
		//Negative Ballooning term
		//gradient.x -= 1000.0 * ballooningTerm * rayWeight;
		return;
	}
	RayDescriptor rayDescriptor = ray_descriptors[depthTileIndex];
	
	if(rayDescriptor.intervalsCount == 0){
		//Negative Ballooning term
		//gradient.x -= 10 * ballooningTerm * rayWeight;
		return;
	}
	
	int sliceIndex = rayDescriptor.texelsPtr;
	int interval_index = 0;

	Interval interval = intervals[rayDescriptor.intervalsPtr + interval_index];
	while(t >= interval.end){
		sliceIndex += interval.end - interval.start;
		interval_index++;
		if(interval_index < rayDescriptor.intervalsCount){
			interval = intervals[rayDescriptor.intervalsPtr + interval_index];
		}else{
			break;
		}
	}

	vec4 firstSample = vec4(0.0f);
	vec4 secondSample = vec4(0.0f);
	
	{
		bool inside = t >= interval.start && t < interval.end;
		if(inside){
			int offset = sliceIndex + t - interval.start;
			
			const ivec3 bindless_coords = getRayTexelCoords(offset, coordsPixel % DEPTHMAP_TILE_WIDTH);
			layout(rgba8ui) uimage2D RayTexels = layout(rgba8ui) uimage2D(RayTexelImageHandles[bindless_coords.z]);
			
			firstSample = loadEncodedFloat(RayTexels, bindless_coords.xy);
		}
		t++;
	}
	if(t >= interval.end && interval_index < rayDescriptor.intervalsCount){
		sliceIndex += interval.end - interval.start;
		interval_index++;
		if(interval_index < rayDescriptor.intervalsCount){
			interval = intervals[rayDescriptor.intervalsPtr + interval_index];
		}
	}
	{
		bool inside = t >= interval.start && t < interval.end;
		if(inside){
			int offset = sliceIndex + t - interval.start;
			
			const ivec3 bindless_coords = getRayTexelCoords(offset, coordsPixel % DEPTHMAP_TILE_WIDTH);
			layout(rgba8ui) uimage2D RayTexels = layout(rgba8ui) uimage2D(RayTexelImageHandles[bindless_coords.z]);
			
			secondSample = loadEncodedFloat(RayTexels, bindless_coords.xy);
		}
	}
	
	
	gradient += mix(firstSample, secondSample, depthWeight) * rayWeight;
}

bool isCameraActive(ivec4 activeCameras, int camIndex){
	return (activeCameras[camIndex/32] & (1<<(camIndex%32))) != 0;
}
void setCameraInactive(inout ivec4 activeCameras, int camIndex){
	activeCameras[camIndex/32] &= ~(1<<(camIndex%32));
}

void main() {
	//One thread per voxel

	const uint tile_index = gl_WorkGroupID.x;
	TileDesc tileDescriptor = tile_descriptors[tile_index];
	const ivec3 tile_coords = ivec3(tileDescriptor.coords_index);
	ivec4 activeCameras = tileDescriptor.activeCameras;
	
    const float voxel_size = tile_size / VOXELS_PER_SIDE;
    const vec3 voxelCenter = TSDFminCorner + vec3(tile_coords) * tile_size + (vec3(gl_LocalInvocationID)+0.5f) * voxel_size;

	float grad_alpha = 0.0f;
	vec3 grad_c0 = vec3(0.0f);
	vec3 grad_c1 = vec3(0.0f);
	vec3 grad_c2 = vec3(0.0f);
	vec3 grad_c3 = vec3(0.0f);
	vec3 grad_c4 = vec3(0.0f);
	vec3 grad_c5 = vec3(0.0f);
	vec3 grad_c6 = vec3(0.0f);
	vec3 grad_c7 = vec3(0.0f);
	vec3 grad_c8 = vec3(0.0f);

	int nActiveCams = 1;
	for(int camIndex=0; camIndex < NumCameras; camIndex++) {
		if(!isCameraActive(activeCameras, camIndex)){
   			continue;
   		}
   		nActiveCams++;
	
		const vec3 d = normalize(voxelCenter - getCamCenter(camIndex));
		const vec3 voxelCenterProj = project(voxelCenter, camIndex);
		const float voxelDepth = voxelCenterProj.z;
		const vec2 coordsNDC = vec2(voxelCenterProj) / voxelDepth;
		const vec2 coordsPixel = toPixels(coordsNDC, camIndex);
		
		const vec2 image_size = getImageSize(camIndex);
		if(!isInsideImageRange(coordsPixel, image_size, 2)){
			continue;
		}

		const vec2 floorPixel = floor(coordsPixel - 0.5f);
		const ivec2 intPixel = ivec2(floorPixel);
		const float u = coordsPixel.x - 0.5f - floorPixel.x;
		const float v = coordsPixel.y - 0.5f - floorPixel.y;
		
		
		const float tile_size_inv = 1.0f / tile_size;
		const float scaling = float(VOXELS_PER_SIDE) / STEP_SIZE;

		const float sampleDepth = voxelDepth * tile_size_inv * scaling;
		const int t = int(floor(sampleDepth));
		const float depthWeight = sampleDepth - floor(sampleDepth);
		
		vec4 alpha_color_grad = vec4(0.0f);
		getSample(intPixel + ivec2(0,0), t, depthWeight, (1.0f-u)*(1.0f-v), camIndex, alpha_color_grad);
		getSample(intPixel + ivec2(0,1), t, depthWeight, u*(1.0f-v),        camIndex, alpha_color_grad);
		getSample(intPixel + ivec2(1,0), t, depthWeight, (1.0f-u)*v,        camIndex, alpha_color_grad);
		getSample(intPixel + ivec2(1,1), t, depthWeight, u*v,               camIndex, alpha_color_grad);
		
		vec3 grad_c = alpha_color_grad.yzw;
		grad_alpha += alpha_color_grad.x;
		switch(SHBands){
			case 2:
			{
				grad_c8 += grad_c * SHcoeffs[8] * (d.x*d.x - d.y*d.y);
				grad_c7 += grad_c * SHcoeffs[7] * (d.x*d.z);
				grad_c6 += grad_c * SHcoeffs[6] * (3.0f*d.z*d.z - 1.0f);
				grad_c5 += grad_c * SHcoeffs[5] * (d.y*d.z);
				grad_c4 += grad_c * SHcoeffs[4] * (d.x*d.y);
			}
			case 1:
			{
				grad_c3 += grad_c * SHcoeffs[3] * d.x;
				grad_c2 += grad_c * SHcoeffs[2] * d.z;
				grad_c1 += grad_c * SHcoeffs[1] * d.y;
			}
			case 0:
			{
				grad_c0 += grad_c;
			}
		}
	}

	grad_alpha *= (1.0f / nActiveCams);
	grad_c0    *= (1.0f / nActiveCams);
	grad_c1    *= (1.0f / nActiveCams);
	grad_c2    *= (1.0f / nActiveCams);
	grad_c3    *= (1.0f / nActiveCams);
	grad_c4    *= (1.0f / nActiveCams);
	grad_c5    *= (1.0f / nActiveCams);
	grad_c6    *= (1.0f / nActiveCams);
	grad_c7    *= (1.0f / nActiveCams);
	grad_c8    *= (1.0f / nActiveCams);
	
    float TotalLoss = 0;
    for(int k=0; k<VoxelsTexturesCount; k++){
    	vec4 G = vec4(0.0f);
    	switch(k){
    		case 0:
			G = vec4(grad_alpha, grad_c0);
				break;
    		case 1:
			G = vec4(grad_c1.xyz, grad_c2.x);
				break;
    		case 2:
			G = vec4(grad_c2.yz, grad_c3.xy);
				break;
    		case 3:
			G = vec4(grad_c3.z, grad_c4.xyz);
				break;
    		case 4:
			G = vec4(grad_c5.xyz, grad_c6.x);
				break;
    		case 5:
			G = vec4(grad_c6.yz, grad_c7.xy);
				break;
    		case 6:
			G = vec4(grad_c7.z, grad_c8.xyz);
    			break;
    	}
    
    
	    readVoxels(tile_coords, k);
		const vec4 voxel_values = getValueFromSharedMem(ivec3(0));
		
		vec4 dpx = getValueFromSharedMem(ivec3(+1, 0, 0));
		vec4 dmx = getValueFromSharedMem(ivec3(-1, 0, 0));
		vec4 dpy = getValueFromSharedMem(ivec3(0, +1, 0));
		vec4 dmy = getValueFromSharedMem(ivec3(0, -1, 0));
		vec4 dpz = getValueFromSharedMem(ivec3(0, 0, +1));
		vec4 dmz = getValueFromSharedMem(ivec3(0, 0, -1));
	
		vec4 Fx = dpx - dmx;
		vec4 Fxx = dpx + dmx - voxel_values * 2.0f;
		vec4 Fy = dpy - dmy;
		vec4 Fyy = dpy + dmy - voxel_values * 2.0f;
		vec4 Fz = dpz - dmz;
		vec4 Fzz = dpz + dmz - voxel_values * 2.0f;
		
		//Laplacian regularization
		vec4 Laplacian = Fxx + Fyy + Fzz;
		
		if(k == 0){
			G -= Laplacian * vec4(opacityLaplacianReg, vec3(baseColorLaplacianReg));
			G.x += 2.0 * voxel_values.x * ballooningTerm;
			
			vec4 L = (Fx*Fx + Fy*Fy + Fz*Fz) * vec4(opacityLaplacianReg, vec3(baseColorLaplacianReg)); 
			TotalLoss += L.x + L.y + L.z + L.w;
			TotalLoss += voxel_values.x * voxel_values.x * ballooningTerm;
			
		}else{
			G -= Laplacian * SHLaplacianReg;
			G += huber_grad(voxel_values) * SHParcimonyReg;
			
			vec4 L = (Fx*Fx + Fy*Fy + Fz*Fz) * vec4(SHLaplacianReg); 
			TotalLoss += L.x + L.y + L.z + L.w;
			TotalLoss += huber_loss(voxel_values) * SHParcimonyReg;
		}
		
		const uint gradient_index = VOXELS_PER_TILE * (tile_index * VoxelsTexturesCount + k) + gl_LocalInvocationIndex;
							
		vec4 g  = decodeSignedFloat8(gradients[gradient_index]);
		vec4 g2 = decodeSignedFloat8(gradients_squared[gradient_index]);
		g = mix(G, g, beta.x);
		g2 = sqrt(mix(G*G, g2*g2, beta.y));
		gradients[gradient_index] = encodeSignedFloat8(g);
		gradients_squared[gradient_index] = encodeSignedFloat8(g2);
		
	}
	
	
	
	TotalLoss = groupAdd(vec4(TotalLoss, 0, 0, 0)).x;
	if(gl_LocalInvocationIndex == 0){
		atomicAdd(atomics[0], TotalLoss);
	}
	
}
	
	
