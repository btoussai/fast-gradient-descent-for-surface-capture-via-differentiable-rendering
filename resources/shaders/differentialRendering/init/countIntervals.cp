/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require
#extension GL_NV_gpu_shader5 : 					enable
#extension GL_KHR_shader_subgroup_ballot:		enable
#extension GL_KHR_shader_subgroup_arithmetic : enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;
const int VOXELS_IN_GROUP = (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2);

layout(local_size_x = DEPTHMAP_TILE_WIDTH, local_size_y = DEPTHMAP_TILE_WIDTH, local_size_z = 1) in;
const int GROUP_SIZE = DEPTHMAP_TILE_SIZE;

struct TileTextureDescriptor{
	int ptr;
	int tile;
	i16vec2 pixelMinCorner;
	i16vec2 dims;
};

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

struct DepthTileDescriptor{
	ivec2 tileCoords;			//coordinates of the tile in the image
	int foregroundPixelCount;	//number of actual foreground pixels in the tile
	int camIndex;				//index of the camera
};

struct RayDescriptor{
	int intervalsPtr;
	int intervalsCount;
	int texelsPtr;
	int texelsCount;
	int depth_tile_x;
	int depth_tile_y;
};

struct Interval{
	int16_t start;//inclusive
	int16_t end;//exclusive
};

shared int intervalsPtr;
shared int texelsPtr;
shared int insideCounter;
shared int intervalCounter;
shared int texelCounter;
shared int intervalLengthCounter;
shared Interval sharedIntervals[64];
shared ivec4 partial_sums[32];

layout(binding = 0) uniform usampler3D CompressedTileOccupancy;
layout(binding = 1) uniform isampler3D TileOccupancy;
layout(binding = 2) uniform sampler3D VoxelsTexture;


layout(std430, binding = 0) restrict readonly buffer DepthTileDescriptorBlock{
	DepthTileDescriptor depthTileDescriptors[];
};

layout(std430, binding = 1) restrict buffer AtomicBlock{
	int atomics[];
};

layout(std430, binding = 2) restrict writeonly buffer RayDescriptorsBlock{
	RayDescriptor ray_descriptors[];
};

layout(std430, binding = 3) restrict writeonly buffer IntervalsBlock{
	Interval intervals[];
};

#include </common/projection.txt>
#include </common/rayVSBox.txt>

uniform vec3 TSDFminCorner;
uniform vec3 TSDFmaxCorner;
uniform ivec3 TSDFTilesCount;
uniform float tile_size;
uniform float STEP_SIZE;
uniform int NumCameras;
uniform int NumTiles;
uniform ivec4 activeCameras;

bool isCameraActive(int camIndex){
	return (activeCameras[camIndex/32] & (1<<(camIndex%32))) != 0;
}

ivec4 groupMin(ivec4 v){

	v = subgroupMin(v);
	if(subgroupElect()){
		partial_sums[gl_SubgroupID] = v;
	}

	memoryBarrierShared();
	barrier();

	if(gl_SubgroupID == 0){
		v = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : ivec4(2147483647);
		ivec4 w = subgroupMin(v);
		partial_sums[gl_SubgroupInvocationID] = w;
	}

	memoryBarrierShared();
	barrier();

	return partial_sums[gl_SubgroupID];
}

ivec4 groupAdd(ivec4 v){

	v = subgroupAdd(v);
	if(subgroupElect()){
		partial_sums[gl_SubgroupID] = v;
	}

	memoryBarrierShared();
	barrier();

	if(gl_SubgroupID == 0){
		v = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : ivec4(0);
		ivec4 w = subgroupAdd(v);
		partial_sums[gl_SubgroupInvocationID] = w;
	}

	memoryBarrierShared();
	barrier();

	return partial_sums[gl_SubgroupID];
}

float getSample(ivec3 p, inout int outside) {
	if(any(lessThan(p, ivec3(0))) || any(greaterThanEqual(p, TSDFTilesCount * VOXELS_PER_SIDE))){
		outside++;
		return 1.0f;
	}
	
	const ivec3 tile_coords = p / VOXELS_PER_SIDE;
	
	const int tile_index = texelFetch(TileOccupancy, tile_coords, 0).x;
	if(tile_index == -1){
		outside++;
		return 1.0f;
	}else if(tile_index == -2){
		return 0.1f;
	}
	
	//convert to voxel local coord
	p %= VOXELS_PER_SIDE;
	
	const ivec3 atlas_dims = textureSize(VoxelsTexture, 0) / VOXELS_PER_SIDE;
	
	// convert to atlas coords
	ivec3 atlas_coords;
	atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
	int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
	atlas_coords.y = remainder / atlas_dims.x;
	atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;
	p += atlas_coords * VOXELS_PER_SIDE;

	return texelFetch(VoxelsTexture, p, 0).x;	
}

float lerpSample(vec3 camCenter, vec3 ray, int t, inout int outside) {
	const vec3 p = (camCenter - vec3(0.5f)) + ray * float(t);
	const vec3 floor_p = floor(p);
	const vec3 fract_p = p - floor_p;
	const ivec3 int_p = ivec3(floor_p);
				
	float v000 = getSample(int_p + ivec3(0, 0, 0), outside);
	float v100 = getSample(int_p + ivec3(1, 0, 0), outside);
	v000 = mix(v000, v100, fract_p.x);
				
	float v010 = getSample(int_p + ivec3(0, 1, 0), outside);
	float v110 = getSample(int_p + ivec3(1, 1, 0), outside);
	v010 = mix(v010, v110, fract_p.x);
	v000 = mix(v000, v010, fract_p.y);
				
	float v001 = getSample(int_p + ivec3(0, 0, 1), outside);
	float v101 = getSample(int_p + ivec3(1, 0, 1), outside);
	v001 = mix(v001, v101, fract_p.x);
				
	float v011 = getSample(int_p + ivec3(0, 1, 1), outside);
	float v111 = getSample(int_p + ivec3(1, 1, 1), outside);
	v011 = mix(v011, v111, fract_p.x);
	v001 = mix(v001, v011, fract_p.y);
	
	v000 = mix(v000, v001, fract_p.z);
	return v000;
}

void main(void){
	//One thread per ray, one group per 2D tile

	if(gl_LocalInvocationIndex == 0){
		insideCounter = 0; // None of the rays are inside
		intervalCounter = 0;
		texelCounter = 0;
		intervalLengthCounter = 0;
		intervalsPtr = -1;
		texelsPtr = -1;
	}

	const int tile_id = int(gl_WorkGroupID.x);
	const DepthTileDescriptor tileDescriptor = depthTileDescriptors[tile_id];
	const int camIndex = tileDescriptor.camIndex;
	
	if(!isCameraActive(camIndex)){
		if(gl_LocalInvocationIndex == 0){
			ray_descriptors[tile_id] = RayDescriptor(
					intervalsPtr,
					intervalCounter,
					texelsPtr,
					texelCounter,
					tileDescriptor.tileCoords.x,
					tileDescriptor.tileCoords.y
				);
		}
		return;
	}

	const ivec2 pixelCoords = tileDescriptor.tileCoords * DEPTHMAP_TILE_WIDTH + ivec2(gl_LocalInvocationID);
	const vec2 uv = vec2(pixelCoords) + vec2(0.5f);
	const vec2 ndcCoords = toNDC(uv, camIndex);

    const float tile_size_inv = 1.0f / tile_size;
    const float scaling = tile_size_inv * float(VOXELS_PER_SIDE);

	const vec3 camCenter = (getCamCenter(camIndex) - TSDFminCorner) * scaling;
	vec3 ray = unproject(ndcCoords, camIndex);
	ray /= dot(cameraData[camIndex].cameraTransformInv[2], vec4(ray, 0.0));
	ray *= STEP_SIZE;
	const vec3 ray_inv = 1.0f / ray;



	float tmin, tmax;
	bool hit = rayVSbox(camCenter, ray_inv, vec3(0.0f), (TSDFmaxCorner-TSDFminCorner)*scaling, tmin, tmax);

	int numHit = groupAdd(ivec4(int(hit))).x;
	if(numHit == 0){
		if(gl_LocalInvocationIndex == 0){
			ray_descriptors[tile_id] = RayDescriptor(
					intervalsPtr,
					intervalCounter,
					texelsPtr,
					texelCounter,
					tileDescriptor.tileCoords.x,
					tileDescriptor.tileCoords.y
				);
		}
		return;//None of the rays hit the volume
	}

	ivec4 bounds = hit ? ivec4(int(floor(max(0.0f, tmin))), -int(ceil(tmax)), 0, 0) : ivec4(2147483647);
	bounds = groupMin(bounds);

	int t = bounds.x;
	const int max_t = -bounds.y;


	const float transmittance_eps = 3.0f / 255.0f;
	bool prev_empty = true;
	float Tpartial = 1.0f;
	while(t <= max_t){
		int q = 0;
		

		if(hit){
			bool isEmpty = true;
			const vec3 p = camCenter + ray * float(t);
			const ivec3 tile_coords = ivec3(floor(p / float(VOXELS_PER_SIDE)));

			if(all(greaterThanEqual(tile_coords, ivec3(0))) && all(lessThan(tile_coords, TSDFTilesCount))){
				const ivec3 compressedCoord = tile_coords / 4;
				const ivec3 localCompressedCoord = tile_coords % 4;
				const uvec2 bitmask = uvec2(texelFetch(CompressedTileOccupancy, ivec3(compressedCoord), 0));//returns 0 when sampling outside
				const int bitPos = localCompressedCoord.x + 4*localCompressedCoord.y + 16 * (localCompressedCoord.z%2);
				isEmpty = ( ((localCompressedCoord.z/2)==0 ? bitmask.x : bitmask.y) & (1<<bitPos)) == 0;
				
				if(!isEmpty){
					int outside = 0;
					float alpha = lerpSample(camCenter, ray, t, outside);
					isEmpty = outside == 8;
					Tpartial *= alpha;
				}
				
			}
			q = isEmpty != prev_empty ? (isEmpty?-1:+1) : 0;
			prev_empty = isEmpty;
		}

		const ivec4 vote = groupAdd(ivec4(q, int(Tpartial > transmittance_eps), 0, 0));

		if(gl_LocalInvocationIndex == 0){
			q += vote.x;
			insideCounter += q;
			
			
			if(q != 0){
				if(insideCounter == 0){//leaving volume
					sharedIntervals[intervalCounter].end = int16_t(t);
					int length = sharedIntervals[intervalCounter].end - sharedIntervals[intervalCounter].start;
					intervalLengthCounter += length;
					intervalCounter++;
				}else if(insideCounter == q){//entering volume
					sharedIntervals[intervalCounter].start = int16_t(t);
				}
			}
		}
		
		t++;
		
		if(vote.y == 0){
			break;
		}
	}



	memoryBarrierShared();
	barrier();

	if(gl_LocalInvocationIndex == 0){
		if(insideCounter > 0){
			//close the last interval
			sharedIntervals[intervalCounter].end = int16_t(t);
			int length = sharedIntervals[intervalCounter].end - sharedIntervals[intervalCounter].start;
			intervalLengthCounter += length;
			intervalCounter++;
		}
		if(intervalCounter > 0){
			intervalsPtr = atomicAdd(atomics[0], intervalCounter);
			texelCounter = intervalLengthCounter;
			texelsPtr = atomicAdd(atomics[1], texelCounter);
			atomicAdd(atomics[2], intervalLengthCounter);
		}


		ray_descriptors[tile_id] = RayDescriptor(
				intervalsPtr,
				intervalCounter,
				texelsPtr,
				texelCounter,
				tileDescriptor.tileCoords.x,
				tileDescriptor.tileCoords.y
			);
	}

	memoryBarrierShared();
	barrier();

	if(gl_LocalInvocationIndex < intervalCounter){
		intervals[intervalsPtr + gl_LocalInvocationIndex] = sharedIntervals[gl_LocalInvocationIndex];
	}

}
