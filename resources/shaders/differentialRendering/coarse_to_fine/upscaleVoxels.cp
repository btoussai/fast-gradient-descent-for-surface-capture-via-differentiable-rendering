/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_NV_gpu_shader5 : 					enable


const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

layout(local_size_x = VOXELS_PER_SIDE, local_size_y = VOXELS_PER_SIDE, local_size_z = VOXELS_PER_SIDE) in;

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

layout(binding=0) uniform isampler3D OldTileOccupancy;
layout(binding=1) uniform sampler3D OldVoxelsTexture;

layout(binding=0, r32i) uniform restrict writeonly iimage3D NewTileOccupancy;
layout(binding=1, rgba8) uniform restrict writeonly image3D NewVoxelsTexture;

layout(std430, binding = 0) restrict buffer NewTilesDescriptorBlock{
	TileDesc new_tile_descriptors[];
};

uniform ivec3 OldTilesDim;
uniform int WriteDescriptors;

ivec3 getVoxelAtlasCoords(int tile_index, ivec3 localCoords, ivec3 voxelsTextureSize) {
	ivec3 atlas_dims = voxelsTextureSize / VOXELS_PER_SIDE;
	
	ivec3 atlas_coords;
	atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
	int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
	atlas_coords.y = remainder / atlas_dims.x;
	atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;
	
	return atlas_coords * VOXELS_PER_SIDE + localCoords;
}

float length2(vec3 v){
	return dot(v, v);
}

vec4 getSample(ivec3 p) {
	if(any(lessThan(p, ivec3(0))) || any(greaterThanEqual(p, OldTilesDim * VOXELS_PER_SIDE))){
		return vec4(1.0f, 0.0f, 0.0f, 0.0f);
	}
	
	const ivec3 tile_coords = p / VOXELS_PER_SIDE;
	
	const int tile_index = texelFetch(OldTileOccupancy, tile_coords, 0).x;
	if(tile_index == -1){
		return vec4(1.0f, 0.0f, 0.0f, 0.0f);
	}else if(tile_index == -2){
		return vec4(0.1f, 0.0f, 0.0f, 0.0f);
	}
	
	//convert to voxel local coord
	p %= VOXELS_PER_SIDE;
	
	const ivec3 atlas_dims = textureSize(OldVoxelsTexture, 0) / VOXELS_PER_SIDE;
	
	// convert to atlas coords
	ivec3 atlas_coords;
	atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
	int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
	atlas_coords.y = remainder / atlas_dims.x;
	atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;
	p += atlas_coords * VOXELS_PER_SIDE;

	return texelFetch(OldVoxelsTexture, p, 0);	
}

vec4 lerpSample(vec3 q) {
	const vec3 p = (q - vec3(0.5f));
	const vec3 floor_p = floor(p);
	const vec3 fract_p = p - floor_p;
	const ivec3 int_p = ivec3(floor_p);
				
	vec4 v000 = getSample(int_p + ivec3(0, 0, 0));
				
	vec4 v100 = getSample(int_p + ivec3(1, 0, 0));
	v000 = mix(v000, v100, fract_p.x);
				
	vec4 v010 = getSample(int_p + ivec3(0, 1, 0));
	vec4 v110 = getSample(int_p + ivec3(1, 1, 0));
	v010 = mix(v010, v110, fract_p.x);
	v000 = mix(v000, v010, fract_p.y);
				
	vec4 v001 = getSample(int_p + ivec3(0, 0, 1));
	vec4 v101 = getSample(int_p + ivec3(1, 0, 1));
	v001 = mix(v001, v101, fract_p.x);
				
	vec4 v011 = getSample(int_p + ivec3(0, 1, 1));
	vec4 v111 = getSample(int_p + ivec3(1, 1, 1));
	v011 = mix(v011, v111, fract_p.x);
	v001 = mix(v001, v011, fract_p.y);
	
	v000 = mix(v000, v001, fract_p.z);
	return v000;
}

void main(void){

	const int new_tile_index = int(gl_WorkGroupID.x);
	const TileDesc tile = new_tile_descriptors[new_tile_index];
	
	const int old_tile_index = tile.coords_index.w;
	
	const ivec3 new_tile_coords = tile.coords_index.xyz;
	const ivec3 new_voxel_coords = new_tile_coords * VOXELS_PER_SIDE + ivec3(gl_LocalInvocationID);
	
	const ivec3 old_voxel_coords = new_voxel_coords / 2;
	
	if(WriteDescriptors != 0 && gl_LocalInvocationIndex == 0){
		imageStore(NewTileOccupancy, new_tile_coords, ivec4(new_tile_index, 0, 0, 0));
		new_tile_descriptors[new_tile_index].coords_index.w = new_tile_index;
	}
	
	const ivec3 oldVoxelsTextureSize = textureSize(OldVoxelsTexture, 0);
	const ivec3 newVoxelsTextureSize = imageSize(NewVoxelsTexture);
	
	vec4 v = lerpSample(new_voxel_coords * 0.5f + 0.25f);
	
	if(WriteDescriptors != 0){
		//v.x = clamp(sqrt(v.x), 0.1f, 1.0f);
	}
	
	const ivec3 writeCoords = getVoxelAtlasCoords(new_tile_index, ivec3(gl_LocalInvocationID), newVoxelsTextureSize);
	imageStore(NewVoxelsTexture, writeCoords, v);
	
}

