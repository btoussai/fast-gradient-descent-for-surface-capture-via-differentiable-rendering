/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require
#extension GL_ARB_compute_shader :              enable
#extension GL_NV_gpu_shader5 : 					enable
#extension GL_KHR_shader_subgroup_ballot:		enable
#extension GL_NV_shader_subgroup_partitioned :	enable
#extension GL_KHR_shader_subgroup_arithmetic :  enable
#extension GL_NV_gpu_shader_fp64 : 				enable
#extension GL_NV_shader_atomic_float64 : 		enable
#extension GL_ARB_bindless_texture :			enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;


const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;
const int VOXELS_IN_GROUP = (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2);

layout(local_size_x = DEPTHMAP_TILE_WIDTH, local_size_y = DEPTHMAP_TILE_WIDTH, local_size_z = 1) in;
const int GROUP_SIZE = DEPTHMAP_TILE_SIZE;

struct DepthTileDescriptor{
	ivec2 tileCoords;			//coordinates of the tile in the image
	int foregroundPixelCount;	//number of actual foreground pixels in the tile
	int camIndex;				//index of the camera
};

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

struct CameraData{
	vec4 K;				// vec4(cx, cy, fx, fy)
	vec4 distortion1;   // vec4(k1, k2, p1, p2)
	vec4 distortion2;   // vec4(k3, 0, w, h)
	vec4 camCenter;
	mat4 cameraTransform;
	mat4 cameraTransformInv;
};

struct RayDescriptor{
	int intervalsPtr;
	int intervalsCount;
	int texelsPtr;
	int texelsCount;
	int depth_tile_x;
	int depth_tile_y;
};

struct Interval{
	int16_t start;//inclusive
	int16_t end;//exclusive
};

layout(std430, binding = 0) restrict readonly buffer TileDescriptorsBlock{
	TileDesc tile_descriptors[];
};

layout(std430, binding = 1) restrict readonly buffer RayDescriptorsBlock{
	RayDescriptor ray_descriptors[];
};

layout(std430, binding = 2) restrict readonly buffer IntervalsBlock{
	Interval intervals[];
};

layout(std430, binding = 4) restrict readonly buffer DepthTileDescriptorBlock{
	DepthTileDescriptor depthTileDescriptors[];
};

#include </common/projection.txt>

layout(std140, binding = 1) uniform ImageHandleBlock
{
	uint64_t RayTexelImageHandles[1024];
};


uniform vec3 TSDFminCorner;
uniform ivec3 TSDFTilesCount;
uniform float tile_size;
uniform float STEP_SIZE;
uniform int NumCameras;

layout(binding = 0) uniform isampler3D TileOccupancy;
layout(binding = 1) uniform sampler3D VoxelsTexture;

ivec3 getRayTexelCoords(uint slice, ivec2 localCoords){
	uint x = bitfieldExtract(slice, 0, 8);
	uint y = bitfieldExtract(slice, 8, 8);
	uint z = bitfieldExtract(slice, 16, 16);
	return ivec3(x * DEPTHMAP_TILE_WIDTH + localCoords.x, y * DEPTHMAP_TILE_WIDTH + localCoords.y, z);
}

shared vec4 partial_sums[gl_NumSubgroups];
vec4 groupAdd(vec4 v){
	
	vec4 w = subgroupAdd(v);
	if(gl_SubgroupInvocationID == gl_SubgroupSize-1){
		partial_sums[gl_SubgroupID] = w;
	}
	memoryBarrierShared();
	barrier();
	
	if(gl_SubgroupID == 0){
		vec4 v2 = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : vec4(0.0f);
		v2 = subgroupAdd(v2);
		if(subgroupElect()){
			partial_sums[0] = v2;
		}
	}
	memoryBarrierShared();
	barrier();
	
	return partial_sums[0];
}

float getSample(ivec3 p) {
	if(any(lessThan(p, ivec3(0))) || any(greaterThanEqual(p, TSDFTilesCount * VOXELS_PER_SIDE))){
		return 1.0f;
	}
	
	const ivec3 tile_coords = p / VOXELS_PER_SIDE;
	
	const int tile_index = texelFetch(TileOccupancy, tile_coords, 0).x;
	if(tile_index == -1){
		return 1.0f;
	}else if(tile_index == -2){
		return 0.1f;
	}
	
	//convert to voxel local coord
	p %= VOXELS_PER_SIDE;
	
	const ivec3 atlas_dims = textureSize(VoxelsTexture, 0) / VOXELS_PER_SIDE;
	
	// convert to atlas coords
	ivec3 atlas_coords;
	atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
	int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
	atlas_coords.y = remainder / atlas_dims.x;
	atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;
	p += atlas_coords * VOXELS_PER_SIDE;

	return texelFetch(VoxelsTexture, p, 0).x;	
}

float lerpSample(vec3 camCenter, vec3 ray, int t) {
	const vec3 p = (camCenter - vec3(0.5f)) + ray * float(t);
	const vec3 floor_p = floor(p);
	const vec3 fract_p = p - floor_p;
	const ivec3 int_p = ivec3(floor_p);
				
	float v000 = getSample(int_p + ivec3(0, 0, 0));
				
	float v100 = getSample(int_p + ivec3(1, 0, 0));
	v000 = mix(v000, v100, fract_p.x);
				
	float v010 = getSample(int_p + ivec3(0, 1, 0));
	float v110 = getSample(int_p + ivec3(1, 1, 0));
	v010 = mix(v010, v110, fract_p.x);
	v000 = mix(v000, v010, fract_p.y);
				
	float v001 = getSample(int_p + ivec3(0, 0, 1));
	float v101 = getSample(int_p + ivec3(1, 0, 1));
	v001 = mix(v001, v101, fract_p.x);
				
	float v011 = getSample(int_p + ivec3(0, 1, 1));
	float v111 = getSample(int_p + ivec3(1, 1, 1));
	v011 = mix(v011, v111, fract_p.x);
	v001 = mix(v001, v011, fract_p.y);
	
	v000 = mix(v000, v001, fract_p.z);
	return v000;
}

shared RayDescriptor rayDescriptor;
shared DepthTileDescriptor depthTileDescriptor;
shared Interval sharedIntervals[GROUP_SIZE];

void main() {
	//One thread per ray, one group per 2D tile
	const int depth_tile_id = int(gl_WorkGroupID.x);
	
	if(gl_LocalInvocationIndex == 0){
		rayDescriptor = ray_descriptors[depth_tile_id];
		depthTileDescriptor = depthTileDescriptors[depth_tile_id];
	}
	memoryBarrierShared();
	barrier();

	const int camIndex = depthTileDescriptor.camIndex;

	const int localLinearIndex = int(gl_LocalInvocationIndex);
	const ivec2 localDepthTileCoords =  ivec2(gl_LocalInvocationID);
	

	if(gl_LocalInvocationIndex < rayDescriptor.intervalsCount){
		sharedIntervals[gl_LocalInvocationIndex] = intervals[rayDescriptor.intervalsPtr + gl_LocalInvocationIndex];
	}
	memoryBarrierShared();
	barrier();
	
	
	
	const ivec2 pixelCoords = depthTileDescriptor.tileCoords * DEPTHMAP_TILE_WIDTH + localDepthTileCoords;
	const vec2 uv = vec2(pixelCoords) + vec2(0.5f);
	const vec2 ndcCoords = toNDC(uv, camIndex);

    const float tile_size_inv = 1.0f / tile_size;
    const float scaling = tile_size_inv * float(VOXELS_PER_SIDE);
    
	const vec3 camCenter = (getCamCenter(camIndex) - TSDFminCorner) * scaling;
	vec3 ray = unproject(ndcCoords, camIndex);
	ray /= dot(cameraData[camIndex].cameraTransformInv[2], vec4(ray, 0.0));
	ray *= STEP_SIZE;
	
	float Tpartial = 1.0f;
	const float transmittance_eps = 6.0f / 255.0f;
	
	//////////////////////////////////////////// March through the volume and integrate
	
	
	int slice = rayDescriptor.texelsPtr;
	for(int i=0; i<min(rayDescriptor.intervalsCount, GROUP_SIZE); i++){
		const Interval interval = sharedIntervals[i];
		
		for(int t = interval.start; t < interval.end; t++){
			
			if(Tpartial > transmittance_eps){
				float alpha = lerpSample(camCenter, ray, t);
				Tpartial *= alpha;
			}else{
				Tpartial = 0.0f;
			}
			
			const ivec3 bindless_coords = getRayTexelCoords(slice, ivec2(gl_LocalInvocationID.xy));
			layout(rgba8ui) uimage2D RayTexels = layout(rgba8ui) uimage2D(RayTexelImageHandles[bindless_coords.z]);
			imageStore(RayTexels, bindless_coords.xy, uvec4(round(Tpartial*255.0f), 0, 0, 0));
			
			slice++;
		}
		
	}
	
	
}


 
