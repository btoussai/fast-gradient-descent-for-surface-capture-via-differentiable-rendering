/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_NV_gpu_shader5 : 					enable
#extension GL_KHR_shader_subgroup_ballot:		enable
#extension GL_NV_shader_subgroup_partitioned :  enable
#extension GL_KHR_shader_subgroup_vote : 		enable
#extension GL_KHR_shader_subgroup_arithmetic :  enable
#extension GL_ARB_bindless_texture :			enable


const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

const int WORK_VOLUME_SIDE = VOXELS_PER_SIDE+2;
const int VOXELS_IN_GROUP = WORK_VOLUME_SIDE * WORK_VOLUME_SIDE * WORK_VOLUME_SIDE;

layout(local_size_x = VOXELS_PER_SIDE, local_size_y = VOXELS_PER_SIDE, local_size_z = VOXELS_PER_SIDE) in;
const int GROUP_SIZE = VOXELS_PER_TILE;


struct RayDescriptor{
	int intervalsPtr;
	int intervalsCount;
	int texelsPtr;
	int texelsCount;
	int depth_tile_x;
	int depth_tile_y;
};

struct Interval{
	int16_t start;//inclusive
	int16_t end;//exclusive
};

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

layout(std430, binding = 0) restrict buffer TileDescriptorsBlock{
	TileDesc tile_descriptors[];
};

layout(std430, binding = 1) restrict readonly buffer RayDescriptorsBlock{
	RayDescriptor ray_descriptors[];
};

layout(std430, binding = 2) restrict readonly buffer IntervalsBlock{
	Interval intervals[];
};

layout(std430, binding = 5) restrict writeonly buffer TileFlagsBlock{
	int tile_flags[];
};


#include </common/projection.txt>

layout(std140, binding = 1) uniform ImageHandleBlock
{
	uint64_t RayTexelImageHandles[1024];
};

layout(binding = 0) uniform isampler2DArray occupancyImage;

uniform vec3 TSDFminCorner;
uniform ivec3 TSDFTilesCount;
uniform float tile_size;
uniform float STEP_SIZE;
uniform int NumCameras;
uniform ivec4 GlobalActiveCameras;

shared vec2 partial_sums_vec2[gl_NumSubgroups];

vec2 groupMinMax(float v){
	vec2 w = subgroupMax(vec2(-v, v));
	if(gl_SubgroupInvocationID == gl_SubgroupSize-1){
		partial_sums_vec2[gl_SubgroupID] = w;
	}
	memoryBarrierShared();
	barrier();
	
	if(gl_SubgroupID == 0){
		vec2 v2 = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums_vec2[gl_SubgroupInvocationID] : vec2(-1.0f/0.0f);
		v2 = subgroupMax(v2);
		if(subgroupElect()){
			partial_sums_vec2[0] = v2;
		}
	}
	memoryBarrierShared();
	barrier();
	
	w = partial_sums_vec2[0];
	return vec2(-w.x, w.y);
}

ivec3 getRayTexelCoords(uint slice, ivec2 localCoords){
	uint x = bitfieldExtract(slice, 0, 8);
	uint y = bitfieldExtract(slice, 8, 8);
	uint z = bitfieldExtract(slice, 16, 16);
	return ivec3(x * DEPTHMAP_TILE_WIDTH + localCoords.x, y * DEPTHMAP_TILE_WIDTH + localCoords.y, z);
}

void getSample(ivec2 coordsPixel, 
		int t, 
		float depthWeight,
		float rayWeight, 
		int camIndex, 
		inout float transmittance) {

	const int depthSliceLocalIndex = (coordsPixel.y % DEPTHMAP_TILE_WIDTH) * DEPTHMAP_TILE_WIDTH
							+ (coordsPixel.x % DEPTHMAP_TILE_WIDTH);				
	const int depthTileIndex = texelFetch(occupancyImage, ivec3(coordsPixel / DEPTHMAP_TILE_WIDTH, camIndex), 0).x;
	
	if(depthTileIndex == -1){
		//outside the visual hull
		transmittance = 1.0f;
		return;
	}
	RayDescriptor rayDescriptor = ray_descriptors[depthTileIndex];
	
	if(rayDescriptor.intervalsCount == 0){
		//outside the visual hull
		transmittance = 1.0f;
		return;
	}
	
	int sliceIndex = rayDescriptor.texelsPtr;
	int interval_index = 0;

	Interval interval = intervals[rayDescriptor.intervalsPtr + interval_index];
	while(t >= interval.end){
		sliceIndex += interval.end - interval.start;
		interval_index++;
		if(interval_index < rayDescriptor.intervalsCount){
			interval = intervals[rayDescriptor.intervalsPtr + interval_index];
		}else{
			break;
		}
	}

	//if the voxel is not in an interval it means that it cannot be seen, 
	// hence the transmittance must be zero by default
	float transmittance1 = 0.0f;
	float transmittance2 = 0.0f;
	
	
	{
		bool inside = t >= interval.start && t < interval.end;
		if(inside){
			int offset = sliceIndex + t - interval.start;
			
			const ivec3 bindless_coords = getRayTexelCoords(offset, coordsPixel % DEPTHMAP_TILE_WIDTH);
			layout(rgba8ui) uimage2D RayTexels = layout(rgba8ui) uimage2D(RayTexelImageHandles[bindless_coords.z]);
			transmittance1 = imageLoad(RayTexels, bindless_coords.xy).x / 255.0f;
		}
		t++;
	}
	if(t >= interval.end && interval_index < rayDescriptor.intervalsCount){
		sliceIndex += interval.end - interval.start;
		interval_index++;
		if(interval_index < rayDescriptor.intervalsCount){
			interval = intervals[rayDescriptor.intervalsPtr + interval_index];
		}
	}
	{
		bool inside = t >= interval.start && t < interval.end;
		if(inside){
			int offset = sliceIndex + t - interval.start;
			
			const ivec3 bindless_coords = getRayTexelCoords(offset, coordsPixel % DEPTHMAP_TILE_WIDTH);
			layout(rgba8ui) uimage2D RayTexels = layout(rgba8ui) uimage2D(RayTexelImageHandles[bindless_coords.z]);
			transmittance2 = imageLoad(RayTexels, bindless_coords.xy).x / 255.0f;
		}
	}
	
	
	transmittance += mix(transmittance1, transmittance2, depthWeight) * rayWeight;
}

ivec3 getVoxelAtlasCoords(int tile_index, ivec3 localCoords, ivec3 voxelsTextureSize) {
	ivec3 atlas_dims = voxelsTextureSize / VOXELS_PER_SIDE;
	
	ivec3 atlas_coords;
	atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
	int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
	atlas_coords.y = remainder / atlas_dims.x;
	atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;
	
	return atlas_coords * VOXELS_PER_SIDE + localCoords;
}

bool isCameraActive(ivec4 activeCameras, int camIndex){
	return (activeCameras[camIndex/32] & (1<<(camIndex%32))) != 0;
}
void setCameraInactive(inout ivec4 activeCameras, int camIndex){
	activeCameras[camIndex/32] &= ~(1<<(camIndex%32));
}

void setCameraActive(inout ivec4 activeCameras, int camIndex, int b){
	activeCameras[camIndex/32] ^= (-b ^ activeCameras[camIndex/32]) & (1 << (camIndex % 32));
}

void main() {
	//One thread per voxel
	const float transmittance_threshold = 0.01f;

	const int tile_index = int(gl_WorkGroupID.x);
	TileDesc tileDescriptor = tile_descriptors[tile_index];
	const ivec3 tile_coords = ivec3(tileDescriptor.coords_index);
	ivec4 activeCameras = tileDescriptor.activeCameras;
	
    const float voxel_size = tile_size / VOXELS_PER_SIDE;
    const vec3 voxelCenter = TSDFminCorner + vec3(tile_coords) * tile_size + (vec3(gl_LocalInvocationID)+0.5f) * voxel_size;

	float overall_max_transmittance = 0.0f;//max transmittance over all the rays for this voxel.

	for(int camIndex=0; camIndex < NumCameras; camIndex++) {
		if(!isCameraActive(GlobalActiveCameras, camIndex)){
			//continue;
		}
	
		const vec3 toCam = normalize(voxelCenter - getCamCenter(camIndex));
		const vec3 voxelCenterProj = project(voxelCenter, camIndex);
		const float voxelDepth = voxelCenterProj.z;
		const vec2 coordsNDC = vec2(voxelCenterProj) / voxelDepth;
		const vec2 coordsPixel = toPixels(coordsNDC, camIndex);
		
		const vec2 image_size = getImageSize(camIndex);
		
		
		bool insideImage = isInsideImageRange(coordsPixel, image_size, 2);
		
		float transmittance = 0.0f;
		if(insideImage){
			const vec2 floorPixel = floor(coordsPixel - 0.5f);
			const ivec2 intPixel = ivec2(floorPixel);
			const float u = coordsPixel.x - 0.5f - floorPixel.x;
			const float v = coordsPixel.y - 0.5f - floorPixel.y;
			
			
			const float tile_size_inv = 1.0f / tile_size;
			const float scaling = float(VOXELS_PER_SIDE) / STEP_SIZE;
	
			const float sampleDepth = voxelDepth * tile_size_inv * scaling;
			const int t = int(floor(sampleDepth));
			const float depthWeight = sampleDepth - floor(sampleDepth);
			
			
			getSample(intPixel + ivec2(0,0), t, depthWeight, (1.0f-u)*(1.0f-v), camIndex, transmittance);
			getSample(intPixel + ivec2(0,1), t, depthWeight, u*(1.0f-v),        camIndex, transmittance);
			getSample(intPixel + ivec2(1,0), t, depthWeight, (1.0f-u)*v,        camIndex, transmittance);
			getSample(intPixel + ivec2(1,1), t, depthWeight, u*v,               camIndex, transmittance);
			
			overall_max_transmittance = max(overall_max_transmittance, transmittance);
		}else{
			//we keep the transmittance at zero to mean that this voxel is not visible
			// however, it does not affect the overall_max_transmittance variable.
		}
		
		vec2 minMaxTransmittance = groupMinMax(transmittance);
		
		if(gl_LocalInvocationIndex == 0){
			
			bool isInactive = minMaxTransmittance.y < transmittance_threshold || minMaxTransmittance == vec2(0.0f, 1.0f);
			setCameraActive(activeCameras, camIndex, int(!isInactive));
			
		}
		
	}
	
	
	float min_of_max_transmittances = groupMinMax(overall_max_transmittance).x;
	
	if(gl_LocalInvocationIndex == 0){
		int flag = 0;
		if(min_of_max_transmittances > 0.98 || activeCameras == ivec4(0)){
			flag = 1;
		}
		
		tile_flags[tile_index] = flag;
		tile_descriptors[tile_index].activeCameras = activeCameras;
	}
}
	
	
