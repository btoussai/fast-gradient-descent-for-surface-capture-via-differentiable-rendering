/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_NV_gpu_shader5 : 					enable


const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

layout(local_size_x = VOXELS_PER_SIDE, local_size_y = VOXELS_PER_SIDE, local_size_z = VOXELS_PER_SIDE) in;

struct CameraData{
	vec4 K;				// vec4(cx, cy, fx, fy)
	vec4 distortion1;   // vec4(k1, k2, p1, p2)
	vec4 distortion2;   // vec4(k3, 0, w, h)
	vec4 camCenter;
	mat4 cameraTransform;
	mat4 cameraTransformInv;
};

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

layout(binding=0, r32i) uniform restrict writeonly iimage3D TileOccupancy;
layout(binding=1, rgba8) uniform restrict readonly image3D OldVoxelsTexture;
layout(binding=2, rgba8) uniform restrict writeonly image3D NewVoxelsTexture;

layout(std430, binding = 0) restrict buffer NewTilesDescriptorBlock{
	TileDesc new_tile_descriptors[];
};

uniform int WriteDescriptors;

ivec3 getVoxelAtlasCoords(int tile_index, ivec3 localCoords, ivec3 voxelsTextureSize) {
	ivec3 atlas_dims = voxelsTextureSize / VOXELS_PER_SIDE;
	
	ivec3 atlas_coords;
	atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
	int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
	atlas_coords.y = remainder / atlas_dims.x;
	atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;
	
	return atlas_coords * VOXELS_PER_SIDE + localCoords;
}

void main(void){

	const int new_tile_index = int(gl_WorkGroupID.x);
	TileDesc tile = new_tile_descriptors[new_tile_index];
	const int old_tile_index = tile.coords_index.w;
	const ivec3 tile_coords = tile.coords_index.xyz;
	
	if(WriteDescriptors != 0 && gl_LocalInvocationIndex == 0){
		const ivec3 tile_coords = tile.coords_index.xyz;
		imageStore(TileOccupancy, tile_coords, ivec4(new_tile_index, 0, 0, 0));
		new_tile_descriptors[new_tile_index].coords_index.w = new_tile_index;
	}
	
	const ivec3 oldVoxelsTextureSize = imageSize(OldVoxelsTexture);
	const ivec3 newVoxelsTextureSize = imageSize(NewVoxelsTexture);
	
	ivec3 readCoords = getVoxelAtlasCoords(old_tile_index, ivec3(gl_LocalInvocationID), oldVoxelsTextureSize);
	ivec3 writeCoords = getVoxelAtlasCoords(new_tile_index, ivec3(gl_LocalInvocationID), newVoxelsTextureSize);
	
	const vec4 v = imageLoad(OldVoxelsTexture, readCoords);
	imageStore(NewVoxelsTexture, writeCoords, v);
	
}

