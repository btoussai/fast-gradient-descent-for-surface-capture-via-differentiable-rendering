/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_NV_gpu_shader5 : 					enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

layout(local_size_x = 32, local_size_y = 1, local_size_z = 1) in;

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

layout(binding = 0) uniform isampler3D TileOccupancy;

layout(std430, binding = 0) restrict readonly buffer TilesDescriptorBlock{
	TileDesc tile_descriptors[];
};

layout(std430, binding = 1) restrict readonly buffer OldTileFlagsBlock{
	int old_tile_flags[];
};

layout(std430, binding = 2) restrict writeonly buffer NewTileFlagsBlock{
	int new_tile_flags[];
};

bool willNeighborTileRemain(ivec3 tile_coords){
	const ivec3 texSize = textureSize(TileOccupancy, 0);
	
	if(any(lessThan(tile_coords, ivec3(0))) || any(greaterThanEqual(tile_coords, texSize))){
		return false;
	}

	const int tile_index = texelFetch(TileOccupancy, tile_coords, 0).x;
	
	if(tile_index < 0){
		return false;
	}
	
	const int flag = old_tile_flags[tile_index];
	return (flag & 1) == 0;
}

void main(void){

	const int tile_index = int(gl_GlobalInvocationID.x);
	if(tile_index < old_tile_flags.length()){
		TileDesc tile = tile_descriptors[tile_index];
		const ivec3 tile_coords = tile.coords_index.xyz;
		
		int flag = old_tile_flags[tile_index];
		flag = flag & 1;
		bool isEmptyOrFull = flag > 0;
		
		if(isEmptyOrFull){
		
			int remainingNeighbors = 0;
			if(willNeighborTileRemain(tile_coords + ivec3(-1, 0, 0))){
				flag |= 1 << 1;
				remainingNeighbors++;
			}
			if(willNeighborTileRemain(tile_coords + ivec3(+1, 0, 0))){
				flag |= 1 << 2;
				remainingNeighbors++;
			}
			if(willNeighborTileRemain(tile_coords + ivec3(0, -1, 0))){
				flag |= 1 << 3;
				remainingNeighbors++;
			}
			if(willNeighborTileRemain(tile_coords + ivec3(0, +1, 0))){
				flag |= 1 << 4;
				remainingNeighbors++;
			}
			if(willNeighborTileRemain(tile_coords + ivec3(0, 0, -1))){
				flag |= 1 << 5;
				remainingNeighbors++;
			}
			if(willNeighborTileRemain(tile_coords + ivec3(0, 0, +1))){
				flag |= 1 << 6;
				remainingNeighbors++;
			}
			if(remainingNeighbors > 0){
				flag |= 1 << 7;
			}
			
		}
		
		
		new_tile_flags[tile_index] = flag;
		
	}
	
}
