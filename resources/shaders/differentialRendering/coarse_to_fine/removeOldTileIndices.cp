/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_NV_gpu_shader5 : 					enable


const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

layout(local_size_x = 64, local_size_y = 1, local_size_z = 1) in;

layout(binding=0, r32i) uniform restrict writeonly iimage3D NewOccupancy;

layout(std430, binding = 0) restrict readonly buffer TilesToRemoveBlock{
	ivec4 coords_isfull[];
};

void main(void){
	
	if(gl_GlobalInvocationID.x < coords_isfull.length()){
	
		const ivec4 v = coords_isfull[gl_GlobalInvocationID.x];
		
		bool isfull = v.w == 1;
		
		imageStore(NewOccupancy, v.xyz, ivec4(isfull ? -2 : -1, 0, 0, 0));
	}
	
}

