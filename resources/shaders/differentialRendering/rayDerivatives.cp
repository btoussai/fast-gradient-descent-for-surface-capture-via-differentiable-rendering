/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require
#extension GL_NV_gpu_shader5 : 					enable
#extension GL_KHR_shader_subgroup_ballot:		enable
#extension GL_NV_shader_subgroup_partitioned :	enable
#extension GL_KHR_shader_subgroup_arithmetic :  enable
#extension GL_NV_gpu_shader_fp64 : 				enable
#extension GL_NV_shader_atomic_float64 : 		enable
#extension GL_ARB_bindless_texture :			enable
#extension GL_NV_gpu_shader_fp64 : 				enable
#extension GL_NV_shader_atomic_float64 : 		enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;


const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;
const int VOXELS_IN_GROUP = (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2);

layout(local_size_x = DEPTHMAP_TILE_WIDTH, local_size_y = DEPTHMAP_TILE_WIDTH, local_size_z = 1) in;
const int GROUP_SIZE = DEPTHMAP_TILE_SIZE;

struct DepthTileDescriptor{
	ivec2 tileCoords;			//coordinates of the tile in the image
	int foregroundPixelCount;	//number of actual foreground pixels in the tile
	int camIndex;				//index of the camera
};

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

struct RayDescriptor{
	int intervalsPtr;
	int intervalsCount;
	int texelsPtr;
	int texelsCount;
	int depth_tile_x;
	int depth_tile_y;
};

struct Interval{
	int16_t start;//inclusive
	int16_t end;//exclusive
};

layout(std430, binding = 0) restrict readonly buffer RayDescriptorsBlock{
	RayDescriptor ray_descriptors[];
};

layout(std430, binding = 1) restrict readonly buffer IntervalsBlock{
	Interval intervals[];
};

layout(std430, binding = 2) restrict readonly buffer DepthTileDescriptorBlock{
	DepthTileDescriptor depthTileDescriptors[];
};

layout(std430, binding = 3) restrict readonly buffer GlobalIntegrationBlock{
	f16vec4 global_integration_texels[];
};

layout(std430, binding = 4) restrict readonly buffer TilesBlock{
	u8vec4 tiles2D[];
};

layout(std430, binding = 5) restrict buffer AtomicBlock{
	float64_t atomics[];
};

#include </common/projection.txt>
#include </common/robust_norm.txt>
#include </common/float_encoding.txt>

layout(std140, binding = 1) uniform ImageHandleBlock
{
	uint64_t RayTexelImageHandles[1024];
};

uniform vec3 TSDFminCorner;
uniform vec3 TSDFmaxCorner;
uniform ivec3 TSDFTilesCount;
uniform float tile_size;
uniform float STEP_SIZE;
uniform int NumCameras;
uniform int NumTiles;

uniform int skipInactiveCamerasIntegration;
uniform int skipInactiveCamerasError;
uniform ivec4 activeCameras;

uniform float backgroundEntropyReg;
uniform float colorDiscrepancyReg;
uniform int NoColorSpilling;
uniform float MaskReg;

layout(binding = 0) uniform sampler2DArray Backgrounds;
layout(binding = 1) uniform sampler1D ExposureLUT;


shared vec4 partial_sums[gl_NumSubgroups];
vec4 groupAdd(vec4 v){
	
	vec4 w = subgroupAdd(v);
	if(gl_SubgroupInvocationID == gl_SubgroupSize-1){
		partial_sums[gl_SubgroupID] = w;
	}
	memoryBarrierShared();
	barrier();
	
	if(gl_SubgroupID == 0){
		vec4 v2 = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : vec4(0.0f);
		v2 = subgroupAdd(v2);
		if(subgroupElect()){
			partial_sums[0] = v2;
		}
	}
	memoryBarrierShared();
	barrier();
	
	return partial_sums[0];
}

ivec3 getRayTexelCoords(uint slice, ivec2 localCoords){
	uint x = bitfieldExtract(slice, 0, 8);
	uint y = bitfieldExtract(slice, 8, 8);
	uint z = bitfieldExtract(slice, 16, 16);
	return ivec3(x * DEPTHMAP_TILE_WIDTH + localCoords.x, y * DEPTHMAP_TILE_WIDTH + localCoords.y, z);
}

shared RayDescriptor rayDescriptor;
shared DepthTileDescriptor depthTileDescriptor;
shared Interval sharedIntervals[GROUP_SIZE];

bool isCameraActive(int camIndex){
	return (activeCameras[camIndex/32] & (1<<(camIndex%32))) != 0;
}


void main() {
	//One thread per ray, one group per 2D tile
	const int depth_tile_id = int(gl_WorkGroupID.x);
	
	if(gl_LocalInvocationIndex == 0){
		rayDescriptor = ray_descriptors[depth_tile_id];
		depthTileDescriptor = depthTileDescriptors[depth_tile_id];
	}
	memoryBarrierShared();
	barrier();

	const int camIndex = depthTileDescriptor.camIndex;
	if(skipInactiveCamerasIntegration>0 && !isCameraActive(camIndex)){
		return;
	}

	const int localLinearIndex = int(gl_LocalInvocationIndex);
	const ivec2 localDepthTileCoords =  ivec2(gl_LocalInvocationID);

	if(gl_LocalInvocationIndex < rayDescriptor.intervalsCount){
		sharedIntervals[gl_LocalInvocationIndex] = intervals[rayDescriptor.intervalsPtr + gl_LocalInvocationIndex];
	}
	memoryBarrierShared();
	barrier();
	
		
	const ivec2 pixelCoords = depthTileDescriptor.tileCoords * DEPTHMAP_TILE_WIDTH + localDepthTileCoords;
	
	const vec2 exposureCorrection = texelFetch(ExposureLUT, camIndex, 0).xy;
	const vec4 real_color = tiles2D[depth_tile_id * DEPTHMAP_TILE_SIZE + gl_LocalInvocationIndex] / 255.0f;
	const vec3 corrected_real_color = real_color.rgb*exposureCorrection.x + vec3(exposureCorrection.y);
	
	// Read the final result of the integration
	const vec4 integral = global_integration_texels[depth_tile_id * DEPTHMAP_TILE_SIZE + localLinearIndex];
	const float Tinf = integral.x;
	const vec3 integrated_color = vec3(integral.yzw);
	
	float Mask = 0.0f;
	if(MaskReg > 0){
		Mask = 1.0 - texelFetch(Backgrounds, ivec3(pixelCoords, camIndex), 0).a;
	}
	
	/////////////////////////////////////////////////////////////////////////////////////
	const vec3 color_diff = integrated_color - corrected_real_color;
	const vec3 grad_color_diff = huber_grad(color_diff);
	
	float TotalLoss = 0.0f;
	
	float Tpartial = 1.0f;
	vec3 Cpartial = vec3(0.0);
	
	bool canSkip = false;
	int slice = rayDescriptor.texelsPtr;
	for(int i=0; i<min(rayDescriptor.intervalsCount, GROUP_SIZE); i++){
		const Interval interval = sharedIntervals[i];
		for(int t = interval.start; t < interval.end; t++){
			vec4 gradient = vec4(0.0f);
			
			const ivec3 bindless_coords = getRayTexelCoords(slice, ivec2(gl_LocalInvocationID.xy));
			layout(rgba8ui) uimage2D RayTexels = layout(rgba8ui) uimage2D(RayTexelImageHandles[bindless_coords.z]);
			
			if(!canSkip){
				vec4 v = loadFixedPoint(RayTexels, bindless_coords.xy);
				float alpha = v.x;
				vec3 c = vec3(v.y, v.z, v.w);
				
				float contrib = Tpartial * (1.0f - alpha);
				Cpartial += c * contrib;

				//Gradient				
				vec3 dCdalpha =  - c * Tpartial + (integrated_color - Cpartial) / alpha;


				/////////////////////////////////////////////////////////Photometric loss
				float dalpha = dot(grad_color_diff, dCdalpha);
				vec3 dc = grad_color_diff * contrib;
				
				
				vec3 sample_color_diff = integrated_color - c;
				vec3 A = contrib * huber_grad(sample_color_diff);


				//////////////////////////////////////////////////////////Color discrepancy loss 				
				dalpha += colorDiscrepancyReg * (
							-Tpartial * huber_loss(sample_color_diff) + 
							dot(A, dCdalpha) 
						);
				dc += colorDiscrepancyReg * A * (contrib - 1.0f);
				
				TotalLoss += huber_loss(sample_color_diff) * contrib * colorDiscrepancyReg;
				
				////////////////////////////////////////////////////////////Background entropy loss
				dalpha += (1.0f - 2.0f*Tinf) * (Tinf / alpha) * backgroundEntropyReg;
				
				/////////////////////////////////////////////////////////////Mask regularization
				dalpha += 2.0f * (Tinf - Mask) * (Tinf / alpha) * MaskReg;
				
				
				if(NoColorSpilling != 0){
					dc *= (1.0f-Tinf) * (1.0f-Tinf);
				}
				
				gradient = vec4(dalpha, dc);

				
				Tpartial *= alpha;
				canSkip = Tpartial < 1.0E-3f;
			}
			
			storeEncodedFloat(RayTexels, bindless_coords.xy, gradient);
			
			slice++;
		}
		
	}
	
	TotalLoss += huber_loss(color_diff); //Photometric loss
	TotalLoss += (1.0 - Tinf) * Tinf * backgroundEntropyReg;
	TotalLoss += (Tinf - Mask) * (Tinf - Mask) * MaskReg;
	
	TotalLoss = groupAdd(vec4(TotalLoss, 0, 0, 0)).x;
	if(gl_LocalInvocationIndex == 0){
		atomicAdd(atomics[0], TotalLoss);
	}
	
}


 
