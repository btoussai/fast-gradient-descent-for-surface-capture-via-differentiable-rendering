/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require
#extension GL_NV_gpu_shader5 : 					enable
#extension GL_KHR_shader_subgroup_ballot:		enable
#extension GL_KHR_shader_subgroup_arithmetic : enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;
const int VOXELS_IN_GROUP = (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2);

layout(local_size_x = DEPTHMAP_TILE_WIDTH, local_size_y = DEPTHMAP_TILE_WIDTH, local_size_z = 1) in;
const int GROUP_SIZE = DEPTHMAP_TILE_SIZE;

struct CameraData{
	vec4 K;				// vec4(cx, cy, fx, fy)
	vec4 distortion1;   // vec4(k1, k2, p1, p2)
	vec4 distortion2;   // vec4(k3, 0, w, h)
	vec4 camCenter;
	mat4 cameraTransform;
	mat4 cameraTransformInv;
};

#define #SHBands
#define #VoxelsTexturesCount

layout(binding = 0) uniform sampler3D VoxelsTexture[VoxelsTexturesCount];
layout(binding = 7) uniform usampler3D CompressedTileOccupancy;
layout(binding = 8) uniform isampler3D TileOccupancy;
layout(binding = 9) uniform sampler2DArray Backgrounds;
layout(binding = 10) uniform sampler1D ExposureLUT;

layout(binding = 0, rgba8) uniform restrict writeonly image2D finalImage;

#include </common/projection.txt>
#include </common/rayVSBox.txt>


//https://en.wikipedia.org/wiki/Table_of_spherical_harmonics#Real_spherical_harmonics
const float baseCoeff = 0.28209479177387814;
const float SHcoeffs[9] = {
	1.0f,   						   // 0.5 * sqrt(1/pi)
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	0.31539156525252005 / baseCoeff,   // 0.25 * sqrt(5 / pi)
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	1.0925484305920792  / baseCoeff    // 0.5 * sqrt(15 / pi)
};


uniform vec3 TSDFminCorner;
uniform vec3 TSDFmaxCorner;
uniform ivec3 TSDFTilesCount;
uniform float tile_size;
uniform float STEP_SIZE;
uniform int NumCameras;
uniform int NumTiles;
uniform int camIndex;

shared ivec4 partial_sums[32];

ivec4 groupMin(ivec4 v){

	v = subgroupMin(v);
	if(subgroupElect()){
		partial_sums[gl_SubgroupID] = v;
	}

	memoryBarrierShared();
	barrier();

	if(gl_SubgroupID == 0){
		v = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : ivec4(2147483647);
		ivec4 w = subgroupMin(v);
		partial_sums[gl_SubgroupInvocationID] = w;
	}

	memoryBarrierShared();
	barrier();

	return partial_sums[gl_SubgroupID];
}

ivec4 groupAdd(ivec4 v){

	v = subgroupAdd(v);
	if(subgroupElect()){
		partial_sums[gl_SubgroupID] = v;
	}

	memoryBarrierShared();
	barrier();

	if(gl_SubgroupID == 0){
		v = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : ivec4(0);
		ivec4 w = subgroupAdd(v);
		partial_sums[gl_SubgroupInvocationID] = w;
	}

	memoryBarrierShared();
	barrier();

	return partial_sums[gl_SubgroupID];
}

void getSampleCoords(ivec3 p, vec3 d, out int tile_index, out ivec3 q) {
	if(any(lessThan(p, ivec3(0))) || any(greaterThanEqual(p, TSDFTilesCount * VOXELS_PER_SIDE))){
		tile_index = -1;
		return;
	}
	
	const ivec3 tile_coords = p / VOXELS_PER_SIDE;
	tile_index = texelFetch(TileOccupancy, tile_coords, 0).x;
	if(tile_index < 0){
		return;
	}
	
	//convert to voxel local coord
	p %= VOXELS_PER_SIDE;
	const ivec3 atlas_dims = textureSize(VoxelsTexture[0], 0) / VOXELS_PER_SIDE;
	
	// convert to atlas coords
	ivec3 atlas_coords;
	atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
	int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
	atlas_coords.y = remainder / atlas_dims.x;
	atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;
	p += atlas_coords * VOXELS_PER_SIDE;
	
	q = p;
}

vec4 mixSample(int tile_index[8], ivec3 q[8], int N, vec3 fract_p){

	vec4 v[8];
	for(int i=0; i<8; i++){
		if(tile_index[i] == -2){
			v[i] = N==0 ? vec4(0.1, vec3(0)) : vec4(0.5);
		}else if(tile_index[i] == -1){
			v[i] = N==0 ? vec4(1.0, vec3(0)) : vec4(0.5);
		}else{
			v[i] = texelFetch(VoxelsTexture[N], q[i], 0);
		}
		
		if(i % 2 == 1){
			v[i] = mix(v[i-1], v[i], fract_p.x);
		}
		
		if(i % 4 == 3){
			v[i] = mix(v[i-2], v[i], fract_p.y);
		}
		
		if(i == 7){
			v[i] = mix(v[i-4], v[i], fract_p.z);
		}
	}
	
	return v[7];
}

vec4 getSample(vec3 camCenter, vec3 ray, int t) {
	const vec3 p = (camCenter - vec3(0.5f)) + ray * float(t);
	const vec3 floor_p = floor(p);
	const vec3 fract_p = p - floor_p;
	const ivec3 int_p = ivec3(floor_p);
	
	int tile_index[8];
	ivec3 q[8];
	
	for(int i=0; i<8; i++){
		getSampleCoords(int_p + ivec3((i>>0)&1, (i>>1)&1, (i>>2)&1), ray, tile_index[i], q[i]);
	}
	
	float alpha = 0.0;
	vec3 c = vec3(0.0f);
	
	#if SHBands==2
		vec4 v6 = 2.0f * mixSample(tile_index, q, 6, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v5 = 2.0f * mixSample(tile_index, q, 5, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v4 = 2.0f * mixSample(tile_index, q, 4, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v3 = 2.0f * mixSample(tile_index, q, 3, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v2 = 2.0f * mixSample(tile_index, q, 2, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v1 = 2.0f * mixSample(tile_index, q, 1, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v0 = mixSample(tile_index, q, 0, fract_p);
		
		vec3 d = normalize(ray);
		c += vec3(v6.yzw)      * SHcoeffs[8] * (d.x*d.x - d.y*d.y);
		c += vec3(v5.zw, v6.x) * SHcoeffs[7] * (d.x*d.z);
		c += vec3(v4.w, v5.xy) * SHcoeffs[6] * (3.0f*d.z*d.z - 1.0f);
		c += vec3(v4.xyz)      * SHcoeffs[5] * (d.y*d.z);
		c += vec3(v3.yzw)      * SHcoeffs[4] * (d.x*d.y);
		c += vec3(v2.zw, v3.x) * SHcoeffs[3] * d.x;
		c += vec3(v1.w, v2.xy) * SHcoeffs[2] * d.z;
		c += vec3(v1.xyz)      * SHcoeffs[1] * d.y;
		c += vec3(v0.yzw)      * SHcoeffs[0];
		alpha = v0.x;
	#elif SHBands==1
		vec4 v3 = 2.0f * mixSample(tile_index, q, 3, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v2 = 2.0f * mixSample(tile_index, q, 2, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v1 = 2.0f * mixSample(tile_index, q, 1, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v0 = mixSample(tile_index, q, 0, fract_p);
		
		vec3 d = normalize(ray);
		c += vec3(v2.zw, v3.x) * SHcoeffs[3] * d.x;
		c += vec3(v1.w, v2.xy) * SHcoeffs[2] * d.z;
		c += vec3(v1.xyz)      * SHcoeffs[1] * d.y;
		c += vec3(v0.yzw)      * SHcoeffs[0];
		alpha = v0.x;
	#else
		vec4 v0 = mixSample(tile_index, q, 0, fract_p);
		c += vec3(v0.yzw);
		alpha = v0.x;
	#endif
	
	return vec4(alpha, c);
}

void main(void){
	//One thread per ray
	
	const ivec2 pixelCoords = ivec2(gl_GlobalInvocationID.xy);
	const vec2 uv = vec2(pixelCoords) + vec2(0.5f);
	const vec2 ndcCoords = toNDC(uv, camIndex);

    const float tile_size_inv = 1.0f / tile_size;
    const float scaling = tile_size_inv * float(VOXELS_PER_SIDE);

	const vec3 camCenter = (getCamCenter(camIndex) - TSDFminCorner) * scaling;
	vec3 ray = unproject(ndcCoords, camIndex);
	ray /= dot(cameraData[camIndex].cameraTransformInv[2], vec4(ray, 0.0));
	ray *= STEP_SIZE;
	const vec3 ray_inv = 1.0f / ray;

	float tmin, tmax;
	bool hit = rayVSbox(camCenter, ray_inv, vec3(0.0f), (TSDFmaxCorner-TSDFminCorner)*scaling, tmin, tmax);

	ivec4 bounds = hit ? ivec4(int(floor(max(0.0f, tmin))), -int(ceil(tmax)), 0, 0) : ivec4(2147483647);
	bounds = groupMin(bounds);

	int t = bounds.x;
	const int max_t = -bounds.y;


	const float transmittance_eps = 6.0f / 255.0f;
	bool prev_empty = true;
	float Tpartial = 1.0f;
	vec3 Cpartial = vec3(0.0f);
	while(t <= max_t){
		if(hit){
			const vec3 p = camCenter + ray * float(t);
			const ivec3 tile_coords = ivec3(floor(p / float(VOXELS_PER_SIDE)));

			if(all(greaterThanEqual(tile_coords, ivec3(0))) && all(lessThan(tile_coords, TSDFTilesCount))){
				const ivec3 compressedCoord = tile_coords / 4;
				const ivec3 localCompressedCoord = tile_coords % 4;
				const uvec2 bitmask = uvec2(texelFetch(CompressedTileOccupancy, ivec3(compressedCoord), 0));//returns 0 when sampling outside
				const int bitPos = localCompressedCoord.x + 4*localCompressedCoord.y + 16 * (localCompressedCoord.z%2);
				bool isEmpty = ( ((localCompressedCoord.z/2)==0 ? bitmask.x : bitmask.y) & (1<<bitPos)) == 0;
				
				if(!isEmpty){
					vec4 v = getSample(camCenter, ray, t);
					float alpha = v.x;
					vec3 color = vec3(v.y, v.z, v.w);

					Cpartial += color * (Tpartial * (1.0f - alpha));
					Tpartial *= alpha;
					
					if(Tpartial < transmittance_eps){
						break;
					}
				}
				
			}
		}

		t++;
	}

	const vec2 exposureCorrection = texelFetch(ExposureLUT, camIndex, 0).xy;
	const vec3 backgroundColor = vec3(texelFetch(Backgrounds, ivec3(pixelCoords, camIndex), 0));
	const vec3 corrected_backgroundColor = backgroundColor*exposureCorrection.x + vec3(exposureCorrection.y);
	vec3 integrated_color = Cpartial; // + corrected_backgroundColor * Tpartial;
	
	//Apply the inverse correction
	//if(exposureCorrection.x > 0)
	//	integrated_color = (integrated_color - exposureCorrection.y) / exposureCorrection.x;

	ivec2 imgSize = imageSize(finalImage);
	imageStore(finalImage, ivec2(gl_GlobalInvocationID.x, imgSize.y - 1 - gl_GlobalInvocationID.y), vec4(integrated_color, 1.0f));
}
