/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require
#extension GL_NV_gpu_shader5 : 					enable
#extension GL_KHR_shader_subgroup_ballot:		enable
#extension GL_NV_gpu_shader_fp64 : 				enable
#extension GL_NV_shader_atomic_float64 : 		enable
#extension GL_KHR_shader_subgroup_arithmetic :  enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;
const int VOXELS_IN_GROUP = (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2);

layout(local_size_x = VOXELS_PER_SIDE, local_size_y = VOXELS_PER_SIDE, local_size_z = VOXELS_PER_SIDE) in;
const int GROUP_SIZE = VOXELS_PER_TILE;

layout(std430, binding = 0) restrict readonly buffer GradientBlock{
	u8vec4 gradients[];
};

layout(std430, binding = 1) restrict readonly buffer GradientSquaredBlock{
	u8vec4 gradients_squared[];
};

#include </common/float_encoding.txt>

#define #SHBands
#define #VoxelsTexturesCount

layout(binding=0, rgba8) uniform restrict image3D VoxelsTexture[VoxelsTexturesCount];

uniform vec2 Biases;
uniform float GlobalLearningRate;

ivec3 getVoxelAtlasCoords(int tile_index, ivec3 localCoords) {
	ivec3 atlas_dims = imageSize(VoxelsTexture[0]) / VOXELS_PER_SIDE;
	
	ivec3 atlas_coords;
	atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
	int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
	atlas_coords.y = remainder / atlas_dims.x;
	atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;
	
	return atlas_coords * VOXELS_PER_SIDE + localCoords;
}

shared vec4 partial_sums[gl_NumSubgroups];
vec4 groupAdd(vec4 v){
	
	vec4 w = subgroupAdd(v);
	if(gl_SubgroupInvocationID == gl_SubgroupSize-1){
		partial_sums[gl_SubgroupID] = w;
	}
	memoryBarrierShared();
	barrier();
	
	if(gl_SubgroupID == 0){
		vec4 v2 = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : vec4(0.0f);
		v2 = subgroupAdd(v2);
		if(subgroupElect()){
			partial_sums[0] = v2;
		}
	}
	memoryBarrierShared();
	barrier();
	
	return partial_sums[0];
}


void main(void){
	//One thread per voxel

	const float epsilon = 1.0E-8f;
	const uint tile_index = gl_WorkGroupID.x;
	ivec3 atlasCoords = getVoxelAtlasCoords(int(tile_index), ivec3(gl_LocalInvocationID));
	
    for(int k=0; k<VoxelsTexturesCount; k++){
		vec4 v = imageLoad(VoxelsTexture[k], atlasCoords);
		if(k>0){
			v = 2.0f * v - 1.0f;
		}
		
		const uint gradient_index = VOXELS_PER_TILE * (tile_index * VoxelsTexturesCount + k) + gl_LocalInvocationIndex;
		
		vec4 g  = decodeSignedFloat8(gradients[gradient_index]);
		vec4 g2 = decodeSignedFloat8(gradients_squared[gradient_index]);
		
		g2 = g2 * g2;
		
		g *= Biases.x;
		g2 *= Biases.y;
		
		vec4 grad = GlobalLearningRate * inversesqrt(g2 + epsilon) * g;
		v -= grad;
		
		if(k==0){
			v.x = clamp(v.x, 0.1f, 1.0f);
			v.yzw = clamp(v.yzw, vec3(0.0f), vec3(1.0f));
		}else{
			v = clamp(v, -1.0f, 1.0f) * 0.5f + 0.5f;
		}
		
		imageStore(VoxelsTexture[k], atlasCoords, v);
		
	}
	
	
}
