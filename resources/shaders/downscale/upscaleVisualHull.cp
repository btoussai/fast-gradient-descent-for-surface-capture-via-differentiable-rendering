/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_KHR_shader_subgroup_arithmetic :  enable
#extension GL_NV_gpu_shader5 : 					enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

layout(local_size_x = DEPTHMAP_TILE_WIDTH, local_size_y = DEPTHMAP_TILE_WIDTH, local_size_z = 1) in;

layout(binding=0, rgba8) uniform restrict readonly image2DArray srcImages;
layout(binding=1, rgba8) uniform restrict image2DArray dstImages;
layout(binding=2, r32i) uniform restrict writeonly iimage2DArray occupancyImages;


struct DepthTileDescriptor{
	ivec2 tileCoords;//coordinates of the tile in the image
	int foregroundPixelCount;//number of actual foreground pixels in the tile
	int camIndex;//index of the camera
};

layout(std430, binding = 0) restrict writeonly buffer DepthTileDescriptorBlock{
	DepthTileDescriptor TileDescriptors[];
};
layout(std430, binding = 1) restrict writeonly buffer TilesBlock{
	u8vec4 tiles2D[];
};
layout(binding = 0) uniform atomic_uint GlobalTileCounter;


shared int partial_sums[32];
int groupAdd(int v){

	v = subgroupAdd(v);
	if(subgroupElect()){
		partial_sums[gl_SubgroupID] = v;
	}

	memoryBarrierShared();
	barrier();

	if(gl_SubgroupID == 0){
		v = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : int(0);
		int w = subgroupAdd(v);
		partial_sums[gl_SubgroupInvocationID] = w;
	}

	memoryBarrierShared();
	barrier();

	return partial_sums[gl_SubgroupID];
}

shared int offset;
void main(void){
	int camIndex = int(gl_WorkGroupID.z);

	const float alpha = imageLoad(srcImages, ivec3(gl_GlobalInvocationID.xy/2, camIndex)).a;
	
	bool isInsideVisualHull = alpha > 0.0f;
			
	vec4 color = imageLoad(dstImages, ivec3(gl_GlobalInvocationID.xy,camIndex));
	color.a = alpha;
	imageStore(dstImages, ivec3(gl_GlobalInvocationID.xy,camIndex), color);
	
	
	int validPixels = groupAdd(int(isInsideVisualHull));
	
	if(validPixels > 0){
		if(gl_LocalInvocationIndex == 0){
			offset = int(atomicCounterAdd(GlobalTileCounter, 1u));
			TileDescriptors[offset] = DepthTileDescriptor(ivec2(gl_WorkGroupID.xy), validPixels, camIndex);
			imageStore(occupancyImages, ivec3(gl_WorkGroupID), ivec4(offset, 0, 0, 0));
		}

		memoryBarrierShared();
		barrier();
		tiles2D[offset * DEPTHMAP_TILE_SIZE + gl_LocalInvocationIndex] = u8vec4(color * 255.0f);
	}
	
}

