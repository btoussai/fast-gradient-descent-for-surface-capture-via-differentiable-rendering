/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

out vec4 out_Color;

in vec2 texCoords;
layout(binding = 0) uniform sampler2D Image;

void main(void){

	vec4 c = texture(Image, texCoords);
	out_Color.rgb = abs(c.rgb);
	
	
}
