/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

out vec4 out_Color;

uniform vec3 camPos;

in vec3 pos_fs;
in vec3 normal_fs;
in vec2 uv_pos;

uniform int useColors;
uniform vec4 clipEquation;

layout(binding = 0) uniform sampler2D Tex;


void main(void){

	if(dot(clipEquation, vec4(pos_fs, 1.0)) > 0.0){
		discard;
	}

	vec3 N = normalize(normal_fs);
	vec3 toCam = normalize(camPos - pos_fs);
	float d = abs(dot(toCam, N));
	
	if(useColors > 0){
		out_Color = vec4(texture(Tex, uv_pos).rgb, 1);
	}else {
		out_Color = vec4(d, d, d, 1);
	}
	
	
}
