/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require

in vec4 position;
in vec4 normal;


const int PROJECT_ON_2D_CANVAS = 0;
const int PROJECT_ON_3D_CANVAS = 1;

//3D canvas
uniform mat4 projection;
uniform mat4 view;
uniform mat4 transform;

//2D canvas
uniform int PROJECTION_TYPE;
uniform mat4 canvas_transform;
uniform int selectedCamIndex;
uniform int RemoveDistortion;

out vec3 pos_fs;
out vec3 normal_fs;
out vec4 color;


#include </common/projection.txt>


void main(void){
	vec4 wsPos = transform * vec4(position.xyz, 1.0f);
	vec4 wsNormal = transform * vec4(normal.xyz, 0.0f);
	normal_fs = normalize(wsNormal.xyz);
	pos_fs = wsPos.xyz;
	color = unpackUnorm4x8(floatBitsToUint(normal.w));

	if(PROJECTION_TYPE == PROJECT_ON_2D_CANVAS) {
		float depth;
		vec2 uv = project_distort(wsPos.xyz, selectedCamIndex, depth, RemoveDistortion==0);
		uv.xy = vec2(2.0*uv.x-1.0, 1.0-2.0*uv.y);
		depth = depth / 10000.0f - 1.0f;
		gl_Position = canvas_transform * vec4(uv.xy, depth, 1.0f);
	}else{
		gl_Position = projection * view * wsPos;
	}

}
