/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

in vec3 position;
in float error;

uniform mat4 projectionView;
uniform mat4 transform;

out float out_error;

void main(void) {
	vec3 p = position;
	gl_Position = projectionView * transform * vec4(p, 1.0);
	out_error = error;
	
	float d = gl_Position.w;
	float dmax = 1;
	d = clamp(d, 0.05, dmax) / dmax;
	
	gl_PointSize = 4/d;
}


