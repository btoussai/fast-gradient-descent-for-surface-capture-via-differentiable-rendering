/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_bindless_texture :			enable
#extension GL_NV_gpu_shader5 : 					enable

out vec4 out_Color;

in vec2 texCoords;

uniform int layer;

layout(std140, binding = 1) uniform ImageHandleBlock
{
	uint64_t RayTexelImageHandles[1024];
};

void main(void){

	uint64_t address = RayTexelImageHandles[layer];
	layout(rgba8ui) uimage2D RayTexels = layout(rgba8ui) uimage2D(address);
	vec4 v = imageLoad(RayTexels, ivec2(texCoords * 2048)) / 255.0f;

	out_Color = v;
	
	if(address == 0){
		out_Color = vec4(1, 0, 1, 1); 
	}
	
	
}
