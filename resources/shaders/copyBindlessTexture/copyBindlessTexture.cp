/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_bindless_texture :			enable
#extension GL_NV_gpu_shader5 : 					enable

layout(local_size_x = 64, local_size_y = 1, local_size_z = 1) in;

uniform int layer;
uniform ivec2 size;

layout(std140, binding = 1) uniform ImageHandleBlock
{
	uint64_t RayTexelImageHandles[1024];
};

layout(std430, binding = 0) restrict writeonly buffer Block{
	u8vec4 pixel_buffer[];
};

void main(void){

	int n = int(gl_GlobalInvocationID.x);
	ivec2 coords = ivec2(n % size.x, n / size.x);

	layout(rgba8ui) uimage2D RayTexels = layout(rgba8ui) uimage2D(RayTexelImageHandles[layer]);
	uvec4 v = imageLoad(RayTexels, coords);
	
	pixel_buffer[n] = u8vec4(v);
	
}
