/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

in vec2 position;
out vec2 texCoords;

uniform mat4 transform;

void main(void) {
	texCoords = vec2(position.x*0.5+0.5, position.y*0.5+0.5);
	gl_Position = transform * vec4(position, 0.0, 1.0);
}


