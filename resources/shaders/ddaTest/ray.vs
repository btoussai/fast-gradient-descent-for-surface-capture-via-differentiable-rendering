/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

in vec4 pos;

uniform mat4 projectionView;
uniform mat4 transform;

void main(void){

	gl_Position = projectionView * transform * pos;
	
}
