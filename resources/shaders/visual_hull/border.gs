/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

layout(points) in;
layout(line_strip, max_vertices=4) out;

out vec4 color_fs;

in Segment{
	vec2 a;
	vec2 b;
} segments[];

uniform mat4 canvas_transform;

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main(void){
	const float PI_OVER_2 = 1.5707963267948966;
	
	vec2 a = segments[0].a;
	vec2 b = segments[0].b;
	
	vec2 c = 0.5f*(a+b);
	vec2 e = b-a;
	vec2 n = vec2(-e.y, e.x);
	
	//float hue = clamp(atan(b.y-a.y, b.x-a.x) / PI_OVER_2 * 0.5f + 0.5f, 0.0f, 1.0f);
	//vec3 hsv = vec3(hue, 1.0f, 1.0f);
	//vec4 c = vec4(hsv2rgb(hsv), 1.0f);
	
	gl_Position = canvas_transform * vec4(a.x, a.y, 0, 1);
	color_fs = vec4(0, 1, 0, 1);
	EmitVertex();
	gl_Position = canvas_transform * vec4(b.x, b.y, 0, 1);
	color_fs = vec4(0, 1, 0, 1);
	EmitVertex();
	EndPrimitive();

	
	gl_Position = canvas_transform * vec4(c, 0, 1);
	color_fs = vec4(1, 0, 0, 1);
	EmitVertex();
	gl_Position = canvas_transform * vec4(c+n, 0, 1);
	color_fs = vec4(1, 0, 0, 1);
	EmitVertex();
	EndPrimitive();
}
