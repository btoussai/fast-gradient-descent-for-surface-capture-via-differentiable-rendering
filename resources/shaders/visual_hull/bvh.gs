/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

layout(points) in;
layout(line_strip, max_vertices=5) out;

in BVHNode{
	vec2 minCorner;
	vec2 maxCorner;
	int childL;
	int childR;
	int level;
	int handle;
} nodes[];

uniform int selectedLevel;

void main(void){
	
	vec2 minCorner = nodes[0].minCorner;
	vec2 maxCorner = nodes[0].maxCorner;
	
	if(selectedLevel >= 0 && selectedLevel != nodes[0].level){
		return;
	}
	
	gl_Position = vec4(minCorner.x, minCorner.y, 0, 1);
	EmitVertex();
	
	gl_Position = vec4(minCorner.x, maxCorner.y, 0, 1);
	EmitVertex();
	
	gl_Position = vec4(maxCorner.x, maxCorner.y, 0, 1);
	EmitVertex();
	
	gl_Position = vec4(maxCorner.x, minCorner.y, 0, 1);
	EmitVertex();
	
	gl_Position = vec4(minCorner.x, minCorner.y, 0, 1);
	EmitVertex();
	
	EndPrimitive();


}
