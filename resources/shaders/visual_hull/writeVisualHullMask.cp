/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_KHR_shader_subgroup_vote: 		enable
#extension GL_KHR_shader_subgroup_ballot : 		enable
#extension GL_KHR_shader_subgroup_arithmetic :  enable
#extension GL_NV_gpu_shader5 : 					enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

layout(local_size_x = DEPTHMAP_TILE_WIDTH, local_size_y = DEPTHMAP_TILE_WIDTH, local_size_z = 1) in;

struct DepthTileDescriptor{
	ivec2 tileCoords;//coordinates of the tile in the image
	int foregroundPixelCount;//number of actual foreground pixels in the tile
	int camIndex;//index of the camera
};

layout(std430, binding = 0) restrict writeonly buffer CompressedDepthTileDescriptorBlock{
	DepthTileDescriptor remainingDescriptors[];//Once we have removed the tiles that don't contain any visual hull pixels
};
layout(std430, binding = 1) restrict writeonly buffer TilesBlock{
	u8vec4 tiles2D[];
};

layout(binding=0, rgba8) uniform restrict image2DArray Images;
layout(binding=1, r32i) uniform restrict writeonly iimage2DArray occupancyImages;//1 int per tile
layout(binding=2, rg32ui) uniform restrict readonly uimage2DArray CompressedOccupancy;//1 bit per pixel
layout(binding=0) uniform atomic_uint GlobalTileCounter;


shared int partial_sums[32];
int groupAdd(int v){

	v = subgroupAdd(v);
	if(subgroupElect()){
		partial_sums[gl_SubgroupID] = v;
	}

	memoryBarrierShared();
	barrier();

	if(gl_SubgroupID == 0){
		v = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : int(0);
		int w = subgroupAdd(v);
		partial_sums[gl_SubgroupInvocationID] = w;
	}

	memoryBarrierShared();
	barrier();

	return partial_sums[gl_SubgroupID];
}

shared int offset;
void main(void){
	
	ivec2 uv = ivec2(gl_GlobalInvocationID);
	int camIndex = int(gl_WorkGroupID.z);
	
	bool currentlyInsideVisualHull = false;
	float alpha = 0.0f;
	float weights = 0.0f;
	const int H = 4;
	for(int j=-H; j<=+H; j++){
		for(int i=-H; i<=+H; i++){
			const ivec2 samplingCoords = uv + ivec2(i, j);
			const ivec2 globalSamplingCoords = samplingCoords / DEPTHMAP_TILE_WIDTH;
			const ivec2 localSamplingCoords = samplingCoords % DEPTHMAP_TILE_WIDTH;
					
			uvec2 data = imageLoad(CompressedOccupancy, ivec3(globalSamplingCoords, camIndex)).rg;
			int k = (localSamplingCoords.y % 4) * DEPTHMAP_TILE_WIDTH + localSamplingCoords.x;
	
			bool insideVisualHull = ((localSamplingCoords.y >= 4 ? data.y : data.x) & (1<<k)) != 0;
			
			if(i==0 && j==0){
				currentlyInsideVisualHull = insideVisualHull;
			}
			
			float weight = exp(-(i*i+j*j) / (H*H));
			if(insideVisualHull){
				alpha += weight;
			}
			weights += weight;
		}
	}
	alpha = clamp(2.0f * alpha / weights, 0.0f, 1.0f);
	
	if(currentlyInsideVisualHull){
		alpha = 1.0f;
	}
	
	vec4 color = imageLoad(Images, ivec3(uv, camIndex));
	color.a = alpha;
	imageStore(Images, ivec3(uv, camIndex), color);
	
	int validPixels = groupAdd(int(alpha > 0.0f));
	
	if(validPixels > 0){
		if(gl_LocalInvocationIndex == 0){
			offset = int(atomicCounterAdd(GlobalTileCounter, 1u));
			remainingDescriptors[offset] = DepthTileDescriptor(ivec2(gl_WorkGroupID.xy), validPixels, camIndex);
			imageStore(occupancyImages, ivec3(gl_WorkGroupID), ivec4(offset, 0, 0, 0));
		}

		memoryBarrierShared();
		barrier();
		tiles2D[offset * DEPTHMAP_TILE_SIZE + gl_LocalInvocationIndex] = u8vec4(color * 255.0f);
	}
	
}

