/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include : require
#extension GL_KHR_shader_subgroup_vote: enable
#extension GL_KHR_shader_subgroup_ballot : enable
#extension GL_KHR_shader_subgroup_arithmetic : enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

layout(local_size_x = DEPTHMAP_TILE_WIDTH, local_size_y = DEPTHMAP_TILE_WIDTH, local_size_z = 1) in;

#include </common/projection.txt>
#include </common/rayVSBox.txt>

struct Point{
	float depth;
};

struct BVHNode{
	vec4 minMaxCorners;
	int childL;
	int childR;
	int level;
	int handle;
};

struct UniformData{
	int viewIndex;
	float centerProjDist;
	int NodeOffset;
	int SegmentOffset;
};

struct DepthTileDescriptor{
	ivec2 tileCoords;//coordinates of the tile in the image
	int foregroundPixelCount;//number of actual foreground pixels in the tile
	int camIndex;//index of the camera
};

layout(std430, binding = 0) restrict writeonly buffer PointsBlock{
	Point points[];
};
layout(std430, binding = 1) restrict readonly buffer BVHBlock{
	BVHNode nodes[];
};
layout(std430, binding = 2) restrict readonly buffer SegmentBlock{
	vec4 segments[];
};
layout(std430, binding = 3) restrict readonly buffer DepthTileDescriptorBlock{
	DepthTileDescriptor depthTileDescriptors[]; //The result of the background segmentation
};

layout(std140, binding = 1) uniform UniformsBlock
{
	UniformData uniformsData[100];
};

layout(binding=0, rgba8) uniform restrict readonly image2DArray Images;
layout(binding=1, rg32ui) uniform restrict writeonly uimage2DArray CompressedOccupancy;//1 bit per pixel

uniform vec3 TSDFminCorner;
uniform vec3 TSDFmaxCorner;
uniform int ViewCount;
uniform int MaxIterations;
uniform int PixelOffset;

vec2 lineIntersect(vec3 A, vec3 B, vec3 C, vec3 D){
	float B2 = dot(B, B);
	float D2 = dot(D, D);
	float BD = dot(B, D);
	vec3 CA = A-C;
	return inverse(mat2(B2, -BD, -BD, D2)) * vec2(-dot(B, CA), dot(D, CA));
}

bool isInsideSilhouette(vec2 pixelUV, int camIndex) {
	ivec2 UV = ivec2(pixelUV);
	float alpha = imageLoad(Images, ivec3(UV, camIndex)).a;
	return alpha > 0.0f;
}

float advanceOutOfImageRange(vec2 startNDC, vec2 rayNDC, vec2 image_size, int camIndex, float prev_t, bool behind) {
	// When behind, we actually bring the end point closer to the start point
	
	if(behind){
		vec2 startUV = toPixels(startNDC, camIndex);
		bool startInside = isInsideImageRange(startUV, image_size);
		if(startInside){
			return 0.0f;//The closest we can be, gives a depth of +inf
		}
	}
	
	float tInf = prev_t;//Always inside
	float tSup = prev_t + (!behind ? 4.0f : -4.0f);//Always outside
	
	for(int k=0; k<10; k++){ //Dichotomy
		float t = (tInf+tSup)*0.5f;
		vec2 endNDC = startNDC + rayNDC * t;
		vec2 endUV = toPixels(endNDC, camIndex);
		bool insideImage = isInsideImageRange(endUV, image_size);
		if(insideImage){
			tInf = t;
		}else{
			tSup = t;
		}
	}
	return tSup; 
}

void rayVSsegment(vec2 base, vec2 ray, vec4 segment, inout float t, float prev_t, bool behind) {
	dvec2 A = base;
	vec2 C = vec2(segment.x, segment.y);
	vec2 D = vec2(segment.z, segment.w);
	
	const float eps = 0.1f;	
	
	vec2 E = D-C;
	vec2 N = vec2(E.y, -E.x);
	float E2 = dot(E, E);
	float NdotRay = dot(N, ray);

	dvec2 AC = C-A;
	double NdotAC = dot(N, AC);

	if(abs(NdotRay) > 1.0E-6f){
		double ta = NdotAC / NdotRay;
		double s = dot(ray*ta - AC, E);
		if(s > -eps*E2 && s < (1.0f+eps)*E2){
			if(!behind){
				if(ta < t && ta > prev_t){
					t = float(ta);
				}
			}else{
				if(ta > t && ta < prev_t){
					t = float(ta);
				}
			}
		}
	}

}

float convert2DProjToDepth(vec2 base, vec2 ray, float t, int borderID, vec3 wsCamCenter, vec3 wsDir) {
	if(t == 0.0f){
		return +1.0f/0.0f;
	}
	vec2 proj = base + ray * t;
	vec3 lineDir = unproject(proj, borderID);	
	lineDir /= dot(cameraData[borderID].cameraTransformInv[2], vec4(lineDir, 0.0));
	
	vec3 A = vec3(cameraData[borderID].camCenter);// lineBase
	vec3 B = lineDir;
	vec3 C = wsCamCenter;
	vec3 D = wsDir;
	
	return lineIntersect(A, B, C, D).y;
}

const int NULL_PTR = -1;
const int STACK_SIZE = 32;
shared int stackPtr[gl_NumSubgroups];
shared int stack[gl_NumSubgroups * STACK_SIZE];



int moveAlongRay(vec3 wsCamCenter, vec3 wsDir, int camIndex, inout float Depth){
	float prevDepth = Depth;
	
	int iterations = 0;
	int out_of_range_projections = 0;//When the point projects outside an image
	int inside_silhouette_projections = 0; // # of times the point is inside the visual hull
	int outside_silhouette_projections = 0; // # of times the point is outside the visual hull
	int border_projections = 0; // # of times the ray hits a segment
	int outside_silhoutte_id = -1;
	for(int k=0; k<ViewCount && !isinf(Depth); k++){
		const UniformData data = uniformsData[k];
		const int borderID = data.viewIndex;
		const int NodeOffset = data.NodeOffset;
		const int SegmentOffset = data.SegmentOffset;
	
		vec3 pixel_proj = project(wsCamCenter+wsDir*Depth, borderID);
		vec2 NDCcoords = vec2(pixel_proj.x, pixel_proj.y) / pixel_proj.z;
		
		vec2 image_size = getImageSize(borderID);
		vec2 PixelCoords = toPixels(NDCcoords, borderID);
		
		vec3 camCenter_proj = project(wsCamCenter, borderID);
		bool behind = camCenter_proj.z <= 0.0f;
		vec2 base = vec2(camCenter_proj.x, camCenter_proj.y) / camCenter_proj.z;
		vec2 centerToPixel = vec2(NDCcoords) - base;

		float prev_t = length(centerToPixel); //The value of t corresponding to the previous depth estimation
		float t = !behind ? +1.0f/0.0f : -1.0f/0.0f; //the smallest value of t greater than prev_t or the largest value of t less than prev_t
		float init_t = float(t);
		
		vec2 ray = vec2(centerToPixel / prev_t);
		vec2 ray_inv = vec2(1.0f / ray);


		if(!isInsideImageRange(PixelCoords, image_size)){
			//The pixel projects outside of the image range, could be an outlier.
			out_of_range_projections++;
			continue;
		}
		if(isInsideSilhouette(PixelCoords, borderID)){
			inside_silhouette_projections++;
			continue; // The pixel is inside the projection of the visual hull
		}else{
			outside_silhouette_projections++;
		}
		
	    
		int nodeIndex = NodeOffset;// Root node of the bvh
		
		float dummy = 0.0f;
		if(!rayVSbox2D(vec2(base), ray_inv, nodes[nodeIndex].minMaxCorners, float(t), float(prev_t), dummy, behind)){
			//The ray does not intersect the root box, could be an outlier. We advance it as far as possible.
			t = advanceOutOfImageRange(vec2(base), vec2(ray), image_size, borderID, float(prev_t), behind);
			Depth = convert2DProjToDepth(base, ray, t, borderID, wsCamCenter, wsDir);
			continue;
		}
	
		if(subgroupElect()){
		    stackPtr[gl_SubgroupID] = 1;
		    stack[gl_SubgroupID * STACK_SIZE + 0] = NULL_PTR; // push
		}
	
		//From https://developer.nvidia.com/blog/thinking-parallel-part-ii-tree-traversal-gpu/
		do{
			float boxOverlapL, boxOverlapR;
			iterations++;
			int childL = nodes[nodeIndex].childL + NodeOffset;
			int childR = nodes[nodeIndex].childR + NodeOffset;
			
			bool overlapL = subgroupAny(rayVSbox2D(base, ray_inv, nodes[childL].minMaxCorners, t, prev_t, boxOverlapL, behind));
			bool leafL = nodes[childL].handle >= 0;
			bool overlapR = subgroupAny(rayVSbox2D(base, ray_inv, nodes[childR].minMaxCorners, t, prev_t, boxOverlapR, behind));
			bool leafR = nodes[childR].handle >= 0;

			if(overlapL && leafL){
				rayVSsegment(base, ray, segments[nodes[childL].handle + SegmentOffset], t, prev_t, behind);
			}
	
			if(overlapR && leafR){
				rayVSsegment(base, ray, segments[nodes[childR].handle + SegmentOffset], t, prev_t, behind);
			}
	        
	        if(subgroupElect()){
		        bool traverseL = (overlapL && !leafL);
		        bool traverseR = (overlapR && !leafR);
		        if(!traverseL && !traverseR) {
		            nodeIndex = stack[gl_SubgroupID * STACK_SIZE + --stackPtr[gl_SubgroupID]]; // pop
		        }else {
		        	if(traverseL && traverseR){
		        		bool swap = boxOverlapR > boxOverlapL;
		        		stack[gl_SubgroupID * STACK_SIZE + stackPtr[gl_SubgroupID]++] = swap ? childR : childL; // push
		        		nodeIndex = swap ? childL : childR;
		        	}else{
			        	nodeIndex = traverseL ? childL : childR;
		        	}
		        }
	        }
	        
			nodeIndex = subgroupBroadcastFirst(nodeIndex);
		} while(nodeIndex != NULL_PTR && stackPtr[gl_SubgroupID] < STACK_SIZE);
		
		if(stackPtr[gl_SubgroupID] >= STACK_SIZE){
			//c = vec4(0, 1, 1, 1);
			return -1;//The stack was too small, should not happen
		}
		if(t == init_t){
			// The ray did not intersect any segments in that border view, could be an outlier. We advance it as far as possible.
			t = advanceOutOfImageRange(vec2(base), vec2(ray), image_size, borderID, float(prev_t), behind);
			Depth = convert2DProjToDepth(base, ray, t, borderID, wsCamCenter, wsDir);
			continue;
		}
		
		Depth = convert2DProjToDepth(base, ray, t, borderID, wsCamCenter, wsDir);
		border_projections++;
	}
	
	if(Depth == prevDepth){//The ray did not progress
		if((outside_silhouette_projections > 0 || inside_silhouette_projections == 0) 
			&& out_of_range_projections != ViewCount) //Hack for the DTU dataset: points not seen by othre cameras are still kept
		{
			Depth = +1.0f/0.0f;
			return -1; //It's an outlier for sure
		}else{
			return +1; //We have a winner
		}
	}

	vec3 p1 = wsCamCenter+wsDir*(Depth - 0.001f);
	vec3 p2 = wsCamCenter+wsDir*(Depth + 0.001f);
	if(any(lessThan(p1, TSDFminCorner)) || any(greaterThan(p2, TSDFmaxCorner))){
		Depth = +1.0f/0.0f;
		return -1;//Outside the reconstruction volume
	}
	
	return 0;//Another iteration is needed
	
}

shared uint ballotResult;

void main(void){
	
	const int index = int(gl_WorkGroupID.x * DEPTHMAP_TILE_SIZE + gl_LocalInvocationIndex);
	
	const int tile_id = (PixelOffset + index) / DEPTHMAP_TILE_SIZE;
	const int pixel_id = int(gl_LocalInvocationIndex);
	
	const DepthTileDescriptor tileDescriptor = depthTileDescriptors[tile_id];
	const int camIndex = tileDescriptor.camIndex;
	
	ivec2 uv = tileDescriptor.tileCoords * DEPTHMAP_TILE_WIDTH + ivec2(pixel_id%DEPTHMAP_TILE_WIDTH, pixel_id/DEPTHMAP_TILE_WIDTH);
	vec2 undistorted_uv = toNDC(vec2(uv) + vec2(0.5f), camIndex);
	
	vec3 wsDir = unproject(undistorted_uv, camIndex);
	wsDir /= dot(cameraData[camIndex].cameraTransformInv[2], vec4(wsDir, 0.0));
	vec3 wsCamCenter = vec3(cameraData[camIndex].camCenter);
	
	float depth = -1.0f/0.0f;
	
	float tmin, tmax;
	bool intersectBox = rayVSbox(wsCamCenter, 1.0f/wsDir, TSDFminCorner, TSDFmaxCorner, tmin, tmax);
	
	vec4 color = imageLoad(Images, ivec3(uv, camIndex));
	
	if(intersectBox && tmin > 0.0f && color.a > 0.0f){
		depth = tmin;
		
		for(int it = 0; it < MaxIterations; it++){
			int res = moveAlongRay(wsCamCenter, wsDir, camIndex, depth);
			if(res != 0){
				break;
			}
		}
	}
	
	if(isnan(depth) || isinf(depth) || depth < tmin){
		depth = 0.0f;
	}
	
	points[PixelOffset + index].depth = depth;
	
	bool isvalid = depth != 0.0f;
	
	uvec4 res = subgroupBallot(isvalid);
	
	if(gl_SubgroupID == 1 && subgroupElect()){
		ballotResult = res.x;
	}
	
	memoryBarrierShared();
	barrier();
	
	if(gl_LocalInvocationIndex == 0){
		res.y = ballotResult;
		imageStore(CompressedOccupancy, ivec3(tileDescriptor.tileCoords, camIndex), res);
	}
	
	
}
