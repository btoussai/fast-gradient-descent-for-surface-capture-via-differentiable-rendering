/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require

in float depth;

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

out vec4 fs_color;

uniform mat4 projectionView;
uniform mat4 transform;
uniform int PROJECTION_TYPE;
uniform mat4 canvas_transform;
uniform int selectedCamIndex;
uniform int RemoveDistortion;
uniform int PixelOffset;
uniform float frustumsLength;

const int PROJECT_ON_2D_CANVAS = 0;
const int PROJECT_ON_3D_CANVAS = 1;

#include </common/projection.txt>

struct DepthTileDescriptor{
	ivec2 tileCoords;//coordinates of the tile in the image
	int foregroundPixelCount;//number of actual foreground pixels in the tile
	int camIndex;//index of the camera
};

layout(std430, binding = 0) restrict readonly buffer OccupancyDescriptorBlock{
	DepthTileDescriptor depthTileDescriptors[];
};

layout(binding=0) uniform sampler2DArray srcImages;

void main(void){
	const int id = gl_VertexID;
	const int tile_id = (PixelOffset + id) / DEPTHMAP_TILE_SIZE;
	const int pixel_id = id % DEPTHMAP_TILE_SIZE;
	
	const DepthTileDescriptor tileDescriptor = depthTileDescriptors[tile_id];
	const int camIndex = tileDescriptor.camIndex;
	
	ivec2 uv_int = tileDescriptor.tileCoords * DEPTHMAP_TILE_WIDTH + ivec2(pixel_id%DEPTHMAP_TILE_WIDTH, pixel_id/DEPTHMAP_TILE_WIDTH);
	vec2 uv = vec2(uv_int) + vec2(0.5f);
	vec2 undistorted_uv = toNDC(uv, camIndex);
	
	float d = depth==0.0f ? frustumsLength : depth;

	vec4 ws_pos = vec4(unproject(undistorted_uv, d, camIndex), 1.0);

	if(PROJECTION_TYPE == PROJECT_ON_2D_CANVAS) {
		float dummy;
		vec2 uv = project_distort(ws_pos.xyz, selectedCamIndex, dummy, RemoveDistortion==0).xy;
		uv = vec2(2.0*uv.x-1.0, 1.0-2.0*uv.y);
		gl_Position = canvas_transform * vec4(uv, 0, 1);
	} else {//PROJECTION_TYPE == PROJECT_ON_3D_CANVAS
		gl_Position = projectionView * transform * ws_pos;
	}
	
	if(depth == 0.0f){
		fs_color = vec4(1, 0, 0, 1);
	}else{
		fs_color = texelFetch(srcImages, ivec3(uv_int, camIndex), 0);
	}
	
}

