/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require

in vec4 segment_vs;

out Segment{
	vec2 a;
	vec2 b;
} segment_gs;

uniform mat4 canvas_transform;
uniform int RemoveDistortion;
uniform int selectedCamIndex;
uniform int viewpoint;


#include </common/projection.txt>

void main(void){
	vec3 ws_pos1 = unproject(segment_vs.xy, 1000.0f, viewpoint);
	vec3 ws_pos2 = unproject(segment_vs.zw, 1000.0f, viewpoint);

	float dummy;
	vec2 uv1 = project_distort(ws_pos1, selectedCamIndex, dummy, RemoveDistortion==0).xy;
	uv1 = vec2(2.0*uv1.x-1.0, 1.0-2.0*uv1.y);
	
	vec2 uv2 = project_distort(ws_pos2, selectedCamIndex, dummy, RemoveDistortion==0).xy;
	uv2 = vec2(2.0*uv2.x-1.0, 1.0-2.0*uv2.y);
	
	segment_gs.a = uv1;
	segment_gs.b = uv2;
	
	gl_Position = vec4(0, 0, 0, 1);
	
}
