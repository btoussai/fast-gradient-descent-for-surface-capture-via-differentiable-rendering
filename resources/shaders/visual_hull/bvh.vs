/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require

in vec4 corners;
in ivec4 data1;


out BVHNode{
	vec2 minCorner;
	vec2 maxCorner;
	int childL;
	int childR;
	int level;
	int handle;
} node;

uniform mat4 canvas_transform;
uniform int RemoveDistortion;
uniform int selectedCamIndex;
uniform int viewpoint;

#include </common/projection.txt>

void main(void){
	vec3 ws_pos1 = unproject(corners.xy, 1000.0f, viewpoint);
	vec3 ws_pos2 = unproject(corners.zw, 1000.0f, viewpoint);
	
	float dummy;
	vec2 uv = project_distort(ws_pos1, selectedCamIndex, dummy, RemoveDistortion==0);
	node.minCorner = (canvas_transform * vec4(2.0*uv.x-1.0, 1.0-2.0*uv.y, 0, 1)).xy;
	
	uv = project_distort(ws_pos2, selectedCamIndex, dummy, RemoveDistortion==0);
	node.maxCorner = (canvas_transform * vec4(2.0*uv.x-1.0, 1.0-2.0*uv.y, 0, 1)).xy;
	
	node.childL = data1.x;
	node.childR = data1.y;
	node.level = data1.z;
	node.handle = data1.w;

	gl_Position = vec4(0, 0, 0, 1);
	
}
