/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require
#extension GL_NV_gpu_shader5 : enable

in vec2 tex_coords;

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;


layout(binding = 0) uniform sampler2DArray originalImage;
layout(binding = 1) uniform sampler2DArray backgroundImage;
layout(binding = 2) uniform isampler2DArray occupancyImage;
layout(binding = 3) uniform sampler1D ExposureLUT;


uniform int viewpoint;
uniform int displayMode;
uniform vec2 CursorNDCPos;
uniform float BrushRadius;
uniform int RemoveDistortion;
uniform int ApplyExposureCorrection;
uniform int doNotUseOriginalImage;

uniform int DepthTiles;//How many depth tiles in that image
uniform int DepthTilesOffset;//How many depth tiles in the previous images

out vec4 out_Color;

layout(std430, binding = 0) restrict readonly buffer GlobalIntegrationTextureBlock{
	f16vec4 global_integration_textures[];
};
layout(std430, binding = 1) restrict readonly buffer TilesBlock{
	u8vec4 tiles2D[];
};

#include </common/projection.txt>

vec3 hsv2rgb(vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0f - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0f, 1.0f), c.y);
}

vec4 correctExposure(vec4 c, vec2 ExposureCorrection){
	c.rgb = c.rgb * ExposureCorrection.x + ExposureCorrection.y;
	return c;
}

void main(void){

	const vec2 ExposureCorrection = ApplyExposureCorrection>0 ? texelFetch(ExposureLUT, viewpoint, 0).xy : vec2(1.0f, 0.0f);

	ivec2 localTileCoords;
	ivec2 tileCoords;
	vec2 distorted_tex_coords = distort_tex_coords(tex_coords, 
										viewpoint, 
										tileCoords, 
										localTileCoords, 
										RemoveDistortion!=0
									);
	
	tileCoords /= DEPTHMAP_TILE_WIDTH;
	localTileCoords %= DEPTHMAP_TILE_WIDTH;
	
	const vec3 sampleCoords = vec3(distorted_tex_coords, viewpoint);
	
	if(displayMode == 0){//Base image
	
		if(doNotUseOriginalImage > 0){
			const int depthTileIndex = texelFetch(occupancyImage, ivec3(tileCoords, viewpoint), 0).r;
			if(depthTileIndex != -1){
				int N = depthTileIndex * DEPTHMAP_TILE_SIZE + localTileCoords.y * DEPTHMAP_TILE_WIDTH + localTileCoords.x;
				out_Color = correctExposure(tiles2D[N] / 255.0f, ExposureCorrection);
				
				if(N > tiles2D.length()){
					out_Color = vec4(1, 0, 1, 1);
				}
				
			}else{
				discard;
			}
		}else{
			out_Color = correctExposure(texture(originalImage, sampleCoords), ExposureCorrection);
		}
		
	}else if(displayMode == 1){//Background
		out_Color = correctExposure(texture(backgroundImage, sampleCoords), ExposureCorrection);
		
	}else if(displayMode == 2){//Segmented image
		const int depthTileIndex = texelFetch(occupancyImage, ivec3(tileCoords, viewpoint), 0).r;
		if(depthTileIndex != -1){
			int N = depthTileIndex * DEPTHMAP_TILE_SIZE + localTileCoords.y * DEPTHMAP_TILE_WIDTH + localTileCoords.x;
			out_Color = correctExposure(tiles2D[N] / 255.0f, ExposureCorrection);
		}else{
			discard;
		}
		
	}else if(displayMode == 3){//Don't care mask
		vec4 c0 = texture(originalImage, vec3(tex_coords, viewpoint));
		vec4 c1 = texture(backgroundImage, vec3(tex_coords, viewpoint));
		
		
		float d = distance(vec2(tex_coords.x*2.0-1.0, 1.0-tex_coords.y*2.0), CursorNDCPos);
		
		if(abs(d - BrushRadius) < 0.01*BrushRadius){
			out_Color = vec4(1) - c0;
		}else if(c1.a == 0){
			out_Color = mix(c0, vec4(1, 0, 0, 1), 0.25);
		}else{
			out_Color = c0;
		}
		
		
	}else if(displayMode == 4 || displayMode == 5){//GLOBAL_INTEGRATION_IMAGE or REMAINING_TRANSPARENCY

		const int depthTileIndex = texelFetch(occupancyImage, ivec3(tileCoords, viewpoint), 0).r;
		if(depthTileIndex != -1){
			int N = depthTileIndex * DEPTHMAP_TILE_SIZE + localTileCoords.y * DEPTHMAP_TILE_WIDTH + localTileCoords.x;
			vec4 transmittance_rgb = global_integration_textures[N];
			
			vec3 rgb = transmittance_rgb.yzw;
			
			if(displayMode == 5){
				out_Color = vec4(vec3(transmittance_rgb.x), 1.0);			
			}else{
				out_Color = vec4(transmittance_rgb.yzw, 1.0);
			}
			
			if(N >= global_integration_textures.length()){
				out_Color = vec4(1, 0, 0, 1);
			}
			
		}else{
			discard;
		}
	}else{
		out_Color = vec4(1, 1, 1, 1);
	}

	out_Color.a = 1.0;
}
