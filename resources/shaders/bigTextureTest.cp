/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_NV_gpu_shader5 : 					enable
#extension GL_KHR_shader_subgroup_ballot:		enable

const int DEPTHMAP_TILE_WIDTH = 8;
layout(local_size_x = 8, local_size_y = 8, local_size_z = 1) in;

layout(binding = 0, rgba8ui) uniform restrict writeonly uimage3D RayTexels;


const int group_loops = 32;


ivec3 getRayTexelCoords(uint slice, ivec2 localCoords){
	uint x = bitfieldExtract(slice, 0, 8);
	uint y = bitfieldExtract(slice, 8, 8);
	uint z = bitfieldExtract(slice, 16, 16);
	return ivec3(x * DEPTHMAP_TILE_WIDTH + localCoords.x, y * DEPTHMAP_TILE_WIDTH + localCoords.y, z);
}

void main(void){

	int group = int(gl_WorkGroupID.x);

	for(int i=0; i<group_loops; i++){
		int slice = group * group_loops + i;
		
		imageStore(
				RayTexels, 
				getRayTexelCoords(slice, ivec2(gl_LocalInvocationID.xy)), 
				uvec4(ivec2(gl_LocalInvocationID.xy) * 32, i * 8, 1)
			);
		
	}

	
}
