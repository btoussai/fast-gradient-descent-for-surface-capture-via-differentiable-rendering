/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

out vec4 out_Color;

flat in int camID;
in float visibility;

uniform int selectedCamera;
uniform int SelectedDepthMap;
uniform ivec4 activeCameras;

vec3 hsv2rgb(vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0f - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0f, 1.0f), c.y);
}

bool isCameraActive(int camIndex){
	return (activeCameras[camIndex/32] & (1<<(camIndex%32))) != 0;
}

void main(void){

	if(visibility < 0.95f){
		discard;
	}
	
	float intensity = 1.0f;
	if(!isCameraActive(camID)){
		intensity = 0.5f;
	}
	
	if(camID == selectedCamera){
		out_Color = vec4(0, intensity, 0, 1);
	}else if(camID == SelectedDepthMap){
		out_Color = vec4(0, 0, 1, 1);
	}else{
		out_Color = vec4(intensity, 0, 0, 1);
	}
	
}
