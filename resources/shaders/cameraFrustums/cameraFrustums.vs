/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require

in vec3 position;
in int cameraIndex;

flat out int camID;
out float visibility;

uniform mat4 projectionView;
uniform mat4 transform;
uniform int PROJECTION_TYPE;
uniform mat4 canvas_transform;
uniform int selectedCamera;
uniform int SelectedDepthMap;
uniform int RemoveDistortion;

const int PROJECT_ON_2D_CANVAS = 0;
const int PROJECT_ON_3D_CANVAS = 1;

#include </common/projection.txt>

void main(void) {
	vec4 p = vec4(position, 1.0);
	visibility = 1.0f;
	
	if(PROJECTION_TYPE == PROJECT_ON_2D_CANVAS) {
		float depth;
		p = transform * p;
		
		vec2 uv = project_distort(p.xyz, selectedCamera, depth, RemoveDistortion==0);
		uv = vec2(2.0*uv.x-1.0, 1.0-2.0*uv.y);
		
		if(depth < 0.0f){
			visibility = 0.0f;
		}
		gl_Position = canvas_transform * vec4(uv, 0, 1);
		
	}else{//PROJECTION_TYPE == PROJECT_ON_3D_CANVAS
		gl_Position = projectionView * transform * p;
	}
	
	camID = cameraIndex;
}


