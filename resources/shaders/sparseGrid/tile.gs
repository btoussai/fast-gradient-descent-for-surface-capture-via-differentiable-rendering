/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require

layout(points) in;
layout(line_strip, max_vertices=24) out;

out vec4 color_fs;

in ivec4 tile_desc_gs[];
in ivec4 activeCameras_gs[];

uniform mat4 projectionView;
uniform mat4 transform;
uniform vec3 TSDFminCorner;
uniform vec3 tile_size;

uniform int PROJECTION_TYPE;
uniform mat4 canvas_transform;
uniform int selectedCamIndex;
uniform int RemoveDistortion;

uniform int USE_FRUSTUM_MODE;
uniform int frustumCamIndex;
uniform vec3 frustumMinCorner;
uniform vec3 frustumMaxCorner;

uniform vec4 color;
uniform int NumTiles;

const int PROJECT_ON_2D_CANVAS = 0;
const int PROJECT_ON_3D_CANVAS = 1;

#include </common/projection.txt>

layout(std430, binding = 0) restrict readonly buffer TileFlagsBlock{
	int tile_flags[];
};



vec4 project2D3D(vec4 ws_pos){
	
	if(USE_FRUSTUM_MODE != 0){
		ws_pos.xy = toNDC(ws_pos.xy, frustumCamIndex);
		ws_pos.xyz = unproject(ws_pos.xy, ws_pos.z, frustumCamIndex);
	}


	if(PROJECTION_TYPE == PROJECT_ON_2D_CANVAS) {
		float dummy;
		vec2 uv = project_distort(ws_pos.xyz, selectedCamIndex, dummy, RemoveDistortion==0);
		uv = vec2(2.0*uv.x-1.0, 1.0-2.0*uv.y);
		return canvas_transform * vec4(uv, 0, 1);
	} else {//PROJECTION_TYPE == PROJECT_ON_3D_CANVAS
		return projectionView * transform * ws_pos;
	}
}


void buildEdges(vec4 corners[8], int indices[24]){
	for(int i=0; i<12; i++){
		gl_Position = corners[indices[2*i+0]];
		EmitVertex();
		gl_Position = corners[indices[2*i+1]];
		EmitVertex();
		EndPrimitive();
	}
}

vec3 hsv2rgb(vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0f - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0f, 1.0f), c.y);
}

bool isCameraActive(ivec4 activeCameras, int camIndex){
	return (activeCameras[camIndex/32] & (1<<(camIndex%32))) != 0;
}


void main(void){
	const float PI_OVER_2 = 1.5707963267948966;
	
	ivec4 desc = tile_desc_gs[0];
	
	
	vec3 minCorner = TSDFminCorner + vec3(desc.xyz) * tile_size;
	vec3 maxCorner = minCorner + tile_size;
	
	if(NumTiles > 0){
	
		color_fs = vec4(hsv2rgb(vec3(float(desc.w) / NumTiles, 1.0, 1.0)), 1.0);

		bool visibleBySelectedCamera = isCameraActive(activeCameras_gs[0], selectedCamIndex);
		
		if(activeCameras_gs[0] == ivec4(0)){
			color_fs.rgb = vec3(0);
		} else if(!visibleBySelectedCamera){
			//color_fs.rgb *= 0.5f;
		}else{
			//return;
		}
		
		/*
		int flag = tile_flags[desc.w];
		
		if((flag & 1) == 0){//the tile will remain
			color_fs.rgb = vec3(0, 1, 0);
		}else if((flag & (1 << 7)) > 0){//the tile has a neighbor that will remain
			color_fs.rgb = vec3(0, 0, 1);
		}else{//the tile won't remain
			color_fs.rgb = vec3(1, 0, 0);
		}
		color_fs.rgb = vec3(0, 1, 0);
		*/
	}else{
		//We are drawing a wireframe box, not a tile
		color_fs = color;
	}
	
	if(USE_FRUSTUM_MODE != 0){
		minCorner = frustumMinCorner;
		maxCorner = frustumMaxCorner;
	}
	
	vec4 corners[8] = vec4[8](
		project2D3D(vec4(minCorner.x, minCorner.y, minCorner.z, 1.0f)),
		project2D3D(vec4(maxCorner.x, minCorner.y, minCorner.z, 1.0f)),
		project2D3D(vec4(minCorner.x, maxCorner.y, minCorner.z, 1.0f)),
		project2D3D(vec4(maxCorner.x, maxCorner.y, minCorner.z, 1.0f)),
		project2D3D(vec4(minCorner.x, minCorner.y, maxCorner.z, 1.0f)),
		project2D3D(vec4(maxCorner.x, minCorner.y, maxCorner.z, 1.0f)),
		project2D3D(vec4(minCorner.x, maxCorner.y, maxCorner.z, 1.0f)),
		project2D3D(vec4(maxCorner.x, maxCorner.y, maxCorner.z, 1.0f))
	);
	
	int indices[24] = int[24](
		0,1,	2,3,	4,5,	6,7,
		0,2,	1,3,	4,6,	5,7,
		0,4,	1,5,	3,7,	2,6
	);
	
	buildEdges(corners, indices);
}
