/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_NV_gpu_shader5 : 					enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;
const int VOXELS_IN_GROUP = (VOXELS_PER_SIDE+1) * (VOXELS_PER_SIDE+1) * (VOXELS_PER_SIDE+1);

layout(local_size_x = VOXELS_PER_SIDE, local_size_y = VOXELS_PER_SIDE, local_size_z = VOXELS_PER_SIDE) in;
const int GROUP_SIZE = VOXELS_PER_TILE;

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

struct Vertex{
	vec4 p;
	vec4 n;
};

layout(std430, binding = 0) restrict buffer VerticesBlock{
	Vertex vertices[];
};
layout(std430, binding = 1) restrict readonly buffer TilesDescriptorBlock{
	TileDesc tile_descriptors[];
};

layout(std430, binding = 2) restrict readonly buffer LUT
{
	//At most 5 faces having 3 vertices each, 256 possibilities + the number of triangles generated for each case
	int facesLUT[256*5*3+256];
};

layout(binding = 0) uniform atomic_uint GlobalVertexCounter;
layout(binding = 0, rgba8) uniform restrict readonly image3D VoxelsTexture;
layout(binding = 0) uniform isampler3D TileOccupancy;

uniform vec3 TSDFminCorner;
uniform float tile_size;
uniform ivec3 TilesTotal;
uniform float SDF_OFFSET;
uniform mat4 transform;

int toWorkVolumeIndex(ivec3 localCoords){
	return localCoords.x + localCoords.y * (VOXELS_PER_SIDE+1) + localCoords.z * (VOXELS_PER_SIDE+1) * (VOXELS_PER_SIDE+1);
}

float progression(vec4 tsdfValuesA, vec4 tsdfValuesB, out vec3 color){// returns value in [-1, 1]
	float d = (tsdfValuesA.x+tsdfValuesB.x)/(tsdfValuesA.x-tsdfValuesB.x);
	color = 0.5f*( (tsdfValuesA.yzw+tsdfValuesB.yzw) + d * (tsdfValuesB.yzw - tsdfValuesA.yzw));
	return d;
}

vec3 makeVertex(int edge, in vec4[8] tsdfValues, float half_voxel_size, out vec3 color) {
	switch(edge){
		case 0:
		{
			float d = progression(tsdfValues[0], tsdfValues[1], color);
			return vec3(-1, +d, -1) * half_voxel_size;
		}
		case 1:
		{
			float d = progression(tsdfValues[1], tsdfValues[2], color);
			return vec3(+d, +1, -1) * half_voxel_size;
		}
		case 2:
		{
			float d = progression(tsdfValues[3], tsdfValues[2], color);
			return vec3(+1, +d, -1) * half_voxel_size; 
		}
		case 3:
		{
			float d = progression(tsdfValues[0], tsdfValues[3], color);
			return vec3(+d, -1, -1) * half_voxel_size;
		}
		case 4:
		{
			float d = progression(tsdfValues[4], tsdfValues[5], color);
			return vec3(-1, +d, +1) * half_voxel_size;
		}
		case 5:
		{
			float d = progression(tsdfValues[5], tsdfValues[6], color);
			return vec3(+d, +1, +1) * half_voxel_size;
		}
		case 6:
		{
			float d = progression(tsdfValues[7], tsdfValues[6], color);
			return vec3(+1, +d, +1) * half_voxel_size;
		}
		case 7:
		{
			float d = progression(tsdfValues[4], tsdfValues[7], color);
			return vec3(+d, -1, +1) * half_voxel_size;
		}
		case 8:
		{
			float d = progression(tsdfValues[0], tsdfValues[4], color);
			return vec3(-1, -1, +d) * half_voxel_size;
		}
		case 9:
		{
			float d = progression(tsdfValues[1], tsdfValues[5], color);
			return vec3(-1, +1, +d) * half_voxel_size;
		}
		case 10:
		{
			float d = progression(tsdfValues[2], tsdfValues[6], color);
			return vec3(+1, +1, +d) * half_voxel_size;
		}
		case 11:
		{
			float d = progression(tsdfValues[3], tsdfValues[7], color);
			return vec3(+1, -1, +d) * half_voxel_size;
		}
	}
}

shared int tile_indices[8];
shared f16vec4 workVolume[VOXELS_IN_GROUP];
shared int GlobalVertexOffset;
shared int GroupVertexCount;
shared int triangleCounts[GROUP_SIZE];

#define NUM_BANKS 32 
#define LOG_NUM_BANKS 5 

#define ZERO_BANK_CONFLICTS 0

#ifdef ZERO_BANK_CONFLICTS 
#define CONFLICT_FREE_OFFSET(n) 0 //((n) >> NUM_BANKS + (n) >> (2 * LOG_NUM_BANKS)) 
#else 
#define CONFLICT_FREE_OFFSET(n) 0 //((n) >> LOG_NUM_BANKS) 
#endif

//	
//	Parallel Prefix Sum (scan) With Cuda (by Mark Harris)
//	Adapted for opengl compute shaders.
//
int prefix_sum(int v){
    int thid = int(gl_LocalInvocationIndex); 
    int offset = 1;
    
    int ai = thid; 
	int bankOffsetA = CONFLICT_FREE_OFFSET(ai);
    triangleCounts[ai + bankOffsetA] = v;
	
	for (int d = GROUP_SIZE/2; d > 0; d /= 2){ // build sum in place up the tree  
	    memoryBarrierShared();
	    barrier();
        if (thid < d){ 
            int ai = offset*(2*thid+1)-1; 
			int bi = offset*(2*thid+2)-1; 
			ai += CONFLICT_FREE_OFFSET(ai); 
			bi += CONFLICT_FREE_OFFSET(bi);
            triangleCounts[bi] += triangleCounts[ai];
  		}
        offset *= 2; 
    }
    
    // clear the last element
    if (thid == 0) {
		int p = GROUP_SIZE - 1 + CONFLICT_FREE_OFFSET(GROUP_SIZE - 1);
		int sum = triangleCounts[p];
    	triangleCounts[p] = 0; 
		if(sum > 0){
			GlobalVertexOffset = int(atomicCounterAdd(GlobalVertexCounter, sum));
		}else{
			GlobalVertexOffset = 0;
		}
		GroupVertexCount = sum;
    }
    
    for (int d = 1; d < GROUP_SIZE; d *= 2){ // traverse down tree & build scan 
        offset /= 2; 
	    memoryBarrierShared();
	    barrier(); 
        if (thid < d) {
            int ai = offset*(2*thid+1)-1; 
			int bi = offset*(2*thid+2)-1; 
			ai += CONFLICT_FREE_OFFSET(ai); 
			bi += CONFLICT_FREE_OFFSET(bi);
            int t             = triangleCounts[ai]; 
            triangleCounts[ai]  = triangleCounts[bi]; 
            triangleCounts[bi] += t;
        }
    }
    memoryBarrierShared();
    barrier();
    return triangleCounts[ai + bankOffsetA];
}


ivec3 getVoxelAtlasCoords(int tile_index, ivec3 localCoords) {
	ivec3 atlas_dims = imageSize(VoxelsTexture) / VOXELS_PER_SIDE;
	
	ivec3 atlas_coords;
	atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
	int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
	atlas_coords.y = remainder / atlas_dims.x;
	atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;
	
	return atlas_coords * VOXELS_PER_SIDE + localCoords;
}

void main(void){
	const int tile_index = int(gl_WorkGroupID.x);
	TileDesc tile = tile_descriptors[tile_index];
	const uvec3 tile_coords = tile.coords_index.xyz;


	if(gl_LocalInvocationIndex == 0){
		tile_indices[0] = tile_index;
	}else if(gl_LocalInvocationIndex < 8){
		uint tile_z = tile_coords.z + ((gl_LocalInvocationIndex>>2) & 1);
		uint tile_y = tile_coords.y + ((gl_LocalInvocationIndex>>1) & 1);
		uint tile_x = tile_coords.x + ((gl_LocalInvocationIndex>>0) & 1);
		
		int tile_packed_index = texelFetch(TileOccupancy, ivec3(tile_x, tile_y, tile_z), 0).x;  
		tile_indices[gl_LocalInvocationIndex] = tile_packed_index;
	}
    memoryBarrierShared();
    barrier();
	
	//grab the tsdf values and store then in a (VOXELS_PER_SIDE+1)^3 work volume in shared memory
	for(uint k=gl_LocalInvocationIndex; k<VOXELS_IN_GROUP; k+=GROUP_SIZE){
		const uint sz = (VOXELS_PER_SIDE+1) * (VOXELS_PER_SIDE+1);
		const uint sy = VOXELS_PER_SIDE+1;
		uint local_z = k / sz;
		uint remainder = k - local_z * sz;
		uint local_y = remainder / sy;
		uint local_x = remainder - local_y * sy;

		uint tile_z = local_z / VOXELS_PER_SIDE;
		local_z = local_z % VOXELS_PER_SIDE;
		uint tile_y = local_y / VOXELS_PER_SIDE;
		local_y = local_y % VOXELS_PER_SIDE;	
		uint tile_x = local_x / VOXELS_PER_SIDE;
		local_x = local_x % VOXELS_PER_SIDE;
		
		int tile_index = tile_indices[(tile_z<<2) | (tile_y<<1) | (tile_x<<0)];
		if(tile_index == -1){
			workVolume[k] = f16vec4(1.0f - SDF_OFFSET, 0.0f, 0.0f, 0.0f);
		}else if(tile_index == -2){
			workVolume[k] = f16vec4(0.1f - SDF_OFFSET, 0.0f, 0.0f, 0.0f);
		}else{
			ivec3 readCoords = getVoxelAtlasCoords(tile_index, ivec3(local_x, local_y, local_z));
			vec4 v = imageLoad(VoxelsTexture, readCoords);
			
			v.x = v.x - SDF_OFFSET;
			workVolume[k] = f16vec4(v); 
		}
		
	}
    memoryBarrierShared();
    barrier();

	float voxel_size = tile_size / VOXELS_PER_SIDE;
	vec3 tileCenter = TSDFminCorner + (tile_coords+0.5f) * tile_size;
	vec4 voxelCorner = vec4(TSDFminCorner + tile_coords * tile_size + (gl_LocalInvocationID+1.0f) * voxel_size, 0.0f);
	
	vec4 tsdfValues[8];
	const ivec3 cornerLocs[8] = ivec3[8](
		ivec3(0,0,0),
		ivec3(0,1,0),
		ivec3(1,1,0),
		ivec3(1,0,0),
		ivec3(0,0,1),
		ivec3(0,1,1),
		ivec3(1,1,1),
		ivec3(1,0,1)
	);
	
	int voxelCase = 0;
	bool undefinedValues = false;
	for(int i=0; i<8; i++){
		ivec3 spatial_offset = cornerLocs[i];
		
		tsdfValues[i] = workVolume[toWorkVolumeIndex(ivec3(gl_LocalInvocationID) + spatial_offset)];
		float density = tsdfValues[i].x;
				
		voxelCase |= (density > 0.0f ? 1<<i : 0);
		undefinedValues = undefinedValues || isinf(density) || isnan(density);
	}
	

	int triangles = undefinedValues ? 0 : facesLUT[voxelCase * 16 + 0];// # of triangles to generate
	
	int offset = prefix_sum(triangles*3);
	
	if(GroupVertexCount != 0){
	
		for(int k=0; k<triangles; k++){
			vec3 color;
			
			vec4 p0 = vec4(makeVertex(facesLUT[voxelCase * 16 + k*3 + 1], tsdfValues, voxel_size*0.5f, color), 1.0f);
			uint c0 = packUnorm4x8(vec4(color, 0));
			vec4 p1 = vec4(makeVertex(facesLUT[voxelCase * 16 + k*3 + 2], tsdfValues, voxel_size*0.5f, color), 1.0f);
			uint c1 = packUnorm4x8(vec4(color, 0));
			vec4 p2 = vec4(makeVertex(facesLUT[voxelCase * 16 + k*3 + 3], tsdfValues, voxel_size*0.5f, color), 1.0f);
			uint c2 = packUnorm4x8(vec4(color, 0));
			
			vec3 N = vec3(transform * vec4(cross(p1.xyz-p0.xyz, p2.xyz-p0.xyz), 0.0));
			N = -normalize(N);
			
			p2 = transform*(p2+voxelCorner);
			p1 = transform*(p1+voxelCorner);
			p0 = transform*(p0+voxelCorner);
			
			vertices[GlobalVertexOffset + offset + k*3 + 0] = Vertex(p2, vec4(N, uintBitsToFloat(c2)));
			vertices[GlobalVertexOffset + offset + k*3 + 1] = Vertex(p1, vec4(N, uintBitsToFloat(c1)));
			vertices[GlobalVertexOffset + offset + k*3 + 2] = Vertex(p0, vec4(N, uintBitsToFloat(c0)));
			
		}
	
	}
	
}
