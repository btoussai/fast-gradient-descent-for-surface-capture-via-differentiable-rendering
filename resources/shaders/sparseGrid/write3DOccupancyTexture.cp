/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_NV_gpu_shader5 : 					enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

layout(local_size_x = 32, local_size_y = 1, local_size_z = 1) in;

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

layout(binding = 0, r32i) uniform restrict writeonly iimage3D TileOccupancy;

layout(std430, binding = 0) restrict readonly buffer TilesDescriptorBlock{
	TileDesc tile_descriptors[];
};

uniform int NumTiles;

void main(void){

	const int tile_index = int(gl_GlobalInvocationID.x);
	if(tile_index < NumTiles){
		TileDesc tile = tile_descriptors[tile_index];

		const ivec3 tile_coords = tile.coords_index.xyz;
		imageStore(TileOccupancy, tile_coords, ivec4(tile_index, 0, 0, 0));
	}
	
}
