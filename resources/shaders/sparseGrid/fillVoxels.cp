/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_NV_gpu_shader5 : 					enable


const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

layout(local_size_x = VOXELS_PER_SIDE, local_size_y = VOXELS_PER_SIDE, local_size_z = VOXELS_PER_SIDE) in;

struct Point{
	float depth;
};

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

layout(binding=0) uniform isampler2DArray occupancyImages;
layout(binding=1) uniform sampler2DArray srcImages;
layout(binding=0, rgba8) uniform restrict writeonly image3D VoxelsTexture;

layout(std430, binding = 0) restrict readonly buffer PointsBlock{
	Point points[];
};

layout(std430, binding = 1) restrict readonly buffer TilesDescriptorBlock{
	TileDesc tile_descriptors[];
};

#include </common/projection.txt>

uniform vec3 TSDFminCorner;
uniform float tile_size;
uniform int NumCameras;

ivec3 getVoxelAtlasCoords(int tile_index, ivec3 localCoords) {
	ivec3 atlas_dims = imageSize(VoxelsTexture) / VOXELS_PER_SIDE;
	
	ivec3 atlas_coords;
	atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
	int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
	atlas_coords.y = remainder / atlas_dims.x;
	atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;
	
	return atlas_coords * VOXELS_PER_SIDE + localCoords;
}

void main(void){
	const float DEFAULT_DEPTH = 500.0f;
	const float voxel_size = tile_size / VOXELS_PER_SIDE;
	const float TSDF_CUTOFF = 4*voxel_size;//in mm

	const int tile_index = int(gl_WorkGroupID.x);
	TileDesc tile = tile_descriptors[tile_index];

	const vec3 tile_coords = tile.coords_index.xyz;
	
	vec3 voxelCenter = TSDFminCorner + tile_coords * tile_size + (gl_LocalInvocationID+0.5f) * voxel_size;
	
	float TSDF = 0;
	float weights = 0;
	float TSDF_FAR = +1.0f/0.0f;
	vec4 color = vec4(0.0f);
	
	int inside = 0;
	int outside = 0;
	int dontKnow = 0;
	for(int camIndex=0; camIndex<NumCameras; camIndex++){
		vec3 proj = project(voxelCenter, camIndex);
		const float voxelDepth = proj.z;
		if(voxelDepth <= 0.0f){
			dontKnow++;
			continue;
		}
		proj.xy /= voxelDepth;
		vec2 pixelUV = toPixels(proj.xy, camIndex);
		
		vec2 image_size = getImageSize(camIndex);
		if(!isInsideImageRange(pixelUV, image_size)){
			dontKnow++;
			continue;
		}
		
		ivec2 pixelUV_int = ivec2(pixelUV);
		ivec2 depthTileUV = pixelUV_int / DEPTHMAP_TILE_WIDTH;
		ivec2 depthPixelUV = pixelUV_int % DEPTHMAP_TILE_WIDTH;
		
		int depthPixelLocalIndex = depthPixelUV.y * DEPTHMAP_TILE_WIDTH + depthPixelUV.x;
		
		int depthTileIndex = texelFetch(occupancyImages, ivec3(depthTileUV, camIndex), 0).r;
		
		if(depthTileIndex == -1){
			outside++;//The voxel projects outside of the image tiles
			continue;
		}
		
		const int depthPixelGlobalIndex = depthTileIndex * DEPTHMAP_TILE_SIZE + depthPixelLocalIndex;
		
		const float depth = points[depthPixelGlobalIndex].depth;
		const vec4 c = texelFetch(srcImages, ivec3(pixelUV_int / image_size * textureSize(srcImages, 0).xy, camIndex), 0);
		
		
		float tsdf;
		if(depth != DEFAULT_DEPTH){
			tsdf = (depth - voxelDepth)/TSDF_CUTOFF;
			if(voxelDepth < depth){
				outside++;
			}else{
				inside++;
			}
		
			if(abs(tsdf) <= 1.0f){
				TSDF += tsdf;
				weights += 1.0f;
				color += c;
			}else if(abs(tsdf) < abs(TSDF_FAR)){
				TSDF_FAR = tsdf;
			}
		}else{
			TSDF_FAR = 10.0f;//Far outside
			outside++;
		}
		
	}
	
	if(weights == 0){
		if(outside > 0 || inside == 0){
			TSDF = +1.0f;
		}else{
			TSDF = -1.0f;
		}
	}else{
		TSDF /= weights;
		color /= weights;
	}
	
	
	// The TSDF is > 0 outside and < 0 inside
	// The opacity transistions from -1 far outside, to +1 far inside
	
	float transparency = 1.0f-smoothstep(-1.0f, 1.0f, -TSDF); // in [0, +1]
	transparency = 0.98f;
	
	
	//set everything to a dummy color
	color.rgb = vec3(0.0f);
	
	
	ivec3 writeCoords = getVoxelAtlasCoords(tile_index, ivec3(gl_LocalInvocationID));
	imageStore(VoxelsTexture, writeCoords, vec4(transparency, color.rgb));
	
}
