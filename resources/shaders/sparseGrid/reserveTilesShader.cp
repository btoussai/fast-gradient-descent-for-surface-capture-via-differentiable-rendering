/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require
#extension GL_NV_gpu_shader5 : 					enable
#extension GL_KHR_shader_subgroup_basic : enable
#extension GL_NV_shader_subgroup_partitioned : enable
#extension GL_KHR_shader_subgroup_vote : enable

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;
const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

const int GROUP_SIZE = 32;
layout(local_size_x = GROUP_SIZE, local_size_y = 1, local_size_z = 1) in;

struct Point{
	float depth;
};

struct TileDesc{
	ivec4 coords_index;
	ivec4 activeCameras;
};

struct DepthTileDescriptor{
	ivec2 tileCoords;//coordinates of the tile in the image
	int foregroundPixelCount;//number of actual foreground pixels in the tile
	int camIndex;//index of the camera
};

layout(std430, binding = 0) restrict writeonly buffer TilesDescriptorBlock{
	TileDesc tile_descriptors[];
};
layout(std430, binding = 1) restrict readonly buffer DepthTileDescriptorsBlock{
	DepthTileDescriptor depthTileDescriptors[];
};

layout(binding = 0) uniform isampler2DArray DepthTileOccupancy;
layout(binding = 0) uniform atomic_uint GlobalTileCounter;

#include </common/projection.txt>

uniform vec3 VolumeMinCorner;
uniform float tile_size;
uniform ivec3 TilesTotal;
uniform int NumCameras;

struct CubicTileProjection{
	i16vec2 pixelMinCorner;
	i16vec2 dims;
};

shared CubicTileProjection allTileProjections[100];

ivec2 safeCeil(vec2 coords){
	return ivec2(floor(coords)) + mix(ivec2(0), ivec2(1), greaterThan(fract(coords), vec2(0.5f)));
}

ivec2 safeFloor(vec2 coords){
	return ivec2(floor(coords)) - mix(ivec2(0), ivec2(1), lessThan(fract(coords), vec2(0.5f)));
}

void computeTileProjections(ivec3 tile_coords){
	vec3 minTileCorner = VolumeMinCorner + vec3(tile_coords) * tile_size;
	vec3 maxTileCorner = minTileCorner + tile_size;
	const float eps = 0.01f;
	minTileCorner += (0.5f-eps) * tile_size / VOXELS_PER_SIDE;
	maxTileCorner -= (0.5f-eps) * tile_size / VOXELS_PER_SIDE;
	const float tile_size_inv = 1.0f / tile_size;

	for(int camIndex = int(gl_LocalInvocationIndex / 8); camIndex < NumCameras; camIndex += GROUP_SIZE / 8){
		//each thread projects one vertex
		int cornerID = int(gl_LocalInvocationIndex % 8);
		int projID = int(gl_SubgroupInvocationID / 8);

		vec3 corner = vec3(
				(cornerID&1)==0?minTileCorner.x : maxTileCorner.x,
				(cornerID&2)==0?minTileCorner.y : maxTileCorner.y,
				(cornerID&4)==0?minTileCorner.z : maxTileCorner.z
			);

		vec3 proj = project(corner, camIndex);
		vec2 NDCCoords = vec2(proj) / proj.z;
		vec2 PixelCoords = toPixels(NDCCoords, camIndex);

		uvec4 ballot = subgroupPartitionNV(projID);
       	vec3 minProj = subgroupPartitionedMinNV(vec3(PixelCoords, proj.z), ballot);
       	vec3 maxProj = subgroupPartitionedMaxNV(vec3(PixelCoords, proj.z), ballot);

       	if(cornerID == 0){
			const ivec2 image_size = ivec2(getImageSize(camIndex));
			ivec2 pixelMax = safeCeil(vec2(maxProj));
	       	ivec2 pixelMin = safeFloor(vec2(minProj));

			ivec2 dims = pixelMax - pixelMin + 1;
			
			if(any(greaterThanEqual(pixelMin, image_size)) || any(lessThan(pixelMax, ivec2(0)))){
				dims = ivec2(0);
			}
			dims = max(dims, ivec2(0));//just in case...

       		allTileProjections[camIndex] = CubicTileProjection(i16vec2(pixelMin), i16vec2(dims));
       	}
	}

    memoryBarrierShared();
    barrier();

}

void main(void){
	const ivec3 tile_coords = ivec3(gl_WorkGroupID.xyz);
	computeTileProjections(tile_coords);
	
	
	int validCameras = 0;//number of cameras that can see the tile
	int insideCounter = 0;//number of cameras that found the tile inside the visual hull
	int outsideCounter = 0;//number of cameras that found the tile outside the visual hull but inside their field of fiew
	
	for(int camIndex=0; camIndex < NumCameras; camIndex++){
		CubicTileProjection tileProj = allTileProjections[camIndex];
		
		const int size = tileProj.dims.x * tileProj.dims.y;
		
		if(size == 0){
			continue;
		}
		
		bool inside = false;
		for(int localIndex = int(gl_LocalInvocationIndex); localIndex < size; localIndex += GROUP_SIZE){
			ivec2 pixelCoords = tileProj.pixelMinCorner + ivec2(localIndex % tileProj.dims.x, localIndex / tileProj.dims.x);
			
			int depth_tile_id = texelFetch(DepthTileOccupancy, ivec3(pixelCoords / DEPTHMAP_TILE_WIDTH, camIndex), 0).r;
			inside = inside || depth_tile_id >= 0;
			
		}
		
		if(subgroupAny(inside)){
			insideCounter++;
		}else{
			outsideCounter++;
		}
		validCameras++;
	}
	
	if(outsideCounter > 4 || validCameras == 0){
		return;
	}
	
	//Reserve a new tile
	if(gl_LocalInvocationIndex == 0){
		const uint index = atomicCounterAdd(GlobalTileCounter, 1u);
		const TileDesc desc = TileDesc(ivec4(tile_coords, int(index)), ivec4(-1));
		tile_descriptors[index] = desc;
	}
	
}


