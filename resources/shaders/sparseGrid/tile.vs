/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

in ivec4 tile_desc;
in ivec4 activeCameras;

out ivec4 tile_desc_gs;
out ivec4 activeCameras_gs;

void main(void){

	tile_desc_gs = tile_desc;
	tile_desc_gs.w = gl_VertexID;
	
	activeCameras_gs = activeCameras;
	
	gl_Position = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	
}
