/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_NV_gpu_shader5 : 					enable
#extension GL_KHR_shader_subgroup_ballot : 		enable
#extension GL_KHR_shader_subgroup_basic : 		enable

layout(local_size_x = 4, local_size_y = 4, local_size_z = 4) in;

layout(std430, binding = 0) restrict readonly buffer TilesOccupancyBlock{
	int tsdf_tiles[];
};

layout(binding = 0) uniform isampler3D TileOccupancy;
layout(binding = 0, rg32ui) uniform restrict writeonly uimage3D CompressedTileOccupancy;

uniform ivec3 TSDFTilesCount;

shared uvec2 shared_mask;

void main(void){
	//We assume a subgroup size of 32 !

	const uvec3 compressedCoord = gl_WorkGroupID;
	const ivec3 expandedCoord = ivec3(compressedCoord * 4 + gl_LocalInvocationID);
	
	ivec3 s = textureSize(TileOccupancy, 0);
	const int id = texelFetch(TileOccupancy, expandedCoord, 0).x;
	
	uvec4 mask = subgroupBallot(id != -1 && all(lessThan(expandedCoord, s)));
	
	shared_mask[gl_SubgroupID] = mask.x;
	
	memoryBarrierShared();
	barrier();
	
	if(gl_LocalInvocationIndex == 0){
		imageStore(CompressedTileOccupancy, ivec3(compressedCoord), uvec4(shared_mask, 0, 0));
	}
	
}
