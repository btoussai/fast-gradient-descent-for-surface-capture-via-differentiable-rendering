/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require
#extension GL_NV_gpu_shader5 : enable

in vec2 tex_coords;
out vec4 out_Color;

in vec3 world_pos;

uniform vec3 camPos;
uniform ivec3 TSDFTilesCount;
uniform vec3 TSDFminCorner;
uniform float tile_size;
uniform int activeChannels;
uniform int TransparencyOnly;

const int VOXELS_PER_SIDE = 4;

#include </common/projection.txt>

vec3 hsv2rgb(vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0f - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0f, 1.0f), c.y);
}

#define #SHBands
#define #VoxelsTexturesCount

//https://en.wikipedia.org/wiki/Table_of_spherical_harmonics#Real_spherical_harmonics
const float baseCoeff = 0.28209479177387814;
const float SHcoeffs[9] = {
	1.0f,   						   // 0.5 * sqrt(1/pi)
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	0.31539156525252005 / baseCoeff,   // 0.25 * sqrt(5 / pi)
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	1.0925484305920792  / baseCoeff    // 0.5 * sqrt(15 / pi)
};


layout(binding = 0) uniform sampler3D VoxelsTexture[VoxelsTexturesCount];
layout(binding = 7) uniform isampler3D TileOccupancy;


void getSampleCoords(ivec3 p, vec3 d, out int tile_index, out ivec3 q) {
	if(any(lessThan(p, ivec3(0))) || any(greaterThanEqual(p, TSDFTilesCount * VOXELS_PER_SIDE))){
		tile_index = -1;
		return;
	}
	
	const ivec3 tile_coords = p / VOXELS_PER_SIDE;
	tile_index = texelFetch(TileOccupancy, tile_coords, 0).x;
	if(tile_index < 0){
		return;
	}
	
	//convert to voxel local coord
	p %= VOXELS_PER_SIDE;
	const ivec3 atlas_dims = textureSize(VoxelsTexture[0], 0) / VOXELS_PER_SIDE;
	
	// convert to atlas coords
	ivec3 atlas_coords;
	atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
	int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
	atlas_coords.y = remainder / atlas_dims.x;
	atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;
	p += atlas_coords * VOXELS_PER_SIDE;
	
	q = p;
}

vec4 mixSample(int tile_index[8], ivec3 q[8], int N, vec3 fract_p){

	vec4 v[8];
	for(int i=0; i<8; i++){
		if(tile_index[i] == -2){
			v[i] = N==0 ? vec4(0.1, vec3(0)) : vec4(0.5);
		}else if(tile_index[i] == -1){
			v[i] = N==0 ? vec4(1.0, vec3(0)) : vec4(0.5);
		}else{
			v[i] = texelFetch(VoxelsTexture[N], q[i], 0);
		}
		
		if(i % 2 == 1){
			v[i] = mix(v[i-1], v[i], fract_p.x);
		}
		
		if(i % 4 == 3){
			v[i] = mix(v[i-2], v[i], fract_p.y);
		}
		
		if(i == 7){
			v[i] = mix(v[i-4], v[i], fract_p.z);
		}
	}
	
	return v[7];
}

bool isChannelActive(int b){
	return ((activeChannels >> b) & 1) > 0;
}

vec4 getSample(vec3 p, vec3 ray) {
	p = p - vec3(0.5f);
	const vec3 floor_p = floor(p);
	const vec3 fract_p = p - floor_p;
	const ivec3 int_p = ivec3(floor_p);
	
	int tile_index[8];
	ivec3 q[8];
	
	for(int i=0; i<8; i++){
		getSampleCoords(int_p + ivec3((i>>0)&1, (i>>1)&1, (i>>2)&1), ray, tile_index[i], q[i]);
	}
	
	float alpha = 0.0;
	vec3 c = vec3(0.0f);
	
	#if SHBands==2
		vec4 v6 = 2.0f * mixSample(tile_index, q, 6, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v5 = 2.0f * mixSample(tile_index, q, 5, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v4 = 2.0f * mixSample(tile_index, q, 4, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v3 = 2.0f * mixSample(tile_index, q, 3, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v2 = 2.0f * mixSample(tile_index, q, 2, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v1 = 2.0f * mixSample(tile_index, q, 1, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v0 = mixSample(tile_index, q, 0, fract_p);
		
		vec3 d = normalize(ray);
		if(isChannelActive(8)) c += vec3(v6.yzw)      * SHcoeffs[8] * (d.x*d.x - d.y*d.y);
		if(isChannelActive(7)) c += vec3(v5.zw, v6.x) * SHcoeffs[7] * (d.x*d.z);
		if(isChannelActive(6)) c += vec3(v4.w, v5.xy) * SHcoeffs[6] * (3.0f*d.z*d.z - 1.0f);
		if(isChannelActive(5)) c += vec3(v4.xyz)      * SHcoeffs[5] * (d.y*d.z);
		if(isChannelActive(4)) c += vec3(v3.yzw)      * SHcoeffs[4] * (d.x*d.y);
		if(isChannelActive(3)) c += vec3(v2.zw, v3.x) * SHcoeffs[3] * d.x;
		if(isChannelActive(2)) c += vec3(v1.w, v2.xy) * SHcoeffs[2] * d.z;
		if(isChannelActive(1)) c += vec3(v1.xyz)      * SHcoeffs[1] * d.y;
		if(isChannelActive(0)) c += vec3(v0.yzw)      * SHcoeffs[0];
		alpha = v0.x;
	#elif SHBands==1
		vec4 v3 = 2.0f * mixSample(tile_index, q, 3, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v2 = 2.0f * mixSample(tile_index, q, 2, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v1 = 2.0f * mixSample(tile_index, q, 1, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v0 = mixSample(tile_index, q, 0, fract_p);
		
		vec3 d = normalize(ray);
		if(isChannelActive(3)) c += vec3(v2.zw, v3.x) * SHcoeffs[3] * d.x;
		if(isChannelActive(2)) c += vec3(v1.w, v2.xy) * SHcoeffs[2] * d.z;
		if(isChannelActive(1)) c += vec3(v1.xyz)      * SHcoeffs[1] * d.y;
		if(isChannelActive(0)) c += vec3(v0.yzw)      * SHcoeffs[0];
		alpha = v0.x;
	#else
		vec4 v0 = mixSample(tile_index, q, 0, fract_p);
		if(isChannelActive(0)) c += vec3(v0.yzw);
		alpha = v0.x;
	#endif
	
	if(isChannelActive(0)){
		c = abs(c);
	}
	
	return vec4(alpha, c);
}

void main(void){
	vec3 toCam = normalize(camPos - world_pos);
	
    const float tile_size_inv = 1.0f / tile_size;
    const float scaling = tile_size_inv * float(VOXELS_PER_SIDE);
	vec3 p = (world_pos - TSDFminCorner) * scaling;
	vec4 transmittance_rgb = getSample(p, -toCam);
	
	if(transmittance_rgb.x > 0.99){
		discard;
	}

	if(TransparencyOnly > 0){
		out_Color = vec4(transmittance_rgb.xxx, 1);
	}else{
		out_Color = vec4(transmittance_rgb.yzw, 1);
	}
	
}



