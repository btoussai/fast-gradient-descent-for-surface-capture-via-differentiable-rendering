/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

layout(location = 0) in vec2 position;

out vec3 world_pos;

uniform mat4 projectionView;
uniform mat4 transform;

void main(void){

	vec4 ws = transform * vec4(position, 0.0, 1.0);
	world_pos = ws.xyz;
	
	gl_Position = projectionView * ws;
}
