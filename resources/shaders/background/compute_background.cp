/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable

const int GROUP_WIDTH = 8;
layout(local_size_x = GROUP_WIDTH, local_size_y = GROUP_WIDTH, local_size_z = 1) in;

const int IMAGE_COUNT = 15;
const int GROUP_SIZE = GROUP_WIDTH*GROUP_WIDTH;

layout(binding=0) uniform sampler2DArray imagesArray;
layout(binding=0, rgba8ui) uniform restrict writeonly uimage2D filterImage;

void main(void){
	const ivec2 coords = ivec2(gl_GlobalInvocationID.xy);
	
	ivec3 texSize = textureSize(imagesArray, 0);
	if(coords.x >= texSize.x || coords.y >= texSize.y){
		return;
	}
	
	
	vec3 average = vec3(0.0);
	for(int layer=0; layer<IMAGE_COUNT; layer++){
		average += texelFetch(imagesArray, ivec3(coords, layer), 0).rgb;
	}
	average /= IMAGE_COUNT;
	
	imageStore(filterImage, coords, uvec4(vec4(average, 1)*255.0f));	
}

