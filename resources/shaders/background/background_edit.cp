/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable

const int GROUP_WIDTH = 16;
layout(local_size_x = GROUP_WIDTH, local_size_y = GROUP_WIDTH, local_size_z = 1) in;

layout(binding=0, rgba8) uniform restrict image2D Image;
uniform vec2 CursorNDCPos;
uniform float BrushRadius;
uniform float value;

void main(void){
	
	vec2 texSize = vec2(imageSize(Image));
	ivec2 tile_center = ivec2(vec2(CursorNDCPos.x*0.5+0.5, 0.5-CursorNDCPos.y*0.5)*texSize);

	ivec2 texelCoords = tile_center + ivec2(gl_GlobalInvocationID.xy) - ivec2(vec2(gl_NumWorkGroups.xy)*vec2(0.5*GROUP_WIDTH));

	
	vec2 texCoords = texelCoords / texSize;
	float d = distance(vec2(texCoords.x*2.0-1.0, 1.0-texCoords.y*2.0), CursorNDCPos);
		
	if(d < BrushRadius){
		vec4 c = imageLoad(Image, texelCoords);
		c.a = value;
		imageStore(Image, texelCoords, c);
	}
	
}

