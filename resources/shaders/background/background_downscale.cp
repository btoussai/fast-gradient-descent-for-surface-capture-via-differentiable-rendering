/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable

const int GROUP_WIDTH = 8;
layout(local_size_x = GROUP_WIDTH, local_size_y = GROUP_WIDTH, local_size_z = 1) in;


layout(binding=0) uniform sampler2D srcImage;
layout(binding=0, r16f) uniform restrict writeonly image2D dstImage;

void main(void){
	ivec2 coords = ivec2(gl_GlobalInvocationID.xy);
	
	float decision = 0.0f;
	for(int j=0; j<2; j++){
		for(int i=0; i<2; i++){
			decision += texelFetch(srcImage, coords*2+ivec2(i, j), 0).r;
		}
	}
	//decision /= 4.0;
	
	imageStore(dstImage, coords, vec4(decision, 0, 0, 0));
}

