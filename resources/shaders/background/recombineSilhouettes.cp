/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

const int GROUP_WIDTH = 8;
layout(local_size_x = GROUP_WIDTH, local_size_y = GROUP_WIDTH, local_size_z = 1) in;

layout(binding=0) uniform sampler2DArray Backgrounds;
layout(binding=1) uniform sampler2DArray Silhouettes;

layout(binding=0, rgba8) uniform restrict image2DArray Images;

void main(void){
	
	ivec3 coords = ivec3(gl_GlobalInvocationID.xyz);
	
	vec4 backgroundColor = texelFetch(Backgrounds, coords, 0);
	vec4 imageColor = imageLoad(Images, coords);
	float silhouetteMask = texelFetch(Silhouettes, coords, 0).x;
	
	bool isOnBackground = imageColor.rgb == vec3(0) && silhouetteMask != 1.0f;
	
	if(isOnBackground){
		imageColor = backgroundColor;
	}
	
	imageStore(Images, coords, imageColor);
}

