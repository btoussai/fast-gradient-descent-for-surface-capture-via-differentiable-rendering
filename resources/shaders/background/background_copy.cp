/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :    require
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable
#extension GL_KHR_shader_subgroup_ballot:		enable
#extension GL_KHR_shader_subgroup_arithmetic:   enable


const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int FILTER_WIDTH = 1;
layout(local_size_x = DEPTHMAP_TILE_WIDTH, local_size_y = DEPTHMAP_TILE_WIDTH, local_size_z = 1) in;

const int GROUP_SIZE = DEPTHMAP_TILE_SIZE;
const int REGION_WIDTH = DEPTHMAP_TILE_WIDTH+2*DEPTHMAP_TILE_WIDTH;
const int REGION_SIZE = REGION_WIDTH*REGION_WIDTH;
const int NEIGHBORHOOD_SIZE = (2*FILTER_WIDTH+1)*(2*FILTER_WIDTH+1);

layout(binding=0) uniform sampler2DArray srcImage;
layout(binding=1) uniform sampler2D decisionImage;
layout(binding=0, rgba8) uniform restrict writeonly image2D dstImage;

uniform int camIndex;
uniform float DecisionBoundary;


struct BorderPoint{
	vec4 uv_viewpoint_type;
};

struct OccupancyDescriptor{
	ivec2 tileCoords;			//coordinates of the tile in the image
	int foregroundPixelCount;	//number of actual foreground pixels in the tile
	int camIndex;				//index of the camera
};

layout(std430, binding = 0) restrict writeonly buffer PointsBorderBlock{
	BorderPoint border_points[];
};
layout(std430, binding = 1) restrict writeonly buffer OccupancyDescriptorBlock{
	OccupancyDescriptor occupancyDescriptors[];
};
layout(binding = 0) uniform atomic_uint GlobalPointCounter;
layout(binding = 0) uniform atomic_uint GlobalPointBorderCounter;


#include </common/projection.txt>


shared uvec2 GlobalOffsets;
shared int regionShared[REGION_SIZE];
shared uvec2 partial_sums[gl_NumSubgroups];

uvec2 prefixSum(uvec2 v){
	
	uvec2 w = subgroupExclusiveAdd(v);
	if(gl_SubgroupInvocationID == gl_SubgroupSize-1){
		partial_sums[gl_SubgroupID] = w + v;
	}
	
	memoryBarrierShared();
	barrier();
	
	if(gl_SubgroupID == 0){
		uvec2 v2 = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : uvec2(0);
		uvec2 w2 = subgroupExclusiveAdd(v2);
		
		if(gl_SubgroupInvocationID == gl_SubgroupSize-1){
			uvec2 globalSums = w2 + v2;
			
			///////////////////////////  Custom behavior for the overall sums
			GlobalOffsets = uvec2(-1);
			if(globalSums.x > 0){
				ivec2 tileCoords = ivec2(gl_WorkGroupID.xy);
				
				uint offset = GlobalOffsets.x = atomicCounterAdd(GlobalPointCounter, GROUP_SIZE);
				occupancyDescriptors[offset / GROUP_SIZE] = OccupancyDescriptor(tileCoords, int(globalSums.x), camIndex);
			}
			if(globalSums.y > 0){
				GlobalOffsets.y = atomicCounterAdd(GlobalPointBorderCounter, uint(globalSums.y));
			}
			/////////////////////////////
			
		}
		
		partial_sums[gl_SubgroupInvocationID] = w2;
	}
	
	memoryBarrierShared();
	barrier();
	
	return w + partial_sums[gl_SubgroupID];
}

int getRegionIndex(ivec2 offset){
	ivec2 regionCoords = ivec2(FILTER_WIDTH) + ivec2(gl_LocalInvocationID) + offset;
	return regionCoords.y * REGION_WIDTH + regionCoords.x;
}

ivec2 toRegionCoord(int region_index){
	int y = region_index / REGION_WIDTH;
	int x = region_index - y * REGION_WIDTH;
	return ivec2(x, y);
}

ivec2 getTexelCoord(int region_index){
	return toRegionCoord(region_index) - ivec2(FILTER_WIDTH) + ivec2(gl_GlobalInvocationID.xy) - ivec2(gl_LocalInvocationID);
}

void main(void){

	for(int region_index = int(gl_LocalInvocationIndex); region_index < REGION_SIZE; region_index += GROUP_SIZE){
		ivec2 coords = getTexelCoord(region_index);
		float decision = texelFetch(decisionImage, coords, 0).r;
		bool isInside = decision > DecisionBoundary;
		regionShared[region_index] = int(isInside);
	}
	
	memoryBarrierShared();
	barrier();
	
	
	int decision = regionShared[getRegionIndex(ivec2(0, 0))];
	
	int u0 = regionShared[getRegionIndex(ivec2(-1, 0))];
	int u1 = regionShared[getRegionIndex(ivec2(+1, 0))];
	int u2 = regionShared[getRegionIndex(ivec2(0, -1))];
	int u3 = regionShared[getRegionIndex(ivec2(0, +1))];
	
	int neighborsOutsideFlags = 0;
	neighborsOutsideFlags |= int(u0==0) << 0;
	neighborsOutsideFlags |= int(u1==0) << 1;
	neighborsOutsideFlags |= int(u2==0) << 2;
	neighborsOutsideFlags |= int(u3==0) << 3;
	neighborsOutsideFlags |= int(regionShared[getRegionIndex(ivec2(-1, -1))]==0) << 4;
	neighborsOutsideFlags |= int(regionShared[getRegionIndex(ivec2(+1, -1))]==0) << 5;
	neighborsOutsideFlags |= int(regionShared[getRegionIndex(ivec2(+1, +1))]==0) << 6;
	neighborsOutsideFlags |= int(regionShared[getRegionIndex(ivec2(-1, +1))]==0) << 7;
	
	bool isInside = decision!=0;
	bool isOnBorder = isInside && neighborsOutsideFlags > 0;
	
	vec4 color = texelFetch(srcImage, ivec3(ivec2(gl_GlobalInvocationID.xy), camIndex), 0);
	color.a = float(int(isInside));
	imageStore(dstImage, ivec2(gl_GlobalInvocationID.xy), color);
	
	uvec2 indices = prefixSum(uvec2(uint(isInside), uint(isOnBorder)));

	vec2 uv = vec2(gl_GlobalInvocationID.xy) + vec2(0.5f);
	
	if(isOnBorder){
		border_points[GlobalOffsets.y + indices.y] = BorderPoint(vec4(uv, camIndex, neighborsOutsideFlags));
	}

}

