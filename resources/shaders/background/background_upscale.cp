/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable

const int GROUP_WIDTH = 8;
layout(local_size_x = GROUP_WIDTH, local_size_y = GROUP_WIDTH, local_size_z = 1) in;


layout(binding=0) uniform sampler2D srcImage1;
layout(binding=1) uniform sampler2D srcImage2;
layout(binding=0, r16f) uniform restrict writeonly image2D dstImage;
uniform int camIndex;
uniform float scale;

void main(void){
	ivec2 coords = ivec2(gl_GlobalInvocationID.xy);
	
	float decision1 = texelFetch(srcImage1, coords, 0).r;
	float decision2 = texelFetch(srcImage2, coords/2, 0).r;
	
	//const float u = 0.2f;
	//vec2 decision = decision1 * u + decision2 * (1.0f - u);
	
	float decision = decision1 + decision2;

	imageStore(dstImage, coords, vec4(decision, 0, 0, 0));
}

