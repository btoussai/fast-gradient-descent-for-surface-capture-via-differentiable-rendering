/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable

const int GROUP_WIDTH = 8;
const int FILTER_WIDTH = 1;
layout(local_size_x = GROUP_WIDTH, local_size_y = GROUP_WIDTH, local_size_z = 1) in;


const int GROUP_SIZE = GROUP_WIDTH*GROUP_WIDTH;
const int REGION_WIDTH = GROUP_WIDTH+2*FILTER_WIDTH;
const int REGION_SIZE = REGION_WIDTH*REGION_WIDTH;
const int NEIGHBORHOOD_SIZE = (2*FILTER_WIDTH+1)*(2*FILTER_WIDTH+1);

layout(binding=0) uniform sampler2D srcImage;
layout(binding=0, r16f) uniform restrict writeonly image2D dstImage;

shared float regionShared[REGION_SIZE];

int getRegionIndex(ivec2 offset){
	ivec2 regionCoords = ivec2(FILTER_WIDTH) + ivec2(gl_LocalInvocationID) + offset;
	return regionCoords.y * REGION_WIDTH + regionCoords.x;
}

ivec2 toRegionCoord(int region_index){
	int y = region_index / REGION_WIDTH;
	int x = region_index - y * REGION_WIDTH;
	return ivec2(x, y);
}

ivec2 getTexelCoord(int region_index){
	return toRegionCoord(region_index) - ivec2(FILTER_WIDTH) + ivec2(gl_GlobalInvocationID.xy) - ivec2(gl_LocalInvocationID);
}


void main(void){

	ivec2 texBounds = textureSize(srcImage, 0).xy - ivec2(1);

	for(int region_index = int(gl_LocalInvocationIndex); region_index < REGION_SIZE; region_index += GROUP_SIZE){
		ivec2 coords = clamp(getTexelCoord(region_index), ivec2(0), texBounds);
		regionShared[region_index] = texelFetch(srcImage, coords, 0).r;
	}
	
	memoryBarrierShared();
	barrier();
	
	float decision = 0.0f;
	const int H = FILTER_WIDTH;
	float weights = 0.0f;
	for(int j=-H; j<=H; j++){
		for(int i=-H; i<=H; i++){
			int neighbor_index = getRegionIndex(ivec2(i, j));
			float weight = exp(-(i*i+j*j));
			decision += regionShared[neighbor_index] * weight;
			weights += weight;
		}
	}
	
	decision = decision / weights;
	imageStore(dstImage, ivec2(gl_GlobalInvocationID.xy), vec4(decision, 0, 0, 0));
}

