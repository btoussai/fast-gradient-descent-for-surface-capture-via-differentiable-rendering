/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_compute_shader :              enable
#extension GL_ARB_shader_storage_buffer_object: enable

const int GROUP_WIDTH = 8;
layout(local_size_x = GROUP_WIDTH, local_size_y = GROUP_WIDTH, local_size_z = 1) in;

layout(binding=0) uniform sampler2DArray srcImage;
layout(binding=1) uniform sampler2DArray filterImage;
layout(binding=0, r16f) uniform restrict writeonly image2D dstImage;

uniform vec4 thresholds;
uniform int camIndex;
uniform int UseBackgroundColors;
uniform int ColorsPaletteCount;
uniform vec4 ColorPalette[10];


vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

float computeDecision(ivec2 coords){
	
	vec3 c0 = texelFetch(srcImage, ivec3(coords, camIndex), 0).rgb;
	vec4 rgba_c1 = texelFetch(filterImage, ivec3(coords, camIndex), 0);
	vec3 rgb_c1 = rgba_c1.rgb;
	vec3 c1 = rgb_c1;
	
	bool shouldCare = rgba_c1.a != 0.0f;
	
	if(!shouldCare){
		return 0.0f;
	}
	
	bool isInside = true;
	if(UseBackgroundColors > 0){
		isInside = isInside && dot(vec4(abs(c0 - c1), 1), thresholds) > 0.0;
	}else{
		for(int i=0; i<ColorsPaletteCount; i++){
			isInside = isInside && dot(vec4(abs(c0 - ColorPalette[i].rgb), 1), thresholds) > 0.0;
		}
	}
	
	return float(isInside);
}

void main(void){
	
	ivec2 coords = ivec2(gl_GlobalInvocationID.xy);
	
	float decision = computeDecision(coords);
	imageStore(dstImage, ivec2(gl_GlobalInvocationID.xy), vec4(decision, 0, 0, 0));
}

