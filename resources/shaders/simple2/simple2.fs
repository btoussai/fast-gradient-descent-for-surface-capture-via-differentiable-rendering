/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

out vec4 out_Color;

in vec2 texCoords;

layout(binding=0, rgba8) uniform restrict readonly image2D Img;

void main(void){

	vec4 c = imageLoad(Img, ivec2(texCoords * imageSize(Img)));
	out_Color = c;
	
}
