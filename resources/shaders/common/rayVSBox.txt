/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/



bool rayVSbox(vec3 base, vec3 ray_inv, vec3 minCorner, vec3 maxCorner, out float tmin, out float tmax) {
	float tx1 = (minCorner.x - base.x) * ray_inv.x;
	float tx2 = (maxCorner.x - base.x) * ray_inv.x;
	
	tmin = min(tx1, tx2);
	tmax = max(tx1, tx2);
	
	float ty1 = (minCorner.y - base.y) * ray_inv.y;
	float ty2 = (maxCorner.y - base.y) * ray_inv.y;
	
	tmin = max(tmin, min(ty1, ty2));
	tmax = min(tmax, max(ty1, ty2));
	
	float tz1 = (minCorner.z - base.z) * ray_inv.z;
	float tz2 = (maxCorner.z - base.z) * ray_inv.z;
	
	tmin = max(tmin, min(tz1, tz2));
	tmax = min(tmax, max(tz1, tz2));
	
 	return tmax >= max(0.0f, tmin);
}

bool rayVSbox2D(vec2 base, vec2 ray_inv, vec4 minMaxCorners, float t, float prev_t, inout float boxOverlap, bool behind) {
	float tx1 = (minMaxCorners.x - base.x) * ray_inv.x;
	float tx2 = (minMaxCorners.z - base.x) * ray_inv.x;
	
	float tmin = min(tx1, tx2);
	float tmax = max(tx1, tx2);
	
	float ty1 = (minMaxCorners.y - base.y) * ray_inv.y;
	float ty2 = (minMaxCorners.w - base.y) * ray_inv.y;
	
	tmin = max(tmin, min(ty1, ty2));
	tmax = min(tmax, max(ty1, ty2));
	
	boxOverlap = !behind ? tmax - prev_t : prev_t - tmin;
	
 	return tmax >= max(0.0f, tmin) && tmin <= max(t, prev_t) && tmax >= min(t, prev_t);
}
