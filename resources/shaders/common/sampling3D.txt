/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/



const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

struct TileDescriptor3D{
	ivec4 coords_index;
	ivec4 activeCameras;
};

layout(std430, binding = 0) restrict readonly buffer TileDescriptorsBlock{
	TileDescriptor3D tile_descriptors[];
};

layout(std140, binding = 1) uniform HandleBlock
{
	int CURRENT_LOD;
	int MAX_LOD;
	uint64_t TileOccupancyTextureHandles[8];
	uint64_t TileOccupancyImageHandles[8];
	uint64_t CompressedTileOccupancyTextureHandles[8];
	uint64_t CompressedTileOccupancyImageHandles[8];
	uint64_t VoxelsTextureHandles[256];
	uint64_t VoxelsImageHandles[256];
	uint64_t GradientsTextureHandles[256];
	uint64_t GradientsImageHandles[256];
	uint64_t RayTexelTextureHandles[256];
	uint64_t RayTexelImageHandles[256];
};


#define #SHBands
#define #VoxelsTexturesCount

//https://en.wikipedia.org/wiki/Table_of_spherical_harmonics#Real_spherical_harmonics
const float baseCoeff = 0.28209479177387814;// 0.5 * sqrt(1/pi)
const float SHcoeffs[9] = {
	1.0f,   						   
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	0.4886025119029199  / baseCoeff,   // sqrt(3 / (4*pi))
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	0.31539156525252005 / baseCoeff,   // 0.25 * sqrt(5 / pi)
	1.0925484305920792  / baseCoeff,   // 0.5 * sqrt(15 / pi)
	1.0925484305920792  / baseCoeff    // 0.5 * sqrt(15 / pi)
};

ivec4 getTileAtlasCoords(uint tile_index, ivec3 localCoords){
	uint x = bitfieldExtract(tile_index, 0, 6);
	uint y = bitfieldExtract(tile_index, 6, 6);
	uint z = bitfieldExtract(tile_index, 12, 6);
	uint w = bitfieldExtract(tile_index, 18, 14);
	return ivec4(ivec3(x, y, z) * VOXELS_PER_SIDE + localCoords, w);
}

void getSampleCoords(ivec3 p, vec3 d, out int tile_index, out ivec4 q, int LOD_OFFSET, isampler2D TileOccupancy) {
	if(any(lessThan(p, ivec3(0))) || any(greaterThanEqual(p, TSDFTilesCount * VOXELS_PER_SIDE / (1<<LOD_OFFSET)))){
		tile_index = -1;
		return;
	}
	
	const ivec3 tile_coords = p / VOXELS_PER_SIDE;
	tile_index = texelFetch(TileOccupancy, tile_coords, 0).x;
	if(tile_index < 0){
		return;
	}
	
	//convert to voxel local coord
	p %= VOXELS_PER_SIDE;
	q = getTileAtlasCoords(tile_index, p);
}

vec4 mixSample(int tile_index[8], ivec4 q[8], int N, vec3 fract_p){

	vec4 v[8];
	for(int i=0; i<8; i++){
		if(tile_index[i] == -2){
			v[i] = N==0 ? vec4(0.1, vec3(0)) : vec4(0.5);
		}else if(tile_index[i] == -1){
			v[i] = N==0 ? vec4(1.0, vec3(0)) : vec4(0.5);
		}else{
			int n = q[i].w * VoxelsTexturesCount + N;
			sampler3D VoxelsTexture = sampler3D(VoxelsTextureHandles[n]);
			v[i] = texelFetch(VoxelsTexture, q[i].xyz, 0);
		}
		
		if(i % 2 == 1){
			v[i] = mix(v[i-1], v[i], fract_p.x);
		}
		
		if(i % 4 == 3){
			v[i] = mix(v[i-2], v[i], fract_p.y);
		}
		
		if(i == 7){
			v[i] = mix(v[i-4], v[i], fract_p.z);
		}
	}
	
	return v[7];
}

vec4 getSample(vec3 camCenter, vec3 ray, int t, int LOD_OFFSET) {
	const vec3 p = ((camCenter - vec3(0.5f)) + ray * float(t)) / (1<<LOD_OFFSET);
	const vec3 floor_p = floor(p);
	const vec3 fract_p = p - floor_p;
	const ivec3 int_p = ivec3(floor_p);
	
	int tile_index[8];
	ivec4 q[8];
	
	{
		isampler2D TileOccupancy = isampler2D(TileOccupancyTextureHandles[min(CURRENT_LOD + LOD_OFFSET, MAX_LOD)]);
		for(int i=0; i<8; i++){
			getSampleCoords(int_p + ivec3((i>>0)&1, (i>>1)&1, (i>>2)&1), ray, tile_index[i], q[i], LOD_OFFSET, TileOccupancy);
		}
	}
	
	float alpha = 0.0;
	vec3 c = vec3(0.0f);
	
	#if SHBands==2
		vec4 v6 = 2.0f * mixSample(tile_index, q, 6, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v5 = 2.0f * mixSample(tile_index, q, 5, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v4 = 2.0f * mixSample(tile_index, q, 4, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v3 = 2.0f * mixSample(tile_index, q, 3, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v2 = 2.0f * mixSample(tile_index, q, 2, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v1 = 2.0f * mixSample(tile_index, q, 1, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v0 = mixSample(tile_index, q, 0, fract_p);
		
		vec3 d = normalize(ray);
		c += vec3(v6.yzw)      * SHcoeffs[8] * (d.x*d.x - d.y*d.y);
		c += vec3(v5.zw, v6.x) * SHcoeffs[7] * (d.x*d.z);
		c += vec3(v4.w, v5.xy) * SHcoeffs[6] * (3.0f*d.z*d.z - 1.0f);
		c += vec3(v4.xyz)      * SHcoeffs[5] * (d.y*d.z);
		c += vec3(v3.yzw)      * SHcoeffs[4] * (d.x*d.y);
		c += vec3(v2.zw, v3.x) * SHcoeffs[3] * d.x;
		c += vec3(v1.w, v2.xy) * SHcoeffs[2] * d.z;
		c += vec3(v1.xyz)      * SHcoeffs[1] * d.y;
		c += vec3(v0.yzw)      * SHcoeffs[0];
		alpha = v0.x;
	#elif SHBands==1
		vec4 v3 = 2.0f * mixSample(tile_index, q, 3, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v2 = 2.0f * mixSample(tile_index, q, 2, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v1 = 2.0f * mixSample(tile_index, q, 1, fract_p) - 1.0f; // send to [-1, 1]
		vec4 v0 = mixSample(tile_index, q, 0, fract_p);
		
		vec3 d = normalize(ray);
		c += vec3(v2.zw, v3.x) * SHcoeffs[3] * d.x;
		c += vec3(v1.w, v2.xy) * SHcoeffs[2] * d.z;
		c += vec3(v1.xyz)      * SHcoeffs[1] * d.y;
		c += vec3(v0.yzw)      * SHcoeffs[0];
		alpha = v0.x;
	#else
		vec4 v0 = mixSample(tile_index, q, 0, fract_p);
		c += vec3(v0.yzw);
		alpha = v0.x;
	#endif
	
	return vec4(alpha, c);
}








