/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

in vec3 position;
out vec3 world_position;

uniform mat4 transform;
uniform mat4 projectionView;
uniform vec3 minCorner;
uniform vec3 maxCorner;

void main(void) {

	world_position = mix(minCorner, maxCorner, position*0.5+0.5);
	gl_Position = projectionView * transform * vec4(world_position, 1.0);
	
}


