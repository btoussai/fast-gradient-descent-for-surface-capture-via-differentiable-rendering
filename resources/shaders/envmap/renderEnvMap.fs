/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core

in vec3 world_position;

uniform vec3 minCorner;
uniform vec3 maxCorner;

layout(binding=0) uniform samplerCube envmap;

out vec4 out_Color;

void main(void) {
	
	vec3 extent = maxCorner - minCorner;
	vec3 center = 0.5*(maxCorner + minCorner);
	vec3 dir = normalize((world_position-center) / extent);

	vec4 c = texture(envmap, dir);
	
	out_Color = c;

}


