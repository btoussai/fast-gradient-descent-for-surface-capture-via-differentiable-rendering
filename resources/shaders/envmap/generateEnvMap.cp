/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

#version 460 core
#extension GL_ARB_shading_language_include :   require

const int GROUP_WIDTH = 8;
layout(local_size_x = GROUP_WIDTH, local_size_y = GROUP_WIDTH, local_size_z = 1) in;

const int GROUP_SIZE = GROUP_WIDTH*GROUP_WIDTH;


layout(binding=0) uniform sampler2DArray srcImages;
layout(binding=0, rgba8) uniform restrict writeonly imageCube dstImage;


#include </common/projection.txt>

uniform vec3 minCorner;
uniform vec3 maxCorner;


void main(void){

	const ivec2 dims = imageSize(dstImage);
	
	const ivec2 faceCoords = ivec2(gl_GlobalInvocationID.xy);
	const int faceID = int(gl_WorkGroupID.z);
	
	const vec2 faceNDC = (vec2(faceCoords) + vec2(0.5f)) / vec2(dims.xy);
	
	const vec3 extent = maxCorner - minCorner;
	

	vec3 worldPos;
	switch(faceID){
		case 0:  //GL_TEXTURE_CUBE_MAP_POSITIVE_X
			worldPos = vec3(maxCorner.x, minCorner.y + (1.0-faceNDC.y) * extent.y, minCorner.z + (1.0-faceNDC.x) * extent.z);
			break;
		case 1:  //GL_TEXTURE_CUBE_MAP_NEGATIVE_X
			worldPos = vec3(minCorner.x, minCorner.y + (1.0-faceNDC.y) * extent.y, minCorner.z + faceNDC.x * extent.z);
			break;
		case 2:  //GL_TEXTURE_CUBE_MAP_POSITIVE_Y
			worldPos = vec3(minCorner.x + faceNDC.x * extent.x, maxCorner.y, minCorner.z + faceNDC.y * extent.z);
			break;
		case 3:	 //GL_TEXTURE_CUBE_MAP_NEGATIVE_Y
			worldPos = vec3(minCorner.x + faceNDC.x * extent.x, minCorner.y, minCorner.z + (1.0-faceNDC.y) * extent.z);
			break;
		case 4:	 //GL_TEXTURE_CUBE_MAP_POSITIVE_Z
			worldPos = vec3(minCorner.x + faceNDC.x * extent.x, minCorner.y + (1.0-faceNDC.y) * extent.y, maxCorner.z);
			break;
		case 5:  //GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
			worldPos = vec3(minCorner.x + (1.0-faceNDC.x) * extent.x, minCorner.y + (1.0-faceNDC.y) * extent.y, minCorner.z);
			break;
		default:
			break;
	}
	
	
	vec4 color = vec4(0, 0, 0, 1);
	if(true){
		const ivec3 texSize = textureSize(srcImages, 0);
		const int NumCameras = texSize.z;
		
		int validCameras = 0;
		for(int camIndex=0; camIndex<NumCameras; camIndex++){
			vec3 NDC = project(worldPos, camIndex);
			NDC.xy /= NDC.z;
			vec2 pixel = toPixels(NDC.xy, camIndex);
	
			bool farEnough = NDC.z > 3.0;
			bool visible = pixel.x > 0 && pixel.y > 0 && pixel.x < texSize.x && pixel.y < texSize.y;
			if(farEnough && visible){
				
				vec4 c = texelFetch(srcImages, ivec3(ivec2(pixel), camIndex), 0);
				color += c;
				validCameras++;
			}
		}
		color /= validCameras;
		
		/*
		float maxDist = 0;
		int bestCamIndex = 0;
		for(int camIndex=0; camIndex<NumCameras; camIndex++){
			vec3 NDC = project(worldPos, camIndex);
			NDC.xy /= NDC.z;
			vec2 pixel = toPixels(NDC.xy, camIndex);
			if(NDC.z > maxDist && pixel.x > 0 && pixel.y > 0 && pixel.x < texSize.x && pixel.y < texSize.y){
				maxDist = NDC.z;
				bestCamIndex = camIndex;
			}
		}
		
		vec3 NDC = project(worldPos, bestCamIndex);
		NDC.xy /= NDC.z;
		vec2 pixel = toPixels(NDC.xy, bestCamIndex);
		color = texelFetch(srcImages, ivec3(ivec2(pixel), bestCamIndex), 0);
		*/
	}
	
	imageStore(dstImage, ivec3(faceCoords, faceID), vec4(color.rgb, 1));
}
