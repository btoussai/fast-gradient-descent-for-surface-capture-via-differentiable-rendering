
This repository contains the code of the paper "Fast Gradient Descent for Surface Capture Via Differentiable Rendering".

# Requirements

- A recent NVidia GPU with at least 16GB of vram, with up to date drivers (it is possible to run the program with less than 16GB of vram but it will crash once it runs out of memory). 
- Windows or Linux.
- Several GB of disk space.

# Installation

A docker container is provided for convenience but it is also possible to manually install the libraries. Read the dockerfile for more details. You don't need docker nor the nvidia container toolkit if you opt for a manual installation.


## Installing docker:

Note that you need sudo to install docker. See
https://docs.docker.com/engine/install/ubuntu/ for more details.

```console
sudo apt-get update
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

Check that docker has been successfully installed:
```console
sudo service docker start
sudo docker run hello-world
```

## Installing the nvidia container toolkit:

See https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html for more details.

```console
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
      && curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | sudo gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
      && curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | \
            sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
            sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list
sudo apt-get update
sudo apt-get install -y nvidia-docker2
sudo systemctl restart docker
```

Check that the toolkit works:
```console
sudo docker run --rm --gpus all nvidia/cuda:11.0.3-base-ubuntu20.04 nvidia-smi
```

## Downloading the datasets and the code

Download the DTU dataset subset used in the IDR project: https://www.dropbox.com/s/ujmakiaiekdl6sh/DTU.zip

Download the nerf syntetic dataset: https://drive.google.com/drive/folders/128yBriW1IG_3NJ5Rp7APSTZsJqdJdfc1 

Clone the repository of the project:
```console
git clone https://gitlab.inria.fr/btoussai/fast-gradient-descent-for-surface-capture-via-differentiable-rendering.git repo
```

The file structure should look like this:

```
somefolder
│   DTU.zip
│   nerf_synthetic.zip    
│
└───repo
    │   README.md
    │   Dockerfile
    │   ...
   
```

Make sure the folder *somefolder* does not contain anything else.

## Building the docker container

```console
unzip -q nerf_synthetic.zip
unzip -q DTU.zip
cd repo
sudo docker build . -t mycontainer:1.0.0
```

The file structure should looks like this after unzipping the datasets:
```
somefolder
│   DTU.zip
│   nerf_synthetic.zip
|   DTU   
|   nerf_synthetic
│
└───repo
    │   README.md
    │   Dockerfile
    │   ...
   
```

## Running the container

Everything should have been installed at this point. You can now run the container.

```console
xhost +local:root 

sudo docker run --rm -it \
--gpus all \
-v /tmp/.X11-unix:/tmp/.X11-unix \
-e DISPLAY=$DISPLAY \
-e QT_X11_NO_MITSHM=1 \
--env LD_LIBRARY_PATH=\
/usr/local/lib64:$LD_LIBRARY_PATH \
--mount type=bind,source="$(pwd)"/../DTU,target=/usr/src/DTU \
--mount type=bind,source="$(pwd)"/../nerf_synthetic,target=/usr/src/nerf_synthetic \
mycontainer:1.0.0
```

The first command enables to run gui programs from the docker container, the second one starts a shell inside the container. The folders **somefolder/DTU** and **somefolder/nerf_synthetic**  are mounted so that they can be accessed from inside the container.

The file structure inside the container looks like this:

```
/usr/src
│   DTU
|   assimp
|   eigen
|   glfw
|   nerf_synthetic
│
└───repo
    │   README.md
    │   Dockerfile
    |   CMakeLists.txt
    |   src
    |   build
    │   ...
```

## Creating the config files

Once inside the container, run the following scripts to build config files for the datasets.

```console
python3 ./scripts/prepare_nerf.py
python3 ./scripts/prepare_dtu.py
```

## Running the program

A gui should appear with the following command:
```console
./build/KinovisReconstruction
```

- You can select a scene in the menu bar: File > import scene config
For instance, try **/usr/src/nerf_synthetic/chair/scene_config.txt**
- Select two spherical harmonic bands in the last slider and click "Load video sequence".
- Take some time to familiarize with the user interface. You can change the font size in the menu bar under "Miscellaneous". Note that any changes will be saved unless you disable "Auto Save" under "Miscellaneous". The parameters of the ui are stored in **repo/params.txt**. Remember that you can exit the container and re-run it to restore everything.
- Toggle "Export Meshes" to export meshes during the reconstruction in **chair/LODn/** where *n* is the level of detail.
- Toogle "Fully automatic mode" to begin the reconstruction.
- You can export rendered images of the object once the reconstruction is finished in the evaluate tab.

# Dependencies

## External dependencies

These libraries are installed when building the docker container.

- glfw (window + OpenGL context creation, zlib license, https://github.com/glfw/glfw)
- assimp (read & write meshes, specific license, https://github.com/assimp/assimp)
- eigen (linear algebra, MPL2 license, https://gitlab.com/libeigen/eigen)

## Internal dependencies

These libraries are packaged in src/

- glad (OpenGL function loader generator, the generated file is included, not the lib, https://github.com/Dav1dde/glad)
- dirent (c++ file & directory queries, MIT license, https://github.com/Dav1dde/glad)
- glm (math library, MIT license, https://github.com/g-truc/glm)
- FileWatcher (file change notifications, MIT License, https://github.com/jameswynn/simplefilewatcher)
- GLText (Draw text with OpenGL, Zlib license, https://github.com/vallentin/glText)
- Dear ImGui (c++ gui library, MIT license, https://github.com/ocornut/imgui)
- ImguiFileDialog (file dialogs for imgui, MIT license, https://github.com/aiekick/ImGuiFileDialog)
- Implot (plots for imgui, MIT license, https://github.com/epezent/implot)
- miniply (fast ply reader, MIT license, https://github.com/vilya/miniply)
- cnpy (read & write npy and npz files, MIT license, https://github.com/rogersce/cnpy)
- stb (c image io, MIT license or Public Domain, https://github.com/nothings/stb)
- TinyXML-2 (c++ xml parser, zlib license, https://github.com/leethomason/tinyxml2)
- libMorton (encode/decode morton codes, MIT license, https://github.com/Forceflow/libmorton)
