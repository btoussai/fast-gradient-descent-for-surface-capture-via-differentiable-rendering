/*
 * InitRenderDoc.cpp
 *
 *  Created on: 15 avr. 2022
 *      Author: Briac
 */

#include "InitRenderDoc.h"

#include <iostream>

#include "renderdoc_app.h"
#include <assert.h>

#ifdef _WIN32
#include <windows.h>
#endif
#ifdef __linux__
#include <dlfcn.h>
#endif

static RENDERDOC_API_1_1_2 *rdoc_api = nullptr;

void InitRenderDoc::init(){
	std::cout <<"Initializing renderdoc..." <<std::endl;

#ifdef _WIN32
//	 At init, on windows
	if(HMODULE mod = GetModuleHandleA("renderdoc.dll"))
	{
	    pRENDERDOC_GetAPI RENDERDOC_GetAPI =
	        (pRENDERDOC_GetAPI)GetProcAddress(mod, "RENDERDOC_GetAPI");
	    int ret = RENDERDOC_GetAPI(eRENDERDOC_API_Version_1_1_2, (void **)&rdoc_api);
	    assert(ret == 1);
	}
#endif

#ifdef __linux__
	// At init, on linux/android.
	if(void *mod = dlopen("librenderdoc.so", RTLD_NOW | RTLD_NOLOAD))
	{
	    pRENDERDOC_GetAPI RENDERDOC_GetAPI = (pRENDERDOC_GetAPI)dlsym(mod, "RENDERDOC_GetAPI");
	    int ret = RENDERDOC_GetAPI(eRENDERDOC_API_Version_1_1_2, (void **)&rdoc_api);
	    assert(ret == 1);
	}
#endif

	if(!rdoc_api){
		std::cout <<"Couldn't load renderdoc dll" <<std::endl;
	}

}

void InitRenderDoc::startFrame() {
	// To start a frame capture, call StartFrameCapture.
	// You can specify NULL, NULL for the device to capture on if you have only one device and
	// either no windows at all or only one window, and it will capture from that device.
	// See the documentation below for a longer explanation
	if(rdoc_api){
		rdoc_api->StartFrameCapture(NULL, NULL);
		std::cout <<"Capturing frame !" <<std::endl;
	}else{
//		std::terminate();
	}
}

void InitRenderDoc::stopFrame() {
	// stop the capture
	if(rdoc_api){
		rdoc_api->EndFrameCapture(NULL, NULL);
		std::cout <<"End of capture !" <<std::endl;
	}else{
//		std::terminate();
	}
}
