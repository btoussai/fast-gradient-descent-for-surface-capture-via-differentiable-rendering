/*
 * InitRenderDoc.h
 *
 *  Created on: 15 avr. 2022
 *      Author: Briac
 */

#ifndef RENDERDOC_INITRENDERDOC_H_
#define RENDERDOC_INITRENDERDOC_H_

class InitRenderDoc {
public:
	static void init();
	static void startFrame();
	static void stopFrame();
};

#endif /* RENDERDOC_INITRENDERDOC_H_ */
