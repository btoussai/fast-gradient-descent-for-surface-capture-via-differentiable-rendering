/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * window.cpp
 *
 *  Created on: 21 mars 2020
 *      Author: Briac
 */

#include "Window.h"

#define GLT_IMPORTS
#include "../GLText/gltext.h"

#include "Camera.h"

#include <math.h>
#include <iostream>
#include <memory>
#include <string>

#include "../imgui/imgui.h"
#include "../imgui/imgui_impl_glfw.h"
#include "../imgui/imgui_impl_opengl3.h"
#include "../ImGuiFileDialog/ImGuiFileDialog.h"

#include "../IconFontCppHeaders/IconsFontAwesome5.h"

#include "../sequence/SceneReferenceFrame.h"
#include "../sequence/CommonTypes.h"
#include "../sequence/SequenceLoader.h"

#include "../sequence/MarchingTest.h"

int imagePyramidLevels = 4;
int segmentationPyramidLevel = 1;

inline void showAllIconsFontAwesome5() {
	if (ImGui::TreeNode("All icons of IconsFontAwesome5")) {
		static ImGuiTableFlags flags = ImGuiTableFlags_SizingFixedFit
				| ImGuiTableFlags_BordersOuter | ImGuiTableFlags_BordersV
				| ImGuiTableFlags_ContextMenuInBody;

		const int columns = 20;
		const int rows = (int) (icon_values.size() + columns - 1) / columns;
		if (ImGui::BeginTable("tableIconsFontAwesome5", columns, flags)) {
			for (int row = 0; row < rows; row++) {
				ImGui::TableNextRow(0, 30);
				for (int column = 0; column < columns; column++) {
					int n = row * columns + column;
					ImGui::TableSetColumnIndex(column);

					if (n < (int) icon_values.size()) {
						ImGui::Text("%s", icon_values[n]);
						if (ImGui::IsItemHovered()) {
							ImGui::BeginTooltip();
							ImGui::PushTextWrapPos(
									ImGui::GetFontSize() * 35.0f);
							ImGui::Text("%s", icon_names[n]);
							ImGui::PopTextWrapPos();
							ImGui::EndTooltip();
						}
					} else {
						ImGui::Text(" ");
					}

				}
			}
			ImGui::EndTable();
		}

		ImGui::TreePop();
	}
}

using namespace ImGui;

static void error_callback(int error, const char *description) {
	fprintf(stderr, "Error: %s\n", description);
	fflush(stderr);
}

static void glad_callback_custom(const char *name, void *funcptr, int len_args,
		...) {
	GLenum error_code;

	(void) funcptr;
	(void) len_args;

	error_code = glad_glGetError();

	if (error_code != GL_NO_ERROR) {
		std::string type("UNKNOWN");
		if (error_code == GL_INVALID_ENUM) {
			type = "GL_INVALID_ENUM";
		} else if (error_code == GL_INVALID_OPERATION) {
			type = "GL_INVALID_OPERATION";
		} else if (error_code == GL_INVALID_VALUE) {
			type = "GL_INVALID_VALUE";
		} else if (error_code == GL_INVALID_INDEX) {
			type = "GL_INVALID_INDEX";
		} else if (error_code == GL_INVALID_FRAMEBUFFER_OPERATION) {
			type = "GL_INVALID_FRAMEBUFFER_OPERATION";
		} else if (error_code == GL_OUT_OF_MEMORY) {
			type = "GL_OUT_OF_MEMORY";
		}

		std::cout << "ERROR " << error_code << " in " << name << " (" << type
				<< ")" << std::endl;

		if (error_code == GL_OUT_OF_MEMORY) {
			throw std::string("OpenGL Fatal Error: Out of memory");
		}
	}
}

static void framebuffer_size_callback(GLFWwindow *window, int width,
		int height) {
	glViewport(0, 0, width, height);
}

static double scroll;
static void scroll_callback(GLFWwindow *window, double xoffset,
		double yoffset) {
	scroll = yoffset;
}
static const ImWchar icons_ranges[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };
static const std::vector<const char*> font_names = { "Default",
		"resources/Roboto-Medium.ttf", "resources/DroidSans.ttf",
		"resources/Cousine-Regular.ttf", "resources/Karla-Regular.ttf",
		"resources/ProggyClean.ttf" };

namespace RenderingBase {

Window::Window(std::string title, int MULTISAMPLE_COUNT) :
		w(NULL), MULTISAMPLE_COUNT(MULTISAMPLE_COUNT) {

	/* Initialize the library */
	if (!glfwInit())
		throw "Error while initializing GLFW";

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_FALSE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_API, GLFW_TRUE);
	glfwWindowHint(GLFW_SAMPLES, MULTISAMPLE_COUNT);
	glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);
	glfwWindowHint(GLFW_FOCUS_ON_SHOW, GLFW_TRUE);

	//Generate errors when the context is lost due to a TDR (Timeout Detection and Recovery)
	//instead of crashing the app miserably
	//This typically happens when a draw call takes too long to complete
	std::cout <<"Window hint: GLFW_LOSE_CONTEXT_ON_RESET" <<std::endl;
	glfwWindowHint(GLFW_CONTEXT_ROBUSTNESS, GLFW_LOSE_CONTEXT_ON_RESET);

	/* Create a windowed mode window and its OpenGL context */
	int& width = params.getInt("width", 800);
	int& height = params.getInt("height", 600);
	width = width==0? 800 : width;
	height = height==0? 600 : height;

	w = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
	if (!w) {
		glfwTerminate();
		throw std::string("Error while creating the window");
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(w);

	/* Load the opengl functions pointers */
	gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);

	std::cout << "Version: " << glGetString(GL_VERSION) << std::endl;
	std::cout << "Vendor: " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION)
			<< std::endl;

	isNVIDIA = std::string((const char*)glGetString(GL_VENDOR)).starts_with("NVIDIA");

	glfwSetErrorCallback(error_callback);
	glfwSetFramebufferSizeCallback(w, framebuffer_size_callback);
	glfwSetScrollCallback(w, scroll_callback);

	glfwSwapInterval(1);

	glEnable(GL_MULTISAMPLE);

	glad_set_post_callback(glad_callback_custom);

	guis.init_IMGUI(w);
	reloadFonts();

	setTitle();
	if (params.getBool("Fullscreen")) {
		toogleFullscreen(params.getBool("Fullscreen"));
	}

	ImGuiFileDialog::Instance()->SetExtentionInfos(".xml", ImVec4(0, 1, 0, 0.9),
	ICON_FA_FILE);
	ImGuiFileDialog::Instance()->SetExtentionInfos(".png,.jpg",
			ImVec4(0, 1, 1, 0.9), ICON_FA_FILE_IMAGE);

	// Initialize glText
	gltInit();

}

Window::~Window() {
	//delete before glfw is terminated
	sequence = nullptr;
//	reconstruction = nullptr;

	gltTerminate();
	glfwTerminate();
}

void Window::reloadFonts() {
	params.getBool("ReloadFonts") = false;

	ImGuiIO &io = ImGui::GetIO();
	io.Fonts->Clear();

	int fontIndex = params.getInt("FontIndex");
	float fontSize = params.getInt("FontSize");
	if (fontIndex == 0) {
		fontSize = 13.0f;
		io.Fonts->AddFontDefault();
	} else {
		io.Fonts->AddFontFromFileTTF(font_names[fontIndex], fontSize);
	}

	// merge in icons from Font Awesome
	ImFontConfig font_config;
	font_config.PixelSnapH = true;
	font_config.MergeMode = true;
	io.Fonts->AddFontFromFileTTF("resources/" FONT_ICON_FILE_NAME_FAS, fontSize,
			&font_config, icons_ranges);
	io.Fonts->Build();
	ImGui_ImplOpenGL3_DestroyDeviceObjects();

}

void Window::setTitle() {
	std::string title = "KinovisReconstruction";
	glfwSetWindowTitle(w, title.c_str());

}

void Window::toogleFullscreen(bool fullScreen) {
	GLFWmonitor *monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode *mode = glfwGetVideoMode(monitor);

	if (fullScreen) {
		glfwSetWindowMonitor(w, monitor, 0, 0, mode->width, mode->height,
				mode->refreshRate);
	} else {
		glfwSetWindowMonitor(w, nullptr, 0, 0, params.getInt("width"),
				params.getInt("height"), mode->refreshRate);
	}

}

static const std::vector<std::string> params_subset = {
		"Window#CalibrationFile",
		"Window#SequenceFile",
		"Window#BackgroundsDir",
		"Window#ExportDir",
		"Window#imagePyramidLevels",
		"Window#segmentationPyramidLevel",
		"SceneReferenceFrame#SceneScale",
		"SceneReferenceFrame#FrustumsLength",
		"SceneReferenceFrame#Rotation",
		"SceneReferenceFrame#Translation",
		"SceneReferenceFrame#SceneScale",
		"SparseGrid#TSDFTilesDim",
		"SparseGrid#VolumeMinCorner",
		"SparseGrid#VoxelSize",
		"DifferentialRendering#activeCameras",
		"SceneReferenceFrame#ScaleMatrixRow0",
		"SceneReferenceFrame#ScaleMatrixRow1",
		"SceneReferenceFrame#ScaleMatrixRow2",
		"SceneReferenceFrame#ScaleMatrixRow3",
};

void Window::OpenDialog() {

	std::function<void(std::string, std::string)> default_action = [this](std::string paramsKey, std::string path){
		params.getString(paramsKey) = path;
	};

	auto dialog = [this](std::string dlgKey, std::string paramsKey, std::function<void(std::string, std::string)> action){
		if (ImGuiFileDialog::Instance()->Display(dlgKey)) {
			if (ImGuiFileDialog::Instance()->IsOk()) {
				std::string filePathName =
						ImGuiFileDialog::Instance()->GetFilePathName();
				std::string filePath =
						ImGuiFileDialog::Instance()->GetCurrentPath();
				action(paramsKey, filePathName);
				params.getString("FileSearchDir") = filePath;
			}

			ImGuiFileDialog::Instance()->Close();
		}
	};

	dialog("ExportSceneConfigDlgKey", "SceneConfigFile", [this](std::string paramsKey, std::string path){
		this->params.saveSubset(path + "/scene_config.txt", params_subset);
	});
	dialog("ChooseSceneConfigDlgKey", "SceneConfigFile", [this](std::string paramsKey, std::string path){
		this->params.getString("SceneConfigPath") = path;
		this->params.loadSubset(path, params_subset);
	});


	dialog("ChooseCalibrationFileDlgKey", "CalibrationFile", default_action);
	dialog("ChooseSequenceFileDlgKey", "SequenceFile", default_action);
	dialog("ChooseBackgroundFoldersDlgKey", "BackgroundsDir", default_action);
	dialog("ChooseExportFolderDlgKey", "ExportDir", default_action);


}

void Window::ShowMainMenuBar(bool &quit) {

//	ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2, 8));
	ImGui::PushStyleVar(ImGuiStyleVar_ItemInnerSpacing, ImVec2(8, 8));

	if (ImGui::BeginMainMenuBar()) {

		if (ImGui::BeginMenu(ICON_FA_FILE " File")) {
			ImVec2 buttonSize = ImVec2(ImGui::GetContentRegionAvailWidth(), 20);

			if (ImGui::Button(ICON_FA_FOLDER_OPEN " Export scene config", buttonSize)) {
				ImGuiFileDialog::Instance()->OpenDialog(
						"ExportSceneConfigDlgKey",
						ICON_FA_FOLDER_OPEN " Export scene config", nullptr,
						params.getString("FileSearchDir", ".").c_str(), "");
			}

			if (ImGui::Button(ICON_FA_FOLDER_OPEN " Import scene config", buttonSize)) {
				ImGuiFileDialog::Instance()->OpenDialog(
						"ChooseSceneConfigDlgKey",
						ICON_FA_FOLDER_OPEN " Choose scene config", ".txt",
						params.getString("FileSearchDir", ".").c_str(), "");
			}

			quit = ImGui::MenuItem("Quit", "");
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Edit")) {
//			if (ImGui::MenuItem("Undo", "CTRL+Z")) {
//			}
//			if (ImGui::MenuItem("Redo", "CTRL+Y", false, false)) {
//			}  // Disabled item
//			ImGui::Separator();
//			if (ImGui::MenuItem("Cut", "CTRL+X")) {
//			}
//			if (ImGui::MenuItem("Copy", "CTRL+C")) {
//			}
			if (ImGui::MenuItem("Paste", "CTRL+V")) {
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Miscellaneous")) {
			ImVec2 buttonSize = ImVec2(ImGui::GetContentRegionAvailWidth(), 20);
			if (ImGui::Checkbox("Fullscreen", &params.getBool("Fullscreen"))) {
				toogleFullscreen(params.getBool("Fullscreen"));
			}
			params.getBool("ReloadFonts") |= ImGui::Combo("Font",
					&params.getInt("FontIndex"), font_names.data(),
					font_names.size(), font_names.size());
			if (params.getInt("FontIndex") != 0) {
				ImGui::SliderInt("Font size", &params.getInt("FontSize"), 13,
						25);
				params.getBool("ReloadFonts") |= ImGui::IsItemDeactivated();
			}
			ImGui::Checkbox("Auto Save", &params.getBool("AutoSave"));
			HelpMarker("Any changes to the parameters will be saved on close, overwriting the scene config file");

			if(ImGui::Button("Save Params", buttonSize)){
				params.save();
			}
			if(ImGui::Button("Reset Params", buttonSize)){
				params.discard();
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("Memory usage")) {

			if(sequence){
				sequence->memUsage(true);
			}

			ImGui::EndMenu();
		}else if(sequence){
			sequence->memUsage(false);
		}

		SceneReferenceFrame::updateMainMenu();

		ImGui::EndMainMenuBar();
	}

	ImGui::PopStyleVar();
//	ImGui::PopStyleVar();
}

void Window::mainloop(int argc, char* argv[]) {
	params.overwriteSubset(argc, argv);
	params.getBool("ShouldQuit") = false;

	params.getVec4("SceneReferenceFrame#ScaleMatrixRow0") = glm::vec4(1, 0, 0, 0);
	params.getVec4("SceneReferenceFrame#ScaleMatrixRow1") = glm::vec4(0, 1, 0, 0);
	params.getVec4("SceneReferenceFrame#ScaleMatrixRow2") = glm::vec4(0, 0, 1, 0);
	params.getVec4("SceneReferenceFrame#ScaleMatrixRow3") = glm::vec4(0, 0, 0, 1);

	if(params.getBool("AutoLoad")){
		params.loadSubset(params.getString("SceneConfigPath"), params_subset);

		SequenceInfo info = SequenceLoader::load(
				params.getString("SequenceFile"),
				params.getString("CalibrationFile"));
		sequence = std::make_unique<Sequence>(info, w);

	}

//
//	MarchingTest();
//	exit(0);

//	int v = -1;
//	glGetIntegerv(GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS, &v);
//	std::cout <<"GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS: " <<v <<std::endl;
//	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &v);
//	std::cout <<"GL_MAX_FRAGMENT_TEXTURE_IMAGE_UNITS: " <<v <<std::endl;
//	glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &v);
//	std::cout <<"GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS: " <<v <<std::endl;

	Camera camera;
	camera.getCameraPos() = glm::vec3(0, 0, 2);

	bool guisHovered = false;
	bool& quit = params.getBool("ShouldQuit");
	while (!glfwWindowShouldClose(this->w) && !quit) {
		if (params.getBool("ReloadFonts")) {
			reloadFonts();
		}

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		ShowMainMenuBar(quit);

		guis.StartWindow(isNVIDIA);

		int width, height;
		glfwGetWindowSize(w, &width, &height);

		if (!params.getBool("Fullscreen")) {
			params.getInt("width") = width;
			params.getInt("height") = height;
		}

		glfwGetFramebufferSize(w, &width, &height);

		//float time = glfwGetTime();

		camera.updateView(w, guisHovered, scroll);

//		showAllIconsFontAwesome5();

		OpenDialog();
//		if (!sequence && !reconstruction) {
		if (!sequence) {
			// Find the calibration file
			if (ImGui::Button(ICON_FA_FOLDER_OPEN " Calibration file")) {
				ImGuiFileDialog::Instance()->OpenDialog(
						"ChooseCalibrationFileDlgKey",
						ICON_FA_FOLDER_OPEN " Choose calibration file", ".xml",
						params.getString("FileSearchDir", ".").c_str(), "");
			}
			ImGui::SameLine();
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "%s",
					params.getString("CalibrationFile").c_str());
			//Find the sequence file
			if (ImGui::Button(ICON_FA_FOLDER_OPEN " Sequence file")) {
				ImGuiFileDialog::Instance()->OpenDialog(
						"ChooseSequenceFileDlgKey",
						ICON_FA_FOLDER_OPEN " Choose sequence file", ".xml",
						params.getString("FileSearchDir", ".").c_str(), "");
			}
			ImGui::SameLine();
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "%s",
					params.getString("SequenceFile").c_str());
			//Find the backgrounds folder
			if (ImGui::Button(ICON_FA_FOLDER_OPEN " Backgrounds folder")) {
				ImGuiFileDialog::Instance()->OpenDialog(
						"ChooseBackgroundFoldersDlgKey",
						ICON_FA_FOLDER_OPEN " Choose backgrounds folder", nullptr,
						params.getString("FileSearchDir", ".").c_str(), "");
			}
			ImGui::SameLine();
			if(params.getString("BackgroundsDir") == ""){
				ImGui::TextColored(ImVec4(1, 0, 0, 1), "Undefined");
			}else{
				ImGui::TextColored(ImVec4(1, 0, 1, 1), "%s",
						params.getString("BackgroundsDir").c_str());
			}
			//Find the export folder
			if (ImGui::Button(ICON_FA_FOLDER_OPEN " Export folder")) {
				ImGuiFileDialog::Instance()->OpenDialog(
						"ChooseExportFolderDlgKey",
						ICON_FA_FOLDER_OPEN " Choose export folder", nullptr,
						params.getString("FileSearchDir", ".").c_str(), "");
			}
			ImGui::SameLine();
			if(params.getString("ExportDir") == ""){
				ImGui::TextColored(ImVec4(1, 0, 0, 1), "Undefined");
			}else{
				ImGui::TextColored(ImVec4(1, 0, 1, 1), "%s",
						params.getString("ExportDir").c_str());
			}

			if (params.getString("CalibrationFile") != ""
					&& params.getString("SequenceFile") != ""
					&& ImGui::Button("Load video sequence")) {

				SequenceInfo info = SequenceLoader::load(
						params.getString("SequenceFile"),
						params.getString("CalibrationFile"));
				sequence = std::make_unique<Sequence>(info, w);

			}


			ImGui::Separator();

			ImGui::SliderInt("Levels in image pyramid", &params.getInt("imagePyramidLevels"), 1, 4);
			ImGui::SliderInt("Pyramid level for segmentation", &params.getInt("segmentationPyramidLevel"), 0, params.getInt("imagePyramidLevels")-1);
			ImGui::SliderInt("Spherical Harmonics bands", &params.getInt("SHBands"), 0, 2);


			if(params.getInt("segmentationPyramidLevel") > params.getInt("imagePyramidLevels")-1){
				params.getInt("segmentationPyramidLevel") = params.getInt("imagePyramidLevels")-1;
			}
			imagePyramidLevels = params.getInt("imagePyramidLevels");
			segmentationPyramidLevel = params.getInt("segmentationPyramidLevel");
			assert(imagePyramidLevels <= 4);

			ImGui::Separator();

			/* Render here */
			glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		} else if(sequence) {
			ImVec2 buttonSize =  ImVec2(ImGui::GetContentRegionAvailWidth(), ImGui::GetTextLineHeight()*1.5f);
			if (ImGui::Button("Close sequence", buttonSize)) {
				sequence = nullptr;
				if(params.getBool("AutoSave")){
					params.saveSubset(params.getString("SceneConfigPath"), params_subset);
				}

			}else{
				/* Render here */
				glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				sequence->updateUI(camera);


				sequence->render(camera);

			}
		}

		ImGui::Separator();

		scroll = 0;

		GLenum status = glGetGraphicsResetStatus();
		if (status != GL_NO_ERROR) {
			std::cerr << "The context has been reset: ";
			if (status == GL_GUILTY_CONTEXT_RESET) {
				std::cerr << "The app has been found guilty." << std::endl;
			} else if (status == GL_INNOCENT_CONTEXT_RESET) {
				std::cerr << "The app has been found innocent." << std::endl;
			} else if (status == GL_UNKNOWN_CONTEXT_RESET) {
				std::cerr << "The cause of the reset is unknown." << std::endl;
			}
			std::cerr
					<< "Anyhow, the app shall crash shortly thereafter so be prepared."
					<< std::endl;
			fflush(stderr);
			throw std::string(
					"OpenGL Fatal Error: A context reset has been detected, recovery is impossible.")
					+ std::string(
							"\nThis situation typically arises when a computation takes too long (more than a few seconds).");
		}

		// Rendering GUIs
		guisHovered = guis.EndWindow();
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		/* Swap front and back buffers */
		glfwSwapBuffers(this->w);

		/* Poll for and process events */
		glfwPollEvents();

	}

	if(params.getBool("AutoSave")){
		params.getBool("AutoLoad") = false;
		params.getBool("Sequence#AutoEval") = false;
		params.getBool("AutoQuit") = false;
		params.getBool("ShouldQuit") = false;
		if(sequence){
			sequence = nullptr;
			params.saveSubset(params.getString("SceneConfigPath"), params_subset);
		}
		params.save();
	}

}

}
