/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * GUIs.cpp
 *
 *  Created on: 10 avr. 2020
 *      Author: Briac
 */

#include "GUIs.h"
#include "../imgui/imgui.h"
#include "../imgui/imgui_impl_glfw.h"
#include "../imgui/imgui_impl_opengl3.h"
#include "../implot/implot.h"

#include <iostream>

namespace RenderingBase {

GUIs::GUIs() {
	params.getBool("MainWindowOpened") = true;
}

GUIs::~GUIs() {
	if(initialized){
		ImPlot::DestroyContext();
		ImGui::DestroyContext();
	}
}

void GUIs::StartWindow(bool isNVIDIA) {

	ImGui::SetNextWindowCollapsed(!params.getBool("MainWindowOpened"), ImGuiCond_Always);

	if(ImGui::Begin("Main window")){
		params.getBool("MainWindowOpened") = true;
	}else{
		params.getBool("MainWindowOpened") = false;
	}

	ImGui::TextColored(ImVec4 { 0, 1, 0, 1 },
			"Average: %.1f ms/frame (%.0f FPS)",
			1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

	if(isNVIDIA){
		ImGui::SameLine(0, -1);

		int memTotal, memCurrentAvailable;
		glGetIntegerv(GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX, &memTotal);
		glGetIntegerv(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, &memCurrentAvailable);
		ImGui::TextColored(ImVec4(0, 1, 0, 1), " Mem: %i / %i Mb", (memTotal-memCurrentAvailable)/1000, memTotal/1000);

	}

}

/**
 * Dessine les éléments de l'interface graphique
 * @return true si l'interface est actuellement utilisée
 */
bool GUIs::EndWindow() {

	ImGui::End();

	return ImGui::IsAnyItemHovered() || ImGui::IsAnyItemActive();
}

/**
 * Initialise imgui
 * @param window
 */
void GUIs::init_IMGUI(GLFWwindow* window) {
	initialized = true;
	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImPlot::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsClassic();

	// Setup Platform/Renderer bindings
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	const char* glsl_version = "#version 130";
	ImGui_ImplOpenGL3_Init(glsl_version);

	// Load Fonts
	// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
	// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
	// - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
	// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
	// - Read 'docs/FONTS.txt' for more instructions and details.
	// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
	//io.Fonts->AddFontDefault();
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
	//ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
	//IM_ASSERT(font != NULL);
}

}
