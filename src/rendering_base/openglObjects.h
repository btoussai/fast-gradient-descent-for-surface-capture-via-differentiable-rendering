/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * openglObjects.h
 *
 *  Created on: 28 mars 2019
 *      Author: Briac
 */

#ifndef OPENGLOBJECTS_H_
#define OPENGLOBJECTS_H_

#include <vector>
#include <map>
#include <iostream>
#include <string>
#include <sstream>
#include <string_view>
#include <inttypes.h>
#include <list>
#include <algorithm>
#include <unordered_map>
#include <functional>

#include "../opengl_loader/glad/glad.h"
#include "../glm/vec2.hpp"
#include "../glm/vec3.hpp"
#include "../glm/vec4.hpp"
#include "../glm/mat3x3.hpp"
#include "../glm/mat4x4.hpp"


namespace RenderingBase {

class VBO {
public:
	VBO(GLuint type);
	virtual ~VBO();

	VBO(VBO&& vbo);
	VBO& operator=(VBO&&);

	VBO(const VBO&) = delete;
	VBO& operator=(const VBO&) = delete;


	void bind();
	void unbind();
	void bindAs(GLuint type);
	void unbindAs(GLuint type);
	void storeData(const GLvoid* data, GLsizeiptr dataSize, GLuint usage);
	void updateData(const GLvoid* data, GLsizeiptr dataSize);
	void clearData(GLenum internalformat, GLenum format, GLenum type, const GLvoid *data);

	GLuint setType(GLuint type){
		GLuint oldType = this->type;
		this->type = type;
		return oldType;
	}

	GLuint getID() const{
		return ID;
	}

	/**
	 * @return The size of the vbo in bytes
	 */
	GLsizeiptr getDatasize() const{
		return dataSize;
	}

	int getAttribNumber() const{
		return attribNumber;
	}

	void setAttribNumber(int attribNumber){
		this->attribNumber = attribNumber;
	}

	void setDataSize(GLuint dataSize){
		this->dataSize = dataSize;
	}

private:
	GLuint ID;
	GLuint type;
	GLsizeiptr dataSize;
	int attribNumber = -1;
};

class VAO {
public:
	VAO();
	virtual ~VAO();

	VAO(VAO&& vao);
	VAO& operator=(VAO&&);

	VAO(const VAO&) = delete;
	VAO& operator=(const VAO&) = delete;


	void bind() const;
	void unbind() const;

	uint32_t getIndexCount() const {
		return indexCount;
	}

	VBO* getIndexVBO() {
		return indexVBO;
	}

	std::vector<VBO*>& getAttributeVBOs() {
		return vbos;
	}

	const std::vector<VBO*>& getAttributeVBOs() const {
		return vbos;
	}

	void bindAttribute(uint32_t attribute) const;
	void unbindAttribute(uint32_t attribute) const;
	void bindAttributes(const std::initializer_list<uint32_t>& attributes) const{
		for(uint32_t attribute : attributes){
			bindAttribute(attribute);
		}
	}
	void unbindAttributes(const std::initializer_list<uint32_t>& attributes) const{
		for(uint32_t attribute : attributes){
			unbindAttribute(attribute);
		}
	}

	void createIndexBuffer(const uint32_t* indices, uint32_t indexCount);
	void createFloatAttribute(uint32_t attribNumber, const float* data,
			uint32_t dataArrayLength, uint32_t size, uint32_t stride, GLenum usage =
			GL_STATIC_DRAW, std::size_t offset=0);
	void createIntAttribute(uint32_t attribNumber, const int32_t* data,
			uint32_t dataArrayLength, uint32_t size, uint32_t stride, GLenum usage =
			GL_STATIC_DRAW, std::size_t offset=0);

private:

	GLuint ID;
	VBO* indexVBO;
	uint32_t indexCount;

	std::vector<VBO*> vbos;
};

class ShaderPreprocessor;

class Shader {
public:
	Shader(const char* computeFilePath);
	Shader(const char* vertexFilePath, const char* fragmentFilePath);
	Shader(const char* vertexFilePath, const char* geometryFilePath,
			const char* fragmentFilePath);
	Shader(const char* vertexFilePath, const char* tessellationControlFilePath, const char* tessellationEvaluationFilePath, const char* geometryFilePath,
			const char* fragmentFilePath);
	Shader(const std::string& computeFile);
	Shader(const std::string& vertexFile, const std::string& fragmentFile);
	Shader(const std::string& vertexFile, const std::string& geometryFile,
			const std::string& fragmentFile);
	Shader(const std::string& vertexFile, const std::string& tessellationControlFile, const std::string& tessellationEvaluationFile, const std::string& geometryFile,
			const std::string& fragmentFile);

	Shader(Shader&&) = delete;
	Shader& operator=(Shader&& shader) = delete;
	Shader& operator=(const Shader& shader) = delete;
	Shader(const Shader& shader) = delete;

	virtual ~Shader();

	void start() const;
	void stop() const;
	void finishInit();

	void bindVertexAttribute(GLuint attribute, const char* variableName);

	void bindFragDataLocation(GLuint colorAttachment, const char* variableName);

	void connectTextureUnit(const std::string& sampler_name, GLint value);

	void loadInt(const std::string& name, GLint value) const;
	void loadInt(const std::string& name, GLuint value) const;
	void loadFloat(const std::string& name, float value) const;
	void loadVec2(const std::string& name, glm::vec2 v) const;
	void loadiVec2(const std::string& name, glm::ivec2 v) const;
	void loaduVec2(const std::string& name, glm::uvec2 v) const;
	void loadVec3(const std::string& name, glm::vec3 v) const;
	void loadiVec3(const std::string& name, glm::ivec3 v) const;
	void loaduVec3(const std::string& name, glm::uvec3 v) const;
	void loadVec4(const std::string& name, glm::vec4 v) const;
	void loadiVec4(const std::string& name, glm::ivec4 v) const;
	void loaduVec4(const std::string& name, glm::uvec4 v) const;
	void loadMat3(const std::string& name, glm::mat3 mat) const;
	void loadMat4(const std::string& name, glm::mat4 mat) const;



	GLint operator[](const std::string& name) {
		return uniforms[name];
	}

	void init_uniforms(const std::vector<std::string>& names);

	GLint getProgramID() const {
		return programID;
	}

	static void setPreprocessor(ShaderPreprocessor* preprocessor){
		Shader::preprocessor = preprocessor;
	}

	static void buildNamedString(const char* path, const char* name);

private:

	GLint programID;
	GLint computeShaderID = 0;
	GLint vertexShaderID = 0;
	GLint tessControlShaderID = 0;
	GLint tessEvaluationShaderID = 0;
	GLint geometryShaderID = 0;
	GLint fragmentShaderID = 0;
	std::map<std::string, GLint> uniforms;

	GLint loadFromFile(const char* filePath, GLuint programType);
	GLint loadFromSource(const std::string& file, GLuint programType);
	GLint getUniformLocation(const char* variableName);

	GLint findUniformLoc(const std::string& name) const {
		const auto& it = uniforms.find(name);
		if (it == end(uniforms)) {
			throw std::string("Error, unknown uniform variable name: ") + std::string(name);
		}
		return it->second;
	}

	static ShaderPreprocessor* preprocessor;
	static std::vector<const char*> includes;

};

class ShaderPreprocessor{
public:
	ShaderPreprocessor(){
		Shader::setPreprocessor(this);
	}

	ShaderPreprocessor(std::unordered_map<std::string, std::string> replacements) :
		replacements(replacements.begin(), replacements.end())
	{
		Shader::setPreprocessor(this);
	}

	~ShaderPreprocessor(){
	}

	void checkAndReplace(std::string& contents, std::string filePath);

	void setReplacements(
			const std::unordered_map<std::string, std::string> &replacements) {
		this->replacements.assign(replacements.begin(), replacements.end());
	}

private:
	std::vector<std::pair<std::string, std::string>> replacements;
};

typedef struct _AttachmentFormat {
	int internalFormat;
	int format;
	int attachment;
	bool isColorAttachment;
	int dataType;
} AttachmentFormat;

static const AttachmentFormat AttachmentFormat_RGB = { GL_RGB, GL_RGB,
GL_COLOR_ATTACHMENT0, true, GL_FLOAT };

static const AttachmentFormat AttachmentFormat_RGB16F = { GL_RGB16F, GL_RGB,
GL_COLOR_ATTACHMENT0, true, GL_FLOAT };
static const AttachmentFormat AttachmentFormat_RGB32F = { GL_RGB32F, GL_RGB,
GL_COLOR_ATTACHMENT0, true, GL_FLOAT };

static const AttachmentFormat AttachmentFormat_RGBA = { GL_RGBA, GL_RGBA,
GL_COLOR_ATTACHMENT0, true, GL_FLOAT };
static const AttachmentFormat AttachmentFormat_RGBA16F = { GL_RGBA16F, GL_RGBA,
GL_COLOR_ATTACHMENT0, true, GL_FLOAT };
static const AttachmentFormat AttachmentFormat_RGBA32F = { GL_RGBA32F, GL_RGBA,
GL_COLOR_ATTACHMENT0, true, GL_FLOAT };

static const AttachmentFormat AttachmentFormat_DEPTH_COMPONENT = {
GL_DEPTH_COMPONENT,
GL_DEPTH_COMPONENT, GL_DEPTH_ATTACHMENT, false, GL_FLOAT };
/**
 * Warning: Doesn't work !
 */
static const AttachmentFormat AttachmentFormat_DEPTH24_STENCIL8 = {
GL_DEPTH24_STENCIL8,
GL_DEPTH_STENCIL, GL_DEPTH_STENCIL_ATTACHMENT, false, GL_UNSIGNED_INT_24_8 };

/**
 * Warning: Doesn't work !
 */
static const AttachmentFormat AttachmentFormat_DEPTH32_STENCIL8 = {
GL_DEPTH32F_STENCIL8,
GL_DEPTH_STENCIL, GL_DEPTH_STENCIL_ATTACHMENT, false,
GL_FLOAT_32_UNSIGNED_INT_24_8_REV };

class FBOAttachment {
public:
	FBOAttachment(GLuint ID, bool isTexture, bool is_multisampled, const AttachmentFormat& format,
			const std::string& name, int colorAttachmentNumber, GLuint target) :
			ID(ID),
			is_texture(isTexture),
			is_multisampled(is_multisampled),
			format(format),
			name(name),
			colorAttachmentNumber(colorAttachmentNumber),
			target(target) {

	}

	void bind() const{
			glBindTexture(target, ID);
		}

	void unbind() const{
		glBindTexture(target, 0);
	}



	void bindAsTexture(int textureUnit) const{
		if (!is_texture) {
			throw "Error, the FBOAttachment " + name + " isn't a texture";
		}
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(target, ID);
	}

	void bindAsTexture(int textureUnit, GLenum filter, GLenum border) const{
		bindAsTexture(textureUnit);

		if(is_multisampled){
			throw std::string("Filtering is not available for multisampled textures");
		}

		glTexParameteri(target, GL_TEXTURE_MAG_FILTER, filter);
		glTexParameteri(target, GL_TEXTURE_MIN_FILTER, filter);
		glTexParameteri(target, GL_TEXTURE_WRAP_S, border);
		glTexParameteri(target, GL_TEXTURE_WRAP_T, border);
	}

	void unbindAsTexture(int textureUnit) const{
		if (!is_texture) {
			throw "Error, the FBOAttachment " + name + " isn't a texture";
		}
		glActiveTexture(GL_TEXTURE0 + textureUnit);
		glBindTexture(target, 0);
	}

	int getID() const{
		return ID;
	}

	bool isTexture() const{
		return is_texture;
	}
	bool isMultisampled() const{
		return is_multisampled;
	}
	const AttachmentFormat getFormat() const{
		return format;
	}

	const std::string& getName() const{
		return name;
	}

	GLenum getColorAttachmentNumber() const{
		return colorAttachmentNumber;
	}

	GLuint getTarget() const{
		return target;
	}

private:
	GLuint ID;
	bool is_texture;//it is a renderbuffer otherwise
	bool is_multisampled;
	AttachmentFormat format;
	std::string name;
	GLenum colorAttachmentNumber;
	GLuint target;

};

class FBO {
public:
	static void blitFBOs(const FBO* src, glm::vec4 srcRegion, std::initializer_list<std::string> readColorAttachments, const FBO* dest,
			glm::vec4 destRegion, std::initializer_list<std::string> drawColorAttachments, int mask, bool pixelSizes);

	FBO(std::string name, int MULTISAMPLE_COUNT = 0);
	FBO(std::string name, int width, int height, int MULTISAMPLE_COUNT = 0);
	virtual ~FBO();

	GLint getID() {
		return ID;
	}

	int getWidth() const{
		return width;
	}
	int getHeight() const{
		return height;
	}
	int getMultisampleCount() const{
		return MULTISAMPLE_COUNT;
	}
	glm::vec4 getRegion() const{
		return glm::vec4(0, 0, width, height);
	}
	glm::vec4 getNDCRegion() const{
		return glm::vec4(0, 0, 1, 1);
	}
	const std::string& getName() const{
		return name;
	}

	void bind();
	void unbind();
	void setViewport();
	void resize(int width, int height);

	void addTextureAttachment(const std::string& name,
			const AttachmentFormat& format);

	void createRenderBufferAttachement(const std::string& name,
			const AttachmentFormat& format);

	void bindColorAttachments(
			std::initializer_list<std::string> colorAttachments);

	void bindNoColorBuffers();

	bool finish();

	void clearColorAttachment(const std::string& attachmentName,
			glm::vec4 color);
	void clearDepthAttachment(float value);
	void clearStencilAttachment(int value);

	const FBOAttachment* getAttachment(const std::string& attachmentName) const{
		const auto it = attachments.find(attachmentName);
		if (it == end(attachments)) {
			std::string allAttachmentNames = "";
			for(const auto&[name, attachment] : attachments){
				allAttachmentNames += name + " ";
			}
			throw "Error, unknown FBO attachment name: "
					+ attachmentName + " for fbo " + name + ", all attachments: " + allAttachmentNames;
		} else {
			return it->second;
		}
	}

private:
	FBO& operator=(const FBO& fbo) = delete;
	FBO(const FBO& fbo) = delete;
	FBO(FBO&& fbo) = delete;
	void operator=(FBO&& fbo) = delete;

	GLuint ID;
	int width;
	int height;
	int MULTISAMPLE_COUNT;
	std::string name;

	int colorAttachments = 0;
	std::map<std::string, FBOAttachment*> attachments;
	std::vector<GLenum> drawBuffers; //the color attachments which are currently bound

};

class Query{
private:
	Query& operator=(const Query&) = delete;
	Query(const Query&) = delete;
	Query(Query&&) = delete;
	void operator=(Query&&) = delete;

	GLuint ID;
	GLenum type;
	GLsync s = {};

public:
	Query(GLenum type) : ID(0), type(type){
		glGenQueries(1, &ID);
	}

	~Query(){
		glDeleteQueries(1, &ID);
		glDeleteSync(s);
	}

	void begin(){
		glBeginQuery(type, ID);
	}

	void end(){
		glEndQuery(type);
		s = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	}

	/**
	 * Only if the query is GL_TIMESTAMP
	 */
	void queryCounter(){
		glQueryCounter(ID, type);
		s = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	}

	bool resultAvailable(){
		return glClientWaitSync(s, GL_SYNC_FLUSH_COMMANDS_BIT, 0) == GL_ALREADY_SIGNALED;
	}

	bool getResultNoWait(int64_t& res){
		if(!resultAvailable()){
			return false;
		}

		GLint status = GL_FALSE;
		glGetQueryObjectiv(ID, GL_QUERY_RESULT_AVAILABLE, &status);

		if(status == GL_TRUE){
			glGetQueryObjecti64v(ID, GL_QUERY_RESULT, &res);
		}
		return true;
	}

	void getResult(int64_t& res){
		glGetQueryObjecti64v(ID, GL_QUERY_RESULT, &res);
	}
};

class QueryBuffer{
private:
	QueryBuffer& operator=(const QueryBuffer&) = delete;
	QueryBuffer(const QueryBuffer&) = delete;


	GLenum type;
	std::list<Query> buffer;
	int64_t lastResult = 0;
	int max_size;
public:
	QueryBuffer(QueryBuffer&&) = default;
	QueryBuffer& operator=(QueryBuffer&&) = default;

	QueryBuffer(GLenum type, int max_size = 10) : type(type), max_size(max_size){

	};
	~QueryBuffer() = default;
	Query& push_back(){
		if(size() > max_size){
			buffer.pop_front();
		}
		return buffer.emplace_back(type);
	}
	Query* last(){
		if(buffer.size() > 0){
			return &buffer.back();
		}
		return nullptr;
	}
	void scoped_commands(const std::function<void(void)>& f){
		Query& q = push_back();
		q.begin();
		f();
		q.end();
	};

	int size() const{
		return buffer.size();
	}
	bool resultAvailable(){
		if(size() > 0){
			return buffer.front().resultAvailable();
		}
		return false;
	}
	int64_t getLastResult(bool update=true) {
		if(update){
			while(getResultAndPopFrontIfAvailable(lastResult));
		}
		return lastResult;
	}
	bool getResultAndPopFrontIfAvailable(int64_t& res){
		if(buffer.empty()){
			return false;
		}
		if(buffer.front().getResultNoWait(res)){
			buffer.pop_front();
			lastResult = res;
			return true;
		}
		return false;
	}
};

class GLStopWatch{
private:
	QueryBuffer starts = QueryBuffer(GL_TIMESTAMP);
	QueryBuffer ends = QueryBuffer(GL_TIMESTAMP);

	GLStopWatch& operator=(const GLStopWatch&) = delete;
	GLStopWatch(const GLStopWatch&) = delete;
	GLStopWatch(GLStopWatch&&) = delete;
	void operator=(GLStopWatch&&) = delete;
public:
	GLStopWatch() = default;
	~GLStopWatch() = default;

	void start(){
		Query& q = starts.push_back();
		q.queryCounter();
	}

	void end(){
		Query& q = ends.push_back();
		q.queryCounter();
	}

	int64_t getDelta(){
		if(starts.resultAvailable() && ends.resultAvailable()){
			return ends.getLastResult(true) - starts.getLastResult(true);
		}
		return ends.getLastResult(false) - starts.getLastResult(false);
	}
};

}

#endif /* OPENGLOBJECTS_H_ */
