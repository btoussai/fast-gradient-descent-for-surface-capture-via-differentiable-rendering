/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Parameters.h
 *
 *  Created on: 13 mars 2021
 *      Author: Briac
 */

#ifndef SRC_PARAMETERS_H_
#define SRC_PARAMETERS_H_

#include <string>
#include <sstream>
#include <unordered_map>
#include <memory>
#include <vector>

#include "../glm/vec2.hpp"
#include "../glm/vec3.hpp"
#include "../glm/vec4.hpp"

namespace RenderingBase {

class Parameters {
public:
	Parameters(const std::string& className);
	virtual ~Parameters();

	Parameters extend(const std::string& className){
		return Parameters(this->className + "#" + className);
	}

	std::string& getString(const std::string &name, const std::string& defaultValue = "") const;
	int& getInt(const std::string &name, int defaultValue = 0) const;
	float& getFloat(const std::string &name, float defaultValue = 0) const;
	bool& getBool(const std::string &name, bool defaultValue = 0) const;
	glm::vec2& getVec2(const std::string &name, glm::vec2 defaultValue = glm::vec2(0)) const;
	glm::vec3& getVec3(const std::string &name, glm::vec3 defaultValue = glm::vec3(0)) const;
	glm::vec4& getVec4(const std::string &name, glm::vec4 defaultValue = glm::vec4(0)) const;
	glm::ivec4& getiVec4(const std::string &name, glm::ivec4 defaultValue = glm::ivec4(0)) const;

	struct Registrable{
		Registrable() = default;
		virtual void init(const std::string& s) = 0;
		virtual std::string toString() const = 0;
		virtual ~Registrable() {
		}

		void read(std::stringstream& ss, bool& v) const{
			ss >> v;
		}
		void read(std::stringstream& ss, int& v) const{
			ss >> v;
		}
		void read(std::stringstream& ss, float& v) const{
			ss >> v;
		}
		void read(std::stringstream& ss, glm::vec3& v) const{
			ss >> v.x >> v.y >> v.z;
		}
		void read(std::stringstream& ss, glm::vec4& v) const{
			ss >> v.x >> v.y >> v.z >> v.w;
		}

		void write(std::stringstream& ss, bool v) const{
			ss <<v <<" ";
		}
		void write(std::stringstream& ss, int v) const{
			ss <<v <<" ";
		}
		void write(std::stringstream& ss, float v) const{
			ss <<v <<" ";
		}
		void write(std::stringstream& ss, glm::vec3 v) const{
			ss <<v.x <<" " <<v.y <<" " <<v.z <<" ";
		}
		void write(std::stringstream& ss, glm::vec4 v) const{
			ss <<v.x <<" " <<v.y <<" " <<v.z <<" " <<v.w <<" ";
		}
	};

	void putRegistrable(const std::string &name, std::shared_ptr<Parameters::Registrable> defaultValue);
	Registrable& get(const std::string & name) const;

	void save(){
		writeParams();
	}
	void discard();

	void saveSubset(std::string path, std::vector<std::string> elements);
	void loadSubset(std::string path, std::vector<std::string> elements);
	void overwriteSubset(int argc, char* argv[]);

private:
	std::string className;

	template<typename T> T& getInternal(std::unordered_map<std::string, T>& m, std::string name, const T& defaultValue) const;
	static void readParams();
	static void writeParams();
};

} /* namespace Rendering */

#endif /* SRC_PARAMETERS_H_ */
