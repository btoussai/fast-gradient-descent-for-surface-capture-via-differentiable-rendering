/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * GUIs.h
 *
 *  Created on: 10 avr. 2020
 *      Author: Briac
 */

#ifndef SRC_GUIS_H_
#define SRC_GUIS_H_

#include "../opengl_loader/glad/glad.h"
#include <GLFW/glfw3.h>

#include "Parameters.h"

namespace RenderingBase {

class GUIs {
public:
	GUIs();
	virtual ~GUIs();

	void init_IMGUI(GLFWwindow* window);
	void StartWindow(bool isNVIDIA);
	bool EndWindow();

private:
	bool initialized = false;
	Parameters params = Parameters("GUIs");
};

}

#endif /* SRC_GUIS_H_ */
