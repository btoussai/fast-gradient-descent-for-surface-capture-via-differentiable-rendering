/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Parameters.cpp
 *
 *  Created on: 13 mars 2021
 *      Author: Briac
 */

#include "Parameters.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <string>
#include <unordered_set>
#include <mutex>

namespace RenderingBase {

struct allMaps{
	int instances = 0;
	std::unordered_map<std::string, std::string> stringParams;
	std::unordered_map<std::string, int> intParams;
	std::unordered_map<std::string, float> floatParams;
	std::unordered_map<std::string, bool> boolParams;
	std::unordered_map<std::string, glm::vec2> vec2Params;
	std::unordered_map<std::string, glm::vec3> vec3Params;
	std::unordered_map<std::string, glm::vec4> vec4Params;
	std::unordered_map<std::string, glm::ivec4> ivec4Params;
	std::unordered_map<std::string, std::shared_ptr<Parameters::Registrable>> customParams;
};
static allMaps* maps;

struct DefaultRegistrable : Parameters::Registrable{
	std::string s;
	DefaultRegistrable(const std::string& s) : s(s){

	}
	virtual ~DefaultRegistrable() {

	}

	virtual void init(const std::string& s) override{

	}
	virtual std::string toString() const override{
		return s;
	}
};

Parameters::Parameters(const std::string &className) :
		className(className) {
	if (maps == nullptr) {
		maps = new allMaps{};
		readParams();
	}

	maps->instances++;
}

Parameters::~Parameters() {
	maps->instances--;

	if (maps->instances == 0) {
		delete maps;
		maps = nullptr;
	}
}

template<typename T>
T& Parameters::getInternal(std::unordered_map<std::string, T>& m, std::string name, const T& defaultValue) const{
	auto p = m.find(name);
	if (p == m.end()) {
		m[name] = defaultValue;
		return m[name];
	} else {
		return p->second;
	}
}

/**
 * For single-line strings only!
 */
std::string& Parameters::getString(const std::string &name, const std::string& defaultValue) const{
	auto& m = maps->stringParams;
	std::string n = name.find('#') == std::string::npos ? className + "#" + name : name;
	return getInternal(m, n, defaultValue);
}

int& Parameters::getInt(const std::string &name, int defaultValue) const{
	auto& m = maps->intParams;
	std::string n = name.find('#') == std::string::npos ? className + "#" + name : name;
	return getInternal(m, n, defaultValue);
}

float& Parameters::getFloat(const std::string &name, float defaultValue) const{
	auto& m = maps->floatParams;
	std::string n = name.find('#') == std::string::npos ? className + "#" + name : name;
	return getInternal(m, n, defaultValue);
}

bool& Parameters::getBool(const std::string &name, bool defaultValue) const{
	auto& m = maps->boolParams;
	std::string n = name.find('#') == std::string::npos ? className + "#" + name : name;
	return getInternal(m, n, defaultValue);
}

glm::vec2& Parameters::getVec2(const std::string &name, glm::vec2 defaultValue) const{
	auto& m = maps->vec2Params;
	std::string n = name.find('#') == std::string::npos ? className + "#" + name : name;
	return getInternal(m, n, defaultValue);
}

glm::vec3& Parameters::getVec3(const std::string &name, glm::vec3 defaultValue) const{
	auto& m = maps->vec3Params;
	std::string n = name.find('#') == std::string::npos ? className + "#" + name : name;
	return getInternal(m, n, defaultValue);
}

glm::vec4& Parameters::getVec4(const std::string &name, glm::vec4 defaultValue) const{
	auto& m = maps->vec4Params;
	std::string n = name.find('#') == std::string::npos ? className + "#" + name : name;
	return getInternal(m, n, defaultValue);
}

glm::ivec4& Parameters::getiVec4(const std::string &name, glm::ivec4 defaultValue) const{
	auto& m = maps->ivec4Params;
	std::string n = name.find('#') == std::string::npos ? className + "#" + name : name;
	return getInternal(m, n, defaultValue);
}

void Parameters::putRegistrable(const std::string &name, std::shared_ptr<Parameters::Registrable> defaultValue){
	auto p = maps->customParams.find(className + "#" + name);
	if (p != maps->customParams.end()) {
		DefaultRegistrable* value = dynamic_cast<DefaultRegistrable*>(p->second.get());
		if(value == nullptr){
			std::cout <<"Warning: Attempted to register an already existing param on "
					<<className << "#" << name
					<<", the already existing param will be kept"
					<<std::endl;
			return;
		}
		defaultValue->init(value->s);
	}
	maps->customParams[className + "#" + name] = defaultValue;
}

Parameters::Registrable& Parameters::get(const std::string &name) const{
	auto p = maps->customParams.find(name.find('#') == std::string::npos ? className + "#" + name : name);
	if (p == maps->customParams.end()) {
		maps->customParams[className + "#" + name] = std::make_unique<DefaultRegistrable>("");
		return *maps->customParams[className + "#" + name].get();
	} else {
		return *p->second.get();
	}
}


void Parameters::readParams() {
	std::ifstream f;
	f.open("params.txt");

	if (!f.is_open()) {
		std::cout << "Couldn't open params.txt" << std::endl;
		return;
	}
	std::cout << "Reading params.txt" << std::endl;

	std::string str;
	std::stringstream ss;

	while (std::getline(f, str)) {
		ss = std::stringstream(str);

//		std::cout << str << std::endl;

		std::string name, type;
		ss >> name >> type;

		if(type == "string"){
			std::string s;
			std::getline(ss, s);
			if(s.size() > 1){
				maps->stringParams[name] = s.substr(1, std::string::npos);
			}else{
				maps->stringParams[name] = "";
			}
		}else if (type == "int") {
			int v;
			ss >> v;
			maps->intParams[name] = v;
		} else if (type == "float") {
			float v;
			ss >> v;
			maps->floatParams[name] = v;
		} else if (type == "bool") {
			bool v;
			ss >> v;
			maps->boolParams[name] = v;
		} else if (type == "vec2") {
			float x, y;
			ss >> x >> y;
			maps->vec2Params[name] = glm::vec2(x, y);
		} else if (type == "vec3") {
			float x, y, z;
			ss >> x >> y >> z;
			maps->vec3Params[name] = glm::vec3(x, y, z);
		} else if (type == "vec4") {
			float x, y, z, w;
			ss >> x >> y >> z >> w;
			maps->vec4Params[name] = glm::vec4(x, y, z, w);
		} else if (type == "ivec4") {
			int x, y, z, w;
			ss >> x >> y >> z >> w;
			maps->ivec4Params[name] = glm::ivec4(x, y, z, w);
		} else if (type == "custom") {
			std::string s;
			int n1 = 0, n2 = 0;
			do{
				std::string tmp;
				ss >> tmp;
				s += " " + tmp;

				n1 = (int)std::count(s.begin(), s.end(), '{');
				n2 = (int)std::count(s.begin(), s.end(), '}');

			}while(n1 != n2);
			s = s.substr(2, s.size()-4);
			maps->customParams[name] = std::make_unique<DefaultRegistrable>(s);
		}

	}

}

void Parameters::writeParams() {
	std::ofstream f;
	f.open("params.txt");

	if (!f.is_open()) {
		std::cout << "Couldn't open params.txt" << std::endl;
		return;
	}
	std::cout << "Writing params.txt" << std::endl;

	std::vector<std::string> v;

	for (const auto& [name, value] : maps->stringParams) {
		v.push_back(name + " " + "string" + " " + value);
	}
	for (const auto& [name, value] : maps->intParams) {
		v.push_back(name + " " + "int" + " " + std::to_string(value));
	}
	for (const auto& [name, value] : maps->floatParams) {
		v.push_back(name + " " + "float" + " " + std::to_string(value));
	}
	for (const auto& [name, value] : maps->boolParams) {
		v.push_back(name + " " + "bool" + " " + std::to_string(value));
	}
	for (const auto& [name, value] : maps->vec2Params) {
		v.push_back(name + " " + "vec2" + " " + std::to_string(value.x) + " " + std::to_string(value.y));
	}
	for (const auto& [name, value] : maps->vec3Params) {
		v.push_back(name + " " + "vec3" + " " + std::to_string(value.x) + " " + std::to_string(value.y) + " " + std::to_string(value.z));
	}
	for (const auto& [name, value] : maps->vec4Params) {
		v.push_back(name + " " + "vec4" + " " + std::to_string(value.x) + " " + std::to_string(value.y) + " " + std::to_string(value.z) + " " + std::to_string(value.w));
	}
	for (const auto& [name, value] : maps->ivec4Params) {
		v.push_back(name + " " + "ivec4" + " " + std::to_string(value.x) + " " + std::to_string(value.y) + " " + std::to_string(value.z) + " " + std::to_string(value.w));
	}
	for (const auto& [name, value] : maps->customParams) {
		v.push_back(name + " custom " + "{" + value->toString() + "}");
	}
	std::sort(v.begin(), v.end());

	for (const std::string &s : v) {
		f << s << std::endl;
	}
}

void Parameters::discard(){
	delete maps;
	maps = new allMaps{};
	readParams();
}


void Parameters::saveSubset(std::string path,
		std::vector<std::string> elements) {

	std::unordered_set<std::string> set(elements.begin(), elements.end());

	std::ofstream f;
	f.open(path);

	if (!f.is_open()) {
		std::cout << "Couldn't open " <<path << std::endl;
		return;
	}
	std::cout << "Writing " <<path << std::endl;

	std::vector<std::string> v;

	for (const auto& [name, value] : maps->stringParams) {
		if(set.contains(name))
			v.push_back(name + " " + "string" + " " + value);
	}
	for (const auto& [name, value] : maps->intParams) {
		if(set.contains(name))
		v.push_back(name + " " + "int" + " " + std::to_string(value));
	}
	for (const auto& [name, value] : maps->floatParams) {
		if(set.contains(name))
		v.push_back(name + " " + "float" + " " + std::to_string(value));
	}
	for (const auto& [name, value] : maps->boolParams) {
		if(set.contains(name))
		v.push_back(name + " " + "bool" + " " + std::to_string(value));
	}
	for (const auto& [name, value] : maps->vec2Params) {
		if(set.contains(name))
		v.push_back(name + " " + "vec2" + " " + std::to_string(value.x) + " " + std::to_string(value.y));
	}
	for (const auto& [name, value] : maps->vec3Params) {
		if(set.contains(name))
		v.push_back(name + " " + "vec3" + " " + std::to_string(value.x) + " " + std::to_string(value.y) + " " + std::to_string(value.z));
	}
	for (const auto& [name, value] : maps->vec4Params) {
		if(set.contains(name))
		v.push_back(name + " " + "vec4" + " " + std::to_string(value.x) + " " + std::to_string(value.y) + " " + std::to_string(value.z) + " " + std::to_string(value.w));
	}
	for (const auto& [name, value] : maps->ivec4Params) {
		if(set.contains(name))
		v.push_back(name + " " + "ivec4" + " " + std::to_string(value.x) + " " + std::to_string(value.y) + " " + std::to_string(value.z) + " " + std::to_string(value.w));
	}
	for (const auto& [name, value] : maps->customParams) {
		if(set.contains(name))
		v.push_back(name + " custom " + "{" + value->toString() + "}");
	}
	std::sort(v.begin(), v.end());

	for (const std::string &s : v) {
		f << s << std::endl;
	}


}

void Parameters::loadSubset(std::string path,
		std::vector<std::string> elements) {
	std::unordered_set<std::string> set(elements.begin(), elements.end());
	std::ifstream f;
	f.open(path);

	if (!f.is_open()) {
		std::cout << "Couldn't open " <<path << std::endl;
		return;
	}
	std::cout << "Reading " <<path << std::endl;

	std::string str;
	std::stringstream ss;

	while (std::getline(f, str)) {
		ss = std::stringstream(str);

		std::string name, type;
		ss >> name >> type;

		if(!set.contains(name)){
			continue;
		}

		if(type == "string"){
			std::string s;
			std::getline(ss, s);
			if(s.size() > 1){
				maps->stringParams[name] = s.substr(1, std::string::npos);
			}else{
				maps->stringParams[name] = "";
			}
		}else if (type == "int") {
			int v;
			ss >> v;
			maps->intParams[name] = v;
		} else if (type == "float") {
			float v;
			ss >> v;
			maps->floatParams[name] = v;
		} else if (type == "bool") {
			bool v;
			ss >> v;
			maps->boolParams[name] = v;
		} else if (type == "vec2") {
			float x, y;
			ss >> x >> y;
			maps->vec2Params[name] = glm::vec2(x, y);
		} else if (type == "vec3") {
			float x, y, z;
			ss >> x >> y >> z;
			maps->vec3Params[name] = glm::vec3(x, y, z);
		} else if (type == "vec4") {
			float x, y, z, w;
			ss >> x >> y >> z >> w;
			maps->vec4Params[name] = glm::vec4(x, y, z, w);
		} else if (type == "ivec4") {
			int x, y, z, w;
			ss >> x >> y >> z >> w;
			maps->ivec4Params[name] = glm::ivec4(x, y, z, w);
		} else if (type == "custom") {
			std::string s;
			int n1 = 0, n2 = 0;
			do{
				std::string tmp;
				ss >> tmp;
				s += " " + tmp;

				n1 = (int)std::count(s.begin(), s.end(), '{');
				n2 = (int)std::count(s.begin(), s.end(), '}');

			}while(n1 != n2);
			s = s.substr(2, s.size()-4);
			maps->customParams[name] = std::make_unique<DefaultRegistrable>(s);
		}

	}

}

void Parameters::overwriteSubset(int argc, char* argv[]){

	for(int k=1; k<argc; k++) {
		std::string str = argv[k];
		std::stringstream ss = std::stringstream(str);

		std::string name, type;
		ss >> name >> type;

		if(type == "string"){
			std::string s;
			std::getline(ss, s);
			if(s.size() > 1){
				maps->stringParams[name] = s.substr(1, std::string::npos);
			}else{
				maps->stringParams[name] = "";
			}
		}else if (type == "int") {
			int v;
			ss >> v;
			maps->intParams[name] = v;
		} else if (type == "float") {
			float v;
			ss >> v;
			maps->floatParams[name] = v;
		} else if (type == "bool") {
			bool v;
			ss >> v;
			maps->boolParams[name] = v;
		} else if (type == "vec2") {
			float x, y;
			ss >> x >> y;
			maps->vec2Params[name] = glm::vec2(x, y);
		} else if (type == "vec3") {
			float x, y, z;
			ss >> x >> y >> z;
			maps->vec3Params[name] = glm::vec3(x, y, z);
		} else if (type == "vec4") {
			float x, y, z, w;
			ss >> x >> y >> z >> w;
			maps->vec4Params[name] = glm::vec4(x, y, z, w);
		} else if (type == "ivec4") {
			int x, y, z, w;
			ss >> x >> y >> z >> w;
			maps->ivec4Params[name] = glm::ivec4(x, y, z, w);
		} else if (type == "custom") {
			std::string s;
			int n1 = 0, n2 = 0;
			do{
				std::string tmp;
				ss >> tmp;
				s += " " + tmp;

				n1 = (int)std::count(s.begin(), s.end(), '{');
				n2 = (int)std::count(s.begin(), s.end(), '}');

			}while(n1 != n2);
			s = s.substr(2, s.size()-4);
			maps->customParams[name] = std::make_unique<DefaultRegistrable>(s);
		}

	}

}


} /* namespace Rendering */
