/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Texture2D.cpp
 *
 *  Created on: 11 avr. 2020
 *      Author: Briac
 */

#include "Texture2D.h"

#include "../opengl_loader/glad/glad.h"
#include "../stb/stb_image.h"
#include "../stb/stb_image_write.h"

#include <iostream>
#include <sstream>
#include <chrono>
#include <assert.h>

namespace RenderingBase {

Texture2D::TextureData::TextureData(const std::string& path, int width, int height, GLenum internalFormat, GLenum format, GLenum type, data_ptr&& data)
		: path(path), width(width), height(height), internalFormat(internalFormat), format(format), type(type), data(std::move(data))
{

}

Texture2D::TextureData::~TextureData() {

}

Texture2D::TextureData Texture2D::readTextureData(const std::string &path) {
	stbi_set_flip_vertically_on_load(false);
	const int req_channels = 4;//slightly faster upload times with 4 channels rgba instead of rgb
	int width = 0, height = 0, nrChannels = 0;
	unsigned char *data = stbi_load(path.c_str(), &width, &height,
	&nrChannels, req_channels);

	if(!data){
		std::stringstream ss;
		ss <<"Couldn't read file " <<path;
		std::cout <<ss.str() <<std::endl;
		std::terminate();
	}
//	std::cout << "Read texture data: " << path << " " << width << "x" << height
//				<< " channels: " << nrChannels << std::endl;
	nrChannels = req_channels;
	GLenum internalFormat = nrChannels == 4 ? GL_RGBA8 : GL_RGB;
	GLenum format = nrChannels == 4 ? GL_RGBA : GL_RGB;
	GLenum type = GL_UNSIGNED_BYTE;

	struct Deleter{
		void operator()(unsigned char* ptr){

		}
	};

	std::function<void(unsigned char*)> del = [](unsigned char* ptr){
		if(ptr){
			stbi_image_free(ptr);
		}
	};
	TextureData::data_ptr p = TextureData::data_ptr(data, del);
	return TextureData(path, width, height, internalFormat, format, type, std::move(p));
}


Texture2D::Texture2D(const TextureData& data) :
		data(data.path, data.width, data.height, data.internalFormat, data.format, data.type, TextureData::data_ptr())
{
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	if (data.data) {
		std::cout << "Loaded texture " << data.path << " " << data.width << "x" << data.height << std::endl;
//		auto t0 = std::chrono::system_clock::now();
		glTexImage2D(GL_TEXTURE_2D, 0, data.internalFormat, data.width, data.height, 0, data.format,
		data.type, data.data.get());
//		auto t1 = std::chrono::system_clock::now();
//		float uploadTime = std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0).count() * 1.0E-3f;
//		std::cout <<"Upload Texture: " <<uploadTime <<" ms" <<std::endl;

	} else {
		std::cout << "Texture failed to load at path: " << data.path
				<< std::endl;
		glDeleteTextures(1, &id);
		id = 0;
		glBindTexture(GL_TEXTURE_2D, 0);
		return;
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);

}

Texture2D::Texture2D(const std::string& path) : Texture2D(readTextureData(path)){

}

Texture2D::Texture2D(const std::string& path, void* data, int width, int height, GLenum internalFormat,
		GLenum format, GLenum dataType, bool clear) : data(path, width, height, internalFormat, format, dataType, TextureData::data_ptr()) {

	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);

	if(clear){
		assert(data != nullptr);
		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format,
				dataType, nullptr);
		glClearTexImage(id, 0, format, dataType, data);
	}else{
		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format,
				dataType, data);
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);
}


Texture2D::~Texture2D() {
//	std::cout << "Deleting texture " << path << std::endl;
	glDeleteTextures(1, &id);
	id = 0;
}


unsigned char* Texture2D::getTextureData(const std::string& path, int nrChannels) const{
	std::cout << "Reading texture data from gpu mem: " << path << " " << data.width << "x" << data.height << std::endl;

	int buff_size = data.width * data.height * nrChannels;
	unsigned char* buff = new unsigned char[buff_size];
	glGetTextureImage(id, 0, data.format, GL_UNSIGNED_BYTE, buff_size, buff);

	return buff;
}

void Texture2D::saveToDisk(const std::string& path, int nrChannels) const {
	unsigned char* buff = getTextureData(path);
	stbi_flip_vertically_on_write(true);
	stbi_write_png(path.c_str(), data.width, data.height, nrChannels, buff, 0);
	delete buff;
}


std::function<void()> Texture2D::saveToDiskLater(const std::string& path, int nrChannels) const{
	unsigned char* buff = getTextureData(path);
	return [&, buff=buff, path=path](){
		std::stringstream ss;
		ss <<"Writing png to ";
		ss <<path;
		std::cout << ss.str() <<std::endl;
		stbi_flip_vertically_on_write(true);
		stbi_write_png(path.c_str(), data.width, data.height, nrChannels, buff, 0);
		delete buff;
	};
}

}
