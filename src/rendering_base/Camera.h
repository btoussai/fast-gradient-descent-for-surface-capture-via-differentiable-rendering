/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Camera.h
 *
 *  Created on: 10 avr. 2020
 *      Author: Briac
 */

#ifndef SRC_CAMERA_H_
#define SRC_CAMERA_H_

#include "../opengl_loader/glad/glad.h"//include glad before every inclusion of glfw
#include "GLFW/glfw3.h"

#include "../glm/vec2.hpp"
#include "../glm/vec3.hpp"
#include "../glm/vec4.hpp"
#include "../glm/mat4x4.hpp"

#include <functional>
#include <math.h>

namespace RenderingBase {

class Camera {
public:
	Camera();
	virtual ~Camera();

	void updateView(GLFWwindow *window, bool windowHovered, float scroll);

	void setVsync(bool vsync);

	glm::mat4 getProjectionMatrix() const;
	glm::mat4 getViewMatrix() const;
	glm::mat4 getInvViewMatrix() const;
	glm::mat4 getViewMatrixNoTranslation() const;
	glm::mat4 getProjectionViewMatrix() const;
	glm::vec3 getCameraPos() const;
	glm::vec3& getCameraPos();
	glm::vec3 getCameraDir() const;
	glm::vec3 getPrevCameraPos() const;
	glm::vec3 getPrevCameraDir() const;

	float getNearPlane() const {
		return nearPlane;
	}
	float getFarPlane() const {
		return farPlane;
	}

	float getScroll() const{
		return scroll;
	}

	bool isGUIHovered() const{
		return windowHovered;
	}
	glm::vec2 getMouseCoords() const {
		return mouseFramebufferCoords;
	}
	glm::vec2 getMouseNDC() const {
		return mouseNDC;
	}
	glm::vec2 getMouseNDCPrev() const {
		return mouseNDCPrev;
	}

	bool is_ctrl_pressed() const {
		return ctrlKey;
	}

	bool is_alt_pressed() const {
		return altKey;
	}

	bool is_shift_pressed() const {
		return shiftKey;
	}

	bool is_lmb_pressed() const {
		return lmbPressed;
	}

	glm::vec3 getCamRight() const {
		return camRight;
	}

	glm::vec3 getCamUp() const {
		return camUp;
	}

	glm::vec2 getWindowSize() const {
		return window_size;
	}

	bool canSee(glm::vec3 position, float radius) const;

	const std::vector<glm::vec4>& getFrustumCorners() const {
		return frustum_corners;
	}

	const std::vector<unsigned int>& getFrustumEdgesIndices() const {
		return frustum_edges_indices;
	}

	const glm::mat4& getInvProjView() const {
		return invProjView;
	}

	GLFWwindow* getWindow() const {
		return window;
	}

	void setMovementsEnabled(bool b){
		movementsEnabled = b;
	}

	void setRotateCam(bool rotateCam = false) {
		this->rotateCam = rotateCam;
	}

	bool isRotateCam() const {
		return rotateCam;
	}

	float getTheta() const{
		return theta;
	}

	int getThetaRotationSteps() const{
		return thetaRotationSteps;
	}

private:
	int thetaRotationSteps = 360;
	float d = 3;
	float theta = 0;
	float phi = 0;

	float nearPlane = 0.005f;
	float farPlane = 100.0f;

	float camSpeed = 0.25f * 1.0f / 60.0f;

	glm::mat4 viewMat;
	glm::mat4 invViewMat;
	glm::mat4 viewMatNoTranslation;
	glm::mat4 projMat;
	glm::mat4 projView;
	glm::mat4 invProjView;
	glm::vec3 camPos = glm::vec3(0, 0, 0);
	glm::vec3 lookPos = glm::vec3(0, 1, 0);
	glm::vec3 camDir;
	glm::vec3 camUp;
	glm::vec3 camRight;
	glm::vec3 prevCamPos = glm::vec3(0.0f);
	glm::vec3 prevCamDir = glm::vec3(1.0f);

	glm::vec2 mouseFramebufferCoords;
	glm::vec2 mouseNDC;
	glm::vec2 mouseNDCPrev;
	glm::vec2 window_size;

	glm::vec4 frustum_planes[6];
	std::vector<glm::vec4> frustum_corners = std::vector<glm::vec4>(8);//Frustum corners in world-space
	const std::vector<unsigned int> frustum_edges_indices = {
			0, 1, 	2, 3,	4, 5,	6, 7,
			0, 2,	1, 3,	4, 6,	5, 7,
			0, 4,	1, 5,	2, 6,	3, 7
	};

	bool freeCam = false;
	bool rotateCam = false;
	bool ctrlKey = false;
	bool altKey = false;
	bool shiftKey = false;
	bool lmbPressed = false;

	bool windowHovered = false;
	float scroll = 0;
	bool movementsEnabled = true;

	GLFWwindow *window = nullptr;

};

}

#endif /* SRC_CAMERA_H_ */
