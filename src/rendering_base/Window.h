/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * window.h
 *
 *  Created on: 21 mars 2020
 *      Author: Briac
 */

#ifndef SRC_RENDERING_GLFW_WINDOW_H_
#define SRC_RENDERING_GLFW_WINDOW_H_

#include "../opengl_loader/glad/glad.h"
#include <GLFW/glfw3.h>

#include "GUIs.h"
#include <string>

#include "Parameters.h"

#include <memory.h>
#include "../sequence/Sequence.h"
//#include "../sequence/AnimatedReconstruction.h"

namespace RenderingBase{

class Window {
public:
	Window(std::string title, int MULTISAMPLE_COUNT);
	virtual ~Window();

	void mainloop(int argc, char* argv[]);
	void reloadFonts();
	void setTitle();
	void toogleFullscreen(bool fullScreen);
	void ShowMainMenuBar(bool& quit);
	void OpenDialog();


private:
	GLFWwindow* w;
	int MULTISAMPLE_COUNT;
	GUIs guis;
	Parameters params = Parameters("Window");

	bool isNVIDIA = false;

	std::unique_ptr<Sequence> sequence;
//	std::unique_ptr<AnimatedReconstruction> reconstruction;

};

} /* namespace Surfaces */

#endif /* SRC_RENDERING_GLFW_WINDOW_H_ */
