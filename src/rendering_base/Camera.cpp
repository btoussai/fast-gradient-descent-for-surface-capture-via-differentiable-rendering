/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Camera.cpp
 *
 *  Created on: 10 avr. 2020
 *      Author: Briac
 */

#include "Camera.h"

#include <iostream>

#include "../glm/vec2.hpp"
#include "../glm/vec3.hpp"
#include "../glm/vec4.hpp"
#include "../glm/mat4x4.hpp"
#include "../glm/ext/matrix_transform.hpp"
#include "../glm/gtc/matrix_inverse.hpp"
#include "../glm/ext/matrix_clip_space.hpp"
#include "../glm/gtx/string_cast.hpp"

#include "../imgui/imgui.h"

namespace RenderingBase {

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

Camera::Camera() {
	camPos = glm::vec3(d * sin(theta) * cos(phi), d * sin(phi),
			d * cos(theta) * cos(phi));

}

Camera::~Camera() {
	// TODO Auto-generated destructor stub
}

void Camera::updateView(GLFWwindow* window, bool windowHovered, float scroll) {
	this->window = window;
	prevCamDir = camDir;
	prevCamPos = camPos;
	static double xpos = 0, ypos = 0;
	double new_xpos, new_ypos;

	this->windowHovered = windowHovered;
	this->scroll = scroll;

	glfwGetCursorPos(window, &new_xpos, &new_ypos);

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	mouseFramebufferCoords = glm::vec2(new_xpos, new_ypos);
	mouseNDCPrev = mouseNDC;
	mouseNDC = glm::vec2(2.0f * xpos / width - 1.0f,
			1.0f - 2.0f * ypos / height);

	float dx = new_xpos - xpos;
	float dy = new_ypos - ypos;

	int lmbState = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
	lmbPressed = lmbState == GLFW_PRESS;
	if (movementsEnabled && lmbPressed && !windowHovered && !ctrlKey) {
		theta -= dx * 0.005f;
		phi += dy * 0.005f;

		if (phi < -M_PI * 0.49f)
			phi = -M_PI * 0.49f;
		if (phi > M_PI * 0.49f)
			phi = M_PI * 0.49f;
	}
	xpos = new_xpos;
	ypos = new_ypos;

	ImGui::Spacing();

	ImGui::Checkbox("Free camera", &this->freeCam);
	if(this->freeCam){
		ImGui::SliderFloat("Cam speed", &camSpeed, 0, 0.5f * 1.0f / 60.0f, "%.5f");


	}else{
		ImGui::SameLine();
		ImGui::Checkbox("Rotate camera", &rotateCam);
		if(rotateCam){
			theta += 2.0 * M_PI / thetaRotationSteps;
			int k = (int)round(theta / (2.0*M_PI) * thetaRotationSteps) % thetaRotationSteps;
			theta = k * 2.0 * M_PI / thetaRotationSteps;
//			phi = sin(theta) * M_PI * 0.2 + 0.1 * M_PI;
		}
	}


	ImGui::Spacing();

	glm::vec3 up(0, 1, 0);
	if (!freeCam) {
		if (!windowHovered) {
			if(movementsEnabled){
				d *= (1.0f - scroll * 0.05f);
			}

			if (d > farPlane / 2.0) {
				d = farPlane / 2.0;
			}
		}
		scroll = 0;

		camDir = -glm::vec3(sin(theta) * cos(phi), sin(phi),
				cos(theta) * cos(phi));


		camPos = -camDir * d + lookPos;

		viewMat = glm::lookAt(camPos, lookPos, up);

		if(movementsEnabled && ctrlKey && lmbPressed){
//			float ratio = (float)window_size.x / window_size.y;
			lookPos += this->getCamRight() * -dx * d / (float)width;
			lookPos += this->getCamUp() * dy * d / (float)height;
		}
	} else {
		if(movementsEnabled){
			if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
				camPos += glm::vec3(sin(theta), 0, cos(theta)) * -camSpeed;
			} else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
				camPos += glm::vec3(sin(theta), 0, cos(theta)) * camSpeed;
			}
			if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
				camPos += glm::vec3(cos(theta), 0, -sin(theta)) * -camSpeed;
			} else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
				camPos += glm::vec3(cos(theta), 0, -sin(theta)) * camSpeed;
			}
			if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
				camPos += glm::vec3(0, 1, 0) * camSpeed;
			} else if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
				camPos += glm::vec3(0, 1, 0) * -camSpeed;
			}
		}

		camDir = -glm::vec3(sin(theta) * cos(phi), sin(phi),
				cos(theta) * cos(phi));
		viewMat = glm::lookAt(camPos, camPos + camDir, up);
	}

	camDir /= glm::length(camDir);
	camRight = glm::cross(camDir, up);
	camRight /= glm::length(camRight);
	camUp = glm::cross(camRight, camDir);
	camUp /= glm::length(camUp);

	invViewMat = viewMat;
	invViewMat = glm::inverse(invViewMat);
	viewMatNoTranslation = invViewMat;
	viewMatNoTranslation[3] = glm::vec4(0, 0, 0, 1);
	viewMatNoTranslation = glm::inverse(viewMatNoTranslation);

	if (width != 0 && height != 0) {
		projMat = glm::perspective((float) M_PI / 4.0f,
				(float) width / (float) height, nearPlane, farPlane);
	}

	projView = projMat * viewMat;
	invProjView = glm::inverse(projView);

	window_size = glm::vec2(width, height);

	ctrlKey = glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS;
	altKey =  glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS;
	shiftKey =  glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS;

	glm::mat4 combo = glm::transpose(projView);
	frustum_planes[0] = combo[3] + combo[0];
	frustum_planes[1] = combo[3] - combo[0];
	frustum_planes[2] = combo[3] + combo[1];
	frustum_planes[3] = combo[3] - combo[1];
	frustum_planes[4] = combo[3] + combo[2];
	frustum_planes[5] = combo[3] - combo[2];

	for (int i = 0; i < 6; i++) {
		glm::vec3 normal(frustum_planes[i]);
		frustum_planes[i] /= glm::length(normal);
	}

	glm::vec4 clipSpaceCorners[8];
	clipSpaceCorners[0] = glm::vec4(-1, -1, -1, 1);
	clipSpaceCorners[1] = glm::vec4(-1, -1, +1, 1);
	clipSpaceCorners[2] = glm::vec4(-1, +1, -1, 1);
	clipSpaceCorners[3] = glm::vec4(-1, +1, +1, 1);
	clipSpaceCorners[4] = glm::vec4(+1, -1, -1, 1);
	clipSpaceCorners[5] = glm::vec4(+1, -1, +1, 1);
	clipSpaceCorners[6] = glm::vec4(+1, +1, -1, 1);
	clipSpaceCorners[7] = glm::vec4(+1, +1, +1, 1);
	for(int i=0; i<8; i++){
		frustum_corners[i] = invProjView * clipSpaceCorners[i];
		frustum_corners[i] /= frustum_corners[i].w;
	}

//	ImGui::Separator();
//	ImGui::Text(std::string("CameraPos " + glm::to_string(camPos)).c_str(), "");
//	ImGui::Separator();
}

void Camera::setVsync(bool vsync){
	glfwSwapInterval(vsync);
}

glm::mat4 Camera::getViewMatrix() const {
	return viewMat;
}

glm::mat4 Camera::getViewMatrixNoTranslation() const {
	return viewMatNoTranslation;
}

glm::mat4 Camera::getProjectionMatrix() const {
	return projMat;
}

glm::mat4 Camera::getProjectionViewMatrix() const {
	return projView;
}

glm::vec3 Camera::getCameraPos() const {
	return camPos;
}

glm::vec3& Camera::getCameraPos() {
	return camPos;
}
glm::vec3 Camera::getCameraDir() const {
	return camDir;
}

glm::vec3 Camera::getPrevCameraPos() const {
	return prevCamPos;
}

glm::vec3 Camera::getPrevCameraDir() const {
	return prevCamDir;
}

glm::mat4 Camera::getInvViewMatrix() const {
	return invViewMat;
}

bool Camera::canSee(glm::vec3 position, float radius) const {

	for (int i = 0; i < 6; i++) {
		float dist = glm::dot(glm::vec4(position, 1.0), frustum_planes[i])
				+ radius;
		if (dist < 0)
			return false;
	}

	return true;
}

}
