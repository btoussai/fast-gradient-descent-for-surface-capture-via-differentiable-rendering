/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Texture2D.h
 *
 *  Created on: 11 avr. 2020
 *      Author: Briac
 */

#ifndef SRC_TEXTURE2D_H_
#define SRC_TEXTURE2D_H_

#include "../opengl_loader/glad/glad.h"
#include <string>
#include <iostream>
#include <memory>
#include <functional>

namespace RenderingBase {

class Texture2D {
public:
	struct TextureData{
		typedef std::unique_ptr<unsigned char, std::function<void(unsigned char*)>> data_ptr;

		std::string path;
		int width = 0, height = 0;
		GLenum internalFormat = GL_RGBA8;
		GLenum format = GL_RGBA;
		GLenum type = GL_UNSIGNED_BYTE;
		data_ptr data = nullptr;

		TextureData() = default;
		TextureData(TextureData&&) = default;
		TextureData(const std::string& path, int width, int height, GLenum internalFormat, GLenum format, GLenum type, data_ptr&& data);
		TextureData& operator=(TextureData&&) = default;
		virtual ~TextureData();

		static data_ptr NonOwningPtr(const void* data){
			return data_ptr((unsigned char*)data, [](unsigned char* ptr){

			});
		}

	};
	static TextureData readTextureData(const std::string& path);

	Texture2D(const std::string& path);
	Texture2D(const TextureData& data);
	Texture2D(const std::string& path, void* data, int width, int height, GLenum internalFormat,
			GLenum format, GLenum dataType, bool clear);

	virtual ~Texture2D();

	Texture2D(const Texture2D&) = delete;
	Texture2D(Texture2D&&) = delete;
	Texture2D& operator=(const Texture2D&) = delete;
	Texture2D& operator=(Texture2D&&) = delete;

	void bind() const{
		if (id == 0) {
			std::cerr << "Error, trying to bind a non-existent texture"
				<< std::endl;
		} else {
			glBindTexture(GL_TEXTURE_2D, id);
		}
	}

	void unbind() const{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void bindToTextureUnit(int textureUnit) const {
		if (id == 0) {
			std::cerr << "Error, trying to bind a non-existent texture"
					<< std::endl;
		} else {
			glBindTextureUnit(textureUnit, id);
		}
	}

	void unbindFromTextureUnit(int textureUnit) const {
		glBindTextureUnit(textureUnit, 0);
	}

	const std::string& getPath() const{
		return data.path;
	}

	GLuint getID() const{
		return id;
	}

	int getWidth() const{
		return data.width;
	}

	int getHeight() const{
		return data.height;
	}

	const TextureData& getData() const{
		return data;
	}

	void saveToDisk(const std::string& path, int nrChannels=4) const;
	std::function<void()> saveToDiskLater(const std::string& path, int nrChannels=4) const;
	unsigned char* getTextureData(const std::string& path, int nrChannels=4) const;

private:
	TextureData data;
	GLuint id;

};

}

#endif /* SRC_TEXTURE2D_H_ */
