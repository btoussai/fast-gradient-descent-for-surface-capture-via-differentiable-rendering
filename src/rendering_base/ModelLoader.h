/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * ModelLoader.h
 *
 *  Created on: 11 avr. 2020
 *      Author: Briac
 */

#ifndef SRC_MODELLOADER_H_
#define SRC_MODELLOADER_H_

#include <string>
#include <vector>
#include <memory>

#include "openglObjects.h"

namespace RenderingBase {

struct MeshData;

class ModelLoader {
public:


	static std::shared_ptr<MeshData> async_load(const std::string& path,
			int flags = 0,
			bool scaleToUnit = true,
			std::string prefixPath="resources/models/",
			bool flatFaces=true);


	static void opengl_load(VAO& vao, const std::shared_ptr<MeshData>& data, int& VertexCount);
	static std::array<std::function<void()>, 2> opengl_async_load(VAO& vao, const std::shared_ptr<MeshData>& data, int& VertexCount);


	static void load(VAO& vao, const std::string& path, int flags = 0, bool scaleToUnit = true, std::string prefixPath="resources/models/");
	static void load(VAO& vao, const std::string& path, std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normalsVector, int flags = 0, bool scaleToUnit = true, std::string prefixPath="resources/models/");
};

}

#endif /* SRC_MODELLOADER_H_ */
