/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * ModelLoader.cpp
 *
 *  Created on: 11 avr. 2020
 *      Author: Briac
 */

#include "ModelLoader.h"

#include "../glm/packing.hpp"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <assert.h>
#include <chrono>

namespace RenderingBase {

static uint32_t* buildIndices(uint32_t &numIndices, aiMesh *mesh) {
	numIndices = mesh->mNumFaces * 3;

	uint32_t *indices = new uint32_t[numIndices];

	for (uint32_t f = 0; f < mesh->mNumFaces; f++) {
		auto face = mesh->mFaces[f];

		indices[3 * f + 0] = face.mIndices[0];
		indices[3 * f + 1] = face.mIndices[1];
		indices[3 * f + 2] = face.mIndices[2];
	}

	return indices;
}

static float* buildPositions(uint32_t &numVertices, aiMesh *mesh,
		bool scaleToUnit) {
	numVertices = mesh->mNumVertices;

	float *vertices = new float[numVertices * 3];
	glm::vec3 Vmin = glm::vec3(+INFINITY);
	glm::vec3 Vmax = glm::vec3(-INFINITY);

	for (uint32_t v = 0; v < numVertices; v++) {
		auto vertex = mesh->mVertices[v];

		vertices[3 * v + 0] = vertex.x;
		vertices[3 * v + 1] = vertex.y;
		vertices[3 * v + 2] = vertex.z;

		glm::vec3 u(vertex.x, vertex.y, vertex.z);
		Vmax = glm::max(Vmax, u);
		Vmin = glm::min(Vmin, u);

	}

	if (scaleToUnit) {
		glm::vec3 size = Vmax - Vmin;
		float extent = std::max(std::max(size.x, size.y), size.z);
		for (uint32_t v = 0; v < numVertices; v++) {
			vertices[3 * v + 0] = (vertices[3 * v + 0] - Vmin.x - 0.5 * size.x)
					/ extent * 2.0;
			vertices[3 * v + 1] = (vertices[3 * v + 1] - Vmin.y - 0.5 * size.y)
					/ extent * 2.0;
			vertices[3 * v + 2] = (vertices[3 * v + 2] - Vmin.z - 0.5 * size.z)
					/ extent * 2.0;
		}
	}

	return vertices;
}

static float* buildNormals(uint32_t &numNormals, aiMesh *mesh) {
	numNormals = mesh->mNumVertices;

	float *normals = new float[numNormals * 3];

	for (uint32_t v = 0; v < numNormals; v++) {
		auto normal = mesh->mNormals[v];

		normals[3 * v + 0] = normal.x;
		normals[3 * v + 1] = normal.y;
		normals[3 * v + 2] = normal.z;
	}

	return normals;
}

static float* buildTexCoords(uint32_t &numTexture, aiMesh *mesh) {
	numTexture = mesh->mNumVertices;

	float *uvs = new float[numTexture * 2];

	for (uint32_t i = 0; i < numTexture; i++) {
		auto tex = mesh->mTextureCoords[0][i];

		uvs[2 * i + 0] = tex.x;
		uvs[2 * i + 1] = tex.y;
	}

	return uvs;
}

static float* buildVertexColors(uint32_t &numColors, aiMesh *mesh){
	numColors = mesh->mNumVertices;

	float *colors = new float[numColors * 4];

	for (uint32_t v = 0; v < numColors; v++) {
		auto color = mesh->mColors[0][v];

		colors[4 * v + 0] = color.r;
		colors[4 * v + 1] = color.g;
		colors[4 * v + 2] = color.b;
		colors[4 * v + 3] = color.a;
	}

	return colors;
}

struct MeshData{
	Assimp::Importer importer;
	const aiScene *scene = nullptr;
	uint32_t indexCount = 0;
	uint32_t *indices = nullptr;
	uint32_t vertexCount = 0;
	float *vertices = nullptr;
	uint32_t normalCount = 0;
	float *normals = nullptr;
	uint32_t textureCount = 0;
	float *uvs = nullptr;
	uint32_t colorsCount = 0;
	float *colors = nullptr;

	int vertexSize = 3;
	int normalSize = 3;
	int colorSize = 4;

	MeshData(const std::string& path, int flags, bool scaleToUnit, std::string prefixPath, bool flatFaces) {
		std::string fullPath = prefixPath + path;

		scene = importer.ReadFile(fullPath,
				aiProcess_Triangulate | aiProcess_FlipUVs | flags);

		if (!scene || scene->mNumMeshes == 0) {
			return;
		}
		aiMesh *mesh = scene->mMeshes[0];
		assert(mesh->mPrimitiveTypes == aiPrimitiveType_TRIANGLE);

		if (mesh->HasFaces()) {
			indices = buildIndices(indexCount, mesh);
		}

		if (mesh->HasPositions()) {
			vertices = buildPositions(vertexCount, mesh, scaleToUnit);
		}

		if (mesh->HasNormals()) {
			normals = buildNormals(normalCount, mesh);
		}

		if (mesh->HasTextureCoords(0)) {
			uvs = buildTexCoords(textureCount, mesh);
		}

		if(mesh->HasVertexColors(0)){
			colors = buildVertexColors(colorsCount, mesh);
		}

		if(flatFaces){
			this->flatFaces();
		}
	};

	void flatFaces() {
		vertexSize = 4;
		normalSize = 4;
		colorSize = 4;

		uint32_t triangles = indexCount / 3;
		float* newVertices = new float[triangles * 3 * vertexSize];
		float* newNormals = new float[triangles * 3 * normalSize];


		for(uint32_t i=0; i<triangles; i++){
			glm::vec3 p0 = ((glm::vec3*)vertices)[indices[3*i+0]];
			glm::vec3 p1 = ((glm::vec3*)vertices)[indices[3*i+1]];
			glm::vec3 p2 = ((glm::vec3*)vertices)[indices[3*i+2]];

			((glm::vec4*)newVertices)[3 * i + 0] = glm::vec4(glm::vec3(p0), 1.0f);
			((glm::vec4*)newVertices)[3 * i + 1] = glm::vec4(glm::vec3(p1), 1.0f);
			((glm::vec4*)newVertices)[3 * i + 2] = glm::vec4(glm::vec3(p2), 1.0f);

			glm::vec3 N = glm::normalize(glm::cross(p1-p0, p2-p0));
			((glm::vec4*)newNormals)[3 * i + 0] = glm::vec4(N, 1.0f);
			((glm::vec4*)newNormals)[3 * i + 1] = glm::vec4(N, 1.0f);
			((glm::vec4*)newNormals)[3 * i + 2] = glm::vec4(N, 1.0f);

			if(colors){
				const glm::vec4 c0 = ((glm::vec4*)colors)[indices[3*i+0]];
				const glm::vec4 c1 = ((glm::vec4*)colors)[indices[3*i+1]];
				const glm::vec4 c2 = ((glm::vec4*)colors)[indices[3*i+2]];

				((glm::vec4*)newNormals)[3 * i + 0].w = glm::uintBitsToFloat(glm::packUnorm4x8(c0));
				((glm::vec4*)newNormals)[3 * i + 1].w = glm::uintBitsToFloat(glm::packUnorm4x8(c1));
				((glm::vec4*)newNormals)[3 * i + 2].w = glm::uintBitsToFloat(glm::packUnorm4x8(c2));
			}

		}

		delete vertices;
		delete normals;
		delete indices;
		delete colors;

		indices = nullptr;
		vertices = newVertices;
		normals = newNormals;
		colors = nullptr;

		colorsCount = vertexCount = normalCount = triangles * 3;
		indexCount = 0;

	}

	~MeshData(){
		delete indices;
		delete vertices;
		delete normals;
		delete uvs;
		delete colors;
	}
};

std::shared_ptr<MeshData> ModelLoader::async_load(const std::string& path, int flags, bool scaleToUnit, std::string prefixPath, bool flatFaces){
	return std::make_shared<MeshData>(path, flags, scaleToUnit, prefixPath, flatFaces);
}

std::array<std::function<void()>, 2> ModelLoader::opengl_async_load(VAO& vao, const std::shared_ptr<MeshData>& data, int& VertexCount) {

	std::array<std::function<void()>, 2> ret;

	GLsizeiptr size = 0;
	if (data->indices) {
		size += data->indexCount * sizeof(uint32_t);
	}
	if (data->vertices) {
		size += data->vertexCount * data->vertexSize * sizeof(float);
	}
	if (data->normals) {
		size += data->normalCount * data->normalSize * sizeof(float);
	}
	if (data->uvs) {
		size += data->textureCount * 2 * sizeof(float);
	}


	GLuint vbo;
	glGenBuffers(1, &vbo);
	glNamedBufferStorage(vbo, size, nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
	char* ptr = (char*)glMapNamedBufferRange(vbo, 0, size, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);

	ret[0] = [ptr, &data](){

		GLsizeiptr offset = 0;
		if (data->indices) {
			GLsizeiptr s = data->indexCount * sizeof(uint32_t);
			memcpy(ptr + offset, data->indices, s);
			offset += s;
		}

		if (data->vertices) {
			GLsizeiptr s = data->vertexCount * data->vertexSize * sizeof(float);
			memcpy(ptr + offset, data->vertices, s);
			offset += s;
		}

		if (data->normals) {
			GLsizeiptr s = data->normalCount * data->normalSize * sizeof(float);
			memcpy(ptr + offset, data->normals, s);
			offset += s;
		}

		if (data->uvs) {
			GLsizeiptr s = data->textureCount * 2 * sizeof(float);
			memcpy(ptr + offset, data->uvs, s);
			offset += s;
		}


	};

	ret[1] = [&vao, &data, &VertexCount, vbo](){

		glUnmapNamedBuffer(vbo);

		vao.bind();

		VertexCount = data->vertexCount;

		GLsizeiptr offset = 0;
		if (data->indices) {
			vao.createIndexBuffer(nullptr, data->indexCount);

			GLsizeiptr s = data->indexCount * sizeof(uint32_t);
			glCopyNamedBufferSubData(vbo, vao.getIndexVBO()->getID(), offset, 0, s);
			offset += s;
		}

		int attrib = 0;
		if (data->vertices) {
			vao.createFloatAttribute(0, nullptr, data->vertexCount * data->vertexSize, data->vertexSize, 0,
			GL_STATIC_DRAW);

			GLsizeiptr s = data->vertexCount * data->vertexSize * sizeof(float);
			glCopyNamedBufferSubData(vbo, vao.getAttributeVBOs()[attrib++]->getID(), offset, 0, s);
			offset += s;
		}

		if (data->normals) {
			vao.createFloatAttribute(1, nullptr, data->normalCount * data->normalSize, data->normalSize, 0,
			GL_STATIC_DRAW);


			GLsizeiptr s = data->normalCount * data->normalSize * sizeof(float);
			glCopyNamedBufferSubData(vbo, vao.getAttributeVBOs()[attrib++]->getID(), offset, 0, s);
			offset += s;
		}

		if (data->uvs) {
			vao.createFloatAttribute(2, nullptr, data->textureCount * 2, 2, 0,
			GL_STATIC_DRAW);

			GLsizeiptr s = data->textureCount * 2 * sizeof(float);
			glCopyNamedBufferSubData(vbo, vao.getAttributeVBOs()[attrib++]->getID(), offset, 0, s);
			offset += s;
		}

		vao.unbind();

		glDeleteBuffers(1, &vbo);
	};


	return ret;
}

void ModelLoader::opengl_load(VAO& vao, const std::shared_ptr<MeshData>& data, int& VertexCount){
	vao.bind();

	VertexCount = data->vertexCount;

	if (data->indices) {
		vao.createIndexBuffer(data->indices, data->indexCount);
	}

	if (data->vertices) {
		vao.createFloatAttribute(0, data->vertices, data->vertexCount * data->vertexSize, data->vertexSize, 0,
		GL_STATIC_DRAW);
	}

	if (data->normals) {
		vao.createFloatAttribute(1, data->normals, data->normalCount * data->normalSize, data->normalSize, 0,
		GL_STATIC_DRAW);
	}

	if (data->uvs) {
		vao.createFloatAttribute(2, data->uvs, data->textureCount * 2, 2, 0,
		GL_STATIC_DRAW);
	}

	vao.unbind();
}

void ModelLoader::load(VAO& vao, const std::string &path, int flags, bool scaleToUnit, std::string prefixPath) {
	std::string fullPath = prefixPath + path;

	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(fullPath,
			aiProcess_Triangulate | aiProcess_FlipUVs | flags);
	if (!scene || scene->mNumMeshes == 0) {
		throw "Could not load " + fullPath;
	}
	aiMesh *mesh = scene->mMeshes[0];
	assert(mesh->mPrimitiveTypes == aiPrimitiveType_TRIANGLE);

	std::cout << "Loading Mesh:" << mesh->mName.C_Str() << std::endl;

	vao.bind();

	if (mesh->HasFaces()) {
		uint32_t indexCount;
		uint32_t *indices = buildIndices(indexCount, mesh);
		std::cout << "Index list detected: " << indexCount << " indices"
				<< std::endl;
		vao.createIndexBuffer(indices, indexCount);
		delete indices;
	}

	if (mesh->HasPositions()) {
		std::cout << "Vertex list detected" << std::endl;
		uint32_t vertexCount;
		float *vertices = buildPositions(vertexCount, mesh, scaleToUnit);
		vao.createFloatAttribute(0, vertices, vertexCount * 3, 3, 0,
		GL_STATIC_DRAW);

		delete vertices;
	}

	if (mesh->HasNormals()) {
		std::cout << "Normal list detected" << std::endl;
		uint32_t normalCount;
		float *normals = buildNormals(normalCount, mesh);
		vao.createFloatAttribute(1, normals, normalCount * 3, 3, 0,
		GL_STATIC_DRAW);

		delete normals;
	}

	if (mesh->HasTextureCoords(0)) {
		std::cout << "Texture list detected" << std::endl;
		uint32_t textureCount;
		float *uvs = buildTexCoords(textureCount, mesh);
		vao.createFloatAttribute(2, uvs, textureCount * 2, 2, 0,
		GL_STATIC_DRAW);
		delete uvs;
	}

	vao.unbind();

}

void ModelLoader::load(VAO& vao, const std::string &path,
		std::vector<glm::vec3> &verticesVector,
		std::vector<glm::vec3> &normalsVector, int flags, bool scaleToUnit, std::string prefixPath) {
	Assimp::Importer importer;

	std::string fullPath = prefixPath + path;
	const aiScene *scene = importer.ReadFile(fullPath,
			aiProcess_Triangulate | aiProcess_FlipUVs | flags);
	if (!scene || scene->mNumMeshes == 0) {
		throw "Could not load " + fullPath;
	}
	aiMesh *mesh = scene->mMeshes[0];
	assert(mesh->mPrimitiveTypes == aiPrimitiveType_TRIANGLE);

	std::cout << "Loading Mesh:" << mesh->mName.C_Str() << std::endl;

	vao.bind();

	if (mesh->HasFaces()) {
		uint32_t indexCount;
		uint32_t *indices = buildIndices(indexCount, mesh);
		std::cout << "Index list detected: " << indexCount << " indices"
				<< std::endl;
		vao.createIndexBuffer(indices, indexCount);
		delete indices;
	}

	if (mesh->HasPositions()) {
		std::cout << "Vertex list detected" << std::endl;
		uint32_t vertexCount;
		float *vertices = buildPositions(vertexCount, mesh, scaleToUnit);
		vao.createFloatAttribute(0, vertices, vertexCount * 3, 3, 0,
		GL_STATIC_DRAW);

		verticesVector.clear();
		for (uint32_t v = 0; v < vertexCount; v++) {
			verticesVector.push_back(
					glm::vec3(vertices[3 * v + 0], vertices[3 * v + 1],
							vertices[3 * v + 2]));
		}

		delete vertices;
	}

	if (mesh->HasNormals()) {
		std::cout << "Normal list detected" << std::endl;
		uint32_t normalCount;
		float *normals = buildNormals(normalCount, mesh);
		vao.createFloatAttribute(1, normals, normalCount * 3, 3, 0,
		GL_STATIC_DRAW);

		normalsVector.clear();
		for (uint32_t v = 0; v < normalCount; v++) {
			normalsVector.push_back(
					glm::vec3(normals[3 * v + 0], normals[3 * v + 1],
							normals[3 * v + 2]));
		}

		delete normals;
	}

	if (mesh->HasTextureCoords(0)) {
		std::cout << "Texture list detected" << std::endl;
		uint32_t textureCount;
		float *uvs = buildTexCoords(textureCount, mesh);
		vao.createFloatAttribute(2, uvs, textureCount * 2, 2, 0,
		GL_STATIC_DRAW);
		delete uvs;
	}

	vao.unbind();

}

}
