/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * openglObjects.c
 *
 *  Created on: 28 mars 2019
 *      Author: Briac
 */

#include "openglObjects.h"

#include "../glm/vec2.hpp"
#include "../glm/vec3.hpp"
#include "../glm/vec4.hpp"
#include "../glm/mat3x3.hpp"
#include "../glm/mat4x4.hpp"

#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <filesystem>

#ifdef _WIN32
#include <direct.h> //_getcwd for windows
#else
#include <unistd.h> //getcwd for unix
char * _getcwd(char *buf, size_t size)
{
	return getcwd(buf, size);
}
#endif

namespace RenderingBase {

VAO::VAO() :
		vbos() {
	glGenVertexArrays(1, &ID);
	indexVBO = NULL;
	indexCount = 0;
}

VAO::VAO(VAO &&vao) :
		vbos(vao.vbos) {
	assert(this != &vao);
	this->indexCount = vao.indexCount;
	this->indexVBO = vao.indexVBO;
	this->ID = vao.ID;

	vao.indexCount = 0;
	vao.indexVBO = NULL;
	vao.ID = 0;

	vao.vbos.erase(vao.vbos.begin(), vao.vbos.end());
}

VAO& VAO::operator=(VAO&& vao){
	if(this != &vao){
		for (VBO *vbo : this->vbos) {
			delete vbo;
		}
		vbos = std::move(vao.vbos);
		this->indexCount = vao.indexCount;

		delete this->indexVBO;
		this->indexVBO = vao.indexVBO;

		glDeleteVertexArrays(1, &ID);
		this->ID = vao.ID;

		vao.indexCount = 0;
		vao.indexVBO = NULL;
		vao.ID = 0;
	}

	return *this;
}

VAO::~VAO() {
	delete this->indexVBO;
	for (VBO *vbo : this->vbos) {
		delete vbo;
	}

	glDeleteVertexArrays(1, &ID);
}

VBO::VBO(GLuint type) {
//	glGenBuffers(1, &ID);
	glCreateBuffers(1, &ID);
	this->type = type;
	this->dataSize = 0;
}

VBO::~VBO() {
	glDeleteBuffers(1, &ID);
}

VBO::VBO(VBO &&vbo) {
	this->dataSize = vbo.dataSize;
	this->type = vbo.type;
	this->ID = vbo.ID;
	this->attribNumber = vbo.attribNumber;
	vbo.ID = 0;
}

VBO& VBO::operator=(VBO&& vbo){
	glDeleteBuffers(1, &ID);
	this->dataSize = vbo.dataSize;
	this->type = vbo.type;
	this->ID = vbo.ID;
	this->attribNumber = vbo.attribNumber;
	vbo.ID = 0;
	return *this;
}

void VBO::bind() {
	glBindBuffer(type, ID);
}

void VBO::unbind() {
	glBindBuffer(type, 0);
}

void VBO::bindAs(GLuint type) {
	glBindBuffer(type, ID);
}

void VBO::unbindAs(GLuint type) {
	glBindBuffer(type, 0);
}

void VBO::storeData(const GLvoid *data, GLsizeiptr dataSize, GLuint usage) {
	glBufferData(type, dataSize, data, usage);
	this->dataSize = dataSize;
}

void VBO::updateData(const GLvoid *data, GLsizeiptr dataSize) {
	glBufferSubData(type, 0, dataSize, data);
	this->dataSize = dataSize;
}

void VBO::clearData(GLenum internalformat, GLenum format, GLenum type, const GLvoid *data) {
	glClearNamedBufferSubData(ID, internalformat, 0, dataSize, format, type, data);
}

void VAO::bind() const {
	glBindVertexArray(ID);
}

void VAO::unbind() const {
	glBindVertexArray(0);
}

void VAO::bindAttribute(uint32_t attribute) const {
	glEnableVertexAttribArray(attribute);
}

void VAO::unbindAttribute(uint32_t attribute) const {
	glDisableVertexAttribArray(attribute);
}

void VAO::createIndexBuffer(const uint32_t *indices, uint32_t indexCount) {
	VBO *indexVBO = new VBO(GL_ELEMENT_ARRAY_BUFFER);
	indexVBO->bind();
	indexVBO->storeData(indices, indexCount * sizeof(uint32_t), GL_STATIC_DRAW);
	//VBO_Unbind(indexVBO); DO NOT UNBIND !!

	this->indexVBO = indexVBO;
	this->indexCount = indexCount;
}

void VAO::createFloatAttribute(uint32_t attribNumber, const float *data,
		uint32_t dataArrayLength, uint32_t size, uint32_t stride,
		GLenum usage, std::size_t offset) {
	VBO *vbo = nullptr;
	bool exists = false;
	for(VBO* v : vbos){
		if(v->getAttribNumber() == (int)attribNumber){
			exists = true;
			vbo = v;
			//throw std::string("Error, vertex attribute ") + std::to_string(attribNumber) + std::string(" already exists.");
		}
	}

	if(!exists){
		vbo = new VBO(GL_ARRAY_BUFFER);
	}
	vbo->bind();
	vbo->storeData(data, dataArrayLength * sizeof(float), usage);
	glVertexAttribPointer(attribNumber, size, GL_FLOAT, GL_FALSE,
			stride * sizeof(float), (GLvoid*) offset);
	vbo->unbind();
	vbo->setAttribNumber(attribNumber);

	if(!exists){
		vbos.push_back(vbo);
	}
}

void VAO::createIntAttribute(uint32_t attribNumber, const int32_t *data,
		uint32_t dataArrayLength, uint32_t size, uint32_t stride,
		GLenum usage, std::size_t offset) {
	for(VBO* vbo : vbos){
		assert(vbo->getAttribNumber() != (int)attribNumber);
	}

	VBO *vbo = new VBO(GL_ARRAY_BUFFER);
	vbo->bind();
	vbo->storeData(data, dataArrayLength * sizeof(int32_t), usage);
	glVertexAttribIPointer(attribNumber, size, GL_INT, stride * sizeof(int32_t),
			(GLvoid*) offset);
	vbo->unbind();
	vbo->setAttribNumber(attribNumber);

	vbos.push_back(vbo);
}

Shader::Shader(const char *computeFilePath) {
	vertexShaderID = loadFromFile(computeFilePath,
	GL_COMPUTE_SHADER);
	programID = glCreateProgram();

	glAttachShader(programID, vertexShaderID);
}

/**
 * Construit un nouveau shader.
 * Ne pas oublier d'appeler:
 *     -Shader::bindVertexAttribute   ***
 *     -Shader::bindFragDataLocation  ***
 *     -Shader::finishInit
 *     -Shader::getUniformLocation    ***
 *     -Shader::start
 *     -Shader::connectTextureUnit    ***
 *     -Shader::stop
 * de fa�on � finaliser l'initialisation du shader.
 *
 */
Shader::Shader(const char *vertexFilePath, const char *fragmentFilePath) {

	vertexShaderID = loadFromFile(vertexFilePath,
	GL_VERTEX_SHADER);
	fragmentShaderID = loadFromFile(fragmentFilePath,
	GL_FRAGMENT_SHADER);

	programID = glCreateProgram();

	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);

}

/**
 * Construit un nouveau shader.
 * Ne pas oublier d'appeler:
 *     -Shader::bindVertexAttribute   ***
 *     -Shader::bindFragDataLocation  ***
 *     -Shader::finishInit
 *     -Shader::getUniformLocation    ***
 *     -Shader::start
 *     -Shader::connectTextureUnit    ***
 *     -Shader::stop
 * de fa�on � finaliser l'initialisation du shader.
 *
 */
Shader::Shader(const char *vertexFilePath, const char *geometryFilePath,
		const char *fragmentFilePath) {

	vertexShaderID = loadFromFile(vertexFilePath,
	GL_VERTEX_SHADER);
	geometryShaderID = loadFromFile(geometryFilePath,
	GL_GEOMETRY_SHADER);
	fragmentShaderID = loadFromFile(fragmentFilePath,
	GL_FRAGMENT_SHADER);

	programID = glCreateProgram();

	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, geometryShaderID);
	glAttachShader(programID, fragmentShaderID);

}


Shader::Shader(const char* vertexFilePath, const char* tessellationControlFilePath, const char* tessellationEvaluationFilePath, const char* geometryFilePath,
		const char* fragmentFilePath) {
	vertexShaderID = loadFromFile(vertexFilePath,
	GL_VERTEX_SHADER);
	tessControlShaderID = loadFromFile(tessellationControlFilePath,
			GL_TESS_CONTROL_SHADER);
	tessEvaluationShaderID = loadFromFile(tessellationEvaluationFilePath,
			GL_TESS_EVALUATION_SHADER);
	geometryShaderID = loadFromFile(geometryFilePath,
	GL_GEOMETRY_SHADER);
	fragmentShaderID = loadFromFile(fragmentFilePath,
	GL_FRAGMENT_SHADER);

	programID = glCreateProgram();

	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, tessControlShaderID);
	glAttachShader(programID, tessEvaluationShaderID);
	glAttachShader(programID, geometryShaderID);
	glAttachShader(programID, fragmentShaderID);
}


Shader::Shader(const std::string &computeFile) {
	vertexShaderID = loadFromSource(computeFile,
	GL_COMPUTE_SHADER);
	programID = glCreateProgram();

	glAttachShader(programID, vertexShaderID);
}

Shader::Shader(const std::string &vertexFile, const std::string &fragmentFile) {
	vertexShaderID = loadFromSource(vertexFile,
	GL_VERTEX_SHADER);
	fragmentShaderID = loadFromSource(fragmentFile,
	GL_FRAGMENT_SHADER);

	programID = glCreateProgram();

	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID);
}
Shader::Shader(const std::string &vertexFile, const std::string &geometryFile,
		const std::string &fragmentFile) {
	vertexShaderID = loadFromSource(vertexFile,
	GL_VERTEX_SHADER);
	geometryShaderID = loadFromSource(geometryFile,
	GL_GEOMETRY_SHADER);
	fragmentShaderID = loadFromSource(fragmentFile,
	GL_FRAGMENT_SHADER);

	programID = glCreateProgram();

	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, geometryShaderID);
	glAttachShader(programID, fragmentShaderID);
}

Shader::Shader(const std::string &vertexFile,
		const std::string &tessellationControlFile,
		const std::string &tessellationEvaluationFile,
		const std::string &geometryFile, const std::string &fragmentFile) {
	vertexShaderID = loadFromSource(vertexFile,
	GL_VERTEX_SHADER);
	tessControlShaderID = loadFromSource(tessellationControlFile,
			GL_TESS_CONTROL_SHADER);
	tessEvaluationShaderID = loadFromSource(tessellationEvaluationFile,
			GL_TESS_EVALUATION_SHADER);
	geometryShaderID = loadFromSource(geometryFile,
	GL_GEOMETRY_SHADER);
	fragmentShaderID = loadFromSource(fragmentFile,
	GL_FRAGMENT_SHADER);

	programID = glCreateProgram();

	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, tessControlShaderID);
	glAttachShader(programID, tessEvaluationShaderID);
	glAttachShader(programID, geometryShaderID);
	glAttachShader(programID, fragmentShaderID);
}

void Shader::finishInit() {
	glLinkProgram(programID);
	GLint linkRes = 0;
	glGetProgramiv(programID, GL_LINK_STATUS, &linkRes);
	if (linkRes == GL_FALSE) {
		std::cout << "Error while linking shader" << std::endl;
		GLint sizeNeeded = 0;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &sizeNeeded);
		GLchar *strbuff = (GLchar*) malloc(sizeNeeded);

		glGetProgramInfoLog(programID, sizeNeeded, NULL, strbuff);
		std::cout << strbuff << std::endl;
		free(strbuff);

		throw std::string("Shader compile error");
	} else {
//		std::cout << "GLSL program linked successfully" << std::endl;
	}
	glValidateProgram(programID);
}
//
//Shader::Shader(Shader &&shader) :
//		programID(shader.programID), computeShaderID(shader.computeShaderID), vertexShaderID(
//				shader.vertexShaderID), tessControlShaderID(
//				shader.tessControlShaderID), tessEvaluationShaderID(
//				shader.tessEvaluationShaderID), geometryShaderID(
//				shader.geometryShaderID), fragmentShaderID(
//				shader.fragmentShaderID), uniforms(std::move(shader.uniforms)) {
//
//	shader.programID = 0;
//	shader.computeShaderID = 0;
//	shader.vertexShaderID = 0;
//	shader.tessControlShaderID = 0;
//	shader.tessEvaluationShaderID = 0;
//	shader.geometryShaderID = 0;
//	shader.fragmentShaderID = 0;
//}
//
//Shader& Shader::operator=(Shader&& shader){
//	programID = shader.programID;
//	computeShaderID = shader.computeShaderID;
//	vertexShaderID = shader.vertexShaderID;
//	tessControlShaderID = shader.tessControlShaderID;
//	tessEvaluationShaderID = shader.tessEvaluationShaderID;
//	geometryShaderID = shader.geometryShaderID;
//	fragmentShaderID = shader.fragmentShaderID;
//	uniforms = std::move(shader.uniforms);
//
//	shader.programID = 0;
//	shader.computeShaderID = 0;
//	shader.vertexShaderID = 0;
//	shader.tessControlShaderID = 0;
//	shader.tessEvaluationShaderID = 0;
//	shader.geometryShaderID = 0;
//	shader.fragmentShaderID = 0;
//	return *this;
//}

Shader::~Shader() {
	glUseProgram(0);
	if (computeShaderID != 0)
		glDetachShader(programID, computeShaderID);
	if (vertexShaderID != 0)
		glDetachShader(programID, vertexShaderID);
	if (tessControlShaderID != 0)
		glDetachShader(programID, tessControlShaderID);
	if (tessEvaluationShaderID != 0)
		glDetachShader(programID, tessEvaluationShaderID);
	if (geometryShaderID != 0)
		glDetachShader(programID, geometryShaderID);
	if (fragmentShaderID != 0)
		glDetachShader(programID, fragmentShaderID);

	if (computeShaderID != 0)
		glDeleteShader(computeShaderID);
	if (vertexShaderID != 0)
		glDeleteShader(vertexShaderID);
	if (tessControlShaderID != 0)
		glDeleteShader(tessControlShaderID);
	if (tessEvaluationShaderID != 0)
		glDeleteShader(tessEvaluationShaderID);
	if (geometryShaderID != 0)
		glDeleteShader(geometryShaderID);
	if (fragmentShaderID != 0)
		glDeleteShader(fragmentShaderID);

	glDeleteProgram(programID);
}

void Shader::start() const {
	glUseProgram(programID);
}

void Shader::stop() const {
	glUseProgram(0);
}

ShaderPreprocessor* Shader::preprocessor = nullptr;
std::vector<const char*> Shader::includes;

void Shader::buildNamedString(const char* path, const char* name) {
	std::string fullPath = "resources/shaders/" + std::string(path);

	std::ifstream f(fullPath);
	if (!f.is_open()) {
		std::cout << "Couldn't find " << path << std::endl;
		char buff[1024];
		_getcwd((char*) &buff, 1024); //current working directory
		std::cout << "Last dir searched: " << buff << "\\" << fullPath
				<< std::endl;
		exit(EXIT_FAILURE);
	}

	std::stringstream ss;
	std::string line;
	while(std::getline(f, line)){
		ss << line <<"\n";
	}
	std::string contents = ss.str();
	if(preprocessor){
		preprocessor->checkAndReplace(contents, path);
	}

	glNamedStringARB(GL_SHADER_INCLUDE_ARB, -1, name, -1, contents.c_str());
	includes.push_back(name);
}



GLint Shader::loadFromFile(const char *filePath, GLuint programType) {

	std::string fullPath = "resources/shaders/" + std::string(filePath);

	std::ifstream f(fullPath);

	if (!f.is_open()) {
		std::cout << "Couldn't find " << filePath << std::endl;
		char buff[1024];
		_getcwd((char*) &buff, 1024); //current working directory
		std::cout << "Last dir searched: " << buff << "\\" << fullPath
				<< std::endl;
		exit(EXIT_FAILURE);
	}


	std::stringstream ss;
	std::string line;
	while(std::getline(f, line)){
		ss << line <<"\n";
	}
	std::string contents = ss.str();
	if(preprocessor){
		preprocessor->checkAndReplace(contents, filePath);
	}

	GLint shaderID = glCreateShader(programType);
	const char *c_str = contents.c_str();
	int length = contents.length();
	glShaderSource(shaderID, 1, &c_str, &length);
//	glCompileShader(shaderID);
	glCompileShaderIncludeARB(shaderID, includes.size(), includes.data(), nullptr);

	GLint status = 0;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {
		std::cout << "Error while compiling " << filePath << " :"
				<< std::endl;
		GLint sizeNeeded = 0;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &sizeNeeded);
		std::string msg(sizeNeeded, ' ');

		glGetShaderInfoLog(shaderID, sizeNeeded, NULL, msg.data());
		std::cout << msg << std::endl;

		{
		std::ofstream output("shaderLog.txt");
		output <<contents;
		}
		exit(EXIT_FAILURE);
	}

	return shaderID;
}

GLint Shader::loadFromSource(const std::string &file, GLuint programType) {

	GLchar *programSource = (GLchar*) malloc(file.size() + 1);
	for (uint32_t i = 0; i < file.size(); i++) {
		programSource[i] = file[i];
	}
	programSource[file.size()] = '\0';

	GLint shaderID = glCreateShader(programType);
	GLint *length = NULL;
	glShaderSource(shaderID, 1, (const GLchar* const*) &programSource, length);
	glCompileShader(shaderID);

	GLint status = 0;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {
		std::cout << "Error while compiling shader string."<< std::endl;
		GLint sizeNeeded = 0;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &sizeNeeded);
		GLchar *strbuff = (GLchar*) malloc(sizeNeeded);

		glGetShaderInfoLog(shaderID, sizeNeeded, NULL, strbuff);
		std::cout << strbuff << std::endl;
		free(strbuff);

		throw std::string("Shader compile error: \n" + file);
	}

	free(programSource);
	programSource = NULL;

	return shaderID;
}

void Shader::init_uniforms(const std::vector<std::string> &names) {
	start();
	for (const std::string &name : names) {
		GLint loc = getUniformLocation(name.c_str());

		if (loc == -1) {
			std::cout << "Uniform location of " << name << " = " << loc
					<< std::endl;
			std::cout
					<< " 	--> The uniform variable name is either incorrect or the uniform variable is not used"
					<< std::endl;
			uniforms[name] = loc;
		} else {
			uniforms[name] = loc;
		}

	}
	stop();
}

void Shader::bindVertexAttribute(GLuint attribute, const char *variableName) {
	glBindAttribLocation(programID, attribute, variableName);
}

void Shader::bindFragDataLocation(GLuint colorAttachment,
		const char *variableName) {
	glBindFragDataLocation(programID, colorAttachment, variableName);
}

GLint Shader::getUniformLocation(const char *variableName) {
	return glGetUniformLocation(programID, variableName);
}

void Shader::connectTextureUnit(const std::string &sampler_name, GLint value) {
	loadInt(sampler_name, value);
}

void Shader::loadInt(const std::string &name, GLint value) const {
	glUniform1i(findUniformLoc(name), value);
}

void Shader::loadInt(const std::string &name, GLuint value) const {
	glUniform1ui(findUniformLoc(name), value);
}

void Shader::loadFloat(const std::string &name, float value) const {
	glUniform1f(findUniformLoc(name), value);
}

void Shader::loadVec2(const std::string &name, glm::vec2 v) const {
	glUniform2f(findUniformLoc(name), v[0], v[1]);
}

void Shader::loadiVec2(const std::string &name, glm::ivec2 v) const {
	glUniform2i(findUniformLoc(name), v[0], v[1]);
}

void Shader::loaduVec2(const std::string &name, glm::uvec2 v) const {
	glUniform2ui(findUniformLoc(name), v[0], v[1]);
}

void Shader::loadVec3(const std::string &name, glm::vec3 v) const {
	glUniform3f(findUniformLoc(name), v[0], v[1], v[2]);
}

void Shader::loadiVec3(const std::string &name, glm::ivec3 v) const {
	glUniform3i(findUniformLoc(name), v[0], v[1], v[2]);
}

void Shader::loaduVec3(const std::string &name, glm::uvec3 v) const {
	glUniform3ui(findUniformLoc(name), v[0], v[1], v[2]);
}

void Shader::loadVec4(const std::string &name, glm::vec4 v) const {
	glUniform4f(findUniformLoc(name), v[0], v[1], v[2], v[3]);
}

void Shader::loadiVec4(const std::string &name, glm::ivec4 v) const {
	glUniform4i(findUniformLoc(name), v[0], v[1], v[2], v[3]);
}

void Shader::loaduVec4(const std::string &name, glm::uvec4 v) const {
	glUniform4ui(findUniformLoc(name), v[0], v[1], v[2], v[3]);
}

void Shader::loadMat3(const std::string& name, glm::mat3 mat) const{
	static float buffer[9];

	for (int x = 0; x < 3; x++) {
		for (int y = 0; y < 3; y++) {
			buffer[3 * x + y] = mat[x][y];
		}
	}

	glUniformMatrix3fv(findUniformLoc(name), 1,
	GL_FALSE, (const GLfloat*) &buffer);
}
void Shader::loadMat4(const std::string &name, glm::mat4x4 mat) const {
	static float buffer[16];

	for (int x = 0; x < 4; x++) {
		for (int y = 0; y < 4; y++) {
			buffer[4 * x + y] = mat[x][y];
		}
	}

	glUniformMatrix4fv(findUniformLoc(name), 1,
	GL_FALSE, (const GLfloat*) &buffer);
}

/**
 * Creates a new fbo with a size of 1x1.
 *
 * @return a new fbo.
 */
FBO::FBO(std::string name, int MULTISAMPLE_COUNT) :
		FBO(name, 1, 1, MULTISAMPLE_COUNT) {

}

/**
 * Creates a new fbo with the specified dimensions.
 *
 * @param width
 * @param height
 * @return a new FBO.
 */
FBO::FBO(std::string name, int width, int height, int MULTISAMPLE_COUNT) :
		ID(0), width(width), height(height), MULTISAMPLE_COUNT(
				MULTISAMPLE_COUNT), name(name) {
	glGenFramebuffers(1, &ID);
}

FBO::~FBO() {

	for (const auto &it : attachments) {
		FBOAttachment *attachment = it.second;
		GLuint id = attachment->getID();
		if (attachment->isTexture()) {
			glDeleteTextures(1, &id);
		} else {
			glDeleteRenderbuffers(1, &id);
		}
	}

	glDeleteFramebuffers(1, &ID);
}

/**
 * Copy a rectangle of pixels from src to dest. If the sizes don't match,
 * the src rectangle is stretched to fit the dest rectangle.
 * src or dest can be NULL to indicate the default FBO
 *
 * @param src
 *            The first fbo from which values will be copied.
 * @param srcRegion
 *            The src rectangle region in (X0, Y0, X1, Y1) format.
 * @param readColorAttachments
 * 			  The name of the color attachment from src that will be read from.
 * 			  It is an error to specify more than one read buffer.
 * 			  Must be empty if src is NULL or if GL_COLOR_BUFFER_BIT is not given as a mask flag.
 * @param dest
 *            The dest fbo which will receive the pixels
 * @param destRegion
 *            The dest rectangle region in (X0, Y0, X1, Y1) format.
 * @param drawColorAttachments
 * 			  The names of the color attachments from dest that will be drawn to.
 * 			  More than one draw buffer can be specified.
 * 			  Note that the formats must correspond with that of the read buffer.
 * 			  Must be empty if dest is NULL or if GL_COLOR_BUFFER_BIT is not given as a mask flag.
 * @param mask
 *            Which buffers are to be copied: GL_COLOR_BUFFER_BIT |
 *            GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT
 * @param pixelSizes
 *            true if the regions are expressed in pixels, false if they are
 *            expressed in relative coords (i.e [0.0, 1.0]).
 */
void FBO::blitFBOs(const FBO* src, glm::vec4 srcRegion, std::initializer_list<std::string> readColorAttachments, const FBO* dest,
		glm::vec4 destRegion, std::initializer_list<std::string> drawColorAttachments, int mask, bool pixelSizes) {

	if (src != NULL) {
		glBindFramebuffer(GL_READ_FRAMEBUFFER, src->ID);
		if((mask & GL_COLOR_BUFFER_BIT) != 0){
			if(readColorAttachments.size() != 1){
				std::string msg = std::string("More than one color attachment has been given to read from in blitFBOs: ");
				for(const auto& s : readColorAttachments){
					msg += s + " ";
				}
				throw msg;
			}
			const FBOAttachment *attachment = src->getAttachment(*readColorAttachments.begin());
			if (attachment->getFormat().isColorAttachment) {
				glReadBuffer(attachment->getColorAttachmentNumber());
			} else {
				throw "Error, " + *readColorAttachments.begin() + " isn't a color attachment for the read framebuffer";
			}
		}else{
			if(readColorAttachments.size() != 0){
				throw std::string("Error, cannot specify a src color attachment in blitFBOs without specifying the flag GL_COLOR_BUFFER_BIT");
			}
		}
	} else {
		if(readColorAttachments.size() != 0){
			throw std::string("Error, cannot specify a src color attachment in blitFBOs for the default framebuffer");
		}

		glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
		glReadBuffer(GL_BACK);
	}

	if (dest != NULL) {
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, dest->ID);

		if((mask & GL_COLOR_BUFFER_BIT) != 0){
			if(drawColorAttachments.size() == 0){
				throw std::string("Error, at least one color attachment must be given to draw to if the flag GL_COLOR_BUFFER_BIT is given and the dest fbo is not the default framebuffer.");
			}

			for (auto &attachmentName : drawColorAttachments) {
				const FBOAttachment *attachment = dest->getAttachment(attachmentName);
				if (attachment->getFormat().isColorAttachment) {
					glDrawBuffer(attachment->getColorAttachmentNumber());
				} else {
					throw "Error, " + attachmentName + " isn't a color attachment for the draw framebuffer";
				}
			}

		}else{
			if(drawColorAttachments.size() != 0){
				throw std::string("Error, cannot specify a dest color attachment in blitFBOs without specifying the flag GL_COLOR_BUFFER_BIT");
			}
		}



	} else {
		if(drawColorAttachments.size() != 0){
			throw std::string("Error, cannot specify a dest color attachment in blitFBOs for the default framebuffer");
		}

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		glDrawBuffer(GL_BACK);
	}

	int srcX0, srcY0, srcX1, srcY1, destX0, destY0, destX1, destY1;

	if (pixelSizes) {

		srcX0 = (int) srcRegion.x;
		srcY0 = (int) srcRegion.y;
		srcX1 = (int) srcRegion.z;
		srcY1 = (int) srcRegion.w;

		destX0 = (int) destRegion.x;
		destY0 = (int) destRegion.y;
		destX1 = (int) destRegion.z;
		destY1 = (int) destRegion.w;

	} else {

		if (src == NULL || dest == NULL) {
			throw "Cannot use fract sizes when bliting FBOs when src or dest is NULL";
		}

		srcX0 = (int) srcRegion.x * src->width;
		srcY0 = (int) srcRegion.y * src->height;
		srcX1 = (int) srcRegion.z * src->width;
		srcY1 = (int) srcRegion.w * src->height;

		destX0 = (int) destRegion.x * dest->width;
		destY0 = (int) destRegion.y * dest->height;
		destX1 = (int) destRegion.z * dest->width;
		destY1 = (int) destRegion.w * dest->height;
	}

	int filter = GL_LINEAR;
	if ((mask & GL_DEPTH_BUFFER_BIT) != 0
			|| (mask & GL_STENCIL_BUFFER_BIT) != 0) {
		filter = GL_NEAREST;
	}

	glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, destX0, destY0, destX1,
			destY1, mask, filter);

	//Reset
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
	glReadBuffer(GL_BACK);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glDrawBuffer(GL_BACK);
}

/**
 * Make sure to call this method after binding the FBO, even with no
 * parameter. Binds an array of color attachments. An attachment that
 * doesn't exists is ignored.
 *
 * @param colorAttachments
 *            The color attachments that have to be bound.
 *
 */
void FBO::bindColorAttachments(
		std::initializer_list<std::string> colorAttachments) {
	drawBuffers.clear();

	for (auto &attachmentName : colorAttachments) {
		const FBOAttachment *attachment = getAttachment(attachmentName);
		if (attachment->getFormat().isColorAttachment) {
			drawBuffers.push_back(attachment->getColorAttachmentNumber());
		} else {
			throw "Error, " + attachmentName + " isn't a color attachment";
		}
	}

	if (drawBuffers.empty()) {
		drawBuffers.push_back(GL_NONE);
	}
	glDrawBuffers(drawBuffers.size(), drawBuffers.data());

}

/**
 * Sets the read and draw buffers to GL_NONE, ie: there is no color data.
 * Useful for shadow mapping for eg.
 */
void FBO::bindNoColorBuffers() {
	glReadBuffer(GL_NONE);
	glDrawBuffer(GL_NONE);
}

/**
 * Sets the viewport to the current size of that FBO.
 */
void FBO::setViewport() {
	glViewport(0, 0, width, height);
}

/**
 * Resize all the FBOAttachments of this FBO. Does nothing if the size is
 * the same.
 *
 * @param width
 * @param height
 */
void FBO::resize(int width, int height) {
	if (this->width == width && this->height == height) {
		return;
	}

	this->width = width;
	this->height = height;

	bind();

	for (const auto &it : attachments) {
		FBOAttachment *attachment = it.second;
		if (attachment->isTexture()) {

			glBindTexture(attachment->getTarget(), attachment->getID());

			if (MULTISAMPLE_COUNT == 0) {
				glTexImage2D(attachment->getTarget(), 0, attachment->getFormat().internalFormat,
						width, height, 0, attachment->getFormat().format,
						GL_FLOAT,
						NULL);
			} else {
				glTexImage2DMultisample(attachment->getTarget(), MULTISAMPLE_COUNT,
						attachment->getFormat().internalFormat, width, height,
						GL_TRUE);
			}

			glBindTexture(attachment->getTarget(), 0);
		} else {
			glBindRenderbuffer(GL_RENDERBUFFER, attachment->getID());

			glRenderbufferStorage(GL_RENDERBUFFER,
					attachment->getFormat().format, width, height);

			glBindRenderbuffer(GL_RENDERBUFFER, 0);
		}
	}

	unbind();
}

/**
 * Creates a new texture attachment for this fbo.
 *
 * @param name
 * @param format
 */
void FBO::addTextureAttachment(const std::string &name,
		const AttachmentFormat &format) {
	bind();

	GLuint target =
			MULTISAMPLE_COUNT == 0 ? GL_TEXTURE_2D : GL_TEXTURE_2D_MULTISAMPLE;

	GLuint textureID;
	glGenTextures(1, &textureID);

	glBindTexture(target, textureID);

	if (MULTISAMPLE_COUNT == 0) {
		glTexImage2D(target, 0, format.internalFormat, width, height, 0,
				format.format, format.dataType,
				NULL);

		glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	} else {
		glTexImage2DMultisample(target, MULTISAMPLE_COUNT,
				format.internalFormat, width, height, GL_TRUE);
	}

	int colorAttachmentNumber = -1;
	if (format.isColorAttachment) {
		colorAttachmentNumber = this->colorAttachments + format.attachment;

		glFramebufferTexture2D(GL_FRAMEBUFFER, colorAttachmentNumber, target,
				textureID, 0);

		this->colorAttachments++;
	} else {
		glFramebufferTexture2D(GL_FRAMEBUFFER, format.attachment, target,
				textureID, 0);
	}

	FBOAttachment *attachment = new FBOAttachment(textureID, true, MULTISAMPLE_COUNT != 0, format, name,
			colorAttachmentNumber, target);

	attachments[name] = attachment;

	glBindTexture(target, 0);
	unbind();
}

/**
 * Creates a new render buffer attchment. Rendering to a render buffer is
 * faster than rendering to a texture but you can't sample from a render
 * buffer.
 * @param name
 * @param format
 */
void FBO::createRenderBufferAttachement(const std::string &name,
		const AttachmentFormat &format) {
	bind();

	GLuint renderBufferID;
	glGenRenderbuffers(1, &renderBufferID);
	glBindRenderbuffer(GL_RENDERBUFFER, renderBufferID);

	glRenderbufferStorage(GL_RENDERBUFFER, format.internalFormat, width,
			height);

	int colorAttachmentNumber = -1;
	if (format.isColorAttachment) {
		colorAttachmentNumber = this->colorAttachments + format.attachment;

		glFramebufferRenderbuffer(GL_FRAMEBUFFER, colorAttachmentNumber,
		GL_RENDERBUFFER, renderBufferID);

		this->colorAttachments++;
	} else {

		glFramebufferRenderbuffer(GL_FRAMEBUFFER, format.attachment,
		GL_RENDERBUFFER, renderBufferID);

	}

	FBOAttachment *attachment = new FBOAttachment(renderBufferID, false, MULTISAMPLE_COUNT == 0, format,
			name, colorAttachmentNumber, -1);
	attachments[name] = attachment;

	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	unbind();
}

/**
 * Binds this framebuffer. Make sure to unbind the fbo when you are finished
 * using it ! This method is called automatically when attachments are
 * created.
 */
void FBO::bind() {
	//Bind both to read and draw framebuffers
	glBindFramebuffer(GL_FRAMEBUFFER, ID);
}

/**
 * Unbinds the current FBO.
 */
void FBO::unbind() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

/**
 * @return true if the FBO is complete.
 */
bool FBO::finish() {
	bool success = false;

	bind();
	drawBuffers.clear();
	drawBuffers.push_back(GL_NONE);
	glDrawBuffers(drawBuffers.size(), drawBuffers.data());

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE) {
		success = true;
	}

	unbind();
	return success;
}

/**
 * Clears a specific color attachment. The attachment must have been bound
 * with <code>bindColorAttachments(String... colorAttachments)</code>
 * otherwise an exception is thrown
 *
 * @param attachmentName
 *            The name of the color attachment.
 * @param color
 *            The color to clear the attachment with.
 */
void FBO::clearColorAttachment(const std::string &attachmentName,
		glm::vec4 color) {

	const FBOAttachment *attachment = getAttachment(attachmentName);
	if (attachment->getFormat().isColorAttachment) {

		for (uint32_t i = 0; i < drawBuffers.size(); i++) {
			if (attachment->getColorAttachmentNumber() == drawBuffers.at(i)) {
				const GLfloat buffer[4] = { color.x, color.y, color.z, color.w };
				glClearBufferfv(GL_COLOR, i, &buffer[0]);
				return;
			}
		}

		throw "FBO color attachment " + attachmentName
				+ " isn't currently bound, cannot clear";
	}
	throw attachmentName + " isn't a color buffer";
}

/**
 * Clears the depth buffer attachment of that FBO. Make sure depth writing
 * is enabled, otherwise nothing will happen.
 *
 * @param value
 *            The float value to clear the buffer with. Should be in the range
 *            [0.0f, 1.0f]
 */
void FBO::clearDepthAttachment(float value) {
	glClearBufferfv(GL_DEPTH, 0, &value);
}

/**
 * Clears the stencil buffer attachment of that FBO.
 *
 * @param value
 *            The int value to clear the buffer with. Must be in the range
 *            [0, 2^m-1] where m is the number of bits in the stencil
 *            buffer.
 */
void FBO::clearStencilAttachment(int value) {
	glClearBufferiv(GL_STENCIL, 0, &value);
}

void ShaderPreprocessor::checkAndReplace(std::string &contents,
		std::string filePath) {

	auto substr_view = [](const std::string& source, size_t offset = 0,
	                std::string_view::size_type count =
	                std::numeric_limits<std::string_view::size_type>::max()) {
	    if (offset < source.size())
	        return std::string_view(source.data() + offset,
	                        std::min(source.size() - offset, count));
	    return std::string_view{};
	};

	std::string result;
	result.reserve(contents.size() * 2);

	std::size_t n = 0;
	std::size_t n_next = 0;
	while((n_next = contents.find('#', n)) < contents.npos){
		result += contents.substr(n, n_next - n);

		n = n_next;
		bool matched = false;
		for(const auto& [m, r] : replacements){
			auto v = substr_view(contents, n, m.length());
			if(v == m){
				result += r;
				n += m.length();
				matched = true;
				break;
			}
		}

		if(!matched){
			result += '#';
			n++;
		}
	}

	result += contents.substr(n, n_next);

	contents = result;

	if(false){
		std::string path = "preprocessor/" + filePath;
		std::string dirpath = path.substr(0, path.find_last_of("\\/"));
		std::filesystem::create_directories(dirpath);
		std::ofstream output(path);
		output <<contents;
	}

}

}
