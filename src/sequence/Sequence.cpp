/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Sequence.cpp
 *
 *  Created on: 6 oct. 2021
 *      Author: Briac
 */

#include "Sequence.h"

#include <sstream>
#include <iostream>
#include <numbers>
#include <string>
#include <filesystem>
#include <chrono>
#include <iomanip>

#include "../imgui/imgui.h"
#include "../rendering_base/ModelLoader.h"

#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/matrix_inverse.hpp"
#include "../glm/gtx/string_cast.hpp"
#include "../glm/gtx/matrix_operation.hpp"

#define GLT_IMPORTS
#include "../GLText/gltext.h"

#include "../IconFontCppHeaders/IconsFontAwesome5.h"

#include "../imgui/progress_bar.h"

#include "SceneReferenceFrame.h"

using namespace RenderingBase;

enum ImageDisplayModes {
	NONE = 0,
	ORIGINAL_IMAGE,
	BACKGROUND,
	SEGMENTED_IMAGE,
	DONT_CARE_MASK_EDITION,
	GLOBAL_INTEGRATION_IMAGE,
	REMAINING_TRANSPARENCY,
	GEOMETRY
};


Sequence::Sequence(SequenceInfo sequenceInfo, GLFWwindow* w) : sequenceInfo(sequenceInfo) {

	imageLODs.at(0).cameras = sequenceInfo.cameras;
	for(int imagePyramidLevel=1; imagePyramidLevel < imagePyramidLevels; imagePyramidLevel++){
		//Copy the cameras
		imageLODs.at(imagePyramidLevel).cameras = imageLODs.at(imagePyramidLevel-1).cameras;
		//Divide the sizes by two
		for(CameraInfo& camInfo : imageLODs.at(imagePyramidLevel).cameras){
			const int width = camInfo.width / 2;
			const int height = camInfo.height / 2;
			const float sx = float(width) / camInfo.width;
			const float sy = float(height) / camInfo.height;
			camInfo.K[0][0] *= sx;
			camInfo.K[2][0] *= sx;
			camInfo.K[1][1] *= sy;
			camInfo.K[2][1] *= sy;

			if(camInfo.projection_model == PROJECTION_FOURDVIEW){
				camInfo.distortion[0] *= pow(sx, -2.0);//k1
				camInfo.distortion[1] *= pow(sy, -4.0);//k2
				camInfo.distortion[2] *= sx;//cx_dist
				camInfo.distortion[3] *= sy;//cy_dist
			}

			camInfo.width = width;
			camInfo.height = height;
		}
	}
	prepareCameras();

	ModelLoader::load(gridFloor, "plane_16.obj", 0, true);

	simpleShader.bindVertexAttribute(0, "position");
	simpleShader.finishInit();
	simpleShader.init_uniforms( { "transform", "color" });

	cameraFrustumsShader.bindVertexAttribute(0, "position");
	cameraFrustumsShader.bindVertexAttribute(1, "cameraIndex");
	cameraFrustumsShader.finishInit();
	cameraFrustumsShader.init_uniforms( { "projectionView", "transform",
			"selectedCamera", "SelectedDepthMap", "PROJECTION_TYPE",
			"canvas_transform", "RemoveDistortion", "activeCameras" });

	quadShader.bindVertexAttribute(0, "position");
	quadShader.finishInit();
	quadShader.init_uniforms( { "transform",
			"displayMode", "viewpoint", "CursorNDCPos", "BrushRadius",
			"RemoveDistortion", "DepthMapSelection",
			"DepthTiles", "DepthTilesOffset",
			"ApplyExposureCorrection", "doNotUseOriginalImage"
			});

	quad.bind();
	float positions[] = { -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f };
	quad.createFloatAttribute(0, positions, 8, 2, 0, GL_STATIC_DRAW);
	quad.unbind();

	std::vector<glm::vec3> imageBorder;
	imageBorder.push_back(glm::vec3(-1, -1, 0));
	imageBorder.push_back(glm::vec3(-1, 1, 0));
	imageBorder.push_back(glm::vec3(1, 1, 0));
	imageBorder.push_back(glm::vec3(1, -1, 0));

	std::vector<unsigned int> imageBorder_indices = { 0, 1, 1, 2, 2, 3, 3, 0 };

	imageBorderVAO.bind();
	imageBorderVAO.createIndexBuffer(imageBorder_indices.data(),
			imageBorder_indices.size());
	imageBorderVAO.createFloatAttribute(0, &imageBorder[0].x,
			imageBorder.size() * 3, 3, 0, GL_STATIC_DRAW);
	imageBorderVAO.unbind();

	glm::ivec2 sizes = glm::ivec2(imageLODs.at(0).cameras.at(0).width, imageLODs.at(0).cameras.at(0).height);
	for (const CameraInfo &cam : imageLODs.at(0).cameras) {
		if (cam.width != sizes.x || cam.height != sizes.y) {
			throw std::string(
					"Error, all the cameras must have the same resolution!");
		}
	}

	ExposureLUT = std::make_unique<Texture1D>(sequenceInfo.nbCameras, GL_RG32F);
	for(int imagePyramidLevel=0; imagePyramidLevel < imagePyramidLevels; imagePyramidLevel++){
		const int s = 1 << imagePyramidLevel;
		imageLODs.at(imagePyramidLevel).allViewpoints.init(sizes.x / s, sizes.y / s, sequenceInfo.nbCameras, GL_RGBA8);
		imageLODs.at(imagePyramidLevel).ExposureLUT = ExposureLUT.get();
	}

	sharedContext = std::make_unique<SharedContext>(w);

	backgrounds.setNbCameras(sequenceInfo.nbCameras, sizes.x, sizes.y, sequenceInfo.hasSilhouettes);

	depthmaps.init();

	loadBackgrounds(backgrounds.getBackgroundsDir());

	differentialRendering.resetExposureCorrection(sequenceInfo.nbCameras);
	params.getInt("SelectedLOD") = 0;
	params.getBool("doNotUseOriginalImage") = false;
}

Sequence::~Sequence() {
	params.getBool("FullAuto") = false;
}

void Sequence::buildImagePyramid(){

	for(int imagePyramidLevel=1; imagePyramidLevel < imagePyramidLevels; imagePyramidLevel++){
		downscale.apply(imageLODs.at(imagePyramidLevel-1).allViewpoints,
						imageLODs.at(imagePyramidLevel).allViewpoints);
	}

}

void Sequence::prepareVolume(){
	bool loadAllViewPoints = params.getBool("LoadAllViewPoints");
	int selectedCameraIndex = params.getInt("SelectedCamera");

	auto begin = std::chrono::steady_clock::now();

	if(!params.getBool("SkipVisualHull")){
		backgrounds.applySegmentation(depthmaps, loadAllViewPoints? -1:selectedCameraIndex);
		visualHull.raytracingOnBorders(depthmaps, sparseGrid);
		visualHull.build2DOccupancyTextures(depthmaps, downscale);
		sparseGrid.build(depthmaps);
		differentialRendering.setFinished(false);
	}else{
		sparseGrid.build();
	}
	differentialRendering.allocateIntervals(backgrounds, sparseGrid);
	glFlush();
	glFinish();
	auto end = std::chrono::steady_clock::now();
	volumePreparationTime = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
	differentialRendering.resetElapsedTime();
}

void Sequence::exportMesh(){
	int selectedTimeStamp = params.getInt("SelectedTimestamp");

	std::stringstream path;
	path <<params.getString("Window#ExportDir");
	path <<"/LOD" <<std::to_string(sparseGrid.getCurrentLOD());

	std::filesystem::create_directories(path.str());

	path <<"/mesh_" << selectedTimeStamp;

	std::cout <<"Exporting mesh to " <<path.str() <<"." <<exportFileFormat <<std::endl;


	auto f = sparseGrid.exportMeshTo(path.str(), exportFileFormat, params.getBool("exportOnlyBinaryBuffers"));

	sharedContext->scheduleTask([f = std::move(f), this](){
		imageLoader.exec(f());
	});
}


void Sequence::updateUI(RenderingBase::Camera &camera) {

	int &selectedTimeStamp = params.getInt("SelectedTimestamp");
	int &selectedCameraIndex = params.getInt("SelectedCamera");

	// Arrow buttons with Repeater
	const float spacing = ImGui::GetStyle().ItemInnerSpacing.x;
	ImGui::PushButtonRepeat(true);
	if (ImGui::ArrowButton("##timestampleft", ImGuiDir_Left)) {
		selectedTimeStamp--;
	}
	ImGui::SameLine(0.0f, spacing);
	if (ImGui::ArrowButton("##timestampright", ImGuiDir_Right)) {
		selectedTimeStamp++;
	}
	ImGui::PopButtonRepeat();
	ImGui::SameLine();
	ImGui::SliderInt("Selected timestamp", &selectedTimeStamp, sequenceInfo.beginId, sequenceInfo.endId);

	if (selectedTimeStamp > sequenceInfo.endId) {
		selectedTimeStamp = sequenceInfo.beginId;
	}
	if (selectedTimeStamp < sequenceInfo.beginId) {
		selectedTimeStamp = sequenceInfo.endId;
	}

	ImGui::PushButtonRepeat(true);
	if (ImGui::ArrowButton("##selected_camera_left", ImGuiDir_Left)) {
		selectedCameraIndex--;
	}
	ImGui::SameLine(0.0f, spacing);
	if (ImGui::ArrowButton("##selected_camera_right", ImGuiDir_Right)) {
		selectedCameraIndex++;
	}
	ImGui::PopButtonRepeat();
	ImGui::SameLine();
	ImGui::SliderInt("Selected camera", &selectedCameraIndex, 0, sequenceInfo.nbCameras - 1);

	if (glfwGetKey(camera.getWindow(), GLFW_KEY_LEFT) == GLFW_RELEASE) {
		selectedCameraIndex--;
	}
	if (glfwGetKey(camera.getWindow(), GLFW_KEY_RIGHT) == GLFW_RELEASE) {
		selectedCameraIndex++;
	}

	if (selectedCameraIndex > sequenceInfo.nbCameras - 1) {
		selectedCameraIndex = 0;
	}
	if (selectedCameraIndex < 0) {
		selectedCameraIndex = sequenceInfo.nbCameras - 1;
	}

	ImGui::SliderInt("Image level of detail", &params.getInt("SelectedLOD"), 0, imagePyramidLevels-1, "%d", ImGuiSliderFlags_AlwaysClamp);
	if(params.getInt("SelectedLOD") < 0){
		params.getInt("SelectedLOD") = 0;
	}
	if(params.getInt("SelectedLOD") > imagePyramidLevels-1){
		params.getInt("SelectedLOD") = imagePyramidLevels-1;
	}


	ImGui::Checkbox("Take selected camera viewpoint",
			&params.getBool("TakeSelectedCameraVewpoint"));
	ImGui::Checkbox("Remove distortion",
			&params.getBool("RemoveDistortion"));

	ImGui::SameLine();
	ImGui::Checkbox("Show exposure correction",
			&params.getBool("ApplyExposureCorrection"));

//	ImGui::Checkbox("doNotUseOriginalImages", &params.getBool("doNotUseOriginalImage"));

	if (params.getBool("TakeSelectedCameraVewpoint")) {
		ImGui::Checkbox("Draw border", &params.getBool("DrawBorder"));
		ImGui::Checkbox("Draw border BVH", &params.getBool("DrawBorderBVH"));
	}

	ImGui::Checkbox("Load all viewpoints",
			&params.getBool("LoadAllViewPoints"));

	const bool loadAllViewPoints = params.getBool("LoadAllViewPoints");

	const char *modes =
			"None\0Original image\0Background\0Segmented image\0Don't care mask edition\0Global integration\0Remaining Transparency\0Geometry\0\0";
	ImGui::Combo("Display Mode", &params.getInt("ImageDisplayMode"), modes, 10);
	if (glfwGetKey(camera.getWindow(), GLFW_KEY_1) == GLFW_PRESS) {
		params.getInt("ImageDisplayMode") = ImageDisplayModes::NONE;
	}
	if (glfwGetKey(camera.getWindow(), GLFW_KEY_2) == GLFW_PRESS) {
		params.getInt("ImageDisplayMode") = ImageDisplayModes::ORIGINAL_IMAGE;
	}
	if (glfwGetKey(camera.getWindow(), GLFW_KEY_3) == GLFW_PRESS) {
		params.getInt("ImageDisplayMode") = ImageDisplayModes::BACKGROUND;
	}
	if (glfwGetKey(camera.getWindow(), GLFW_KEY_4) == GLFW_PRESS) {
		params.getInt("ImageDisplayMode") = ImageDisplayModes::SEGMENTED_IMAGE;
	}
	if (glfwGetKey(camera.getWindow(), GLFW_KEY_5) == GLFW_PRESS) {
		params.getInt("ImageDisplayMode") =
				ImageDisplayModes::GLOBAL_INTEGRATION_IMAGE;
	}
	if (glfwGetKey(camera.getWindow(), GLFW_KEY_6) == GLFW_PRESS) {
		params.getInt("ImageDisplayMode") =
				ImageDisplayModes::REMAINING_TRANSPARENCY;
	}
	if (glfwGetKey(camera.getWindow(), GLFW_KEY_7) == GLFW_PRESS) {
		params.getInt("ImageDisplayMode") =
				ImageDisplayModes::GEOMETRY;
	}

	if (params.getInt("ImageDisplayMode")
			== ImageDisplayModes::DONT_CARE_MASK_EDITION) {
		ImGui::SliderFloat("BrushRadius", &params.getFloat("BrushRadius"), 0.0f,
				1.0f, "%.3f");
	}

	ImVec2 buttonSize = ImVec2(ImGui::GetContentRegionAvailWidth(),
			ImGui::GetTextLineHeight() * 1.5f);
//	if (params.getBool("TakeSelectedCameraVewpoint")
//			&& ImGui::Button("Save image", buttonSize)) {
//		auto time = std::time(nullptr);
//		std::stringstream ss;
//		ss << std::put_time(std::localtime(&time), "%Y-%m-%d_%H-%M-%S"); // ISO 8601 without timezone information.
//		auto s = ss.str();
//		std::replace(s.begin(), s.end(), ':', '-');
//		std::string path = "./" + ss.str() + ".png";
//
//		std::cout << "Saving image at {" << path << "}" << std::endl;
//
//		RenderingBase::Texture2D texture = RenderingBase::Texture2D(path,
//				nullptr, 2048, 2048, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, false);
//
//		GLuint fbo;
//		glGenFramebuffers(1, &fbo);
//		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
//		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
//		GL_TEXTURE_2D, texture.getID(), 0);
//
//		if (glCheckFramebufferStatus(
//		GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
//			throw "Error while creating copy framebuffer";
//		}
//
//		glm::ivec4 oldViewport;
//		glGetIntegerv(GL_VIEWPORT, &oldViewport.x);
//		glViewport(0, 0, texture.getWidth(), texture.getHeight());
//		drawImages( { glm::mat4(1.0) }, selectedCameraIndex);
//
//		glBindFramebuffer(GL_FRAMEBUFFER, 0);
//		glDeleteFramebuffers(1, &fbo);
//
//		texture.saveToDisk(path);
//
//		glViewport(oldViewport.x, oldViewport.y, oldViewport.z, oldViewport.w);
//	}

	bool& FullAuto = params.getBool("FullAuto");
	static bool preloadingNextFrame = false;
	static bool started = false;

	camera.setVsync(!started);
	if(!FullAuto){
		preloadingNextFrame = false;
	}

	ImGui::Separator();
	ImGuiTabBarFlags tab_bar_flags = ImGuiTabBarFlags_None;
	static bool overflow;
	overflow = false;
	if (ImGui::BeginTabBar("MyTabBar", tab_bar_flags)) {
		if (ImGui::BeginTabItem("General")) {
			bool readyToCompute = backgrounds.Loaded() && asyncTexLoader.isFrameComplete(selectedTimeStamp);
			if(!loadAllViewPoints){
				ImGui::TextColored(ImVec4(0, 1, 0, 1), "All viewpoints must be loaded in order to build a mesh !");
			}else {
				//ImGui::Checkbox("Skip visual hull", &params.getBool("SkipVisualHull"));

				if(overflow){
					started = false;
				}

				if(ImGui::Checkbox("Fully automatic mode", &FullAuto)){
					if(!FullAuto){
						started = false;
					}
				}


				ImGui::SameLine();
				ImGui::Checkbox("Export meshes", &params.getBool("ExportMeshes"));
				if(params.getBool("ExportMeshes")){
					ImGui::SameLine();
					ImGui::Checkbox("Only binary buffers", &params.getBool("exportOnlyBinaryBuffers"));
				}


				ImGui::SameLine();
				ImGui::Checkbox("Render mesh alone", &params.getBool("RenderMeshAlone"));

				if(params.getBool("RenderMeshAlone")){
					ImGui::SameLine();
					ImGui::Checkbox("Side by side views", &params.getBool("SideBySideViews"));
				}

				if(!FullAuto){
					if(ImGui::Button("Build image pyramid", buttonSize)) {
						buildImagePyramid();
					}
					if(ImGui::Button("Prepare volume", buttonSize)) {
						prepareVolume();
					}

					if(!started && ImGui::Button("Optimize", buttonSize)){
						differentialRendering.resetElapsedTime();
						started = true;
					}else if(started && ImGui::Button("Stop", buttonSize)){
						started = false;
					}

				}else if(readyToCompute && !started){
					buildImagePyramid();
					prepareVolume();
					preloadingNextFrame = true;
					started = true;
				}

				if(differentialRendering.getTotalIterations() > 0){
					int iterations = differentialRendering.getIterations().at(sparseGrid.getCurrentLOD());
					int maxIterations = differentialRendering.getMaxIterations()[sparseGrid.getCurrentLOD()];
					ImGui::ProgressBar((float)iterations / maxIterations, buttonSize, 0);
				}

				if(started){
					if(sparseGrid.getTotalTiles() > 0){
						differentialRendering.step(backgrounds, sparseGrid, true, [this](){
							if(params.getBool("ExportMeshes")){
								exportMesh();
							}
						});
					}

					if(sparseGrid.getTotalTiles() == 0 || differentialRendering.hasFinished()){
						started = false;
						if(FullAuto){
							if(params.getBool("AutoEval")){
								eval.eval(camera, backgrounds, sparseGrid, differentialRendering, imageLoader);
							}
							if(params.getBool("Window#AutoQuit")){
								params.getBool("Window#ShouldQuit") = true;
							}

							preloadingNextFrame = false;
							selectedTimeStamp++;
							if(selectedTimeStamp > sequenceInfo.endId){
								selectedTimeStamp = sequenceInfo.beginId;
								FullAuto = false;
							}
						}
					}
				}

				ImGui::Separator();

			}

			overflow |= backgrounds.displayStatusMsg();
			overflow |= visualHull.displayStatusMsg();
			overflow |= sparseGrid.displayStatusMsg();
			overflow |= differentialRendering.displayStatusMsg();
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Volume preparation time: %.3f s, Optimization time: %.2f s",
					volumePreparationTime*1.0E-3f, differentialRendering.getElapsedTime());

			ImGui::EndTabItem();
		}

		if (ImGui::BeginTabItem("Segmentation")) {
			bool generateFilters, loadFilters;
			bool updateRequired = backgrounds.updateGUI(camera, generateFilters, loadFilters);

			if (generateFilters)
				this->generateBackgrounds(backgrounds.getBackgroundsDir());
			if (loadFilters)
				this->loadBackgrounds(backgrounds.getBackgroundsDir());
			if (updateRequired) {
				backgrounds.applySegmentation(depthmaps, loadAllViewPoints? -1:selectedCameraIndex);
			}
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Visual hull")) {
			visualHull.updateGUI(camera, backgrounds, depthmaps, downscale, sparseGrid, selectedCameraIndex);
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Sparse grid")) {
			sparseGrid.updateGUI(depthmaps);
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Differential Rendering")) {
			differentialRendering.updateGUI(camera, backgrounds, sparseGrid);
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Debug data")) {
			ddaTest.updateGUIs(camera, imageLODs, depthmaps, sparseGrid, differentialRendering, backgrounds, selectedPixel, selectedDepthTile);
			ImGui::EndTabItem();
		}
//		if (ImGui::BeginTabItem("EnvMap")) {
//			envMap.updateGUI(backgrounds);
//			ImGui::EndTabItem();
//		}
		if (ImGui::BeginTabItem("MeshSequence")) {
			meshSequence.updateGUI([this](std::function<void()>&& f){
				imageLoader.exec(std::move(f));
			}, sequenceInfo.beginId, sequenceInfo.endId);
			ImGui::EndTabItem();
		}
		if(ImGui::BeginTabItem("Evaluate")){
			eval.updateGUIs(camera, backgrounds, sparseGrid, differentialRendering, imageLoader);
			ImGui::EndTabItem();
		}

		ImGui::EndTabBar();
	}

	int prefetchCount = loadAllViewPoints ? multiViewPrefetchCount : singleViewPrefetchCount;
	std::vector<int> timestamps;
	timestamps.reserve(prefetchCount);
	for (int i = -1; i <= prefetchCount; i++) {
		int t = selectedTimeStamp + i;
		if(t > sequenceInfo.endId || t < sequenceInfo.beginId){
			continue;
		}
		timestamps.push_back(t);
	}

	if(preloadingNextFrame && selectedTimeStamp < sequenceInfo.endId){
		asyncTexLoader.update(timestamps, selectedTimeStamp + 1,
				selectedCameraIndex, loadAllViewPoints,
				sequenceInfo.nbCameras, sequenceInfo.sequenceDir,
				sequenceInfo.silhouettesDir, sequenceInfo.hasSilhouettes);
		params.getBool("doNotUseOriginalImage") = true;
	}else{
		asyncTexLoader.update(timestamps, selectedTimeStamp,
				selectedCameraIndex, loadAllViewPoints,
				sequenceInfo.nbCameras, sequenceInfo.sequenceDir,
				sequenceInfo.silhouettesDir, sequenceInfo.hasSilhouettes);
//		params.getBool("doNotUseOriginalImage") = false;
	}


	const auto& shortcut = [&camera](int key, auto action){
		static std::unordered_map<int, int> prev_state;
		int new_state = glfwGetKey(camera.getWindow(), key);
		if(new_state == GLFW_PRESS && prev_state[key] == GLFW_RELEASE){
			action();
		}
		prev_state[key] = new_state;
	};

	//color control
	shortcut(GLFW_KEY_C, [this](){
		params.getBool("SparseGrid#useColors") = !params.getBool("SparseGrid#useColors");
	});

	//render tiles on / off
	shortcut(GLFW_KEY_T, [this](){
		params.getBool("SparseGrid#RenderTSDFTiles") = !params.getBool("SparseGrid#RenderTSDFTiles");
	});

	//rotation control
	shortcut(GLFW_KEY_R, [&camera](){
		camera.setRotateCam(!camera.isRotateCam());
	});

	//start optimization
	shortcut(GLFW_KEY_O, [this](){
		started = !started;
	});

	//start preparation + optimization
	shortcut(GLFW_KEY_A, [this](){
		buildImagePyramid();
		prepareVolume();
		started = true;
	});

	//window control
	shortcut(GLFW_KEY_H, [this](){
		params.getBool("GUIs#MainWindowOpened") = !params.getBool("GUIs#MainWindowOpened");
	});

}

void Sequence::generateBackgrounds(const std::string &filtersDir) {
	backgrounds.computeFromSequence(filtersDir, sequenceInfo.sequenceDir,
			imageLoader, downscale);
}

void Sequence::loadBackgrounds(const std::string &filtersDir) {
	backgrounds.loadFromFile(filtersDir, imageLoader, downscale);
}

void Sequence::render(Camera &camera) {
	glEnable(GL_DEPTH_TEST);
	bool &takeSelectedCameraViewpoint = params.getBool(
			"TakeSelectedCameraVewpoint");
	bool &drawAllImages = params.getBool("LoadAllViewPoints");
	const int selectedCameraIndex = params.getInt("SelectedCamera");

	prepareCameras();

	camera.setMovementsEnabled(!takeSelectedCameraViewpoint);

	if (!takeSelectedCameraViewpoint) {
//		renderGridFloor(camera);

		sparseGrid.render(selectedCameraIndex, camera, params.getBool("RenderMeshAlone"), params.getBool("SideBySideViews"));
		meshSequence.render(camera,
			[this](std::function<void()>&& f){
				imageLoader.exec(std::move(f));
			}
		);

		if(!params.getBool("RenderMeshAlone")){
			ddaTest.render(camera, sparseGrid, imageLODs, RENDER_PROJECTION_TYPE::PROJECT_ON_3D_CANVAS, glm::mat4(1.0f), params.getBool("RemoveDistortion"));

			visualHull.render(camera, depthmaps);
			renderCameraFrustums(camera);

			envMap.render(camera, sparseGrid.getWireframeTileShader());

			const auto &make_transform = [&](const CameraInfo &camData) {
				glm::mat4 transform = glm::mat4(1.0f);
				const auto &K = camData.K;
				const auto &R = camData.R;
				const auto &T = camData.T;

				glm::mat3 KRinv = glm::inverse(K * R);

				glm::vec3 t = KRinv
						* (glm::vec3(0, 0, SceneReferenceFrame::getFrustumsLength()) - K * T);
				transform = glm::mat4(KRinv * SceneReferenceFrame::getFrustumsLength());
				transform[3] = glm::vec4(t, 1.0);

				glm::mat4 toPixelCoords = glm::mat4(1.0);
				toPixelCoords[0][0] = camData.width * 0.5;
				toPixelCoords[1][1] = -camData.height * 0.5;
				toPixelCoords[3][0] = camData.width * 0.5;
				toPixelCoords[3][1] = camData.height * 0.5;
				transform *= toPixelCoords;
				return camera.getProjectionViewMatrix() * transform;
			};

			if (!drawAllImages) {
				glm::mat4 transform = make_transform(
						imageLODs.at(params.getInt("SelectedLOD")).cameras.at(selectedCameraIndex));
				drawImages( { transform }, selectedCameraIndex);
			} else {
				std::vector<glm::mat4> transforms;
				transforms.reserve(sequenceInfo.nbCameras);
				for (int i = 0; i < sequenceInfo.nbCameras; i++) {
					const auto &camData = imageLODs.at(params.getInt("SelectedLOD")).cameras.at(i);
					glm::mat4 transform = make_transform(camData);
					transforms.push_back(transform);
				}
				drawImages(transforms, -1);
			}

			drawCameraIDs(camera);
		}
	} else {
		static float zoom = 1.0f;
		static glm::vec2 translation = glm::vec2(0, 0);

		float width = camera.getWindowSize().x;
		float height = camera.getWindowSize().y;

		if (!camera.is_ctrl_pressed()) {
			zoom = std::min(
					std::max(zoom * (1.0f + 0.15f * camera.getScroll()), 0.25f),
					400.0f);
			if (camera.is_lmb_pressed() && !camera.isGUIHovered()) {
				glm::vec2 T = (camera.getMouseNDC() - camera.getMouseNDCPrev())
						/ zoom;

				translation += T;
			}
		}
		const auto &camData = imageLODs.at(params.getInt("SelectedLOD")).cameras.at(selectedCameraIndex);
		const auto &images_size = glm::vec2(camData.width, camData.height);

		float r0 = images_size.y / images_size.x;
		float r1 = height / width;
		float x = r0 / r1;

		glm::mat4 canvas_transform = glm::mat4(1.0);
		canvas_transform = glm::translate(canvas_transform,
				glm::vec3(translation.x * zoom, translation.y * zoom, 0));
		canvas_transform = glm::scale(canvas_transform,
				glm::vec3(std::min(1.0f, 1.0f / x) * zoom,
						std::min(1.0f, x) * zoom, 1.0));

		glm::mat4 canvas_inverse = glm::inverse(canvas_transform);

		glm::vec2 &CursorNDCPos = params.getVec2("CursorNDCPos");
		CursorNDCPos.x = canvas_inverse[0][0] * camera.getMouseNDC().x
				+ canvas_inverse[3][0];
		CursorNDCPos.y = canvas_inverse[1][1] * camera.getMouseNDC().y
				+ canvas_inverse[3][1];

		drawImages( { canvas_transform }, selectedCameraIndex);
		drawImageBorder(canvas_transform);
		glDisable(GL_DEPTH_TEST);


		sparseGrid.render(selectedCameraIndex,
				canvas_transform,
				params.getBool("RemoveDistortion"),
				params.getInt("ImageDisplayMode") == ImageDisplayModes::GEOMETRY,
				params.getBool("RenderMeshAlone"));

		ddaTest.render(camera, sparseGrid, imageLODs,
				RENDER_PROJECTION_TYPE::PROJECT_ON_2D_CANVAS,
				canvas_transform,
				params.getBool("RemoveDistortion"));

		visualHull.render(selectedCameraIndex, canvas_transform,
				depthmaps, params.getBool("RemoveDistortion"), params.getBool("DrawBorder"), params.getBool("DrawBorderBVH"));

		if(!params.getBool("RenderMeshAlone")){
			renderCameraFrustums2D(canvas_transform);
			drawCameraIDs2D(camera, canvas_transform, selectedCameraIndex);
			drawMousePixelCoords(camera, canvas_transform);
		}


		glEnable(GL_DEPTH_TEST);

		const int mode = params.getInt("ImageDisplayMode");
		if (mode == ImageDisplayModes::DONT_CARE_MASK_EDITION) {
			float &brushRadius = params.getFloat("BrushRadius");
			if (camera.is_ctrl_pressed()) {
				brushRadius = std::min(
						std::max(
								brushRadius
										* (1.0f + 0.15f * camera.getScroll()),
								0.001f), 1.0f);
				if (camera.is_lmb_pressed()) {
					backgrounds.editMask(selectedCameraIndex, CursorNDCPos,
							brushRadius, camera.is_shift_pressed());
				}
			}
		}
	}

	drawCornerText(camera);
	differentialRendering.renderRayTexels();
}

void Sequence::renderGridFloor(Camera &camera) {

	simpleShader.start();
	simpleShader.loadMat4("transform", camera.getProjectionViewMatrix());
	simpleShader.loadVec4("color", glm::vec4(1.0));

	gridFloor.bind();
	gridFloor.bindAttribute(0);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawElements(GL_TRIANGLES, gridFloor.getIndexCount(), GL_UNSIGNED_INT, 0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	gridFloor.unbindAttribute(0);
	gridFloor.unbind();

	simpleShader.stop();
}

void Sequence::renderCameraFrustums(Camera &camera) {
	cameraFrustumsShader.start();
	cameraFrustumsShader.loadMat4("projectionView",
			camera.getProjectionViewMatrix());
	cameraFrustumsShader.loadMat4("transform", glm::mat4(1.0));
	cameraFrustumsShader.loadInt("selectedCamera", params.getInt("SelectedCamera"));
	int DepthMapSelection = params.getInt("PointCloud#DepthmapSelection");
	cameraFrustumsShader.loadInt("SelectedDepthMap", DepthMapSelection);
	cameraFrustumsShader.loadInt("PROJECTION_TYPE", 1); //PROJECTION_TYPE == PROJECT_ON_3D_CANVAS
	cameraFrustumsShader.loadiVec4("activeCameras", differentialRendering.getActiveCameras());

	cameraFrustumsVAO->bind();
	cameraFrustumsVAO->bindAttributes( { 0, 1 });

	glLineWidth(1.0f);
	glDrawElements(GL_LINES, cameraFrustumsVAO->getIndexCount(), GL_UNSIGNED_INT,
			0);

	cameraFrustumsVAO->unbindAttributes( { 0, 1 });
	cameraFrustumsVAO->unbind();

	cameraFrustumsShader.stop();
}

void Sequence::renderCameraFrustums2D(glm::mat4 canvas_transform) {
	glm::mat4 transform = glm::mat4(1.0);

	cameraFrustumsShader.start();
	cameraFrustumsShader.loadMat4("transform", transform);
	cameraFrustumsShader.loadMat4("canvas_transform", canvas_transform);
	cameraFrustumsShader.loadInt("selectedCamera", params.getInt("SelectedCamera"));
	int DepthMapSelection = params.getInt("PointCloud#DepthmapSelection");
	cameraFrustumsShader.loadInt("SelectedDepthMap", DepthMapSelection);
	cameraFrustumsShader.loadInt("PROJECTION_TYPE", 0); //PROJECTION_TYPE == PROJECT_ON_3D_CANVAS
	cameraFrustumsShader.loadiVec4("activeCameras", differentialRendering.getActiveCameras());
	cameraFrustumsShader.loadInt("RemoveDistortion", params.getBool("RemoveDistortion"));

	cameraFrustumsVAO->bind();
	cameraFrustumsVAO->bindAttributes( { 0, 1 });

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLODs.at(0).CameraDataBlock.getID());

	glLineWidth(1.0f);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDrawElements(GL_LINES, cameraFrustumsVAO->getIndexCount(), GL_UNSIGNED_INT,
			0);
	glDisable(GL_BLEND);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);

	cameraFrustumsVAO->unbindAttributes( { 0, 1 });
	cameraFrustumsVAO->unbind();

	cameraFrustumsShader.stop();
}

void Sequence::drawMousePixelCoords(Camera &camera,
		glm::mat4 canvas_transform) {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	int width = (int) camera.getWindowSize().x;
	int height = (int) camera.getWindowSize().y;
	gltViewport(0, 0, width, height);
	GLTtext *text = gltCreateText();
	gltBeginDraw();

	glm::mat4 canvas_inverse = glm::inverse(canvas_transform);

	float x = camera.getMouseNDC().x;
	float y = camera.getMouseNDC().y;

	// in [-1, 1]
	float u = canvas_inverse[0][0] * x + canvas_inverse[3][0];
	float v = canvas_inverse[1][1] * y + canvas_inverse[3][1];

	const auto &camData = imageLODs.at(params.getInt("SelectedLOD")).cameras.at(params.getInt("SelectedCamera"));
	const auto &images_size = glm::ivec2(camData.width, camData.height);

	// to [0, 2048]
	u = (u * 0.5 + 0.5) * images_size.x;
	v = (v * -0.5 + 0.5) * images_size.y;

	float true_u = u;
	float true_v = v;

	if(params.getBool("RemoveDistortion")){
		float fx = camData.K[0][0];
		float fy = camData.K[1][1];
		float cx = camData.K[2][0];
		float cy = camData.K[2][1];

		u = (u - cx) / fx;
		v = (v - cy) / fy;

		glm::vec2 distortedPixels = camData.toPixels(glm::vec2(u, v));
		u = distortedPixels.x;
		v = distortedPixels.y;

	}

	x = (x * 0.5 + 0.5) * width + 10;
	y = (y * -0.5 + 0.5) * height + 10;

	selectedPixel = glm::ivec2(floor(glm::vec2(u, v)));
	selectedDepthTile = glm::ivec2(floor(glm::vec2(selectedPixel) / (float)DEPTHMAP_TILE_WIDTH));


	std::stringstream ss;
	ss <<"px: " <<selectedPixel.x <<" " <<selectedPixel.y <<std::endl;
	ss <<"true px: " <<true_u <<" " <<true_v <<std::endl;
	ss <<"2D tile: " <<selectedDepthTile.x <<" "<<selectedDepthTile.y <<std::endl;
	gltSetText(text, ss.str().c_str());
	gltColor(1.0f, 1.0f, 1.0f, 1.0f);

	gltDrawText2D(text, x, y, 1.0f);

	gltEndDraw();
	gltDeleteText(text);

	glDisable(GL_BLEND);
}

void Sequence::drawCameraIDs(Camera &camera) {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	int width = (int) camera.getWindowSize().x;
	int height = (int) camera.getWindowSize().y;
	gltViewport(0, 0, width, height);
	GLTtext *text = gltCreateText();
	gltBeginDraw();

	for (int i = 0; i < sequenceInfo.nbCameras; i++) {
		[[maybe_unused]]const auto &camData = imageLODs.at(params.getInt("SelectedLOD")).cameras.at(i);
		glm::vec4 camCenter = camera.getProjectionViewMatrix()
				* glm::vec4(camera_frustum_points.at(5 * i + 4), 1.0);
		if (camCenter.w < 0.0f) {
			continue;
		}
		camCenter /= camCenter.w;
		glm::vec2 NDC = glm::vec2(camCenter.x, camCenter.y);

		float x = (NDC.x * 0.5 + 0.5) * width + 10;
		float y = (NDC.y * -0.5 + 0.5) * height + 10;
		//int ID = camData.ID;
		int ID = i;
		std::string s = std::to_string(ID);
		gltSetText(text, s.c_str());
		gltColor(1.0f, 1.0f, 1.0f, 1.0f);
		gltDrawText2D(text, x, y, 1.0f);
	}

	gltEndDraw();
	gltDeleteText(text);

	glDisable(GL_BLEND);
}

void Sequence::drawCornerText(RenderingBase::Camera &camera){
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	int width = (int) camera.getWindowSize().x;
	int height = (int) camera.getWindowSize().y;
	gltViewport(0, 0, width, height);
	GLTtext *text = gltCreateText();
	gltBeginDraw();

	std::stringstream ss;
	ss <<"Volume preparation: " <<(volumePreparationTime*1.0E-3f) <<" s\n";
	ss <<"Optimization: " <<differentialRendering.getElapsedTime() <<" s\n";
	ss <<"SH Bands: " <<sparseGrid.getSHBands() <<"\n";
	ss <<"Params per voxel: " <<sparseGrid.getParamsPerVoxel() <<"\n";
	ss <<"Voxels: " <<prettyPrintQuantity(sparseGrid.getTotalTiles() * VOXELS_PER_TILE) <<"\n";
	ss <<"Triangles: " <<prettyPrintQuantity(sparseGrid.getTriangleCount()) <<"\n";

	gltSetText(text, ss.str().c_str());
	gltColor(1.0f, 1.0f, 1.0f, 1.0f);
	gltDrawText2D(text, width - 400, 25, 1.5f);
	gltEndDraw();
	gltDeleteText(text);

	glDisable(GL_BLEND);
}

void Sequence::drawCameraIDs2D(RenderingBase::Camera &camera,
		glm::mat4 canvas_transform, int selectedCameraIndex) {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	int width = (int) camera.getWindowSize().x;
	int height = (int) camera.getWindowSize().y;
	gltViewport(0, 0, width, height);
	GLTtext *text = gltCreateText();
	gltBeginDraw();

	for (int i = 0; i < sequenceInfo.nbCameras; i++) {
		const auto &camData = imageLODs.at(params.getInt("SelectedLOD")).cameras.at(selectedCameraIndex);
		glm::vec3 camCenter = camera_frustum_points.at(5 * i + 4);
		glm::vec3 camCenterProj = camData.R * camCenter + camData.T;

		if(camCenterProj.z < 0.0f){
			continue;
		}

		camCenterProj /= camCenterProj.z;
		camCenterProj = camData.K * camCenterProj;

		glm::vec2 NDC = glm::vec2(camCenterProj.x, camCenterProj.y)
				/ glm::vec2(camData.size());
		NDC = glm::vec2(NDC.x * 2.0f - 1.0f, 1.0f - NDC.y * 2.0f);
		glm::vec4 glPos = canvas_transform * glm::vec4(NDC, 0.0f, 1.0f);

		float x = (glPos.x * 0.5 + 0.5) * width + 10;
		float y = (glPos.y * -0.5 + 0.5) * height + 10;
		//int ID = camData.ID;
		int ID = i;
		std::string s = std::to_string(ID);
		gltSetText(text, s.c_str());
		gltColor(1.0f, 1.0f, 1.0f, 1.0f);
		gltDrawText2D(text, x, y, 1.0f);
	}

	gltEndDraw();
	gltDeleteText(text);

	glDisable(GL_BLEND);
}

void Sequence::drawImageBorder(glm::mat4 canvas_transform) {
	simpleShader.start();
	simpleShader.loadMat4("transform", canvas_transform);
	simpleShader.loadVec4("color", glm::vec4(0, 1, 0, 1));
	imageBorderVAO.bind();
	imageBorderVAO.bindAttribute(0);

	glPointSize(2);
	glLineWidth(1.0f);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDrawArrays(GL_POINTS, 0, 4);
	glDrawElements(GL_LINES, imageBorderVAO.getIndexCount(),
	GL_UNSIGNED_INT, 0);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	imageBorderVAO.unbindAttribute(0);
	imageBorderVAO.unbind();
	simpleShader.stop();
}

void Sequence::drawImages(const std::vector<glm::mat4> &transforms,
		int viewpoint) {

	quad.bind();
	quad.bindAttribute(0);
	quadShader.start();
	const int mode = params.getInt("ImageDisplayMode");
	quadShader.loadInt("displayMode", mode - 1);
	quadShader.loadVec2("CursorNDCPos", params.getVec2("CursorNDCPos"));
	quadShader.loadFloat("BrushRadius", params.getFloat("BrushRadius"));
	quadShader.loadInt("RemoveDistortion", params.getBool("RemoveDistortion"));
	quadShader.loadInt("ApplyExposureCorrection", params.getBool("ApplyExposureCorrection"));
	quadShader.loadInt("doNotUseOriginalImage", params.getBool("doNotUseOriginalImage"));



	const int selectedLOD = params.getInt("SelectedLOD");

	imageLODs.at(selectedLOD).allViewpoints.bindToTextureUnit(0);
	backgrounds.getImages(selectedLOD).bindToTextureUnit(1);
	ExposureLUT->bindToTextureUnit(3);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLODs.at(selectedLOD).CameraDataBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, differentialRendering.getGlobalIntegrationTextureBlock().getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, imageLODs.at(selectedLOD).depthTilesBlock.getID());

	const auto &render = [&](int i, const glm::mat4 &transform) {

		bool show = false;
		switch (mode) {
		case ImageDisplayModes::ORIGINAL_IMAGE:
			if (imageLODs.at(selectedLOD).allViewpoints.isLayerCreated(i))
				show = true;
			break;
		case ImageDisplayModes::BACKGROUND:
			if (backgrounds.getImages(selectedLOD).isLayerCreated(i))
				show = true;
			break;
		case ImageDisplayModes::SEGMENTED_IMAGE:
			if (imageLODs.at(selectedLOD).allViewpoints.isLayerCreated(i))
				show = true;
			break;
		case ImageDisplayModes::DONT_CARE_MASK_EDITION:
			if (backgrounds.getImages(selectedLOD).isLayerCreated(i))
				show = true;
			break;
		case ImageDisplayModes::GLOBAL_INTEGRATION_IMAGE:
			if(differentialRendering.getGlobalIntegrationTextureBlock().getDatasize() > 0)
				show = true;
			break;
		case ImageDisplayModes::REMAINING_TRANSPARENCY:
			if(differentialRendering.getGlobalIntegrationTextureBlock().getDatasize() > 0)
				show = true;
			break;
		case ImageDisplayModes::GEOMETRY:
			break;
		default:
			break;
		}

		if (show) {
			if(imageLODs.at(selectedLOD).occupancyTexture.getID() > 0){
				imageLODs.at(selectedLOD).occupancyTexture.bindToTextureUnit(2);
			}
			quadShader.loadInt("viewpoint", i);
			quadShader.loadMat4("transform", transform);
			quadShader.loadInt("DepthTiles", imageLODs.at(selectedLOD).TotalDepthTiles);
			quadShader.loadInt("DepthTilesOffset", 0);
			glDisable(GL_CULL_FACE);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			glEnable(GL_CULL_FACE);
			imageLODs.at(selectedLOD).occupancyTexture.unbindFromTextureUnit(2);
		}

	};

	if (viewpoint != -1) {
		assert((int )transforms.size() == 1);
		const auto &transform = transforms.at(0);
		render(viewpoint, transform);
	} else {
		assert((int )transforms.size() == sequenceInfo.nbCameras);
		for (int i = 0; i < sequenceInfo.nbCameras; i++) {
			const auto &transform = transforms.at(i);
			render(i, transform);
		}
	}

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	imageLODs.at(selectedLOD).allViewpoints.unbindFromTextureUnit(0);
	backgrounds.getImages(selectedLOD).unbindFromTextureUnit(1);
	ExposureLUT->unbindFromTextureUnit(3);


	quadShader.stop();

	quad.unbindAttribute(0);
	quad.unbind();
}

void Sequence::memUsage(bool showGui) {

	size_t total = 0;
	total += backgrounds.memUsage(showGui);
	total += depthmaps.memUsage(showGui);
	total += sparseGrid.memUsage(showGui);
	total += differentialRendering.memUsage(showGui);

	static size_t imageLODs_total = 0;

	bool details = false;
	if(showGui){
		details = ImGui::TreeNode("Image LODs", "Image LODs: (%d cameras, %d levels, %s)",
				sequenceInfo.nbCameras, imagePyramidLevels, prettyPrintMem(imageLODs_total).c_str());
	}

	imageLODs_total = 0;
	for(int i=0; i<(int)imageLODs.size(); i++){
		auto& allViewpoints = imageLODs.at(i).allViewpoints;

		size_t s1 = allViewpoints.memUsage(4);
		size_t s2 = imageLODs.at(i).occupancyTexture.memUsage(4);
		size_t s3 = imageLODs.at(i).depthTileDescriptorsBlock.getDatasize();
		size_t s4 = imageLODs.at(i).depthTilesBlock.getDatasize();
		size_t s = s1 + s2 + s3 + s4;
		if(details && ImGui::TreeNode("LOD", "LOD %d (%d x %d, %s)", i, allViewpoints.getWidth(), allViewpoints.getHeight(), prettyPrintMem(s).c_str())){
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Images: %s", prettyPrintMem(s1).c_str());
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Occupancy: %s", prettyPrintMem(s2).c_str());
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Descriptors: %s", prettyPrintMem(s3).c_str());
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "2D Tiles: %s", prettyPrintMem(s4).c_str());
			ImGui::TreePop();
		}
		imageLODs_total += s;
	}

	if(details){
		ImGui::TreePop();
	}
	total += imageLODs_total;

	if(showGui){
		ImGui::TextColored(ImVec4(0, 1, 1, 1), "Total: %s", prettyPrintMem(total).c_str());
	}

	params.getFloat("VRAMUsage") = total / 1.0E9;


}

void Sequence::prepareCameras() {


	for(int imagePyramidLevel=0; imagePyramidLevel < imagePyramidLevels; imagePyramidLevel++){
		for (CameraInfo &cam : imageLODs.at(imagePyramidLevel).cameras) {
			cam.applyTransform(SceneReferenceFrame::getSceneRotation(), SceneReferenceFrame::getSceneTranslation(), SceneReferenceFrame::getSceneScale());
		}
	}


	cameraFrustumsVAO = std::make_unique<RenderingBase::VAO>();
	cameraFrustumsVAO->bind();
	std::vector<unsigned int> camera_base_indices = { 0, 1, 1, 2, 2, 3, 3, 0, 4,
			0, 4, 1, 4, 2, 4, 3 };

	std::vector<unsigned int> camera_indices;
	std::vector<int> camera_ids;
	camera_indices.reserve(sequenceInfo.nbCameras * (int) camera_base_indices.size());
	camera_frustum_points.clear();
	camera_frustum_points.reserve(sequenceInfo.nbCameras * 5);
	camera_ids.reserve(sequenceInfo.nbCameras * 5);
	for (int i = 0; i < sequenceInfo.nbCameras; i++) {
		//we invert the coordinates of the corners of the image to directly obtain 3D points
		std::vector<glm::vec3> uvs;
		const auto &camData = imageLODs.at(0).cameras[i];
		const auto &K = camData.K;
		const auto &R = camData.R;
		const auto &T = camData.T;
		const auto &images_size = glm::ivec2(camData.width, camData.height);

		uvs.push_back(glm::vec3(0, 0, 1));
		uvs.push_back(glm::vec3(images_size.x, 0, 1));
		uvs.push_back(glm::vec3(images_size.x, images_size.y, 1));
		uvs.push_back(glm::vec3(0, images_size.y, 1));
		uvs.push_back(glm::vec3(0, 0, 0));

		for (int index : camera_base_indices) {
			camera_indices.push_back(
					index + (int) camera_frustum_points.size());
		}
		for (const glm::vec3 &p : uvs) {
			// p = K (RX  + T)

			glm::vec3 X = glm::inverse(K * R) * (p * SceneReferenceFrame::getFrustumsLength() - K * T);


			camera_frustum_points.push_back(X);
			camera_ids.push_back(i);
		}

//		cameraVAO.createIndexBuffer(camera_indices.data(),
//				camera_indices.size());
//		cameraVAO.createFloatAttribute(0, &camera_points[0].x,
//				camera_points.size() * 3, 3, 0, GL_DYNAMIC_DRAW);

	}

	cameraFrustumsVAO->createIndexBuffer(camera_indices.data(),
			camera_indices.size());
	cameraFrustumsVAO->createFloatAttribute(0, &camera_frustum_points[0].x,
			camera_frustum_points.size() * 3, 3, 0, GL_STATIC_DRAW);
	cameraFrustumsVAO->createIntAttribute(1, camera_ids.data(),
			camera_ids.size(), 1, 0, GL_STATIC_DRAW);
	cameraFrustumsVAO->unbind();

	//What gets uploaded to the gpu
	struct CameraData {
		glm::vec4 K;				// vec4(cx, cy, fx, fy)
		glm::vec4 distortion1;
		glm::vec4 distortion2;
		glm::vec4 camCenter;
		glm::mat4 cameraTransform;
		glm::mat4 cameraTransformInv;
	};

	for(int imagePyramidLevel=0; imagePyramidLevel < imagePyramidLevels; imagePyramidLevel++){
		std::vector<CameraData> v;
		v.reserve(sequenceInfo.nbCameras);

		for (const CameraInfo &cam : imageLODs.at(imagePyramidLevel).cameras) {
			CameraData data;
			data.K = glm::vec4(cam.K[0][0], cam.K[1][1], cam.K[2][0], cam.K[2][1]);
			data.distortion1 = glm::vec4(cam.distortion[0], cam.distortion[1],
					cam.distortion[2], cam.distortion[3]);
			data.distortion2 = glm::vec4(cam.distortion[4], 0.0, cam.width,
					cam.height);
			data.camCenter = glm::vec4(-glm::inverse(cam.R) * cam.T, 1.0);
			glm::mat4 cameraTransform = glm::mat4(cam.R);
			cameraTransform[3] = glm::vec4(cam.T, 1.0);
			data.cameraTransform = cameraTransform;
			data.cameraTransformInv = glm::inverse(cameraTransform);
			v.push_back(data);
		}

		imageLODs.at(imagePyramidLevel).CameraDataBlock.bind();
		imageLODs.at(imagePyramidLevel).CameraDataBlock.storeData(v.data(), v.size() * sizeof(CameraData),
				GL_STATIC_DRAW);
		imageLODs.at(imagePyramidLevel).CameraDataBlock.unbind();

	}

}

