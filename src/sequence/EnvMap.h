/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * EnvMap.h
 *
 *  Created on: 18 mars 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_ENVMAP_H_
#define SEQUENCE_ENVMAP_H_

#include <memory>

#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Camera.h"
#include "../rendering_base/Parameters.h"
#include "Texture2DArray.h"
#include "CubeMap.h"
#include "CommonTypes.h"
#include "Backgrounds.h"

class EnvMap {
public:
	EnvMap(const std::vector<ImageLOD>& imageLODs);
	virtual ~EnvMap();

	void updateGUI(const Backgrounds &backgrounds);
	void render(const RenderingBase::Camera& camera, RenderingBase::Shader& wireframeTileShader);

	void genEnvMap(const Backgrounds &backgrounds);

	glm::vec3 getMinCorner() const{
		return params.getVec3("MinCorner");
	}
	glm::vec3 getMaxCorner() const{
		return params.getVec3("MaxCorner");
	}
private:
	RenderingBase::Parameters params = RenderingBase::Parameters("EnvMap");
	const std::vector<ImageLOD>& imageLODs;

	std::unique_ptr<CubeMap> cubemap;

	RenderingBase::Shader envMapGenShader = RenderingBase::Shader("envmap/generateEnvMap.cp");
	RenderingBase::Shader envMapRenderShader = RenderingBase::Shader("envmap/renderEnvMap.vs", "envmap/renderEnvMap.fs");
	RenderingBase::VAO cube;

	std::vector<RenderingBase::QueryBuffer> timers;
};

#endif /* SEQUENCE_ENVMAP_H_ */
