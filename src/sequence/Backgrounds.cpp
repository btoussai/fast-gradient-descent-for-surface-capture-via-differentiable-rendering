/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * BackgroundFilter.cpp
 *
 *  Created on: 7 oct. 2021
 *      Author: Briac
 */

#include "Backgrounds.h"

#include <iostream>

#include "../imgui/imgui.h"

#include "../ImGuiFileDialog/ImGuiFileDialog.h"
#include "../glm/gtx/string_cast.hpp"

#include "../IconFontCppHeaders/IconsFontAwesome5.h"

using namespace RenderingBase;

Backgrounds::Backgrounds(const std::vector<ImageLOD>& imageLODs) : imageLODs(imageLODs){
	backgroundEditShader.finishInit();
	backgroundEditShader.init_uniforms({"CursorNDCPos", "BrushRadius", "value"});

	backgroundApplyShader.finishInit();
	backgroundApplyShader.init_uniforms({"thresholds", "camIndex", "UseBackgroundColors", "ColorsPaletteCount",
	"ColorPalette[0]", "ColorPalette[1]", "ColorPalette[2]","ColorPalette[3]", "ColorPalette[4]",
	"ColorPalette[5]", "ColorPalette[6]", "ColorPalette[7]","ColorPalette[8]", "ColorPalette[9]"});


	backgroundSmoothShader.finishInit();

	backgroundDownscaleShader.finishInit();
	backgroundUpscaleShader.finishInit();
	backgroundCopyShader.finishInit();

	backgroundCopyShader.init_uniforms({"camIndex", "DecisionBoundary"});

	writeOccupancyShader.finishInit();
	writeOccupancyShader.init_uniforms({"NumTiles"});

	recombineSilhouettesShader.finishInit();

	const int N = 3;
	timers.reserve(N);
	for(int i=0; i<N; i++){
		timers.emplace_back(GL_TIME_ELAPSED);
	}

}

Backgrounds::~Backgrounds() {

	bool shouldSave = false;
	for(int i=0; i<nbCameras; i++){
		bool moddified = moddifiedImages.at(i);
		if(moddified){
			shouldSave = true;
			break;
		}
	}

	if(shouldSave){
		AsyncLoader<ImageLoadingTask> imageLoader;
		std::vector<std::function<void()>> tasks;
		for(int i=0; i<nbCameras; i++){
			bool moddified = moddifiedImages.at(i);
			if(moddified){
				std::cout <<"Saving changes on " <<background_images.at(0).getImagePath(i) <<" ..." <<std::endl;
				tasks.push_back(background_images.at(0).saveToDiskLater(background_images.at(0).getImagePath(i), i));
			}
		}
		imageLoader.execAll(tasks);
	}



}

void Backgrounds::loadFromFile(const std::string &backgroundsDir, AsyncLoader<ImageLoadingTask>& imageLoader, const DownScale& downscale) {
	loaded = true;

	std::vector<std::function<ImageLoadingTask()>> tasks;
	tasks.reserve(nbCameras);
	for(int i=0; i<nbCameras; i++){
		std::string path = backgroundsDir + "/cam-" + std::to_string(imageLODs.at(0).cameras.at(i).ID) + ".png";
		if(!std::filesystem::exists(path)){
			std::cout <<"Cannot load background: " <<path <<std::endl;
		}else{
			tasks.push_back([&, i, path](){
				return ImageLoadingTask{i, 0, Texture2D::readTextureData(path)};
			});
		}
	}

	std::list<ImageLoadingTask> imagesData = std::list<ImageLoadingTask>();
	std::chrono::milliseconds time = imageLoader.runAll(tasks, [&](ImageLoadingTask&& imageData){
		imagesData.emplace_back(std::move(imageData));
	});

	std::cout <<"Loaded background images in " <<time.count() <<"ms." <<std::endl;

	auto t0 = std::chrono::system_clock::now();
	for(const auto& imageData : imagesData){
		background_images.at(0).createLayer(imageData.camIndex, imageData.result);
	}
	for(int imagePyramidLevel=1; imagePyramidLevel < imagePyramidLevels; imagePyramidLevel++){
		downscale.apply(
				background_images.at(imagePyramidLevel-1),
				background_images.at(imagePyramidLevel));
	}

	auto t1 = std::chrono::system_clock::now();
	time = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);

	std::cout <<"Uploaded background images in " <<time.count() <<"ms." <<std::endl;

	this->downscale = &downscale;

}

void Backgrounds::computeFromSequence(const std::string& saveDir, const std::string& sequenceDir, AsyncLoader<ImageLoadingTask>& imageLoader, const DownScale& downscale) {
	const int IMAGE_COUNT = 15;
	const int local_size_x = 8, local_size_y = 8;

	auto t0 = std::chrono::system_clock::now();

	Shader shader = Shader("background/compute_background.cp");
	shader.finishInit();
	shader.init_uniforms({"imagesArray", "filterImage"});
	shader.start();
	shader.loadInt("imagesArray", 0);
	shader.loadInt("filterImage", 0);
	shader.stop();

	for(int i=0; i<nbCameras; i++){
		const auto& camData = imageLODs.at(0).cameras.at(i);

		std::vector<std::function<ImageLoadingTask()>> tasks;
		int stride = (int)std::max(1.0f, (float)(camData.endId - camData.beginId) / (float)(IMAGE_COUNT-1));
		int imageIndex = 0;
		for(int t=camData.beginId; t<=camData.endId; t+=stride){
			tasks.push_back([&camData, &sequenceDir, imageIndex, t](){
				std::string path = camData.pathOfNthImage(sequenceDir, (int)t, false);
				return ImageLoadingTask{imageIndex, (int)t, Texture2D::readTextureData(path)};
			});
			imageIndex++;

			if(tasks.size() == IMAGE_COUNT){
				break;
			}
		}
		assert(tasks.size() == IMAGE_COUNT);

		GLuint textureArrayID = 0;

		GLsizei width = camData.width;
		GLsizei height = camData.height;
		GLsizei mipLevelCount = 1;

		glGenTextures(1,&textureArrayID);
		glBindTexture(GL_TEXTURE_2D_ARRAY, textureArrayID);
		// Allocate the storage.
		glTexStorage3D(GL_TEXTURE_2D_ARRAY, mipLevelCount, GL_RGB8, width, height, IMAGE_COUNT);
		// Upload pixel data.

		std::cout <<"Loading images..." <<std::endl;
		imageLoader.runAll(tasks, [&](ImageLoadingTask&& imageData){

			const auto& texData =  imageData.result;
			glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, imageData.camIndex, width, height, 1, texData.format, texData.type, texData.data.get());

		});

		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER,GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		std::cout <<"Creating background texture..." <<std::endl;
		std::string savePath = saveDir + "/cam-" + std::to_string(i+1) + ".png";
		auto texData = RenderingBase::Texture2D::TextureData{savePath, width, height, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, nullptr};

		shader.start();
		glActiveTexture(GL_TEXTURE0 + 0);
		glBindTexture(GL_TEXTURE_2D_ARRAY, textureArrayID);
		glBindImageTexture(0, background_images.at(0).getID(), 0, GL_FALSE, i, GL_WRITE_ONLY, GL_RGBA8UI);

		glm::ivec2 groups = glm::ivec2((width + local_size_x-1) / local_size_x, (height + local_size_y-1) / local_size_y);
		glDispatchCompute(groups.x, groups.y, 1);

		glDeleteTextures(1, &textureArrayID);
		shader.stop();

		background_images.at(0).validateLayer(i, std::move(texData));

	}

	glMemoryBarrier(GL_TEXTURE_UPDATE_BARRIER_BIT);
	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
	glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8UI);

	std::cout <<"Saving to disk..." <<std::endl;
	for(int i=0; i<nbCameras; i++){
		imageLoader.exec(
			background_images.at(0).saveToDiskLater(background_images.at(0).getImagePath(i), i)
		);
	}
	auto t1 = std::chrono::system_clock::now();

	auto delta = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
	std::cout <<"Done in " <<delta.count() <<"ms." <<std::endl;

	loaded = true;
	this->downscale = &downscale;

	for(int imagePyramidLevel=1; imagePyramidLevel < imagePyramidLevels; imagePyramidLevel++){
		for(int camIndex=0; camIndex<nbCameras; camIndex++){
			downscale.apply(
				background_images.at(imagePyramidLevel-1),
				background_images.at(imagePyramidLevel),
				camIndex);
		}
	}
}


bool Backgrounds::displayStatusMsg(){
	static bool overflow = false;
	overflow = params.getInt("TotalForegroundPixelTiles") > params.getFloat("ExpectedForegroundPixelBlocks")*1000.0f;

	if(overflow)ImGui::SetNextItemOpen(true, ImGuiCond_Always);
	if(ImGui::TreeNode("Segmentation status")){

		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Segmentation Time: %.3f ms", timers.at(0).getLastResult(true) * 1.0E-6);
		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Reordering tiles & writing occupancy texture: %.3f ms", timers.at(1).getLastResult(true) * 1.0E-6);
		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Building BVHs: %.3f ms", timers.at(2).getLastResult(true) * 1.0E-6);

		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Total foreground pixels tiles: %5.3f K", params.getInt("TotalForegroundPixelTiles") / 1000.0f);
		if(overflow){
			ImGui::SameLine(0.0f, -1.0f);
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Overflow!");
		}

		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Total border pixels: %5.3f K", params.getInt("TotalBorderPixels") / 1000.0f);
		if(params.getInt("TotalBorderPixels") > params.getFloat("ExpectedBorderPixels")*1000.0f){
			ImGui::SameLine(0.0f, -1.0f);
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Overflow!");
		}

		ImGui::TreePop();
	}
	return overflow;
}

void Backgrounds::openDialog() {
	if (ImGuiFileDialog::Instance()->Display("ChooseBackgroundFoldersDlgKey")) {
		if (ImGuiFileDialog::Instance()->IsOk()) {
			std::string filePathName =
					ImGuiFileDialog::Instance()->GetFilePathName();
			std::string filePath =
					ImGuiFileDialog::Instance()->GetCurrentPath();
			params.getString("Window#BackgroundsDir") = filePathName;
			params.getString("Window#FileSearchDir") = filePath;
		}

		ImGuiFileDialog::Instance()->Close();
	}
}

bool Backgrounds::updateGUI(const RenderingBase::Camera& camera, bool& generateFilters, bool& loadMasks) {

	openDialog();

	ImGui::Checkbox("Use colors from backgrounds", &params.getBool("UseBackgroundColors"));

	std::stringstream ss;
	ss <<"The alpha component of the backgrounds is still used to cull unwanted regions," <<std::endl;
	ss <<"regardless of the colors in the palette." <<std::endl;
	ss <<"The alpha component can be edited manually by changing the display mode of the images to" <<std::endl;
	ss <<"\"Don't care mask edition\" and by taking the view point of the cameras." <<std::endl;
	ss <<"Hold ctrl to paint. ctrl + shift to clear, ctrl + scroll to change the brush size." <<std::endl;
	HelpMarker(ss.str().c_str());

	if(!params.getBool("UseBackgroundColors")){
		ImGui::SliderInt("Colors count", &params.getInt("ColorsPaletteCount"), 1, 10);
		for(int i=0; i<params.getInt("ColorsPaletteCount"); i++){
			std::string name = "ColorPalette[" + std::to_string(i) + "]";
			if(i > 0){
				ImGui::SameLine();
			}
			ImGui::PushID(i);
			ImGui::ColorEdit3("", &params.getVec4(name).x, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoLabel);
			ImGui::PopID();
		}


		ImGui::SameLine();
		HelpMarker("Pick a color on the screen with ctrl + alt + lmb");

		if(camera.is_lmb_pressed() && camera.is_alt_pressed() && camera.is_lmb_pressed()){
			std::string name = "ColorPalette[" + std::to_string(params.getInt("ColorsPaletteCount")-1) + "]";

			glm::ivec2 c = glm::ivec2(camera.getMouseCoords());
			std::cout <<c.x <<" " <<c.y <<std::endl;

			glReadPixels(c.x, (int)camera.getWindowSize().y - c.y, 1, 1, GL_RGBA, GL_FLOAT, &params.getVec4(name).x);
		}
	}

	if (ImGui::Button(ICON_FA_FOLDER_OPEN " Background images")) {
		ImGuiFileDialog::Instance()->OpenDialog(
				"ChooseBackgroundFoldersDlgKey",
				ICON_FA_FOLDER_OPEN " Choose background folder", nullptr,
				params.getString("Window#FileSearchDir", ".").c_str(), "");
	}
	ImGui::SameLine();
	if(getBackgroundsDir() == ""){
		ImGui::TextColored(ImVec4(1, 0, 0, 1), "Undefined");
	}else{
		ImGui::TextColored(ImVec4(1, 0, 1, 1), "%s",
				getBackgroundsDir().c_str());
	}

	ImVec2 buttonSize =  ImVec2(ImGui::GetContentRegionAvailWidth(), ImGui::GetTextLineHeight()*1.5f);
	generateFilters = false;
	if(ImGui::Button("Generate backgrounds", buttonSize)){
		ImGui::OpenPopup("Continue?");
	}


	// Always center this window when appearing
	ImVec2 center = ImGui::GetMainViewport()->GetCenter();
	ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

	if (ImGui::BeginPopupModal("Continue?", NULL, ImGuiWindowFlags_AlwaysAutoResize))
	{
		ImGui::Text("All existing masks will be overwritten.\nDo you really want to procceed?\nGenerating the masks can take a few minutes.\n");
		ImGui::Separator();

		if (ImGui::Button("Yes", ImVec2(120, 0))) {
			generateFilters = true;
			ImGui::CloseCurrentPopup();
		}
		ImGui::SetItemDefaultFocus();
		ImGui::SameLine();
		if (ImGui::Button("Cancel", ImVec2(120, 0))) {
			ImGui::CloseCurrentPopup();
		}
		ImGui::EndPopup();
	}


	loadMasks = ImGui::Button("Load backgrounds", buttonSize);


	ImGui::Separator();

	bool moddif = false;
	moddif |= ImGui::SliderInt("Filter Downscale", &params.getInt("FilterDownscale"), 0, 5);
	moddif |= ImGui::Checkbox("Filter Smooth after Apply", &params.getBool("FilterSmoothApply"));
	moddif |= ImGui::Checkbox("Filter Smooth after Downscale", &params.getBool("FilterSmoothDownscale"));
	moddif |= ImGui::Checkbox("Filter Smooth after Upscale", &params.getBool("FilterSmoothUpscale"));
	moddif |= ImGui::SliderFloat("Threshold 1", &params.getVec4("FilterThresholds").x, 0.0f, 2.0f, "%.3f");
	moddif |= ImGui::SliderFloat("Threshold 2", &params.getVec4("FilterThresholds").y, 0.0f, 2.0f, "%.3f");
	moddif |= ImGui::SliderFloat("Threshold 3", &params.getVec4("FilterThresholds").z, 0.0f, 2.0f, "%.3f");
	moddif |= ImGui::SliderFloat("Threshold 4", &params.getVec4("FilterThresholds").w, -1.0f, 1.0f, "%.3f");

	ImGui::Separator();

	moddif |= ImGui::SliderFloat("Inside Boundary", &params.getFloat("DecisionBoundary"), 0.0f, 1.0f, "%.3f");

	ImGui::Separator();

	ImGui::SliderFloat("Expected Foreground pixels tiles", &params.getFloat("ExpectedForegroundPixelBlocks", 1), 1.0f, 500.0f, "%.0fK");
	ImGui::SliderFloat("Expected Border pixels", &params.getFloat("ExpectedBorderPixels", 1), 0.0f, 1000.0f, "%.0fK");

	ImGui::Separator();

	displayStatusMsg();

	ImGui::Separator();
	ImGui::PushButtonRepeat(true);
	moddif |= ImGui::Button("Compute Segmentation", buttonSize);
	ImGui::PopButtonRepeat();

	return moddif;
}

void Backgrounds::applySegmentation(AllDepthmaps& depthmaps, int viewpoint) {
	const int local_size_x = DEPTHMAP_TILE_WIDTH, local_size_y = DEPTHMAP_TILE_WIDTH;

	const GLenum FORMAT = GL_R16F;

	int width = imageLODs.at(segmentationPyramidLevel).allViewpoints.getWidth();
	int height = imageLODs.at(segmentationPyramidLevel).allViewpoints.getHeight();
	TextureVector cascade = TextureVector();
	TextureVector smooth_cascade = TextureVector();
	const int cascade_count = params.getInt("FilterDownscale");
	cascade.resize(cascade_count+1);
	smooth_cascade.resize(cascade_count+1);
	for (int k = 0; k < cascade_count+1; k++) {
		int s = 1 << k;
		float zero = 0.0f;
		cascade.at(k) = std::make_unique<Texture2D>("", &zero, width / s, height / s,
				FORMAT, GL_RED, GL_FLOAT, true);
		smooth_cascade.at(k) = std::make_unique<Texture2D>("", &zero, width / s, height / s,
				FORMAT, GL_RED, GL_FLOAT, true);
	}

	typedef std::unique_ptr<RenderingBase::Texture2D> texptr;

	const auto& segment = [&](int camIndex, glm::ivec2 groups, const texptr& destTex){
		backgroundApplyShader.start();
		if(!params.getBool("UseBackgroundColors")){
			backgroundApplyShader.loadInt("UseBackgroundColors", false);
			backgroundApplyShader.loadInt("ColorsPaletteCount", params.getInt("ColorsPaletteCount"));
			for(int i=0; i<params.getInt("ColorsPaletteCount"); i++){
				std::string name = "ColorPalette[" + std::to_string(i) + "]";
				backgroundApplyShader.loadVec4(name, params.getVec4(name));
			}
		}else{
			backgroundApplyShader.loadInt("UseBackgroundColors", true);
		}
		backgroundApplyShader.loadVec4("thresholds", params.getVec4("FilterThresholds"));
		backgroundApplyShader.loadInt("camIndex", camIndex);

		imageLODs.at(segmentationPyramidLevel).allViewpoints.bindToTextureUnit(0);
		background_images.at(segmentationPyramidLevel).bindToTextureUnit(1);
		glBindImageTexture(0, destTex->getID(), 0, GL_FALSE, 0, GL_WRITE_ONLY, FORMAT);
		glDispatchCompute(groups.x, groups.y, 1);
		glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
		glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, FORMAT);
		imageLODs.at(segmentationPyramidLevel).allViewpoints.unbindFromTextureUnit(0);
		background_images.at(segmentationPyramidLevel).unbindFromTextureUnit(1);
		backgroundApplyShader.stop();
	};


	const auto& downscale = [&](glm::ivec2 groups, const texptr& srcTex, const texptr& destTex){
		backgroundDownscaleShader.start();
		srcTex->bindToTextureUnit(0);
		glBindImageTexture(0, destTex->getID(), 0, GL_FALSE, 0, GL_WRITE_ONLY, FORMAT);
		glDispatchCompute(groups.x, groups.y, 1);
		glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
		backgroundDownscaleShader.stop();
	};

	const auto& smooth = [&](glm::ivec2 groups, texptr& srcTex, texptr& destTex){
		backgroundSmoothShader.start();
		srcTex->bindToTextureUnit(0);
		glBindImageTexture(0, destTex->getID(), 0, GL_FALSE, 0, GL_WRITE_ONLY, FORMAT);
		glDispatchCompute(groups.x, groups.y, 1);
		glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
		backgroundSmoothShader.stop();
		std::swap(srcTex, destTex);
	};

	const auto& upscale = [&](glm::ivec2 groups, texptr& srcTex, texptr& destTex){
		backgroundUpscaleShader.start();
		destTex->bindToTextureUnit(0);
		srcTex->bindToTextureUnit(1);
		glBindImageTexture(0, destTex->getID(), 0, GL_FALSE, 0, GL_WRITE_ONLY, FORMAT);
		glDispatchCompute(groups.x, groups.y, 1);
		glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
		backgroundUpscaleShader.stop();
	};

	processedImages = 0;
	const auto& apply = [&](int i){
		const CameraInfo& camData = imageLODs.at(segmentationPyramidLevel).cameras.at(i);
		if(!imageLODs.at(segmentationPyramidLevel).allViewpoints.isLayerCreated(i) || !background_images.at(segmentationPyramidLevel).isLayerCreated(i)){
			return;
		}
		processedImages++;
		int width = camData.width;
		int height = camData.height;


		glm::ivec2 groups = glm::ivec2(width + local_size_x-1, height + local_size_y-1) / glm::ivec2(local_size_x, local_size_y);

		segment(i, groups, cascade.at(0));
		if(params.getBool("FilterSmoothApply")){
			smooth(groups, cascade.at(0), smooth_cascade.at(0));
		}

		int max_pixels = 1;
		for(int p=0; p<cascade_count; p++){
			int s = 1 << (p+1);
			max_pixels += s*s;
			groups = glm::ivec2(width/s + local_size_x-1, height/s + local_size_y-1) / glm::ivec2(local_size_x, local_size_y);

			downscale(groups, cascade.at(p), cascade.at(p+1));

			if(params.getBool("FilterSmoothDownscale")){
				smooth(groups, cascade.at(p+1), smooth_cascade.at(p+1));
			}
		}
		for(int p=cascade_count-1; p >= 0; p--){
			int s = 1 << p;

			groups = glm::ivec2(width/s + local_size_x-1, height/s + local_size_y-1) / glm::ivec2(local_size_x, local_size_y);

			upscale(groups, cascade.at(p+1), cascade.at(p));
			if(params.getBool("FilterSmoothUpscale")){
				smooth(groups, cascade.at(p), smooth_cascade.at(p));
			}
		}

		groups = glm::ivec2(width + local_size_x-1, height + local_size_y-1) / glm::ivec2(local_size_x, local_size_y);

		backgroundCopyShader.start();
		backgroundCopyShader.loadInt("camIndex", i);
		backgroundCopyShader.loadFloat("DecisionBoundary", params.getFloat("DecisionBoundary") * max_pixels);
		imageLODs.at(segmentationPyramidLevel).allViewpoints.bindToTextureUnit(0);
		cascade.at(0)->bindToTextureUnit(1);
		glBindImageTexture(0, imageLODs.at(segmentationPyramidLevel).allViewpoints.getID(), 0, GL_FALSE, i, GL_WRITE_ONLY, GL_RGBA8);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, depthmaps.borderPixelsBlock.getID());
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, depthmaps.depthTileDescriptorsBlock.getID());
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, depthmaps.AtomicBlock.getID());
		glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLODs.at(segmentationPyramidLevel).CameraDataBlock.getID());
		glDispatchCompute(groups.x, groups.y, 1);

		GLuint flags = 0;
		flags |= GL_TEXTURE_FETCH_BARRIER_BIT;
		flags |= GL_SHADER_STORAGE_BARRIER_BIT;
		flags |= GL_ATOMIC_COUNTER_BARRIER_BIT;
		flags |= GL_CLIENT_MAPPED_BUFFER_BARRIER_BIT;
		flags |= GL_BUFFER_UPDATE_BARRIER_BIT;
		glMemoryBarrier(flags);

//		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, 0);
		glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
		imageLODs.at(segmentationPyramidLevel).allViewpoints.unbindFromTextureUnit(0);
		cascade.at(0)->unbindFromTextureUnit(1);
		backgroundCopyShader.stop();

		glCopyNamedBufferSubData(depthmaps.AtomicBlock.getID(), depthmaps.AtomicBlock.getID(), 0, (i+1)*8, 8);

	};


	depthmaps.allocSpace(params.getFloat("ExpectedBorderPixels")*1000, params.getFloat("ExpectedForegroundPixelBlocks")*1000);

	params.getInt("TotalForegroundPixels") = 0;
	params.getInt("TotalBorderPixels") = 0;


	Query& q1 = timers.at(0).push_back();
	q1.begin();
	if(viewpoint == -1){
		for(int i=0; i<nbCameras; i++){
			apply(i);
		}
	}else{
		apply(viewpoint);
	}
	q1.end();

		Query& q2 = timers.at(1).push_back();
		q2.begin();
		const int* ptr = (const int*)glMapNamedBuffer(depthmaps.AtomicBlock.getID(), GL_READ_ONLY);
		int TotalForegroundPixels = ptr[0];
		int totalTiles = (TotalForegroundPixels + DEPTHMAP_TILE_SIZE-1) / DEPTHMAP_TILE_SIZE;
		glUnmapNamedBuffer(depthmaps.AtomicBlock.getID());
		if(totalTiles > 0){
			if(SortDepthTilesByMortonOrder){
				//Reorder the 2D tiles in morton order
				DepthTileDescriptor* descPtr = (DepthTileDescriptor*)glMapNamedBuffer(depthmaps.depthTileDescriptorsBlock.getID(), GL_READ_WRITE);
				const int N = std::min(params.getInt("ExpectedForegroundPixelBlocks")*1000, totalTiles);
				std::vector<DepthTileDescriptor> vec = std::vector<DepthTileDescriptor>(descPtr, descPtr + N);
				std::sort(std::begin(vec), std::end(vec), [](const DepthTileDescriptor& a, const DepthTileDescriptor& b){
					if(a.camIndex != b.camIndex){
						return a.camIndex < b.camIndex;
					}
					int morton_a = m2D_e_magicbits<int32_t, int32_t>(a.tileCoords.x, a.tileCoords.y);
					int morton_b = m2D_e_magicbits<int32_t, int32_t>(b.tileCoords.x, b.tileCoords.y);
					return morton_a < morton_b;
				});
				std::copy_n(vec.begin(), N, descPtr);
				glUnmapNamedBuffer(depthmaps.depthTileDescriptorsBlock.getID());
			}

			writeOccupancyShader.start();
			writeOccupancyShader.loadInt("NumTiles", totalTiles);
			glBindImageTexture(0, depthmaps.occupancyTexture.getID(), 0, GL_FALSE, 0, GL_READ_ONLY, GL_R32I);
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, depthmaps.depthTileDescriptorsBlock.getID());
			glDispatchCompute((totalTiles + 32 - 1) / 32, 1, 1);
			glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
			writeOccupancyShader.stop();

		}
		q2.end();

	Query& q3 = timers.at(2).push_back();
	q3.begin();
	depthmaps.buildVAOs();
	q3.end();

	params.getInt("TotalForegroundPixelTiles") = depthmaps.TotalForegroundPixelTiles;
	params.getInt("TotalBorderPixels") = depthmaps.TotalBorderPixels;

}

void recombineSilhouettes(Texture2DArray& allviewpoints){

}

void Backgrounds::editMask(int viewpoint, glm::vec2 cursorNDCPos, float brushRadius, bool remove) {

	moddifiedImages.at(viewpoint) = true;

	backgroundEditShader.start();
	backgroundEditShader.loadVec2("CursorNDCPos", cursorNDCPos);
	backgroundEditShader.loadFloat("BrushRadius", brushRadius);
	backgroundEditShader.loadFloat("value", remove ? 1.0f: 0.0f);

	const int pixel_width = brushRadius * background_images.at(0).getWidth();
	const int GROUP_WIDTH = 16;
	const int groups = (pixel_width + GROUP_WIDTH-1) / GROUP_WIDTH;

	glBindImageTexture(0, background_images.at(0).getID(), 0, GL_FALSE, viewpoint, GL_READ_WRITE, GL_RGBA8);
	glDispatchCompute(groups, groups, 1);
	glMemoryBarrier(GL_TEXTURE_UPDATE_BARRIER_BIT);
	glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA8);

	backgroundEditShader.stop();

	for(int imagePyramidLevel=1; imagePyramidLevel < imagePyramidLevels; imagePyramidLevel++){
		downscale->apply(
			background_images.at(imagePyramidLevel-1),
			background_images.at(imagePyramidLevel),
			viewpoint);
	}

}

size_t Backgrounds::memUsage(bool showGui) {

	size_t s1 = 0;
	for(int i=0; i<(int)background_images.size(); i++){
		s1 += background_images.at(i).memUsage(4);// RGBA8
	}
	size_t s2 = silhouettes ? silhouettes->memUsage(1) : 0;

	if(showGui && ImGui::TreeNode("Backgrounds", "Backgrounds (%s)", prettyPrintMem(s1).c_str())){
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Background images: %s", prettyPrintMem(s1).c_str());
		if(silhouettes){
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Silhouettes: %s", prettyPrintMem(s2).c_str());
		}
		ImGui::TreePop();
	}

	return s1 + s2;
}

void Backgrounds::recombineSilhouettes(Texture2DArray &allviewpoints) {
	recombineSilhouettesShader.start();

	background_images.at(0).bindToTextureUnit(0);
	silhouettes->bindToTextureUnit(1);
	glBindImageTexture(0, allviewpoints.getID(), 0, true, 0, GL_READ_WRITE, GL_RGBA8);

	glm::ivec2 groups = glm::ivec2((allviewpoints.getWidth()+7) / 8, (allviewpoints.getHeight()+7) / 8);
	glDispatchCompute(groups.x, groups.y, allviewpoints.getLayers());

	background_images.at(0).unbindFromTextureUnit(0);
	silhouettes->unbindFromTextureUnit(1);
	glBindImageTexture(0, 0, 0, true, 0, GL_READ_WRITE, GL_RGBA8);

	recombineSilhouettesShader.stop();

}




