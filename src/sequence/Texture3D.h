/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Texture3D.h
 *
 *  Created on: 8 mars 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_TEXTURE3D_H_
#define SEQUENCE_TEXTURE3D_H_

#include "../rendering_base/openglObjects.h"

class Texture3D {
public:
	Texture3D(int width, int height, int depth, GLenum internalFormat) :
		width(width), height(height), depth(depth), internalFormat(internalFormat)
	{
		glGenTextures(1, &ID);
		glBindTexture(GL_TEXTURE_3D, ID);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexStorage3D(GL_TEXTURE_3D, 1, internalFormat, width, height, depth);
		glBindTexture(GL_TEXTURE_3D, 0);
	}
	~Texture3D(){
		glDeleteTextures(1, &ID);
	}
	Texture3D(const Texture3D&) = delete;
	Texture3D& operator=(const Texture3D&) = delete;

	void bind() const{
		glBindTexture(GL_TEXTURE_3D, ID);
	}

	void unbind() const{
		glBindTexture(GL_TEXTURE_3D, 0);
	}

	void bindToTextureUnit(int unit) const{
		glBindTextureUnit(unit, ID);
	}

	void unbindFromTextureUnit(int unit) const{
		glBindTextureUnit(unit, 0);
	}

	GLuint getID() const{
		return ID;
	}

	glm::ivec3 getDims() const{
		return glm::ivec3(width, height, depth);
	}

	int getDepth() const {
		return depth;
	}

	int getHeight() const {
		return height;
	}

	int getWidth() const {
		return width;
	}

	size_t memUsage(int bytesPerPixel) const {
		return size_t(width) * size_t(height) * size_t(depth) * bytesPerPixel;
	}


private:
	GLuint ID = 0;
	int width;
	int height;
	int depth;
	GLenum internalFormat;
};

#endif /* SEQUENCE_TEXTURE3D_H_ */
