/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * TSDF.cpp
 *
 *  Created on: 5 janv. 2022
 *      Author: Briac
 */

#include "SparseGrid.h"

#include <assimp/Importer.hpp>
#include <assimp/Exporter.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Logger.hpp>
#include <assimp/DefaultLogger.hpp>
#include <sstream>
#include <iomanip>

#include "emulator/ShaderEmulator.h"
#include "emulator/FillTSDF.h"
#include "Morton.h"
#include "AsyncLoader.h"
#include "SceneReferenceFrame.h"

#include <cstddef>
#include<fstream>

SparseGrid::SparseGrid(const std::vector<ImageLOD>& imageLODs) : imageLODs(imageLODs) {

	reserveTilesShader.finishInit();
	reserveTilesShader.init_uniforms({"VolumeMinCorner", "tile_size", "TilesTotal", "NumCameras"});

	writeOccupancyShader.finishInit();
	writeOccupancyShader.init_uniforms({"NumTiles"});

	fillVoxelsShader.finishInit();
	fillVoxelsShader.init_uniforms({"TSDFminCorner", "tile_size", "NumCameras"});

	compressOccupancyShader.finishInit();
	compressOccupancyShader.init_uniforms({"TSDFTilesCount"});

	wireframeTileShader.bindVertexAttribute(0, "tile_desc");
	wireframeTileShader.finishInit();
	wireframeTileShader.init_uniforms({"projectionView", "transform", "TSDFminCorner", "tile_size", "color",
		"PROJECTION_TYPE", "canvas_transform", "selectedCamIndex", "RemoveDistortion",
	"USE_FRUSTUM_MODE", "frustumCamIndex", "frustumMinCorner", "frustumMaxCorner", "NumTiles"});


	std::vector<std::string> samplerNames(getVoxelsTexturesCount());
	for(int i=0; i<getVoxelsTexturesCount(); i++){
		samplerNames.push_back("VoxelsTexture[" + std::to_string(i) + "]");
	}

	meshShader.bindVertexAttribute(0, "position");
	meshShader.bindVertexAttribute(1, "normal");
	meshShader.finishInit();
	meshShader.init_uniforms({"projection", "view", "transform", "color", "camPos",
		"useColors", "useVolumeColors", "activeChannels",
		"PROJECTION_TYPE", "canvas_transform", "selectedCamIndex", "RemoveDistortion",
		"tile_size", "TSDFminCorner", "TSDFTilesCount", "clipEquation"});

	meshShader.init_uniforms(samplerNames);
	meshShader.start();
	for(int i=0; i<getVoxelsTexturesCount(); i++){
		meshShader.connectTextureUnit(samplerNames[i], i);
	}
	meshShader.stop();

	planeCutShader.finishInit();
	planeCutShader.init_uniforms({"projectionView", "transform", "camPos",
		"TSDFTilesCount", "TSDFminCorner", "tile_size", "activeChannels", "TransparencyOnly"});
	planeCutShader.init_uniforms(samplerNames);
	planeCutShader.start();
	for(int i=0; i<getVoxelsTexturesCount(); i++){
		planeCutShader.connectTextureUnit(samplerNames[i], i);
	}
	planeCutShader.stop();

	const int N = 5;
	timers.reserve(N);
	for(int i=0; i<N; i++){
		timers.emplace_back(GL_TIME_ELAPSED);
	}

	quad.bind();
	float positions[] = { -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f };
	quad.createFloatAttribute(0, positions, 8, 2, 0, GL_STATIC_DRAW);
	quad.unbind();
}

SparseGrid::~SparseGrid() {
}

bool SparseGrid::displayStatusMsg(){
	static bool overflow1 = false;
	static bool overflow2 = false;
	overflow1 = TilesCounter > params.getInt("ExpectedNonEmptyTiles");
	overflow2 = TotalVertices > params.getFloat("ExpectedVertices")*1000.0f;

	if(overflow1 || overflow2)ImGui::SetNextItemOpen(true, ImGuiCond_Always);
	if(ImGui::TreeNode("Sparse grid status")){

		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Non-Empty tiles: %d", TilesCounter);
		if(overflow1){
			ImGui::SameLine(0.0f, -1.0f);
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Overflow!");
		}
		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Reserve Tiles & write occupancy: %.3f ms", timers.at(0).getLastResult(true)*1.0E-6f);
		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Fill voxels: %.3f ms", timers.at(1).getLastResult(true)*1.0E-6f);
		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Compress occupancy: %.3f ms", timers.at(3).getLastResult(true)*1.0E-6f);
		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Marching cubes: %.3f ms", timers.at(2).getLastResult(true)*1.0E-6f);
		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Total vertices: %.3fK ", TotalVertices*1.0E-3f);
		if(overflow2){
			ImGui::SameLine(0.0f, -1.0f);
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Overflow!");
		}

		ImGui::TreePop();
	}
	return overflow1 || overflow2;
}

void SparseGrid::updateGUI(AllDepthmaps& depthmaps) {
	ImVec2 buttonSize =  ImVec2(ImGui::GetContentRegionAvailWidth(), ImGui::GetTextLineHeight()*1.5f);

	ImGui::Checkbox("Render Tiles", &params.getBool("RenderTSDFTiles"));
	ImGui::Separator();
	ImGui::SliderFloat3("Volume Min Corner", &params.getVec3("VolumeMinCorner").x, -10, 10, "%.3f");
	ImGui::SliderFloat3("Tiles", &params.getVec3("TSDFTilesDim", glm::vec3(16)).x, 4, 1024, "%.0f");
	ImGui::SliderFloat("Voxel size (mm)", &params.getFloat("VoxelSize", 1), 1, 20, "%.2f");
	ImGui::SliderFloat("Step size (relative to the voxel size)", &params.getFloat("StepSize", sqrt(3.0f)), 0.1f, sqrt(3.0f), "%.2f");


	ImGui::SliderInt("Expected non-empty tiles", &params.getInt("ExpectedNonEmptyTiles"), 100, 1000000, "%.0f");
	ImGui::Separator();

	bool change = false;

	if(ImGui::Button("Reserve Tiles", buttonSize)){
		reserveTiles();
	}
	if(ImGui::Button("Fill Tiles", buttonSize)){
		fillVoxels();
		change = true;
	}

	if(ImGui::Button("Compress occupancy", buttonSize)){
		compressOccupancy();
	}

	ImGui::Separator();
	ImGui::Checkbox("Render Mesh", &params.getBool("RenderMesh"));
	if(params.getBool("RenderMesh")){
		ImGui::SameLine();
		ImGui::Checkbox("Use colors", &params.getBool("useColors"));
		if(params.getBool("useColors")){
			ImGui::SameLine();
			ImGui::Checkbox("Use volume colors", &params.getBool("useVolumeColors"));
			if(params.getBool("useVolumeColors")){
				ImGui::SameLine();
				ImGui::Checkbox("Select channels", &params.getBool("selectChannels"));

				int& activeChannels = params.getInt("activeChannels");
				if(params.getBool("selectChannels")){
					static std::array<bool, 9> states = {};

					static const char* Ys[] = {"1", "y", "z", "x", "xy", "yz", "3z²-1", "xz", "x²-y²"};

					for(int i=0; i<9; i++){
						states[i] = (activeChannels>>i) & 1;
					}

					int COLUMNS_COUNT = 2*getSHBands()+1;
					if (ImGui::BeginTable("table_activechannels", COLUMNS_COUNT, ImGuiTableFlags_Borders))
					{

					int l = 0;
					int i = 0;
					for (int row = 0; row <= getSHBands(); row++)
					{
						int m = -getSHBands();
						ImGui::TableNextRow();
						for (int column = 0; column < COLUMNS_COUNT; column++)
						{
							ImGui::TableSetColumnIndex(column);
							if(m >= -l && m <= l){
								char buf[32];
								sprintf(buf, "Y_%d_%d  (%s) ", l, m, Ys[i]);
								ImGui::Checkbox(buf, &states[i]);
								i++;
							}
							m++;
						}
						l++;
					}
					ImGui::EndTable();
					}

					for(int i=0; i<9; i++){
						activeChannels ^= (-states[i] ^ activeChannels) & (1UL << (i % 32));
					}
				}else{
					activeChannels = -1;
				}


			}
		}
	}

	ImGui::SliderFloat("Expected Vertices", &params.getFloat("ExpectedVertices"), 1, 20000, "%.0fK");
	change |= ImGui::SliderFloat("Transparency offset", &params.getFloat("SDF_OFFSET"), 0.0f, 1.0f, "%.3f");

	if(ImGui::Button("Extract Mesh", buttonSize) || change){
		extractMesh();
	}
	ImGui::Separator();
	if(ImGui::Button("Export Mesh to temp", buttonSize)){
		exportMeshToTemp();
	}
	ImGui::Separator();

	displayStatusMsg();

	if(ImGui::TreeNode("Plane cuts")){
		int& selection = params.getInt("PlaneSelection");
		ImGui::Text("Plane: ");
		ImGui::SameLine();
		ImGui::RadioButton("x", &selection, 0);
		ImGui::SameLine();
		ImGui::RadioButton("y", &selection, 1);
		ImGui::SameLine();
		ImGui::RadioButton("z", &selection, 2);
		ImGui::SameLine();
		ImGui::Checkbox("Flip plane", &params.getBool("PlaneCutFlipped"));
		ImGui::SliderFloat("PlaneOffset", &params.getFloat("PlaneOffset"), -1, 1);

		ImGui::Checkbox("Draw plane cut", &params.getBool("DrawPlaneCut"));
		ImGui::SameLine();
		ImGui::Checkbox("Transparency", &params.getBool("PlaneCutTransparencyOnly"));
		IsPlaneCutActive = true;
		ImGui::TreePop();
	}else{
		IsPlaneCutActive = false;
	}
}

void SparseGrid::render(int selectedCameraIndex, const RenderingBase::Camera &camera, bool RenderMeshAlone, bool SideBySideViews) {

	glm::mat4 transform = glm::mat4(1.0);

	wireframeTileShader.start();
	wireframeTileShader.loadInt("PROJECTION_TYPE", RENDER_PROJECTION_TYPE::PROJECT_ON_3D_CANVAS);
	wireframeTileShader.loadMat4("projectionView", camera.getProjectionViewMatrix());
	wireframeTileShader.loadMat4("transform", transform);
	wireframeTileShader.loadInt("selectedCamIndex", selectedCameraIndex);

	if(!RenderMeshAlone){
		//Reconstruction volume
		wireframeTileShader.loadVec3("TSDFminCorner", getTSDFMinCorner(false));
		glLineWidth(1.0f);
		wireframeTileShader.loadVec4("color", glm::vec4(0, 1, 1, 1));
		wireframeTileShader.loadVec3("tile_size", getTSDFMaxCorner(false) - getTSDFMinCorner(false));
		RenderingBase::VAO gridVolume;
		gridVolume.bind();
		glm::ivec4 data = glm::ivec4(0);
		gridVolume.createIntAttribute(0, &data.x, 4, 4, 0, GL_STATIC_DRAW, 0);
		gridVolume.bindAttribute(0);
		glDrawArrays(GL_POINTS, 0, 1);
		gridVolume.unbindAttribute(0);
		gridVolume.unbind();
	}

	if(!RenderMeshAlone){
		//Reduced volume
		wireframeTileShader.loadVec3("TSDFminCorner", getTSDFMinCorner());
		glLineWidth(1.0f);
		wireframeTileShader.loadVec4("color", glm::vec4(1, 1, 1, 1));
		wireframeTileShader.loadVec3("tile_size", getTSDFMaxCorner() - getTSDFMinCorner());
		RenderingBase::VAO gridVolume;
		gridVolume.bind();
		glm::ivec4 data = glm::ivec4(0);
		gridVolume.createIntAttribute(0, &data.x, 4, 4, 0, GL_STATIC_DRAW, 0);
		gridVolume.bindAttribute(0);
		glDrawArrays(GL_POINTS, 0, 1);
		gridVolume.unbindAttribute(0);
		gridVolume.unbind();
	}
	wireframeTileShader.stop();

	if(!RenderMeshAlone && IsPlaneCutActive && TilesCounter > 0){

		int p = params.getInt("PlaneSelection");
		glm::vec3 a = (getTSDFMaxCorner() - getTSDFMinCorner()) * 0.5f;
		glm::vec3 b = (getTSDFMaxCorner() + getTSDFMinCorner()) * 0.5f;
		float offset = (b + params.getFloat("PlaneOffset") * a)[p];

		glm::mat4 transform = glm::mat4();
		transform[0][0] = a.x;
		transform[1][1] = a.y;
		transform[2][2] = a.z;
		transform[3] = glm::vec4(b, 1.0);
		transform[3][p] = offset;

		if(p == 0){
			transform = transform * glm::rotate(glm::mat4(1.0f), (float)std::numbers::pi / 2.0f, glm::vec3(0, 1, 0));
		}else if(p == 1){
			transform = transform * glm::rotate(glm::mat4(1.0f), (float)std::numbers::pi / 2.0f, glm::vec3(1, 0, 0));
		}
		clipEquation = glm::vec4(0.0);
		clipEquation[p] = 1;
		clipEquation.w = -offset;
		if(params.getBool("PlaneCutFlipped")){
			clipEquation *= -1.0f;
		}

		if(params.getBool("DrawPlaneCut")){
			planeCutShader.start();
			planeCutShader.loadMat4("transform", transform);
			planeCutShader.loadMat4("projectionView", camera.getProjectionViewMatrix());
			planeCutShader.loadVec3("camPos", camera.getCameraPos());
			planeCutShader.loadiVec3("TSDFTilesCount", getTilesDim());
			planeCutShader.loadVec3("TSDFminCorner", getTSDFMinCorner());
			planeCutShader.loadFloat("tile_size", getTileSize());
			planeCutShader.loadInt("activeChannels", params.getInt("activeChannels"));
			planeCutShader.loadInt("TransparencyOnly", params.getBool("PlaneCutTransparencyOnly"));

			for(int i=0; i<getVoxelsTexturesCount(); i++){
				getVoxelsTextures()[i]->bindToTextureUnit(i);
			}
			getTileOccupancy()->bindToTextureUnit(7);

			quad.bind();
			quad.bindAttribute(0);
			glDisable(GL_CULL_FACE);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			glEnable(GL_CULL_FACE);
			quad.unbindAttribute(0);
			quad.unbind();

			for(int i=0; i<getVoxelsTexturesCount(); i++){
				getVoxelsTextures()[i]->unbindFromTextureUnit(i);
			}
			getTileOccupancy()->unbindFromTextureUnit(7);
			planeCutShader.stop();
		}
	}else{
		clipEquation = glm::vec4(0.0);
	}

	const auto renderTiles = [this](glm::vec3 offset){
		if(TilesCounter == 0){
			return;
		}
		glLineWidth(1.0f);
		wireframeTileShader.start();
		wireframeTileShader.loadVec3("TSDFminCorner", offset + getTSDFMinCorner());
		wireframeTileShader.loadVec4("color", glm::vec4(1, 0, 1, 1));
		wireframeTileShader.loadVec3("tile_size", glm::vec3(getTileSize()));
		wireframeTileShader.loadInt("NumTiles", TilesCounter);
		RenderingBase::VAO vao;
		vao.bind();
		glBindBuffer(GL_ARRAY_BUFFER, TileDescriptorsBlock.getID());
		glVertexAttribIPointer(0, 4, GL_INT, sizeof(TileDesc), (GLvoid*) 0);
		glVertexAttribIPointer(1, 4, GL_INT, sizeof(TileDesc), (GLvoid*) offsetof(TileDesc, activeCameras));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		vao.bindAttributes({0, 1});

		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, getTileFlags().getID());
		glDrawArrays(GL_POINTS, 0, TilesCounter);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);

		vao.unbindAttributes({0, 1});
		vao.unbind();
		wireframeTileShader.loadInt("NumTiles", 0);
		wireframeTileShader.stop();
	};

	const auto renderMesh = [this, &camera](bool useColors, glm::mat4 transform){
		if(TotalVertices == 0){
			return;
		}
		meshShader.start();
		meshShader.loadMat4("projection", camera.getProjectionMatrix());
		meshShader.loadMat4("view", camera.getViewMatrix());
		meshShader.loadMat4("transform", transform);
		meshShader.loadVec3("camPos", camera.getCameraPos());
		meshShader.loadInt("useColors", useColors);
		bool volumeColors = params.getBool("useVolumeColors");
		meshShader.loadInt("useVolumeColors", volumeColors);
		meshShader.loadInt("PROJECTION_TYPE", RENDER_PROJECTION_TYPE::PROJECT_ON_3D_CANVAS);
		meshShader.loadFloat("tile_size", getTileSize());
		meshShader.loadVec3("TSDFminCorner", getTSDFMinCorner());
		meshShader.loadiVec3("TSDFTilesCount", getTilesDim());
		meshShader.loadInt("activeChannels", params.getInt("activeChannels"));
		meshShader.loadVec4("clipEquation", clipEquation);

		RenderingBase::VAO vao;
		vao.bind();
		VerticesBlock.bindAs(GL_ARRAY_BUFFER);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)16);
		VerticesBlock.unbindAs(GL_ARRAY_BUFFER);
		vao.bindAttributes({0, 1});

		glDisable(GL_CULL_FACE);
		meshShader.loadVec4("color", glm::vec4(1, 1, 1, 1));

		if(volumeColors){
			for(int i=0; i<getVoxelsTexturesCount(); i++){
				getVoxelsTextures()[i]->bindToTextureUnit(i);
			}
			getTileOccupancy()->bindToTextureUnit(7);
		}
		glDrawArrays(GL_TRIANGLES, 0, TotalVertices);
		if(volumeColors){
			for(int i=0; i<getVoxelsTexturesCount(); i++){
				getVoxelsTextures()[i]->unbindFromTextureUnit(i);
			}
			getTileOccupancy()->unbindFromTextureUnit(7);
		}
		glEnable(GL_CULL_FACE);

		vao.unbindAttributes({0, 1});
		vao.unbind();

		meshShader.stop();
	};

	transform = SceneReferenceFrame::getSceneTransform();
	transform = glm::inverse(transform);

	if(!RenderMeshAlone || !SideBySideViews){
		if(params.getBool("RenderTSDFTiles")) {
			renderTiles(glm::vec3(0));
		}

		if(params.getBool("RenderMesh")){
			renderMesh(params.getBool("useColors"), transform);
		}
	}else if(RenderMeshAlone){
		glm::vec3 offset = glm::vec3(getTilesDim().x, 0, 0) * getTileSize();
		renderTiles(offset);
		renderMesh(true, transform);
		renderMesh(false, glm::translate(transform, -offset));
	}



}

void SparseGrid::render(int selectedCameraIndex, const glm::mat4& canvas_transform, bool RemoveDistortion,
		bool RenderMesh, bool NoVolumeLines){

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLODs.at(getCurrentLOD()).CameraDataBlock.getID());

	wireframeTileShader.start();
	wireframeTileShader.loadInt("PROJECTION_TYPE", RENDER_PROJECTION_TYPE::PROJECT_ON_2D_CANVAS);
	wireframeTileShader.loadMat4("canvas_transform", canvas_transform);
	wireframeTileShader.loadInt("selectedCamIndex", selectedCameraIndex);
	wireframeTileShader.loadInt("RemoveDistortion", RemoveDistortion);

	if(!NoVolumeLines){
		//Reconstruction volume
		wireframeTileShader.loadVec3("TSDFminCorner", getTSDFMinCorner(false));
		glLineWidth(1.0f);
		wireframeTileShader.loadVec4("color", glm::vec4(0, 1, 1, 1));
		wireframeTileShader.loadVec3("tile_size", getTSDFMaxCorner(false) - getTSDFMinCorner(false));
		RenderingBase::VAO gridVolume;
		gridVolume.bind();
		glm::ivec4 data = glm::ivec4(0);
		gridVolume.createIntAttribute(0, &data.x, 4, 4, 0, GL_STATIC_DRAW, 0);
		gridVolume.bindAttribute(0);
		glDrawArrays(GL_POINTS, 0, 1);
		gridVolume.unbindAttribute(0);
		gridVolume.unbind();
	}

	if(!NoVolumeLines){
		//Reduced volume
		wireframeTileShader.loadVec3("TSDFminCorner", getTSDFMinCorner());
		glLineWidth(1.0f);
		wireframeTileShader.loadVec4("color", glm::vec4(1, 1, 1, 1));
		wireframeTileShader.loadVec3("tile_size", getTSDFMaxCorner() - getTSDFMinCorner());
		RenderingBase::VAO gridVolume;
		gridVolume.bind();
		glm::ivec4 data = glm::ivec4(0);
		gridVolume.createIntAttribute(0, &data.x, 4, 4, 0, GL_STATIC_DRAW, 0);
		gridVolume.bindAttribute(0);
		glDrawArrays(GL_POINTS, 0, 1);
		gridVolume.unbindAttribute(0);
		gridVolume.unbind();
	}

	if(params.getBool("RenderTSDFTiles") && TilesCounter > 0) {
		glLineWidth(1.0f);
		wireframeTileShader.loadVec4("color", glm::vec4(1, 0, 1, 1));
		wireframeTileShader.loadVec3("tile_size", glm::vec3(getTileSize()));
		wireframeTileShader.loadInt("NumTiles", TilesCounter);
		RenderingBase::VAO vao;
		vao.bind();
		glBindBuffer(GL_ARRAY_BUFFER, TileDescriptorsBlock.getID());
		glVertexAttribIPointer(0, 4, GL_INT, sizeof(TileDesc), (GLvoid*) 0);
		glVertexAttribIPointer(1, 4, GL_INT, sizeof(TileDesc), (GLvoid*) offsetof(TileDesc, activeCameras));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		vao.bindAttributes({0, 1});

		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, getTileFlags().getID());
		glDrawArrays(GL_POINTS, 0, TilesCounter);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);

		vao.unbindAttributes({0, 1});
		vao.unbind();
		wireframeTileShader.loadInt("NumTiles", 0);
	}

	wireframeTileShader.stop();

	if(RenderMesh && TotalVertices > 0){
		glm::mat4 transform = SceneReferenceFrame::getSceneTransform();
		transform = glm::inverse(transform);

		meshShader.start();
		meshShader.loadMat4("transform", transform);
		meshShader.loadVec3("camPos", imageLODs.at(getCurrentLOD()).cameras.at(selectedCameraIndex).center());
		meshShader.loadInt("useColors", params.getBool("useColors"));
		bool volumeColors = params.getBool("useVolumeColors");
		meshShader.loadInt("useVolumeColors", volumeColors);
		meshShader.loadFloat("tile_size", getTileSize());
		meshShader.loadVec3("TSDFminCorner", getTSDFMinCorner());
		meshShader.loadiVec3("TSDFTilesCount", getTilesDim());
		meshShader.loadInt("PROJECTION_TYPE", RENDER_PROJECTION_TYPE::PROJECT_ON_2D_CANVAS);
		meshShader.loadMat4("canvas_transform", canvas_transform);
		meshShader.loadInt("selectedCamIndex", selectedCameraIndex);
		meshShader.loadInt("RemoveDistortion", RemoveDistortion);
		meshShader.loadInt("activeChannels", params.getInt("activeChannels"));

		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

		RenderingBase::VAO vao;
		vao.bind();
		VerticesBlock.bindAs(GL_ARRAY_BUFFER);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)16);
		VerticesBlock.unbindAs(GL_ARRAY_BUFFER);
		vao.bindAttributes({0, 1});
		meshShader.loadVec4("color", glm::vec4(1, 1, 1, 1));

		if(volumeColors){
			for(int i=0; i<getVoxelsTexturesCount(); i++){
				getVoxelsTextures()[i]->bindToTextureUnit(i);
			}
			getTileOccupancy()->bindToTextureUnit(7);
		}
		getImageLoDs().at(getCurrentLOD()).ExposureLUT->bindToTextureUnit(10);

		glDrawArrays(GL_TRIANGLES, 0, TotalVertices);

		if(volumeColors){
			for(int i=0; i<getVoxelsTexturesCount(); i++){
				getVoxelsTextures()[i]->unbindFromTextureUnit(i);
			}
			getTileOccupancy()->unbindFromTextureUnit(7);
		}
		getImageLoDs().at(getCurrentLOD()).ExposureLUT->unbindFromTextureUnit(10);

		vao.unbindAttributes({0, 1});
		vao.unbind();
		glDisable(GL_DEPTH_TEST);


		meshShader.stop();
	}

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
}

void SparseGrid::renderTestCamera(int selectedCameraIndex, const glm::mat4& canvas_transform,
		std::vector<CameraInfo> cameras, RenderingBase::VBO& TestCameraDataBlock, bool useColors){
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, TestCameraDataBlock.getID());

	glm::mat4 transform = SceneReferenceFrame::getSceneTransform();
	transform = glm::inverse(transform);

	meshShader.start();
	meshShader.loadMat4("transform", transform);
	meshShader.loadVec3("camPos", cameras.at(selectedCameraIndex).center());
	meshShader.loadInt("useColors", useColors);
	bool volumeColors = params.getBool("useVolumeColors");
	meshShader.loadInt("useVolumeColors", volumeColors);
	meshShader.loadFloat("tile_size", getTileSize());
	meshShader.loadVec3("TSDFminCorner", getTSDFMinCorner());
	meshShader.loadiVec3("TSDFTilesCount", getTilesDim());
	meshShader.loadInt("PROJECTION_TYPE", RENDER_PROJECTION_TYPE::PROJECT_ON_2D_CANVAS);
	meshShader.loadMat4("canvas_transform", canvas_transform);
	meshShader.loadInt("selectedCamIndex", selectedCameraIndex);
	meshShader.loadInt("RemoveDistortion", false);
	meshShader.loadInt("activeChannels", params.getInt("activeChannels"));

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	RenderingBase::VAO vao;
	vao.bind();
	VerticesBlock.bindAs(GL_ARRAY_BUFFER);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)16);
	VerticesBlock.unbindAs(GL_ARRAY_BUFFER);
	vao.bindAttributes({0, 1});
	meshShader.loadVec4("color", glm::vec4(1, 1, 1, 1));

	if(volumeColors){
		for(int i=0; i<getVoxelsTexturesCount(); i++){
			getVoxelsTextures()[i]->bindToTextureUnit(i);
		}
		getTileOccupancy()->bindToTextureUnit(7);
	}
	getImageLoDs().at(getCurrentLOD()).ExposureLUT->bindToTextureUnit(10);


	glDrawArrays(GL_TRIANGLES, 0, TotalVertices);

	if(volumeColors){
		for(int i=0; i<getVoxelsTexturesCount(); i++){
			getVoxelsTextures()[i]->unbindFromTextureUnit(i);
		}
		getTileOccupancy()->unbindFromTextureUnit(7);
	}
	getImageLoDs().at(getCurrentLOD()).ExposureLUT->unbindFromTextureUnit(10);

	vao.unbindAttributes({0, 1});
	vao.unbind();
	glDisable(GL_DEPTH_TEST);

	meshShader.stop();
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
}

/**
 * Reserve the tiles by taking the visual hull into account
 */
void SparseGrid::build(AllDepthmaps& allDepthmaps){

	currentLOD = imagePyramidLevels-1;
	params.getInt("Sequence#SelectedLOD") = currentLOD;

	reserveTiles();
	fillVoxels();
	compressOccupancy();
	extractMesh();

}

/**
 * Reserve all the tiles, without using the visual hull
 */
void SparseGrid::build(){
	assert(false); // Does not work yet, do not use.

	currentLOD = imagePyramidLevels-1;
	params.getInt("Sequence#SelectedLOD") = currentLOD;

	TilesCounter = getTilesDim(false).x * getTilesDim(false).y * getTilesDim(false).z;
	reducedVolumeMinCorner = getTSDFMinCorner(false);
	reducedVolumeTilesDim = getTilesDim(false);

	TileDescriptorsBlock.bind();
	TileDescriptorsBlock.storeData(nullptr, sizeof(TileDesc)*TilesCounter, GL_STATIC_DRAW);
	TileDescriptorsBlock.unbind();

	std::vector<TileDesc> vec = std::vector<TileDesc>(TilesCounter);
	int n = 0;// x * Sy * Sz + y * Sy + z
	for(TileDesc& desc : vec){
		int k = n / (getTilesDim().x * getTilesDim().y);
		int j = (n - k * (getTilesDim().x * getTilesDim().y)) / getTilesDim().x;
		int i = n - k * (getTilesDim().x * getTilesDim().y) - j * getTilesDim().x;
		desc.coords_index = glm::ivec4(i, j, k, 0);
		desc.coords_index.w = m3D_e_magicbits<uint32_t, uint32_t>(i, j, k);
		n++;
	}
	std::sort(std::begin(vec), std::end(vec), [](const TileDesc& a, const TileDesc& b){
		return uint32_t(a.coords_index.w) < uint32_t(b.coords_index.w);
	});

	glm::ivec4 activeCameras = params.getiVec4("DifferentialRendering#activeCameras");
	for(int i=getNbCameras(); i<MAX_CAMERAS; i++){
		setCameraActive(activeCameras, i, false);//disable the non-existing cameras
	}
	for(int i=0; i<TilesCounter; i++){
		vec[i].coords_index.w = i;
		vec[i].activeCameras = activeCameras;
	}

	TileDescriptorsBlock.bind();
	TileDescriptorsBlock.storeData(vec.data(), sizeof(TileDesc)*TilesCounter, GL_STATIC_DRAW);
	TileDescriptorsBlock.unbind();

	allocateOccupancy(false);
	writeOccupancy();

	allocateVoxels();
	fillVoxels();
	compressOccupancy();
}

void SparseGrid::reserveTiles() {
	auto& q = timers.at(0).push_back();
	q.begin();


	reserveTilesShader.start();
	reserveTilesShader.loadVec3("VolumeMinCorner", getTSDFMinCorner(false));
	reserveTilesShader.loadFloat("tile_size", getTileSize());
	reserveTilesShader.loadiVec3("TilesTotal", getTilesDim(false));
	reserveTilesShader.loadInt("NumCameras", getNbCameras());

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLODs.at(getCurrentLOD()).CameraDataBlock.getID());

	const int zero = 0;
	RenderingBase::VBO GlobalTileCounter = RenderingBase::VBO(GL_ATOMIC_COUNTER_BUFFER);
	GlobalTileCounter.bind();
	GlobalTileCounter.storeData(&zero, sizeof(int), GL_STATIC_DRAW);
	GlobalTileCounter.unbind();

	TileDescriptorsBlock.bind();
	TileDescriptorsBlock.storeData(nullptr, sizeof(TileDesc)*params.getInt("ExpectedNonEmptyTiles"), GL_STATIC_DRAW);
	TileDescriptorsBlock.unbind();

	imageLODs.at(getCurrentLOD()).occupancyTexture.bindToTextureUnit(0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, TileDescriptorsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, imageLODs.at(getCurrentLOD()).depthTileDescriptorsBlock.getID());
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, GlobalTileCounter.getID());

	glDispatchCompute(getTilesDim(false).x, getTilesDim(false).y, getTilesDim(false).z);

//	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
	imageLODs.at(getCurrentLOD()).occupancyTexture.unbindFromTextureUnit(0);

	reserveTilesShader.stop();
	int* countPtr = (int*)glMapNamedBuffer(GlobalTileCounter.getID(), GL_READ_ONLY);
	TilesCounter = countPtr[0];
	glUnmapNamedBuffer(GlobalTileCounter.getID());

	if(TilesCounter == 0){
		reducedVolumeTilesDim = glm::ivec3(0);
		q.end();
		return;
	}

	//Reorder the 3D tiles in morton order
	TileDesc* descPtr = (TileDesc*)glMapNamedBuffer(TileDescriptorsBlock.getID(), GL_READ_WRITE);
	const int N = std::min(params.getInt("ExpectedNonEmptyTiles"), TilesCounter);

	glm::ivec3 minCoord(1000000);
	glm::ivec3 maxCoord(-1000000);

	std::vector<TileDesc> vec = std::vector<TileDesc>(descPtr, descPtr + N);
	for(TileDesc& desc : vec){
		minCoord = min(minCoord, glm::ivec3(desc.coords_index));
		maxCoord = max(maxCoord, glm::ivec3(desc.coords_index));
	}
	minCoord -= glm::ivec3(1);
	maxCoord += glm::ivec3(2);

	for(TileDesc& desc : vec){
		desc.coords_index -= glm::ivec4(minCoord, 0);
		desc.coords_index.w = m3D_e_magicbits<uint32_t, uint32_t>(desc.coords_index.x, desc.coords_index.y, desc.coords_index.z);
	}
	std::sort(std::begin(vec), std::end(vec), [](const TileDesc& a, const TileDesc& b){
		return uint32_t(a.coords_index.w) < uint32_t(b.coords_index.w);
	});

	glm::ivec4 activeCameras = params.getiVec4("DifferentialRendering#activeCameras");
	for(int i=getNbCameras(); i<MAX_CAMERAS; i++){
		setCameraActive(activeCameras, i, false);//disable the non-existing cameras
	}
	for(int i=0; i<N; i++){
		vec[i].coords_index.w = i;
		vec[i].activeCameras = activeCameras;
	}
	std::copy_n(vec.begin(), N, descPtr);
	glUnmapNamedBuffer(TileDescriptorsBlock.getID());

	reducedVolumeMinCorner = getTSDFMinCorner(false) + glm::vec3(minCoord) * getTileSize();
	reducedVolumeTilesDim = maxCoord - minCoord + glm::ivec3(1);

	allocateOccupancy(true);
	writeOccupancy();

	allocateVoxels();

	q.end();
}


void SparseGrid::writeOccupancy(){
	const int N = std::min(params.getInt("ExpectedNonEmptyTiles"), TilesCounter);
	writeOccupancyShader.start();
	writeOccupancyShader.loadInt("NumTiles", N);
	glBindImageTexture(0, tileOccupancy->getID(), 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, TileDescriptorsBlock.getID());
	glDispatchCompute((TilesCounter + 32 - 1) / 32, 1, 1);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);
	glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	writeOccupancyShader.stop();

}

void SparseGrid::fillVoxels() {

	auto& q = timers.at(1).push_back();
	q.begin();

	if(TilesCounter > 0){
		for(int i=0; i<getVoxelsTexturesCount(); i++){
			glm::vec4 v = (i == 0) ? glm::vec4(254.0f/255.0f, 0.0, 0.0, 0.0) : glm::vec4(0.5f);
			glClearTexImage(VoxelsTexture[i]->getID(), 0, GL_RGBA, GL_FLOAT, &v);
		}
	}
	q.end();

}

void SparseGrid::extractMesh(){
	if(getTotalTiles() == 0){
		return;
	}

	auto& q = timers.at(2).push_back();
	q.begin();
	VerticesBlock.bind();
	VerticesBlock.storeData(nullptr, params.getFloat("ExpectedVertices")*1000*sizeof(Vertex), GL_STATIC_DRAW);
	VerticesBlock.unbind();

	if(TilesCounter == 0){
		TotalVertices = 0;
	}else{
		TotalVertices = marchingCubes.extractMesh(getTSDFMinCorner(),
				getTileSize(),
				getTilesDim(),
				TilesCounter,
				params.getFloat("SDF_OFFSET"),
				tileOccupancy,
				TileDescriptorsBlock,
				VoxelsTexture[0]->getID(),
				VerticesBlock,
				SceneReferenceFrame::getSceneTransform());
	}

	q.end();
}


std::function<std::function<void()>()> SparseGrid::exportMeshTo(const std::string& path,
		const std::string& format, bool exportOnlyBinaryBuffers){
	const unsigned int N = (unsigned int)std::min(TotalVertices, int(params.getFloat("ExpectedVertices")*1000));

//	glm::mat3 R = SceneReferenceFrame::getSceneRotation();
//	glm::vec3 T = SceneReferenceFrame::getSceneTranslation();
//	float s = SceneReferenceFrame::getSceneScale();

	[[maybe_unused]]glm::mat4 M = SceneReferenceFrame::getScaleMatrix();

//	glm::mat4 transform = glm::mat4(s*R);
//	transform[3] = glm::vec4(T, 1.0f);

	glm::mat4 transform = glm::mat4(1.0);

	transform = M * transform;

	if(N==0){
		return [](){
			return [](){};
		};//No vertices at all
	}

	std::unique_lock < std::mutex > lock(transfer_mutex);
	transfer_condition.wait_for(lock, std::chrono::milliseconds(100), [this]() {
		return isTransferBufferAvailable.load();
	});

	isTransferBufferAvailable = false;
	uint32_t data_size = (uint32_t)(8 * N * sizeof(float));

	if(TransferBuffer.getDatasize() < data_size){
		//reallocate the vbo
		std::cout <<"Re-allocating transfer buffer !" <<std::endl;
		if(mappedPtr!=nullptr){
			glUnmapNamedBuffer(TransferBuffer.getID());
		}
		uint32_t buffsize = (uint32_t)(data_size * 1.3f);
		TransferBuffer = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
		glNamedBufferStorage(TransferBuffer.getID(), buffsize, nullptr, GL_MAP_READ_BIT | GL_MAP_PERSISTENT_BIT);
		TransferBuffer.setDataSize(buffsize);
		mappedPtr = (const char*)glMapNamedBufferRange(TransferBuffer.getID(), 0, buffsize, GL_MAP_READ_BIT | GL_MAP_PERSISTENT_BIT);
	}

	glCopyNamedBufferSubData(VerticesBlock.getID(), TransferBuffer.getID(), 0, 0, data_size);

	glDeleteSync(copySync);
	copySync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
	glFlush();


	return [this, path, data_size, N, format, exportOnlyBinaryBuffers, transform](){

		//the wait is executed by the shared context
		while(glClientWaitSync(copySync, 0, 10000) != GL_ALREADY_SIGNALED){

		}

		//this is executed by a normal worker thread
		return [this, path, data_size, N, format, exportOnlyBinaryBuffers, transform](){
			std::vector<float> rawData = std::vector<float>(8 * N);

			{
				std::unique_lock < std::mutex > lock(transfer_mutex);
				std::memcpy(rawData.data(), mappedPtr, data_size);
				isTransferBufferAvailable = true;
				transfer_condition.notify_one();
			}

			{
				std::ofstream f(path + ".bin", std::ios::out | std::ios::binary);
				f.write((const char*)rawData.data(), rawData.size() * sizeof(float));
			}

			if(exportOnlyBinaryBuffers){
				return;
			}

			aiScene scene;
			scene.mRootNode = new aiNode();
			aiMesh* mesh = new aiMesh();
			scene.mNumMeshes = 1;
			scene.mMeshes = new aiMesh*[1]{mesh};
			mesh->mNumVertices = N;
			mesh->mNumFaces = N/3;

			scene.mMaterials = new aiMaterial*[ 1 ];
			scene.mMaterials[ 0 ] = nullptr;
			scene.mNumMaterials = 1;
			scene.mMaterials[ 0 ] = new aiMaterial();

			scene.mMeshes[ 0 ]->mMaterialIndex = 0;

			scene.mRootNode->mMeshes = new unsigned int[ 1 ];
			scene.mRootNode->mMeshes[ 0 ] = 0;
			scene.mRootNode->mNumMeshes = 1;

			aiVector3D* vertices = new aiVector3D[N];
			aiFace* faces = new aiFace[N/3];
			mesh->mVertices = vertices;
			mesh->mFaces = faces;

			aiColor4D* colors = new aiColor4D[N];
			mesh->mColors[0] = colors;

			const auto data = rawData.data();
			for(unsigned int i=0; i<N; i++){
				glm::vec4 p = glm::vec4(data[8*i+0], data[8*i+1], data[8*i+2], 1.0);
				p = transform * p;
				vertices[i].x = p.x;
				vertices[i].y = p.y;
				vertices[i].z = p.z;

				glm::vec4 s = glm::unpackUnorm4x8(glm::floatBitsToUint(data[8*i+7]));
				colors[i] = aiColor4D(s.x, s.y, s.z, s.w);

			}

			for(unsigned int i=0; i<N/3; i++){
				faces[i].mIndices = new unsigned int[3]{3*i+0, 3*i+1, 3*i+2};
				faces[i].mNumIndices = 3;
			}

			std::string fullpath = path + "." + format;

			Assimp::DefaultLogger::create(ASSIMP_DEFAULT_LOG_NAME, Assimp::Logger::NORMAL);
			Assimp::DefaultLogger::get()->attachStream(Assimp::LogStream::createDefaultStream(aiDefaultLogStream_STDOUT), Assimp::Logger::NORMAL);
			Assimp::Exporter exporter;
			aiReturn ret = exporter.Export(&scene, format, fullpath, aiProcess_JoinIdenticalVertices);

			if(ret == aiReturn_SUCCESS){
				std::cout << "SUCCESS" << std::endl;
			}else if(ret == aiReturn_FAILURE){
				std::cout << "FAILURE" << std::endl;
				std::cout << exporter.GetErrorString() << std::endl;
			}else if(ret == aiReturn_OUTOFMEMORY){
				std::cout << "OUTOFMEMORY" << std::endl;
			}

		};
	};
}

void SparseGrid::exportMeshToTemp(){
	auto time = std::time(nullptr);
	std::stringstream ss;
	ss << std::put_time(std::localtime(&time), "%Y-%m-%d_%H-%M-%S"); // ISO 8601 without timezone information.
	auto s = ss.str();
	std::replace(s.begin(), s.end(), ':', '-');
	std::string path =  "./" + ss.str() + ".ply";

	std::cout << "Saving mesh at {" << path << "}" << std::endl;

	std::function<void()> f = exportMeshTo(path, "ply", false);
	f();
}


void SparseGrid::allocateOccupancy(bool clear){
	const unsigned int negative_one = -1;
	tileOccupancy = std::make_unique<Texture3D>(getTilesDim().x, getTilesDim().y, getTilesDim().z, GL_R32I);


//	std::cout <<"New tile occupancy texture: " <<getTilesDim().x <<" " <<getTilesDim().y <<" " <<getTilesDim().z <<std::endl;

	if(clear){
		glClearTexImage(tileOccupancy->getID(), 0, GL_RED_INTEGER, GL_INT, (GLvoid*)&negative_one);
	}
}

void SparseGrid::allocateVoxels(bool allocateVoxelsTextures) {
	size_t gradients_count = getVoxelsTexturesCount() * TilesCounter * VOXELS_PER_TILE;

	glm::vec4 zero = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
	GradientBlock.bind();
	GradientBlock.storeData(nullptr, gradients_count * sizeof(Gradient), GL_STATIC_DRAW);
	GradientBlock.clearData(voxelGradsInternalFormat, GL_RGBA, GL_FLOAT, &zero);
	GradientBlock.unbind();
	GradientSquaredBlock.bind();
	GradientSquaredBlock.storeData(nullptr, gradients_count * sizeof(Gradient), GL_STATIC_DRAW);
	GradientSquaredBlock.clearData(voxelGradsInternalFormat, GL_RGBA, GL_FLOAT, &zero);
	GradientSquaredBlock.unbind();

	if(gradients_count > INT32_MAX){
		throw "Too many gradients: " + std::to_string(gradients_count);
	}

	int multiplier = ceil(cbrt(TilesCounter));

//	std::cout <<"voxels 3D texture side: " <<VOXELS_PER_SIDE * multiplier <<std::endl;

	if(TilesCounter == 0){
		voxelsTextureSize = glm::ivec3(0);
		for(int i=0; i<getVoxelsTexturesCount(); i++){
			VoxelsTexture[i] = nullptr;
		}
		return;
	}

	voxelsTextureSize = glm::ivec3(VOXELS_PER_SIDE * multiplier);

	if(allocateVoxelsTextures){
		for(int i=0; i<getVoxelsTexturesCount(); i++){
			VoxelsTexture[i] = std::make_unique<Texture3D>(
					voxelsTextureSize.x,
					voxelsTextureSize.y,
					voxelsTextureSize.z,
					voxelInternalFormat);
		}
	}

}

void SparseGrid::compressOccupancy() {
	if(getTotalTiles() == 0){
		return;
	}

	auto& q = timers.at(3).push_back();
	q.begin();

	glm::ivec3 groups = (getTilesDim() + glm::ivec3(3)) / glm::ivec3(4);


	compressedTileOccupancy = std::make_unique<Texture3D>(groups.x, groups.y, groups.z,
			GL_RG32UI);

//	std::cout <<"Compressed tile occupancy size: " <<glm::to_string(groups) <<std::endl;

	glBindImageTexture(0, compressedTileOccupancy->getID(), 0, false, 0, GL_WRITE_ONLY, GL_RG32UI);
	tileOccupancy->bindToTextureUnit(0);

	compressOccupancyShader.start();
	compressOccupancyShader.loadiVec3("TSDFTilesCount", getTilesDim());

	glDispatchCompute(groups.x, groups.y, groups.z);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	compressOccupancyShader.stop();

	glBindImageTexture(0, 0, 0, false, 0, GL_WRITE_ONLY, GL_RG32UI);
	tileOccupancy->unbindFromTextureUnit(0);

	q.end();
}

size_t SparseGrid::memUsage(bool showGui) {
	size_t s = 0;

	size_t s1 = TileDescriptorsBlock.getDatasize();
	size_t s2 = tileOccupancy?tileOccupancy->memUsage(4):0;
	size_t s3 = compressedTileOccupancy?compressedTileOccupancy->memUsage(8):0;
	size_t s4 = 0;
	{
		for(int i=0; i<getVoxelsTexturesCount(); i++){
			s4 += VoxelsTexture[i]?VoxelsTexture[i]->memUsage(4):0;
		}
	}
	size_t s5 = GradientBlock.getDatasize();
	size_t s6 = GradientSquaredBlock.getDatasize();
	size_t s7 = VerticesBlock.getDatasize();

	s = s1 + s2 +s3 + s4 + s5 + s6 + s7;

	if(showGui && ImGui::TreeNode("Volumetric data", "Volumetric data (%s)", prettyPrintMem(s).c_str())){
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Tile descriptors: %s", prettyPrintMem(s1).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Tiles occupancy: %s", prettyPrintMem(s2).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Tiles compressed occupancy: %s", prettyPrintMem(s3).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Voxels: %s", prettyPrintMem(s4).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Gradients: %s", prettyPrintMem(s5).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Gradients squared: %s", prettyPrintMem(s6).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Vertices: %s", prettyPrintMem(s7).c_str());
		ImGui::TreePop();
	}

	return s;

}
