/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * TextureArray.h
 *
 *  Created on: 25 oct. 2021
 *      Author: Briac
 */

#ifndef SEQUENCE_TEXTURE2DARRAY_H_
#define SEQUENCE_TEXTURE2DARRAY_H_

#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Texture2D.h"

#include <string>
#include <vector>
#include <functional>
#include <assert.h>

class Texture2DArray {
public:
	Texture2DArray();
	virtual ~Texture2DArray();

	void init(int width, int height, int layers, GLenum internalformat);

	Texture2DArray(const Texture2DArray&) = delete;
	Texture2DArray& operator=(const Texture2DArray&) = delete;
	Texture2DArray(Texture2DArray&&) = delete;
	Texture2DArray& operator=(Texture2DArray&&) = delete;

	void uploadAllLayers(const std::vector<unsigned char>& merged_data);
	void createLayer(int layer,
			const RenderingBase::Texture2D::TextureData &data,
			bool clear = false, bool PBO = false);
	void createLayerFromTexture(int layer, RenderingBase::Texture2D& texture);

	void validateLayer(int layer, RenderingBase::Texture2D::TextureData&& texData){
		occupancy.at(layer) = true;
		dataInfo.at(layer) = std::move(texData);
	}

	void bind() const {
		glBindTexture(GL_TEXTURE_2D_ARRAY, ID);
	}

	void bindToTextureUnit(int textureUnit) const {
		if (ID == 0) {
			std::cerr << "Error, trying to bind non existent texture"
					<< std::endl;
		} else {
			glBindTextureUnit(textureUnit, ID);
		}
	}

	void setFilter(GLenum filter) const{
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, filter);
		glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, filter);
	}

	void unbindFromTextureUnit(int textureUnit) const {
		glBindTextureUnit(textureUnit, 0);
	}

	void unbind() const {
		glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
	}

	void clearLayer(int layer) {
		occupancy[layer] = false;
	}

	bool isLayerCreated(int layer) const {
		return occupancy.at(layer);
	}

	void invalidateLayer(int layer) {
		occupancy.at(layer) = false;
	}

	int getWidth() const {
		return width;
	}
	int getHeight() const {
		return height;
	}

	glm::ivec2 getSize() const{
		return glm::ivec2(width, height);
	}

	GLuint getID() const {
		return ID;
	}

	int getLayers() const {
		return layers;
	}

	size_t memUsage(int bytesPerPixel) const {
		return size_t(width) * size_t(height) * size_t(layers) * bytesPerPixel;
	}

	const std::string& getImagePath(int layer) const {
		return dataInfo.at(layer).path;
	}

	unsigned char* getTextureData(const std::string &path, int layer, int nrChannels=4) const;
	std::function<void()> saveToDiskLater(const std::string &path,
			int layer, int nrChannels=4) const;
	void saveToDisk(const std::string &path,
			int layer, int nrChannels=4) const;

	const std::vector<RenderingBase::Texture2D::TextureData>& getDataInfo() const{
		return dataInfo;
	}
private:
	int width, height;
	int layers;
	GLenum internalformat;
	GLuint ID = 0;
	std::vector<bool> occupancy;
	std::vector<RenderingBase::Texture2D::TextureData> dataInfo;

};

#endif /* SEQUENCE_TEXTURE2DARRAY_H_ */
