/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * PartialIntegrationShader.h
 *
 *  Created on: 1 févr. 2022
 *      Author: Briac
 */

#ifndef SRC_SEQUENCE_PARTIALINTEGRATIONSHADER_H_
#define SRC_SEQUENCE_PARTIALINTEGRATIONSHADER_H_

#include "ShaderEmulator.h"
#include "../CommonTypes.h"
#include <array>

namespace PartialIntegration{
using namespace glm;
using namespace GLEmulator;

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;
const int VOXELS_IN_GROUP = (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2);
const float STEP_SIZE = sqrt(3.0);// relative to the size of a voxel

const int GROUP_SIZE = VOXELS_PER_TILE;

static const SizedArray<int> tsdf_tiles;
static const SizedArray<TileDesc> tile_descriptors;
static const SizedArray<RayDescriptor> ray_descriptors;
static const SizedArray<Interval> intervals;
static SizedArray<f16vec4> ray_texels;
static const SizedArray<CameraData> cameraData;

static vec3 TSDFminCorner;
static vec3 TSDFmaxCorner;
static vec3 TSDFTilesCount;
static float tile_size;
static int NumCameras;
static int NumTiles;
static float OpticalDensity;

static isampler2DArray* occupancyImage;
static image3D* VoxelTextures;

[[maybe_unused]]static int partial_sums[512/32];
struct CubicTileProjection{
	i16vec2 pixelMinCorner;
	i16vec2 dims;
};

static CubicTileProjection allTileProjections[100];
struct RayToCompute{
	int k;
	float tmin;
	float tmax;
	vec3 ray;
	vec3 p;
};

static int sharedCamIndex;
static int sharedRayOffset;
static int totalValidRays;
static RayToCompute allValidRays[GROUP_SIZE];

static uint tile_indices[27];
static f16vec4 workVolume[VOXELS_IN_GROUP];

struct Invocation : public InvocationBase{


	vec3 project(vec3 position, int camIndex){
		vec4 p = cameraData[camIndex].cameraTransform * vec4(position, 1.0);
		return vec3(p.x, p.y, p.z);
	}

	vec3 unproject(vec2 coords, int camIndex){
		vec4 p = cameraData[camIndex].cameraTransformInv * vec4(coords, 1.0, 0.0);
		return normalize(vec3(p.x, p.y, p.z));
	}

	vec3 getCamCenter(int camIndex){
		return vec3(cameraData[camIndex].camCenter);
	}

	vec2 getImageSize(int camIndex){
		vec4 distortion2 = cameraData[camIndex].distortion2;
		vec2 image_size = vec2(distortion2.z, distortion2.w);
		return image_size;
	}

	vec2 toPixels(vec2 NDCPixel, int camIndex) {
		vec4 K = cameraData[camIndex].K;
		vec4 distortion1 = cameraData[camIndex].distortion1;

		float fx = K.x;
		float fy = K.y;
		float cx = K.z;
		float cy = K.w;
		float k1 = distortion1.x;
		float k2 = distortion1.y;

		float r2 = NDCPixel.x * NDCPixel.x + NDCPixel.y * NDCPixel.y;
		float l = 1.0 + r2 * (k1 + r2 * k2);

		NDCPixel *= l;
		NDCPixel = vec2(fx * NDCPixel.x + cx, fy * NDCPixel.y + cy);
		return NDCPixel;
	}

	vec2 toNDC(vec2 Pixel, int camIndex) {
		vec4 K = cameraData[camIndex].K;
		vec4 distortion1 = cameraData[camIndex].distortion1;

		float fx = K.x;
		float fy = K.y;
		float cx = K.z;
		float cy = K.w;
		float k1 = distortion1.x;
		float k2 = distortion1.y;

		float x_distorted = (Pixel.x - cx) / fx;
		float y_distorted = (Pixel.y - cy) / fy;

		float rDist = sqrt(x_distorted*x_distorted + y_distorted*y_distorted);

		float r = rDist;
		for(int i=0; i<5; i++){//Newton's method
			float r2 = r*r;
			float f = r * ( 1.0 + r2 * (k1 + r2 * k2)  ) - rDist;
			float fprime = 1.0 + r2 * (3.0 * k1 + r2 * 5.0 * k2);
			r -= f / fprime;
		}

		float s = r / max(rDist, 1.0E-6f);
		vec2 coordsNDC = vec2(x_distorted, y_distorted) * s;
		return coordsNDC;

	};

	bool isInsideImageRange(vec2 pixelUV, vec2 image_size) {
		return pixelUV.x > 0.0f && pixelUV.x < image_size.x && pixelUV.y > 0.0f && pixelUV.y < image_size.y;
	}


	//https://tavianator.com/cgit/dimension.git/tree/libdimension/bvh/bvh.c#n196
	bool rayVSbox(vec3 base, vec3 ray_inv, vec3 minCorner, vec3 maxCorner, float& tmin, float& tmax) {
		float tx1 = (minCorner.x - base.x) * ray_inv.x;
		float tx2 = (maxCorner.x - base.x) * ray_inv.x;

		tmin = min(tx1, tx2);
		tmax = max(tx1, tx2);

		float ty1 = (minCorner.y - base.y) * ray_inv.y;
		float ty2 = (maxCorner.y - base.y) * ray_inv.y;

		tmin = max(tmin, min(ty1, ty2));
		tmax = min(tmax, max(ty1, ty2));

		float tz1 = (minCorner.z - base.z) * ray_inv.z;
		float tz2 = (maxCorner.z - base.z) * ray_inv.z;

		tmin = max(tmin, min(tz1, tz2));
		tmax = min(tmax, max(tz1, tz2));

	 	return tmax >= max(0.0f, tmin);
	}


	void readTileIndices(uvec3 tile_coords){

		if(gl_LocalInvocationIndex < 27){
			uint tile_z = tile_coords.z + ((gl_LocalInvocationIndex/9) % 3) - 1;
			uint tile_y = tile_coords.y + ((gl_LocalInvocationIndex/3) % 3) - 1;
			uint tile_x = tile_coords.x + ((gl_LocalInvocationIndex/1) % 3) - 1;

			ivec3 neighbor_tile_coords = ivec3(tile_x, tile_y, tile_z);
			if(any(lessThan(neighbor_tile_coords, ivec3(0))) || any(greaterThanEqual(neighbor_tile_coords, ivec3(TSDFTilesCount)))){
				tile_indices[gl_LocalInvocationIndex] = -1;
			}else{
				uint tile_global_index = tile_x + int(TSDFTilesCount.x) * (tile_y + int(TSDFTilesCount.y) * tile_z);
				uint tile_packed_index = tsdf_tiles[tile_global_index];
				tile_indices[gl_LocalInvocationIndex] = tile_packed_index;
			}

		}
	    memoryBarrierShared();
	    barrier();
	}

	ivec3 getVoxelAtlasCoords(int tile_index, ivec3 localCoords) {
		ivec3 atlas_dims = imageSize(VoxelTextures) / VOXELS_PER_SIDE;

		ivec3 atlas_coords;
		atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
		int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
		atlas_coords.y = remainder / atlas_dims.x;
		atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;

		return atlas_coords * VOXELS_PER_SIDE + localCoords;
	}

	void readVoxels(){
	    //grab the tsdf values and store them in a (VOXELS_PER_SIDE+2)^3 work volume in shared memory
		for(uint k=gl_LocalInvocationIndex; k<VOXELS_IN_GROUP; k+=GROUP_SIZE){
			const uint sz = (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2);
			const uint sy = VOXELS_PER_SIDE+2;
			uint local_z = k / sz;
			uint remainder = k - local_z * sz;
			uint local_y = remainder / sy;
			uint local_x = remainder - local_y * sy;

			uint tile_z = (local_z + VOXELS_PER_SIDE-1) / VOXELS_PER_SIDE;
			uint tile_y = (local_y + VOXELS_PER_SIDE-1) / VOXELS_PER_SIDE;
			uint tile_x = (local_x + VOXELS_PER_SIDE-1) / VOXELS_PER_SIDE;

			local_z = (local_z + VOXELS_PER_SIDE-1) % VOXELS_PER_SIDE;
			local_y = (local_y + VOXELS_PER_SIDE-1) % VOXELS_PER_SIDE;
			local_x = (local_x + VOXELS_PER_SIDE-1) % VOXELS_PER_SIDE;

			uint tile_index = tile_indices[tile_z*9 + tile_y*3 + tile_x*1];
			if(tile_index == uint(-1)){
				workVolume[k] = f16vec4(0.0f, 0.0f, 0.0f, 0.0f);
			}else{
				ivec3 readCoords = getVoxelAtlasCoords(int(tile_index), ivec3(local_x, local_y, local_z));
				workVolume[k] = f16vec4(imageLoad(VoxelTextures, readCoords));
			}

		}
	    memoryBarrierShared();
	    barrier();
	}

	f16vec4 getTSDFAt(int x, int y, int z){// in [-1, 1]
		ivec3 localCoords = ivec3(gl_LocalInvocationID) + ivec3(x, y, z) + 1;// in [0, VOXELS_PER_SIDE+1]
		int k = localCoords.x + localCoords.y * (VOXELS_PER_SIDE+2) + localCoords.z * (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2);
		return workVolume[k];
	}

	vec4 lerpTSDFAt(vec3 coords){// in [0, 1]
		coords = coords + 0.5f;
		ivec3 intCoords = ivec3(coords);
		const ivec3 dims = ivec3(1, VOXELS_PER_SIDE+2, (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2));

		int k = intCoords.x + intCoords.y * dims.y + intCoords.z * dims.z;

		vec3 fracCoords = fract(coords);

		vec4 v000 = workVolume[k];
		vec4 v001 = workVolume[k + 1];

		vec4 v00x = mix(v000, v001, fracCoords.x);

		k += dims.y;
		vec4 v010 = workVolume[k];
		vec4 v011 = workVolume[k + 1];

		vec4 v01x = mix(v010, v011, fracCoords.x);
		vec4 v0y = mix(v00x, v01x, fracCoords.y);

		k += dims.z - dims.y;
		vec4 v100 = workVolume[k];
		vec4 v101 = workVolume[k + 1];

		vec4 v10x = mix(v100, v101, fracCoords.x);

		k += dims.y;
		vec4 v110 = workVolume[k];
		vec4 v111 = workVolume[k + 1];

		vec4 v11x = mix(v110, v111, fracCoords.x);
		vec4 v1y = mix(v10x, v11x, fracCoords.y);
		vec4 vz = mix(v0y, v1y, fracCoords.z);

		return vz;
	}



	int prefixSum(int v){

		int w = subgroupExclusiveAdd(v);
		if(gl_SubgroupInvocationID == gl_SubgroupSize-1){
			partial_sums[gl_SubgroupID] = w + v;
		}
		memoryBarrierShared();
		barrier();

		if(gl_SubgroupID == 0){
			int v2 = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : 0;
			int w2 = subgroupExclusiveAdd(v2);
			partial_sums[gl_SubgroupInvocationID] = w2;
		}
		memoryBarrierShared();
		barrier();

		return w + partial_sums[gl_SubgroupID];


//		static std::vector<int> values(1024);
//		values.at(gl_LocalInvocationIndex) = v;
//		barrier();
//
//		v = 0;
//		for(int i=0; i<gl_LocalInvocationIndex; i++){
//			v += values.at(i);
//		}
//		return v;
	}

	void computeTileProjections(ivec3 tile_coords){
		vec3 minTileCorner = TSDFminCorner + vec3(tile_coords) * tile_size;
		vec3 maxTileCorner = minTileCorner + tile_size;

		for(int camIndex = int(gl_LocalInvocationIndex / 8); camIndex < NumCameras; camIndex += GROUP_SIZE / 8){
			int cornerID = int(gl_LocalInvocationIndex % 8);
			[[maybe_unused]]int projID = int(gl_SubgroupInvocationID / 8);

			vec3 corner = vec3(
					(cornerID&1)==0?minTileCorner.x : maxTileCorner.x,
					(cornerID&2)==0?minTileCorner.y : maxTileCorner.y,
					(cornerID&4)==0?minTileCorner.z : maxTileCorner.z
				);

			vec3 proj = project(corner, camIndex);
			vec2 NDCCoords = vec2(proj) / proj.z;
			vec2 PixelCoords = toPixels(NDCCoords, camIndex);

			uvec4 ballot = subgroupPartitionNV(projID);
	       	vec4 result = subgroupPartitionedMaxNV(vec4(PixelCoords, -PixelCoords), ballot);

	       	if(cornerID == 0){
				vec2 image_size = getImageSize(camIndex);
				ivec2 pixelMax = min(ivec2(ceil(vec2(result.x, result.y))), ivec2(image_size)-1);
		       	ivec2 pixelMin = max(ivec2(floor(-vec2(result.z, result.w))), ivec2(0));

				ivec2 dims = pixelMax - pixelMin + 1;
				dims = max(dims, ivec2(0));

	       		allTileProjections[camIndex] = CubicTileProjection{i16vec2(pixelMin), i16vec2(dims)};
	       	}
		}

	    memoryBarrierShared();
	    barrier();

	}

	int compactRays(int& nextCamIndex, int& prevProjRayOffset, ivec3 tile_coords) {
		const vec3 minTileCorner = vec3(tile_coords);
		const vec3 maxTileCorner = minTileCorner + vec3(1.0);

		const float tile_size_inv = 1.0f / tile_size;

		if(gl_LocalInvocationIndex == 0){
			totalValidRays = 0;
		}
	    memoryBarrierShared();
	    barrier();

		// as long as we don't have GROUP_SIZE valid rays and that all the projections aren't used up
		while(totalValidRays < GROUP_SIZE && nextCamIndex < NumCameras){
			int reservedRays = 0;
			int localRayIndex = -1;
			int camIndex = -1;
			CubicTileProjection selectedProj;
			int selectedProjSize = 0;// number of texels in the projection

			//we check GROUP_SIZE rays at a time
			while(reservedRays < GROUP_SIZE && nextCamIndex < NumCameras){
				CubicTileProjection proj = allTileProjections[nextCamIndex];
				int projSize = proj.dims.x * proj.dims.y;

				int delta = min(projSize - prevProjRayOffset, GROUP_SIZE - reservedRays);

				if(gl_LocalInvocationIndex >= reservedRays && gl_LocalInvocationIndex-reservedRays < delta && localRayIndex == -1) {
					//bingo
					localRayIndex = prevProjRayOffset + int(gl_LocalInvocationIndex)-reservedRays;
					camIndex = nextCamIndex;
					selectedProj = proj;
					selectedProjSize = projSize;
				}

				prevProjRayOffset += delta;
				reservedRays += delta;
				if(prevProjRayOffset == projSize){
					//All the texels for the projection have been used up, move on to the next camera
					prevProjRayOffset = 0;
					nextCamIndex++;
				}
			};

			bool rayIsValid = gl_LocalInvocationIndex < reservedRays;

			vec3 camCenter;
			vec3 ray;
			float tmin, tmax;
			ivec2 intRayCoords;
			if(rayIsValid){
				camCenter = (getCamCenter(camIndex) - TSDFminCorner) * tile_size_inv;
				intRayCoords = ivec2(selectedProj.pixelMinCorner) + ivec2(localRayIndex % selectedProj.dims.x, localRayIndex / selectedProj.dims.x);
				vec2 rayCoords = toNDC(vec2(intRayCoords) + vec2(0.5f), camIndex);
				ray = unproject(rayCoords, camIndex);
				ray /= dot(cameraData[camIndex].cameraTransformInv[2], vec4(ray, 0.0));

				vec3 rayInv = 1.0f / ray;

				if(!rayVSbox(camCenter, rayInv, minTileCorner, maxTileCorner, tmin, tmax) || (tmax - tmin) < 0.01f){
					rayIsValid = false;
				}
			}

			int k;
			if(rayIsValid){
				int depthTileIndex = texelFetch(occupancyImage, ivec3(intRayCoords/DEPTHMAP_TILE_WIDTH, camIndex), 0).x;
				if(depthTileIndex == -1) {
					rayIsValid = false;
				}else{
					depthTileIndex &= ~(1 << 31);//Remove the occupancy flag on the msb
					k = depthTileIndex * DEPTHMAP_TILE_SIZE + (intRayCoords.y % DEPTHMAP_TILE_WIDTH) * DEPTHMAP_TILE_WIDTH + (intRayCoords.x % DEPTHMAP_TILE_WIDTH);
				}
			}


			int i = prefixSum(int(rayIsValid));
			bool shouldBeAddedInNextBatch = false;

			if(rayIsValid){
				if(totalValidRays + i < GROUP_SIZE){
					vec3 p = camCenter - minTileCorner;
					allValidRays[totalValidRays + i] = RayToCompute{k, tmin, tmax, ray, p};
				}else{
					shouldBeAddedInNextBatch = true;
				}
			}

			bool lastInGroup = gl_LocalInvocationIndex == GROUP_SIZE-1;
			//	Increment the number of valid rays
			if(lastInGroup){
				int validRays = i + int(rayIsValid);
				totalValidRays += min(validRays, GROUP_SIZE - totalValidRays);
				if(totalValidRays < GROUP_SIZE){
					sharedCamIndex = nextCamIndex;
					sharedRayOffset = prevProjRayOffset;
				}
			}

			int j = prefixSum(int(shouldBeAddedInNextBatch));

			if(totalValidRays == GROUP_SIZE) {
				if(shouldBeAddedInNextBatch && j==0){ //the first ray that must be included next
					sharedCamIndex = camIndex;
					sharedRayOffset = localRayIndex;
				}else if(lastInGroup && j==0) {
					localRayIndex++;
					if(localRayIndex == selectedProjSize){
						localRayIndex=0;
						camIndex++;
					}
					sharedCamIndex = camIndex;
					sharedRayOffset = localRayIndex;
				}
			}

		    memoryBarrierShared();
	    	barrier();

		}

		return totalValidRays;
	}



	void main_shader() override {

		const uint tile_index = gl_WorkGroupID.x;
		TileDesc tile = tile_descriptors[tile_index];
		const ivec3 tile_coords = ivec3(tile.coords_index);

		readTileIndices(tile_coords);
	    readVoxels();	//	One thread per voxel
	    computeTileProjections(tile_coords);

	    if(gl_LocalInvocationIndex == 63){
	    	std::cout <<"plop" <<std::endl;
	    }

	   	int camIndex = -1;
//	   	int rayOffset = 0; //index of the next texel to process in the projection of the tile
	   	while(++camIndex < NumCameras) { // one subgroup per camera
//	   		int totalValidRays = compactRays(camIndex, rayOffset, tile_coords);
//	   		camIndex = sharedCamIndex;
//	   		rayOffset = sharedRayOffset;
//
//	   		if(gl_LocalInvocationIndex >= totalValidRays){
//	   			continue;
//	   		}

	   		CubicTileProjection proj = allTileProjections[camIndex];
			int projSize = proj.dims.x * proj.dims.y;
			const vec3 minTileCorner = vec3(tile_coords);
			const vec3 maxTileCorner = minTileCorner + vec3(1.0);

			const float tile_size_inv = 1.0f / tile_size;


	   		for(int localRayIndex = gl_LocalInvocationIndex; localRayIndex < projSize; localRayIndex+=GROUP_SIZE){
	   			vec3 camCenter;
				vec3 ray;
				float tmin, tmax;
				ivec2 intRayCoords;
				camCenter = (getCamCenter(camIndex) - TSDFminCorner) * tile_size_inv;
				intRayCoords = ivec2(proj.pixelMinCorner) + ivec2(localRayIndex % proj.dims.x, localRayIndex / proj.dims.x);
				vec2 rayCoords = toNDC(vec2(intRayCoords) + vec2(0.5f), camIndex);
				ray = unproject(rayCoords, camIndex);
				ray /= dot(cameraData[camIndex].cameraTransformInv[2], vec4(ray, 0.0));

				vec3 rayInv = 1.0f / ray;

				if(!rayVSbox(camCenter, rayInv, minTileCorner, maxTileCorner, tmin, tmax) || (tmax - tmin) < 0.01f){
					continue;
				}


				int k;
				int depthTileIndex = texelFetch(occupancyImage, ivec3(intRayCoords/DEPTHMAP_TILE_WIDTH, camIndex), 0).x;
				if(depthTileIndex == -1) {
					continue;
				}else{
					depthTileIndex &= ~(1 << 31);//Remove the occupancy flag on the msb
					k = depthTileIndex * DEPTHMAP_TILE_SIZE + (intRayCoords.y % DEPTHMAP_TILE_WIDTH) * DEPTHMAP_TILE_WIDTH + (intRayCoords.x % DEPTHMAP_TILE_WIDTH);
				}

				vec3 p = camCenter - minTileCorner;
				RayToCompute rayToCompute = RayToCompute{k, tmin, tmax, ray, p};
//	   			RayToCompute rayToCompute = allValidRays[gl_LocalInvocationIndex];

				//compute the sub integral in tile space [0, VOXELS_PER_SIDE]^3
				float transmittance = 1.0f;
				vec3 color = vec3(0.0f);
				tmin = rayToCompute.tmin * float(VOXELS_PER_SIDE) / STEP_SIZE;
				tmax = rayToCompute.tmax * float(VOXELS_PER_SIDE) / STEP_SIZE;
				ray = rayToCompute.ray * STEP_SIZE;


				int stepsStart = int(tmin);
				int stepsEnd = int(tmax);


				p = rayToCompute.p; // camCenter - minTileCorner
				p = p * float(VOXELS_PER_SIDE) + ray * float(stepsStart);

				for(int step = stepsStart; step < stepsEnd; step++){
					p += ray;

					vec4 extinction_color = lerpTSDFAt(p);

					float e = exp(-extinction_color.x * STEP_SIZE * OpticalDensity);
					color.x += extinction_color.y * (transmittance * (1.0f - e));
					color.y += extinction_color.z * (transmittance * (1.0f - e));
					color.z += extinction_color.w * (transmittance * (1.0f - e));
					transmittance *= e;
				}


//				const RayDescriptor rayDescriptor = ray_descriptors[rayToCompute.k];

//				ivec3 intStart = ivec3(rayDescriptor.startTilex, rayDescriptor.startTiley, rayDescriptor.startTilez);
//				ivec3 diff = abs(tile_coords - intStart);
//				int crossings = diff.x + diff.y + diff.z;

//
//				int texelIndex = 0;
//				bool insideInterval = false;
//				for(int i=0; i < rayDescriptor.intervalsCount; i++){
//					Interval interval = intervals[rayDescriptor.intervalsPtr + i];
//					if(crossings >= interval.start && crossings < interval.end){
//						texelIndex += crossings - interval.start;
//						insideInterval = true;
//						 break; //we are good
//					}else{
//						texelIndex += interval.end - interval.start;
//					}
//				}
//
//				if(!insideInterval){
//					continue; //should not happen, but it seems to happen in some cases...
//				}

//				ray_texels[rayDescriptor.texelsPtr + texelIndex] = f16vec4(vec4(transmittance, color));
	   		}
	   	}
	}



	Invocation(Barrier* barrier) : InvocationBase(barrier){

	}


};

class PartialIntegrationShader : protected ShaderBase{
public:
	PartialIntegrationShader() : ShaderBase(4,4,4)
	{

	};
	virtual ~PartialIntegrationShader(){

	};


	void dispatchCompute(int gx, int gy, int gz) override {
		Barrier group_barrier(local_size_x * local_size_y * local_size_z);

		std::vector<std::unique_ptr<InvocationBase>> group;
		for(int e=0; e<local_size_x * local_size_y * local_size_z; e++){
			group.emplace_back(std::make_unique<Invocation>(&group_barrier));
		}

		dispatchComputeInternal(gx, gy, gz, group);
	}

	void runGroup(ivec3 gl_WorkGroupID, std::vector<std::unique_ptr<InvocationBase>>& group) override {


		if(ivec3(tile_descriptors[gl_WorkGroupID.x].coords_index) != ivec3(63, 49, 9)){
			return;
		}

		ivec3 t = ivec3(tile_descriptors[gl_WorkGroupID.x].coords_index);
		std::cout <<"Processing tile " <<glm::to_string(t) <<std::endl;

		std::vector<std::thread> threads;
		for (std::unique_ptr<InvocationBase>& e : group) {
			Invocation& invoc = dynamic_cast<Invocation&>(*e);
			threads.emplace_back([&]{
				invoc.main_shader();
			});
		}
		for (std::thread& thread : threads) {
			thread.join();
		}
	}

	void setUniforms(GL_PTR& tsdf_tilesPTR, GL_PTR& tile_descriptorsPTR,
			GL_PTR& ray_descriptorsPTR,
			GL_PTR& intervalsPTR, GL_PTR& ray_texelsPTR, GL_PTR& cameraDataPTR,
			isampler2DArray& occupancyImageSampler,
			image3D& VoxelTexturesSampler,
			vec3 _TSDFminCorner,
			vec3 _TSDFmaxCorner,
			vec3 _TSDFTilesCount,
			float _tile_size,
			int _NumCameras,
			int _NumTiles){

		tsdf_tiles.set((int*)tsdf_tilesPTR.data, tsdf_tilesPTR.size);
		tile_descriptors.set((TileDesc*)tile_descriptorsPTR.data, tile_descriptorsPTR.size);
		ray_descriptors.set((RayDescriptor*)ray_descriptorsPTR.data, ray_descriptorsPTR.size);
		intervals.set((Interval*)intervalsPTR.data, intervalsPTR.size);
		ray_texels.set((f16vec4*)ray_texelsPTR.data, ray_texelsPTR.size);
		cameraData.set((CameraData*)cameraDataPTR.data, cameraDataPTR.size);

		occupancyImage = &occupancyImageSampler;
		VoxelTextures = &VoxelTexturesSampler;

		TSDFminCorner = _TSDFminCorner;
		TSDFmaxCorner = _TSDFmaxCorner;
		TSDFTilesCount = _TSDFTilesCount;
		tile_size = _tile_size;
		NumCameras = _NumCameras;
		NumTiles = _NumTiles;
	}



};

};



#endif /* SRC_SEQUENCE_PARTIALINTEGRATIONSHADER_H_ */
