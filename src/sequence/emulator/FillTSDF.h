/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * PartialIntegrationShader.h
 *
 *  Created on: 1 févr. 2022
 *      Author: Briac
 */

#ifndef SRC_SEQUENCE_PARTIALINTEGRATIONSHADER_H_
#define SRC_SEQUENCE_PARTIALINTEGRATIONSHADER_H_

#include "ShaderEmulator.h"
#include "../CommonTypes.h"
#include <array>

namespace FillTSDF{
using namespace glm;
using namespace GLEmulator;

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

const int GROUP_SIZE = VOXELS_PER_TILE;

static const SizedArray<float> points;
static const SizedArray<TileDesc> tile_descriptors;
static const SizedArray<CameraData> cameraData;

static vec3 TSDFminCorner;
static float tile_size;
static int NumCameras;

static isampler2DArray* occupancyImages;
static sampler2DArray* srcImages;
static image3D* VoxelTextures;

struct Invocation : public InvocationBase{


	vec3 project(vec3 position, int camIndex){
		vec4 p = cameraData[camIndex].cameraTransform * vec4(position, 1.0);
		return vec3(p.x, p.y, p.z);
	}

	vec3 unproject(vec2 coords, float depth, int camIndex){
		vec4 p = cameraData[camIndex].cameraTransformInv * vec4(coords * depth, depth, 1.0);
		return vec3(p.x, p.y, p.z);
	}


	vec3 unproject(vec2 coords, int camIndex){
		vec4 p = cameraData[camIndex].cameraTransformInv * vec4(coords, 1.0, 0.0);
		return normalize(vec3(p.x, p.y, p.z));
	}

	vec2 getImageSize(int camIndex){
		vec4 distortion2 = cameraData[camIndex].distortion2;
		vec2 image_size = vec2(distortion2.z, distortion2.w);
		return image_size;
	}

	vec2 toPixels(vec2 NDCPixel, int camIndex) {
		vec4 K = cameraData[camIndex].K;
		vec4 distortion1 = cameraData[camIndex].distortion1;

		float fx = K.x;
		float fy = K.y;
		float cx = K.z;
		float cy = K.w;
		float k1 = distortion1.x;
		float k2 = distortion1.y;

		float r2 = NDCPixel.x * NDCPixel.x + NDCPixel.y * NDCPixel.y;
		float l = 1.0 + r2 * (k1 + r2 * k2);

		NDCPixel *= l;
		NDCPixel = vec2(fx * NDCPixel.x + cx, fy * NDCPixel.y + cy);
		return NDCPixel;
	}

	vec2 toNDC(vec2 Pixel, int camIndex) {
		vec4 K = cameraData[camIndex].K;
		vec4 distortion1 = cameraData[camIndex].distortion1;

		float fx = K.x;
		float fy = K.y;
		float cx = K.z;
		float cy = K.w;
		float k1 = distortion1.x;
		float k2 = distortion1.y;

		float x_distorted = (Pixel.x - cx) / fx;
		float y_distorted = (Pixel.y - cy) / fy;

		float rDist = sqrt(x_distorted*x_distorted + y_distorted*y_distorted);

		float r = rDist;
		for(int i=0; i<5; i++){//Newton's method
			float r2 = r*r;
			float f = r * ( 1.0 + r2 * (k1 + r2 * k2)  ) - rDist;
			float fprime = 1.0 + r2 * (3.0 * k1 + r2 * 5.0 * k2);
			r -= f / fprime;
		}

		float s = r / max(rDist, 1.0E-6f);
		vec2 coordsNDC = vec2(x_distorted, y_distorted) * s;
		return coordsNDC;

	};

	bool isInsideImageRange(vec2 pixelUV, vec2 image_size) {
		return pixelUV.x > 0.0f && pixelUV.x < image_size.x && pixelUV.y > 0.0f && pixelUV.y < image_size.y;
	}

	ivec3 getVoxelAtlasCoords(int tile_index, ivec3 localCoords) {
		ivec3 atlas_dims = imageSize(VoxelTextures) / VOXELS_PER_SIDE;

		ivec3 atlas_coords;
		atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
		int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
		atlas_coords.y = remainder / atlas_dims.x;
		atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;

		return atlas_coords * VOXELS_PER_SIDE + localCoords;
	}

	void main_shader() override{
		const float DEFAULT_DEPTH = 500.0f;
		const float voxel_size = tile_size / VOXELS_PER_SIDE;
		const float TSDF_CUTOFF = 4*voxel_size;//in mm

		if(gl_LocalInvocationIndex == 0){
			std::cout <<"plop" <<std::endl;
		}

		const int tile_index = int(gl_WorkGroupID.x);
		TileDesc tile = tile_descriptors[tile_index];

		const vec3 tile_coords = vec3(tile.coords_index);

		vec3 voxelCenter = TSDFminCorner + tile_coords * tile_size + (vec3(gl_LocalInvocationID)+0.5f) * voxel_size;

		float TSDF = 0;
		float weights = 0;
		float TSDF_FAR = +1.0f/0.0f;
		vec4 color = vec4(0.0f);

		int inside = 0;
		int outside = 0;
		int dontKnow = 0;
		for(int camIndex=0; camIndex<1; camIndex++){
			vec3 proj = project(voxelCenter, camIndex);
			const float voxelDepth = proj.z;
			if(voxelDepth <= 0.0f){
				dontKnow++;
				continue;
			}
			proj.x /= voxelDepth;
			proj.y /= voxelDepth;
			vec2 pixelUV = toPixels(vec2(proj), camIndex);

			vec2 image_size = getImageSize(camIndex);
			if(!isInsideImageRange(pixelUV, image_size)){
				dontKnow++;
				continue;
			}

			ivec2 pixelUV_int = ivec2(pixelUV);
			ivec2 depthTileUV = pixelUV_int / DEPTHMAP_TILE_WIDTH;
			ivec2 depthPixelUV = pixelUV_int % DEPTHMAP_TILE_WIDTH;

			int depthPixelLocalIndex = depthPixelUV.y * DEPTHMAP_TILE_WIDTH + depthPixelUV.x;

			int depthTileIndex = texelFetch(occupancyImages, ivec3(depthTileUV, camIndex), 0).x;

			const vec4 c = texelFetch(srcImages, ivec3(pixelUV_int, camIndex), 0);

			color = c;
			continue;

			if(depthTileIndex == -1){
				outside++;//The voxel projects outside of the image tiles
				continue;
			}

			const int depthPixelGlobalIndex = depthTileIndex * DEPTHMAP_TILE_SIZE + depthPixelLocalIndex;

			const float depth = points[depthPixelGlobalIndex];

			float tsdf;
			if(depth != DEFAULT_DEPTH){
				tsdf = (depth - voxelDepth)/TSDF_CUTOFF;
				if(voxelDepth < depth){
					outside++;
				}else{
					inside++;
				}

				if(abs(tsdf) <= 1.0f){
					TSDF += tsdf;
					weights += 1.0f;
					color += c;
				}else if(abs(tsdf) < abs(TSDF_FAR)){
					TSDF_FAR = tsdf;
				}
			}else{
				TSDF_FAR = 10.0f;//Far outside
				outside++;
			}

		}

		if(weights == 0){
			if(outside > 0 || inside == 0){
				TSDF = +1.0f;
			}else{
				TSDF = -1.0f;
			}
		}else{
			TSDF /= weights;
			color /= weights;
		}


		// The TSDF is > 0 outside and < 0 inside
		// The opacity transistions from -1 far outside, to +1 far inside

		[[maybe_unused]]float transparency = 1.0f-smoothstep(-1.0f, 1.0f, -TSDF); // in [0, +1]
		//transparency = 0.98f;


		//set everything to a dummy color
		//color.rgb = vec3(0.0f);


		[[maybe_unused]]ivec3 writeCoords = getVoxelAtlasCoords(tile_index, ivec3(gl_LocalInvocationID));
//		imageStore(VoxelTextures, writeCoords, vec4(transparency, vec3(color)));

	}

	Invocation(Barrier* barrier) : InvocationBase(barrier){

	}


};

class FillTSDFShader : protected ShaderBase{
public:
	FillTSDFShader() : ShaderBase(4,4,4)
	{

	};
	virtual ~FillTSDFShader(){

	};


	void dispatchCompute(int gx, int gy, int gz) override {
		Barrier group_barrier(local_size_x * local_size_y * local_size_z);

		std::vector<std::unique_ptr<InvocationBase>> group;
		for(int e=0; e<local_size_x * local_size_y * local_size_z; e++){
			group.emplace_back(std::make_unique<Invocation>(&group_barrier));
		}

		dispatchComputeInternal(gx, gy, gz, group);
	}

	void runGroup(ivec3 gl_WorkGroupID, std::vector<std::unique_ptr<InvocationBase>>& group) override {


		if(ivec3(tile_descriptors[gl_WorkGroupID.x].coords_index) != ivec3(193, 211, 54)){
			return;
		}

//		ivec3 t = ivec3(tile_descriptors[gl_WorkGroupID.x].coords_index);
//		std::cout <<"Processing tile " <<glm::to_string(t) <<std::endl;

		std::vector<std::thread> threads;
		for (std::unique_ptr<InvocationBase>& e : group) {
			Invocation& invoc = dynamic_cast<Invocation&>(*e);
			threads.emplace_back([&]{
				invoc.main_shader();
			});
		}
		for (std::thread& thread : threads) {
			thread.join();
		}
	}

	void setUniforms(
			GL_PTR& pointsPTR,
			GL_PTR& tile_descriptorsPTR,
			GL_PTR& cameraDataPTR,
			isampler2DArray& occupancyImageSampler,
			sampler2DArray& srcImagesSampler,
			image3D& VoxelTexturesSampler,
			vec3 _TSDFminCorner,
			float _tile_size,
			int _NumCameras){


		points.set((float*)pointsPTR.data, pointsPTR.size);
		tile_descriptors.set((TileDesc*)tile_descriptorsPTR.data, tile_descriptorsPTR.size);
		cameraData.set((CameraData*)cameraDataPTR.data, cameraDataPTR.size);

		occupancyImages = &occupancyImageSampler;
		srcImages = &srcImagesSampler;
		VoxelTextures = &VoxelTexturesSampler;

		TSDFminCorner = _TSDFminCorner;
		tile_size = _tile_size;
		NumCameras = _NumCameras;
	}



};

};



#endif /* SRC_SEQUENCE_PARTIALINTEGRATIONSHADER_H_ */
