/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/


#pragma once

#include "../../glm/glm.hpp"
#include "../../glm/detail/type_half.hpp"
#include "../../rendering_base/openglObjects.h"
#include <string.h>
#include <memory>
#include <iostream>
#include <barrier>
#include <thread>
#include <list>

#include "../CommonTypes.h"
#include "../CustomBarrier.h"

namespace GLEmulator{

#define shared static
#define uniform static

using namespace glm;

struct GL_PTR{
	const RenderingBase::VBO* vbo = nullptr;
	bool readonly = true;
	char* data = nullptr;
	GLsizeiptr size = 0;

	GL_PTR(){

	}

	GL_PTR(const RenderingBase::VBO& vbo, bool readonly){
		set(vbo, readonly);
	}

	void set(const RenderingBase::VBO& vbo, bool readonly) {
		clean();
		this->vbo = &vbo;
		this->readonly = readonly;
		void* ptr = glMapNamedBufferRange(vbo.getID(), 0, vbo.getDatasize(), GL_MAP_READ_BIT);
		size = vbo.getDatasize();
		data = new char[vbo.getDatasize()];
		memcpy(data, ptr, vbo.getDatasize());
		glUnmapNamedBuffer(vbo.getID());
	}

	void clean(){
		if(!data){
			return;
		}
		if(!readonly){
			void* ptr = glMapNamedBufferRange(vbo->getID(), 0, vbo->getDatasize(), GL_MAP_WRITE_BIT);
			memcpy(ptr, data, vbo->getDatasize());
			glUnmapNamedBuffer(vbo->getID());
		}
		delete[] data;
		data = nullptr;
		vbo = nullptr;
		size = 0;
	}

	~GL_PTR(){
		clean();
	}
};

template<typename T>
struct SizedArray{
	mutable T* elems = nullptr;
	mutable GLsizeiptr size = 0;

	void set(T* elems, GLsizeiptr size) const{
		this->elems = elems;
		int s = sizeof(T);
		assert(size % s == 0);
		this->size = size / s;
	}

	void clear() const{
		this->elems = nullptr;
		this->size = 0;
	}

	T& operator[](unsigned int i) {
		assert(i < size && i >= 0);
		assert(elems != nullptr);
		return elems[i];
	}

	const T& operator[](unsigned int i) const{
		assert(i < size && i >= 0);
		assert(elems != nullptr);
		return elems[i];
	}
};

template<typename T, unsigned int N>
struct SharedArray{
	mutable std::array<T, N> elems;
	mutable int size = N;

	T& operator[](int i) {
		assert(i < size && i >= 0);
		return elems[i];
	}

	const T& operator[](int i) const{
		assert(i < size && i >= 0);
		return elems[i];
	}
};

class isampler2DArray{
public:
	isampler2DArray(const Texture2DArray& tex, int nrChannels, int bytesPerChannel, GLenum format, GLenum type) {
		layers = tex.getLayers();
		width = tex.getWidth();
		height = tex.getHeight();
		this->nrChannels = nrChannels;
		this->bytesPerChannel = bytesPerChannel;

		dataLength = layers * width * height * nrChannels * bytesPerChannel;
		data = new unsigned char[dataLength];
		glGetTextureSubImage(tex.getID(), 0, 0, 0, 0, width, height, layers, format, type, dataLength, data);
	}
	virtual ~isampler2DArray(){
		delete[] data;
		data = nullptr;
	}
	unsigned char* data = nullptr;
	int64_t dataLength;
	int layers;
	int width;
	int height;
	int nrChannels;
	int bytesPerChannel;
};

class sampler2DArray : public isampler2DArray{
public:
	sampler2DArray(const Texture2DArray& tex, int nrChannels, int bytesPerChannel, GLenum format, GLenum type) : isampler2DArray(tex, nrChannels, bytesPerChannel, format, type){

	}
	virtual ~sampler2DArray() = default;
};

class sampler3D{
public:
	sampler3D(GLuint id, int nrChannels, int bytesPerChannel, int width, int height, int depth, GLenum format, GLenum type) {
		this->width = width;
		this->height = height;
		this->depth = depth;
		this->nrChannels = nrChannels;
		this->bytesPerChannel = bytesPerChannel;

		dataLength = depth * width * height * nrChannels * bytesPerChannel;
		data = new unsigned char[dataLength];
		glGetTextureSubImage(id, 0, 0, 0, 0, width, height, depth, format, type, dataLength, data);
	}
	virtual ~sampler3D(){
		delete[] data;
	}
	unsigned char* data = nullptr;
	int64_t dataLength;
	int depth;
	int width;
	int height;
	int nrChannels;
	int bytesPerChannel;
};

class usampler3D : public sampler3D{
public:
	usampler3D(GLuint id, int nrChannels, int bytesPerChannel, int width, int height,
			int depth, GLenum format, GLenum type) :
		sampler3D(id, nrChannels, bytesPerChannel, width, height, depth, format, type) {

	}
	virtual ~usampler3D(){

	}

};

class isampler3D : public sampler3D{
public:
	isampler3D(GLuint id, int nrChannels, int bytesPerChannel, int width, int height,
			int depth, GLenum format, GLenum type) :
		sampler3D(id, nrChannels, bytesPerChannel, width, height, depth, format, type) {

	}
	virtual ~isampler3D(){

	}

};


class image2D{
public:
	image2D(GLuint id, int nrChannels, int bytesPerChannel, int64_t width, int64_t height, GLenum format, GLenum type) {
		this->width = width;
		this->height = height;
		this->nrChannels = nrChannels;
		this->bytesPerChannel = bytesPerChannel;

		dataLength = width * height * nrChannels * bytesPerChannel;
		data = new unsigned char[dataLength];
		glGetTextureSubImage(id, 0, 0, 0, 0, width, height, 1, format, type, dataLength, data);
	}

	image2D(int nrChannels, int bytesPerChannel, int64_t width, int64_t height, unsigned char* pixels) {
			this->width = width;
			this->height = height;
			this->nrChannels = nrChannels;
			this->bytesPerChannel = bytesPerChannel;

			dataLength = width * height * nrChannels * bytesPerChannel;
			data = pixels;
		}
	virtual ~image2D(){
		delete[] data;
	}
	unsigned char* data = nullptr;
	int64_t dataLength;
	int width;
	int height;
	int nrChannels;
	int bytesPerChannel;
};

class uimage2D : public image2D{
public:
	uimage2D(GLuint id, int nrChannels, int bytesPerChannel,
			int64_t width, int64_t height, GLenum format, GLenum type) :
			image2D(id, nrChannels, bytesPerChannel, width, height, format, type) {

	}
	uimage2D(int nrChannels, int bytesPerChannel,
				int64_t width, int64_t height, unsigned char* pixels) :
		image2D(nrChannels, bytesPerChannel, width, height, pixels) {

		}
	virtual ~uimage2D(){
	}
};



class image3D{
public:
	image3D(GLuint id, int nrChannels, int bytesPerChannel, int64_t width, int64_t height, int64_t depth, GLenum format, GLenum type) {
		this->width = width;
		this->height = height;
		this->depth = depth;
		this->nrChannels = nrChannels;
		this->bytesPerChannel = bytesPerChannel;

		dataLength = depth * width * height * nrChannels * bytesPerChannel;
		data = new unsigned char[dataLength];
		glGetTextureSubImage(id, 0, 0, 0, 0, width, height, depth, format, type, dataLength, data);
	}
	virtual ~image3D(){
		delete[] data;
	}
	unsigned char* data = nullptr;
	int64_t dataLength;
	int depth;
	int width;
	int height;
	int nrChannels;
	int bytesPerChannel;
};

class uimage3D : public image3D{
public:
	uimage3D(GLuint id, int nrChannels, int bytesPerChannel,
			int64_t width, int64_t height, int64_t depth, GLenum format, GLenum type) :
			image3D(id, nrChannels, bytesPerChannel, width, height, depth, format, type) {

	}
	virtual ~uimage3D(){
	}
};



static inline ivec3 textureSize(sampler3D* samplerPTR, int lod){
	sampler3D& sampler = *samplerPTR;
	assert(lod==0);
	return ivec3(sampler.width, sampler.height, sampler.depth);
}
static inline ivec3 textureSize(isampler2DArray* samplerPTR, int lod){
	isampler2DArray& sampler = *samplerPTR;
	assert(lod==0);
	return ivec3(sampler.width, sampler.height, sampler.layers);
}

static inline ivec3 imageSize(image3D* samplerPTR){
	image3D& sampler = *samplerPTR;
	return ivec3(sampler.width, sampler.height, sampler.depth);
}

static inline ivec4 texelFetch(isampler2DArray* samplerPTR, ivec3 coords, int mipmap){
	isampler2DArray& sampler = *samplerPTR;
	ivec4 result = ivec4(0);
	for(int k=0; k<sampler.nrChannels; k++){
		int64_t i = coords.z * sampler.width * sampler.height + coords.y * sampler.width + coords.x;
		i = i * sampler.nrChannels + k;
		assert(i >= 0 && i < sampler.dataLength/sampler.bytesPerChannel);
		switch(sampler.bytesPerChannel){
		case 1:
				result[k] = ((int8_t*)sampler.data)[i];
				break;
		case 2:
				result[k] = ((int16_t*)sampler.data)[i];
				break;
		case 4:
				result[k] = ((int32_t*)sampler.data)[i];
				break;
		default:
			assert(false);
			break;
		}
	}
	return result;
}

static inline vec4 texelFetch(sampler2DArray* samplerPTR, ivec3 coords, int mipmap){
	sampler2DArray& sampler = *samplerPTR;
	vec4 result = vec4(0);
	for(int k=0; k<sampler.nrChannels; k++){
		int64_t i = coords.z * sampler.width * sampler.height + coords.y * sampler.width + coords.x;
		i = i * sampler.nrChannels + k;
		assert(i >= 0 && i < sampler.dataLength/sampler.bytesPerChannel);
		switch(sampler.bytesPerChannel){
		case 1:
			result[k] = sampler.data[i] / 255.0f;
			break;
		case 2:
				result[k] = glm::detail::toFloat32(((glm::detail::hdata*)sampler.data)[i]);
				break;
		case 4:
				result[k] = ((float*)sampler.data)[i];
				break;
		default:
			assert(false);
			break;
		}
	}
	return result;
}

static inline vec4 texelFetch(sampler3D* samplerPTR, ivec3 coords, int mipmap){
	sampler3D& sampler = *samplerPTR;
	vec4 result = vec4(0);
	for(int k=0; k<sampler.nrChannels; k++){
		int64_t i = coords.z * sampler.width * sampler.height + coords.y * sampler.width + coords.x;
		i = i * sampler.nrChannels + k;
		assert(i >= 0 && i < sampler.dataLength/sampler.bytesPerChannel);
		switch(sampler.bytesPerChannel){
		case 2:
				result[k] = glm::detail::toFloat32(((glm::detail::hdata*)sampler.data)[i]);
				break;
		case 4:
				result[k] = ((float*)sampler.data)[i];
				break;
		default:
			assert(false);
			break;
		}
	}
	return result;
}

static inline uvec4 texelFetch(usampler3D* samplerPTR, ivec3 coords, int mipmap){
	usampler3D& sampler = *samplerPTR;
	uvec4 result = uvec4(0);
	for(int k=0; k<sampler.nrChannels; k++){
		int64_t i = coords.z * sampler.width * sampler.height + coords.y * sampler.width + coords.x;
		i = i * sampler.nrChannels + k;
		assert(i >= 0 && i < sampler.dataLength/sampler.bytesPerChannel);
		switch(sampler.bytesPerChannel){
		case 1:
				result[k] = ((uint8_t*)sampler.data)[i];
				break;
		case 2:
				result[k] = ((uint16_t*)sampler.data)[i];
				break;
		case 4:
				result[k] = ((uint32_t*)sampler.data)[i];
				break;
		default:
			assert(false);
			break;
		}
	}
	return result;
}

static inline ivec4 texelFetch(isampler3D* samplerPTR, ivec3 coords, int mipmap){
	isampler3D& sampler = *samplerPTR;
	uvec4 result = uvec4(0);
	for(int k=0; k<sampler.nrChannels; k++){
		int64_t i = coords.z * sampler.width * sampler.height + coords.y * sampler.width + coords.x;
		i = i * sampler.nrChannels + k;
		assert(i >= 0 && i < sampler.dataLength/sampler.bytesPerChannel);
		switch(sampler.bytesPerChannel){
		case 1:
				result[k] = ((int8_t*)sampler.data)[i];
				break;
		case 2:
				result[k] = ((int16_t*)sampler.data)[i];
				break;
		case 4:
				result[k] = ((int32_t*)sampler.data)[i];
				break;
		default:
			assert(false);
			break;
		}
	}
	return result;
}


static inline vec4 imageLoad(image2D* samplerPTR, ivec2 coords) {
	image2D& sampler = *samplerPTR;
	vec4 result = vec4(0);
	for(int k=0; k<sampler.nrChannels; k++){
		int64_t i = coords.y * sampler.width + coords.x;
		i = i * sampler.nrChannels + k;
		assert(i >= 0 && i < sampler.dataLength/sampler.bytesPerChannel);
		switch(sampler.bytesPerChannel){
		case 2:
				result[k] = glm::detail::toFloat32(((glm::detail::hdata*)sampler.data)[i]);
				break;
		case 4:
				result[k] = ((float*)sampler.data)[i];
				break;
		default:
			assert(false);
			break;
		}
	}
	return result;
}

static inline uvec4 imageLoad(uimage2D* samplerPTR, ivec2 coords) {
	uimage2D& sampler = *samplerPTR;
	vec4 result = vec4(0);
	for(int k=0; k<sampler.nrChannels; k++){
		int64_t i = coords.y * sampler.width + coords.x;
		i = i * sampler.nrChannels + k;
		assert(i >= 0 && i < sampler.dataLength/sampler.bytesPerChannel);
		switch(sampler.bytesPerChannel){
		case 1:
				result[k] = ((uint8_t*)sampler.data)[i];
				break;
		case 2:
				result[k] = ((uint16_t*)sampler.data)[i];
				break;
		case 4:
				result[k] = ((uint32_t*)sampler.data)[i];
				break;
		default:
			assert(false);
			break;
		}
	}
	return result;
}

static inline vec4 imageLoad(image3D* samplerPTR, ivec3 coords) {
	image3D& sampler = *samplerPTR;
	vec4 result = vec4(0);
	for(int k=0; k<sampler.nrChannels; k++){
		int64_t i = coords.z * sampler.width * sampler.height + coords.y * sampler.width + coords.x;
		i = i * sampler.nrChannels + k;
		assert(i >= 0 && i < sampler.dataLength/sampler.bytesPerChannel);
		switch(sampler.bytesPerChannel){
		case 2:
				result[k] = glm::detail::toFloat32(((glm::detail::hdata*)sampler.data)[i]);
				break;
		case 4:
				result[k] = ((float*)sampler.data)[i];
				break;
		default:
			assert(false);
			break;
		}
	}
	return result;
}

static inline uvec4 imageLoad(uimage3D* samplerPTR, ivec3 coords) {
	uimage3D& sampler = *samplerPTR;
	vec4 result = vec4(0);
	for(int k=0; k<sampler.nrChannels; k++){
		int64_t i = coords.z * sampler.width * sampler.height + coords.y * sampler.width + coords.x;
		i = i * sampler.nrChannels + k;
		assert(i >= 0 && i < sampler.dataLength/sampler.bytesPerChannel);
		switch(sampler.bytesPerChannel){
		case 1:
				result[k] = ((uint8_t*)sampler.data)[i];
				break;
		case 2:
				result[k] = ((uint16_t*)sampler.data)[i];
				break;
		case 4:
				result[k] = ((uint32_t*)sampler.data)[i];
				break;
		default:
			assert(false);
			break;
		}
	}
	return result;
}
static inline vec4 texture(sampler2DArray* samplerPTR, vec3 coords){
	sampler2DArray& sampler = *samplerPTR;
	assert(sampler.layers > coords.z && coords.z >= 0.0f);
	assert(sampler.nrChannels == 4);
	assert(sampler.bytesPerChannel == 1);
	vec4 results[4];

	const int layer = int(coords.z);

	vec2 coordsPixel = clamp(vec2(coords)*vec2(sampler.width, sampler.height), vec2(0.5f), vec2(sampler.width-0.5f, sampler.height-0.5f));
	ivec2 coords_00 = ivec2(coordsPixel - vec2(0.5f));
	ivec2 whole_coords[4] = {
		coords_00 + ivec2(0, 0),
		coords_00 + ivec2(1, 0),
		coords_00 + ivec2(0, 1),
		coords_00 + ivec2(1, 1)
	};

	const vec2 fCoords = fract(coordsPixel - vec2(0.5f));
	vec4 lerp_weights = vec4((1.0f-fCoords.x) * (1.0f-fCoords.y), fCoords.x * (1.0f-fCoords.y), (1.0f-fCoords.x) * fCoords.y, fCoords.x*fCoords.y);


	for(int k=0; k<4; k++){
		ivec2 intCoords = whole_coords[k];
		const int64_t i = layer * sampler.width * sampler.height + intCoords.y * sampler.width + intCoords.x;
		assert(i >= 0 && i < sampler.dataLength/(sampler.bytesPerChannel*sampler.nrChannels));
		results[k] = unpackUnorm4x8(((unsigned int*)sampler.data)[i]);
	}

	vec4 result = vec4(0.0f);
	for(int k=0; k<4; k++){
		result += results[k] * lerp_weights[k];
	}

	return result;
}

static inline vec4 texture(sampler3D* samplerPTR, vec3 coords){
	sampler3D& sampler = *samplerPTR;
	assert(1.0f >= coords.z && coords.z >= 0.0f);
	assert(1.0f >= coords.y && coords.y >= 0.0f);
	assert(1.0f >= coords.x && coords.x >= 0.0f);
	assert(sampler.nrChannels == 4);
	assert(sampler.bytesPerChannel == 2);

	vec3 coordsPixel = clamp(vec3(coords)*vec3(sampler.width, sampler.height, sampler.depth), vec3(0.5f), vec3(sampler.width, sampler.height, sampler.depth)-vec3(0.5f));
	ivec3 whole_coords = ivec3(coordsPixel - vec3(0.5f));

	const vec3 fCoords = fract(coordsPixel - vec3(0.5f));

	const int dx = whole_coords.x == sampler.width-1 ? 0 : 1;
	const int dy = whole_coords.y == sampler.height-1 ? 0 : 1;
	const int dz = whole_coords.z == sampler.depth-1 ? 0 : 1;


	vec4 v = texelFetch(samplerPTR, whole_coords+ivec3(0,0,0), 0);
	vec4 vx = texelFetch(samplerPTR, whole_coords+ivec3(dx,0,0), 0);
	v = mix(v, vx, fCoords.x);

	vec4 vy = texelFetch(samplerPTR, whole_coords+ivec3(0,dy,0), 0);
	vec4 vxy = texelFetch(samplerPTR, whole_coords+ivec3(dx,dy,0), 0);
	vy = mix(vy, vxy, fCoords.x);
	v = mix(v, vy, fCoords.y);


	vec4 vz = texelFetch(samplerPTR, whole_coords+ivec3(0,0,dz), 0);
	vec4 vxz = texelFetch(samplerPTR, whole_coords+ivec3(dx,0,dz), 0);
	vz = mix(vz, vxz, fCoords.x);

	vec4 vyz = texelFetch(samplerPTR, whole_coords+ivec3(0,dy,dz), 0);
	vec4 vxyz = texelFetch(samplerPTR, whole_coords+ivec3(dx,dy,dz), 0);
	vyz = mix(vyz, vxyz, fCoords.x);
	vz = mix(vz, vyz, fCoords.y);
	v = mix(v, vz, fCoords.z);

	return v;
}

struct OnBarrierCompletion{
	void operator()(){
	}
};

struct InvocationBase{

	ivec3 gl_NumWorkGroups;
	ivec3 gl_WorkGroupSize;
	ivec3 gl_GlobalInvocationID;
	ivec3 gl_LocalInvocationID;
	ivec3 gl_WorkGroupID;
	int gl_LocalInvocationIndex = 0;

	uint gl_SubgroupID = 0;
	const uint gl_SubgroupSize = 32;
	uint gl_NumSubgroups = 0;
	uint gl_SubgroupInvocationID = 0;

	Barrier* sync_point;

	struct GroupBarrier{
		Barrier barrier = Barrier(32);
	};

	void memoryBarrierShared(){

	}
	void barrier(){
	    sync_point->arrive_and_wait(gl_LocalInvocationIndex);
	}

	void subgroupMemoryBarrierShared(){

	}

    void subgroupBarrier(){
    	static std::array<GroupBarrier, 32> sync_point_subgroup = {};

    	sync_point_subgroup.at(gl_SubgroupID).barrier.arrive_and_wait(gl_SubgroupInvocationID);
    }

    int atomicAdd(int& mem, int v){
    	std::lock_guard<std::mutex> l(sync_point->mMutex);
    	int old = mem;
    	mem += v;
		return old;
	}

    uint atomicAdd(uint& mem, uint v){
    	std::lock_guard<std::mutex> l(sync_point->mMutex);
    	uint old = mem;
    	mem += v;
		return old;
	}

    bool subgroupElect(){
    	return gl_SubgroupInvocationID == 0;
    }

    template<typename T>
	T subgroupMin(T v){
		static std::vector<std::vector<T>> values(32, std::vector<T>(32));
		values.at(gl_SubgroupID).at(gl_SubgroupInvocationID) = v;
		subgroupBarrier();

		v = values.at(gl_SubgroupID).at(0);
		for(int i=1; i<32; i++){
			v = min(v, values.at(gl_SubgroupID).at(i));
		}

		subgroupBarrier();

		return v;
	}

    template<typename T>
	T subgroupExclusiveAdd(T v){
		static std::vector<std::vector<T>> values(32, std::vector<T>(32));
		values.at(gl_SubgroupID).at(gl_SubgroupInvocationID) = v;
		subgroupBarrier();

		v = T(0.0f);
		for(int i=0; i<int(gl_SubgroupInvocationID); i++){
			v += values.at(gl_SubgroupID).at(i);
		}

		subgroupBarrier();

		return v;
	}

    template<typename T>
	T subgroupExclusiveMul(T v){
		static std::vector<std::vector<T>> values(32, std::vector<T>(32));
		values.at(gl_SubgroupID).at(gl_SubgroupInvocationID) = v;
		subgroupBarrier();

		v = T(1.0f);
		for(int i=0; i<int(gl_SubgroupInvocationID); i++){
			v *= values.at(gl_SubgroupID).at(i);
		}

		subgroupBarrier();

		return v;
	}

    template<typename T>
	T subgroupAdd(T v){
		static std::vector<std::vector<T>> values(32, std::vector<T>(32));
		values.at(gl_SubgroupID).at(gl_SubgroupInvocationID) = v;
		subgroupBarrier();

		v = T(0);
		for(int i=0; i<32; i++){
			v += values.at(gl_SubgroupID).at(i);
		}

		subgroupBarrier();

		return v;
	}

    template<typename T>
	uvec4 subgroupPartitionNV(T v){
		static std::vector<std::vector<T>> values(32, std::vector<T>(32));
		values.at(gl_SubgroupID).at(gl_SubgroupInvocationID) = v;
		subgroupBarrier();

		int ballot = 0;
		for(int i=0; i<32; i++){
			ballot |= (values.at(gl_SubgroupID).at(i) == v) << i;
		}

		subgroupBarrier();

		return uvec4(ballot, 0, 0, 0);
	}

    template<typename T>
	T subgroupPartitionedMaxNV(T v, uvec4 ballot){
		static std::vector<std::vector<T>> values(32, std::vector<T>(32));
		values.at(gl_SubgroupID).at(gl_SubgroupInvocationID) = v;
		subgroupBarrier();

		for(int i=0; i<32; i++){
			if((ballot.x & (1<<i)) != 0){
				v = max(v, values.at(gl_SubgroupID).at(i));
			}
		}
		subgroupBarrier();

		return v;
	}

    template<typename T>
	T subgroupPartitionedMinNV(T v, uvec4 ballot){
		static std::vector<std::vector<T>> values(32, std::vector<T>(32));
		values.at(gl_SubgroupID).at(gl_SubgroupInvocationID) = v;
		subgroupBarrier();

		for(int i=0; i<32; i++){
			if((ballot.x & (1<<i)) != 0){
				v = min(v, values.at(gl_SubgroupID).at(i));
			}
		}
		subgroupBarrier();

		return v;
	}

    template<typename T>
	T subgroupPartitionedExclusiveMulNV(T v, uvec4 ballot){
		static std::vector<std::vector<T>> values(32, std::vector<T>(32));
		values.at(gl_SubgroupID).at(gl_SubgroupInvocationID) = v;
		subgroupBarrier();

		int N = 0;
		for(int i=0; i<int(gl_SubgroupInvocationID); i++){
			if((ballot.x & (1<<i)) != 0){
				N++;
			}
		}

		v = T(1.0);
		int n = 0;
		for(int i=0; i<32; i++){
			if((ballot.x & (1<<i)) != 0 && n < N){
				v *= values.at(gl_SubgroupID).at(i);
				n++;
			}
		}

		subgroupBarrier();

		return v;
	}

    template<typename T>
	T subgroupPartitionedExclusiveAddNV(T v, uvec4 ballot){
		static std::vector<std::vector<T>> values(32, std::vector<T>(32));
		values.at(gl_SubgroupID).at(gl_SubgroupInvocationID) = v;
		subgroupBarrier();

		int N = 0;
		for(int i=0; i<int(gl_SubgroupInvocationID); i++){
			if((ballot.x & (1<<i)) != 0){
				N++;
			}
		}

		v = T(0.0);
		int n = 0;
		for(int i=0; i<32; i++){
			if((ballot.x & (1<<i)) != 0 && n < N){
				v += values.at(gl_SubgroupID).at(i);
				n++;
			}
		}

		subgroupBarrier();

		return v;
	}


    template<typename T>
	T subgroupBroadcast(T v, int id){
		static std::vector<std::vector<T>> values(32, std::vector<T>(32));
		values.at(gl_SubgroupID).at(gl_SubgroupInvocationID) = v;
		subgroupBarrier();
		v  = values.at(gl_SubgroupID).at(id);
		subgroupBarrier();
		return v;
	}

	virtual void main_shader() = 0;
	
	InvocationBase(Barrier* sync_point) : sync_point(sync_point) {
	};
	virtual ~InvocationBase() {
	};

};

class ShaderBase{
public:
	ShaderBase(int local_size_x, int local_size_y, int local_size_z) :
		local_size_x(local_size_x), local_size_y(local_size_y), local_size_z(local_size_z) {

	};
	virtual ~ShaderBase(){

	};


	virtual void dispatchCompute(int gx, int gy, int gz) = 0;
	virtual void runGroup(ivec3 gl_WorkGroupID, std::vector<std::unique_ptr<InvocationBase>>& group) = 0;

protected:

	void dispatchComputeInternal(int gx, int gy, int gz, std::vector<std::unique_ptr<InvocationBase>>& group){

		assert((int)group.size() == local_size_x * local_size_y * local_size_z);
		assert(local_size_x * local_size_y * local_size_z <= 1024);

		//Groups
		for(int c=0; c<gz; c++){
			for(int b=0; b<gy; b++){
				for(int a=0; a<gx; a++){

					dispatchWorkGroup(gx, gy, gz, a, b, c, group);

				}
			}
		}

	}

	void dispatchWorkGroup(int gx, int gy, int gz, int a, int b, int c, std::vector<std::unique_ptr<InvocationBase>>& group){
		int e = 0;

		for(int k=0; k<local_size_z; k++){
			for(int j=0; j<local_size_y; j++){
				for(int i=0; i<local_size_x; i++){
					auto& invoc = *group.at(e);

					const int workGroupSize = local_size_x * local_size_y * local_size_z;

					invoc.gl_NumWorkGroups = ivec3(gx, gy, gz);
					invoc.gl_WorkGroupSize = ivec3(local_size_x , local_size_y, local_size_z);
					invoc.gl_WorkGroupID = ivec3(a, b, c);
					invoc.gl_LocalInvocationID = ivec3(i, j, k);
					invoc.gl_GlobalInvocationID = invoc.gl_WorkGroupID * invoc.gl_WorkGroupSize + invoc.gl_LocalInvocationID;
					invoc.gl_LocalInvocationIndex =
							invoc.gl_LocalInvocationID.z * invoc.gl_WorkGroupSize.x * invoc.gl_WorkGroupSize.y +
							invoc.gl_LocalInvocationID.y * invoc.gl_WorkGroupSize.x +
							invoc.gl_LocalInvocationID.x;

					invoc.gl_NumSubgroups = workGroupSize / invoc.gl_SubgroupSize;
					invoc.gl_SubgroupID = invoc.gl_LocalInvocationIndex / invoc.gl_SubgroupSize;
					invoc.gl_SubgroupInvocationID = invoc.gl_LocalInvocationIndex % invoc.gl_SubgroupSize;


					e++;
				}
			}
		}

		runGroup(ivec3(a, b, c), group);
	}

	int local_size_x, local_size_y, local_size_z;
};

};
