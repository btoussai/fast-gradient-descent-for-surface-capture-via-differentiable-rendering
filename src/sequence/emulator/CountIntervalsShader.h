/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * CountIntervalsShader.h
 *
 *  Created on: 9 mars 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_EMULATOR_COUNTINTERVALSSHADER_H_
#define SEQUENCE_EMULATOR_COUNTINTERVALSSHADER_H_

#include "../../glm/glm.hpp"
#include "ShaderEmulator.h"
#include "../CommonTypes.h"


namespace CountIntervals{
using namespace glm;
using namespace GLEmulator;

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;
const int VOXELS_IN_GROUP = (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2) * (VOXELS_PER_SIDE+2);
const float STEP_SIZE = sqrt(3.0);// relative to the size of a voxel


uniform usampler3D* CompressedTileOccupancy;
static const SizedArray<DepthTileDescriptor> depthTileDescriptors;
static SizedArray<int> atomics;
static SizedArray<RayDescriptor> ray_descriptors;
static SizedArray<Interval> intervals;
static const SizedArray<CameraData> cameraData;

shared int intervalsPtr;
shared int texelsPtr;
shared int insideCounter;
shared int intervalCounter;
shared int texelCounter;
shared int intervalLengthCounter;
shared Interval sharedIntervals[64];
shared ivec4 partial_sums[32];

uniform vec3 TSDFminCorner;
uniform vec3 TSDFmaxCorner;
uniform ivec3 TSDFTilesCount;
uniform float tile_size;
uniform int NumCameras;
uniform int NumTiles;
uniform int samplingSpacing;


struct Invocation : public InvocationBase{
	Invocation(Barrier* barrier) : InvocationBase(barrier){

	}


	vec3 project(vec3 position, int camIndex){
		vec4 p = cameraData[camIndex].cameraTransform * vec4(position, 1.0);
		return vec3(p.x, p.y, p.z);
	}

	vec3 unproject(vec2 coords, int camIndex){
		vec4 p = cameraData[camIndex].cameraTransformInv * vec4(coords, 1.0, 0.0);
		return normalize(vec3(p.x, p.y, p.z));
	}

	vec3 getCamCenter(int camIndex){
		return vec3(cameraData[camIndex].camCenter);
	}

	vec2 getImageSize(int camIndex){
		vec4 distortion2 = cameraData[camIndex].distortion2;
		vec2 image_size = vec2(distortion2.z, distortion2.w);
		return image_size;
	}

	vec2 toNDC(vec2 Pixel, int camIndex) {
		vec4 K = cameraData[camIndex].K;
		vec4 distortion1 = cameraData[camIndex].distortion1;

		float fx = K.x;
		float fy = K.y;
		float cx = K.z;
		float cy = K.w;
		float k1 = distortion1.x;
		float k2 = distortion1.y;

		float x_distorted = (Pixel.x - cx) / fx;
		float y_distorted = (Pixel.y - cy) / fy;

		float rDist = sqrt(x_distorted*x_distorted + y_distorted*y_distorted);

		float r = rDist;
		for(int i=0; i<5; i++){//Newton's method
			float r2 = r*r;
			float f = r * ( 1.0 + r2 * (k1 + r2 * k2)  ) - rDist;
			float fprime = 1.0 + r2 * (3.0 * k1 + r2 * 5.0 * k2);
			r -= f / fprime;
		}

		float s = r / max(rDist, 1.0E-6f);
		vec2 coordsNDC = vec2(x_distorted, y_distorted) * s;
		return coordsNDC;

	};

	//https://tavianator.com/cgit/dimension.git/tree/libdimension/bvh/bvh.c#n196
	bool rayVSbox(vec3 base, vec3 ray_inv, vec3 minCorner, vec3 maxCorner, float &tmin, float& tmax) {
		float tx1 = (minCorner.x - base.x) * ray_inv.x;
		float tx2 = (maxCorner.x - base.x) * ray_inv.x;

		tmin = min(tx1, tx2);
		tmax = max(tx1, tx2);

		float ty1 = (minCorner.y - base.y) * ray_inv.y;
		float ty2 = (maxCorner.y - base.y) * ray_inv.y;

		tmin = max(tmin, min(ty1, ty2));
		tmax = min(tmax, max(ty1, ty2));

		float tz1 = (minCorner.z - base.z) * ray_inv.z;
		float tz2 = (maxCorner.z - base.z) * ray_inv.z;

		tmin = max(tmin, min(tz1, tz2));
		tmax = min(tmax, max(tz1, tz2));

	 	return tmax >= max(0.0f, tmin);
	}

	ivec4 groupMin(ivec4 v){

		v = subgroupMin(v);
		if(subgroupElect()){
			partial_sums[gl_SubgroupID] = v;
		}

		memoryBarrierShared();
		barrier();

		if(gl_SubgroupID == 0){
			v = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : ivec4(2147483647);
			ivec4 w = subgroupMin(v);
			partial_sums[gl_SubgroupInvocationID] = w;
		}

		memoryBarrierShared();
		barrier();

		return partial_sums[gl_SubgroupID];
	}

	ivec4 groupAdd(ivec4 v){

		v = subgroupAdd(v);
		if(subgroupElect()){
			partial_sums[gl_SubgroupID] = v;
		}

		memoryBarrierShared();
		barrier();

		if(gl_SubgroupID == 0){
			v = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : ivec4(0);
			ivec4 w = subgroupAdd(v);
			partial_sums[gl_SubgroupInvocationID] = w;
		}

		memoryBarrierShared();
		barrier();

		return partial_sums[gl_SubgroupID];
	}

	void main_shader(void) override{
		//One thread per ray, one group per 2D tile

		const int tile_id = int(gl_WorkGroupID.x);
		const DepthTileDescriptor tileDescriptor = depthTileDescriptors[tile_id];
		const int camIndex = tileDescriptor.camIndex;

		const ivec2 pixelCoords = tileDescriptor.tileCoords * DEPTHMAP_TILE_WIDTH + ivec2(gl_LocalInvocationID);
		const vec2 uv = vec2(pixelCoords) + vec2(0.5f);
		const vec2 ndcCoords = toNDC(uv, camIndex);

	    const float tile_size_inv = 1.0f / tile_size;
	    const float scaling = tile_size_inv * float(VOXELS_PER_SIDE);

		const vec3 camCenter = (getCamCenter(camIndex) - TSDFminCorner) * scaling;
		vec3 ray = unproject(ndcCoords, camIndex);
		ray /= dot(cameraData[camIndex].cameraTransformInv[2], vec4(ray, 0.0));
		ray *= STEP_SIZE;
		const vec3 ray_inv = 1.0f / ray;

		if(gl_LocalInvocationIndex == 0){
			insideCounter = 0; // None of the rays are inside
			intervalCounter = 0;
			texelCounter = 0;
			intervalLengthCounter = 0;
			intervalsPtr = -1;
			texelsPtr = -1;
		}


		float tmin, tmax;
		bool hit = rayVSbox(camCenter, ray_inv, vec3(0.0f), (TSDFmaxCorner-TSDFminCorner)*scaling, tmin, tmax);

		int numHit = groupAdd(ivec4(int(hit))).x;
		if(numHit == 0){
			if(gl_LocalInvocationIndex == 0){
				ray_descriptors[tile_id] = RayDescriptor{
						intervalsPtr,
						intervalCounter,
						texelsPtr,
						texelCounter,
						tileDescriptor.tileCoords.x,
						tileDescriptor.tileCoords.y
				};
			}
			return;//None of the rays hit the volume
		}

		ivec4 bounds = hit ? ivec4(int(floor(max(0.0f, tmin))), -int(ceil(tmax)), 0, 0) : ivec4(2147483647);
		bounds = groupMin(bounds);

		int t = bounds.x;
		const int max_t = -bounds.y;


		bool prev_empty = true;
		while(t <= max_t){
			int q = 0;

			if(hit){
				bool isEmpty = true;
				const vec3 p = camCenter + ray * float(t);
				const ivec3 tile_coords = ivec3(floor(p / float(VOXELS_PER_SIDE)));

				if(all(greaterThanEqual(tile_coords, ivec3(0))) && all(lessThan(tile_coords, TSDFTilesCount))){
					const ivec3 compressedCoord = tile_coords / 4;
					const ivec3 localCompressedCoord = tile_coords % 4;
					const uvec2 bitmask = uvec2(texelFetch(CompressedTileOccupancy, ivec3(compressedCoord), 0));//returns 0 when sampling outside
					const int bitPos = localCompressedCoord.x + 4*localCompressedCoord.y + 16 * (localCompressedCoord.z%2);
					isEmpty = ( ((localCompressedCoord.z/2)==0 ? bitmask.x : bitmask.y) & (1<<bitPos)) == 0;
				}
				q = isEmpty != prev_empty ? (isEmpty?-1:+1) : 0;
				prev_empty = isEmpty;
			}

			q = subgroupAdd(q);
			if(gl_SubgroupID == 1 && subgroupElect()){
				partial_sums[0].x = q;
			}
			memoryBarrierShared();
			barrier();

			if(gl_SubgroupID == 0 && subgroupElect()){
				q += partial_sums[0].x;
				insideCounter += q;
				if(q != 0){
					if(insideCounter == 0){//leaving volume
						sharedIntervals[intervalCounter].end = int16_t(t);
						int length = sharedIntervals[intervalCounter].end - sharedIntervals[intervalCounter].start;
						intervalLengthCounter += length;
						intervalCounter++;
					}else if(insideCounter == q){//entering volume
						sharedIntervals[intervalCounter].start = int16_t(t);
					}
				}
			}

			t++;
		}



		memoryBarrierShared();
		barrier();

		if(gl_LocalInvocationIndex == 0){
			if(insideCounter > 0){
				//close the last interval
				sharedIntervals[intervalCounter].end = int16_t(t);
				int length = sharedIntervals[intervalCounter].end - sharedIntervals[intervalCounter].start;
				intervalLengthCounter += length;
				intervalCounter++;
			}
			if(intervalCounter > 0){
				intervalsPtr = atomicAdd(atomics[0], intervalCounter);
				texelCounter = (intervalLengthCounter + samplingSpacing - 1) / samplingSpacing;
				texelsPtr = atomicAdd(atomics[1], texelCounter);
				atomicAdd(atomics[2], intervalLengthCounter);
			}


			ray_descriptors[tile_id] = RayDescriptor{
					intervalsPtr,
					intervalCounter,
					texelsPtr,
					texelCounter,
					tileDescriptor.tileCoords.x,
					tileDescriptor.tileCoords.y
				};
		}

		memoryBarrierShared();
		barrier();

		if(gl_LocalInvocationIndex < intervalCounter){
			intervals[intervalsPtr + gl_LocalInvocationIndex] = sharedIntervals[gl_LocalInvocationIndex];
		}

	}


};


class CountIntervalsShader : protected ShaderBase{
public:

	CountIntervalsShader() : ShaderBase(DEPTHMAP_TILE_WIDTH, DEPTHMAP_TILE_WIDTH, 1)
	{

	};
	virtual ~CountIntervalsShader(){

	};


	void dispatchCompute(int gx, int gy, int gz) override {
		Barrier group_barrier(local_size_x * local_size_y * local_size_z);

		std::vector<std::unique_ptr<InvocationBase>> group;
		for(int e=0; e<local_size_x * local_size_y * local_size_z; e++){
			group.emplace_back(std::make_unique<Invocation>(&group_barrier));
		}

		dispatchComputeInternal(gx, gy, gz, group);
	}

	void runGroup(ivec3 gl_WorkGroupID, std::vector<std::unique_ptr<InvocationBase>>& group) override {
//
//		if(ivec3(tile_descriptors[gl_WorkGroupID.x].coords_index) != ivec3(14, 14, 6)){
//			return;
//		}
//		if(ivec3(tile_descriptors[gl_WorkGroupID.x].coords_index) != ivec3(params.getVec3("DDATest#Tile"))){
//			return;
//		}
		const int tile_id = int(gl_WorkGroupID.x);
		const DepthTileDescriptor tileDescriptor = depthTileDescriptors[tile_id];
		const int camIndex = tileDescriptor.camIndex;

		if(camIndex != 41 || tileDescriptor.tileCoords != ivec2(18, 16)){
			return;
		}



		std::vector<std::thread> threads;
		for (std::unique_ptr<InvocationBase>& e : group) {
			Invocation& invoc = dynamic_cast<Invocation&>(*e);
			threads.emplace_back([&]{
				invoc.main_shader();
			});
		}
		for (std::thread& thread : threads) {
			thread.join();
		}

	}

	void setUniforms(
			GL_PTR& depthTileDescriptorsPTR,
			GL_PTR& atomicsPTR,
			GL_PTR& ray_descriptorsPTR,
			GL_PTR& intervalsPTR,
			GL_PTR& cameraDataPTR,
			usampler3D& CompressedTileOccupancySampler,
			vec3 _TSDFminCorner,
			vec3 _TSDFmaxCorner,
			ivec3 _TSDFTilesCount,
			float _tile_size,
			int _NumCameras,
			int _NumTiles,
			int _samplingSpacing){

		depthTileDescriptors.set((DepthTileDescriptor*)depthTileDescriptorsPTR.data, depthTileDescriptorsPTR.size);
		atomics.set((int*)atomicsPTR.data, atomicsPTR.size);
		ray_descriptors.set((RayDescriptor*)ray_descriptorsPTR.data, ray_descriptorsPTR.size);
		intervals.set((Interval*)intervalsPTR.data, intervalsPTR.size);
		cameraData.set((CameraData*)cameraDataPTR.data, cameraDataPTR.size);

		CompressedTileOccupancy = &CompressedTileOccupancySampler;


		TSDFminCorner = _TSDFminCorner;
		TSDFmaxCorner = _TSDFmaxCorner;
		TSDFTilesCount = _TSDFTilesCount;
		tile_size = _tile_size;
		NumCameras = _NumCameras;
		NumTiles = _NumTiles;
		samplingSpacing = _samplingSpacing;


	}



};


}



#endif /* SEQUENCE_EMULATOR_COUNTINTERVALSSHADER_H_ */
