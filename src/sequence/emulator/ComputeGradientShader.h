/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * ComputeGradientShader.h
 *
 *  Created on: 14 févr. 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_EMULATOR_COMPUTEGRADIENTSHADER_H_
#define SEQUENCE_EMULATOR_COMPUTEGRADIENTSHADER_H_

#include "../../glm/glm.hpp"
#include "ShaderEmulator.h"
#include "../CommonTypes.h"


namespace ComputeGradient{
using namespace glm;
using namespace GLEmulator;

static RenderingBase::Parameters params = RenderingBase::Parameters("ComputeGradientShader");

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

const int WORK_VOLUME_SIDE = VOXELS_PER_SIDE+2;
const int VOXELS_IN_GROUP = WORK_VOLUME_SIDE * WORK_VOLUME_SIDE * WORK_VOLUME_SIDE;

const float STEP_SIZE = sqrt(3.0);// relative to the size of a voxel
const int GROUP_SIZE = VOXELS_PER_TILE;



static const SizedArray<RayDescriptor> ray_descriptors;
static const SizedArray<Interval> intervals;
static const SizedArray<f16vec4> ray_texels;
static const SizedArray<f16vec4> global_integration_textures;
static const SizedArray<TileDesc> tile_descriptors;
static SizedArray<f16vec4> gradients;
static SizedArray<f16vec4> gradients_squared;
static const SizedArray<CameraData> cameraData;
static SizedArray<ivec4> debugArray;

uniform sampler2DArray* originalImage;
uniform isampler2DArray* occupancyImage;
uniform sampler3D* VoxelTextures;
uniform isampler3D* TileOccupancy;
uniform sampler3D* VoxelsMarginTexture;


uniform vec3 TSDFminCorner;
uniform ivec3 TSDFTilesCount;
uniform float tile_size;
uniform int NumCameras;
uniform int NumTiles;
uniform vec4 LearningRate;
uniform vec4 LearningRate2;
uniform int checkpointsSpacing;
uniform int ADAM;
uniform vec2 beta;

struct CubicTileProjection{
	i16vec2 pixelMinCorner;
	i16vec2 dims;
	float minDepth;
	float maxDepth;
};

shared SharedArray<CubicTileProjection, 100> allTileProjections;
const int FRUSTUM_CONE_ARRAY_LENGTH = 8*8*6;
shared SharedArray<f16vec4, FRUSTUM_CONE_ARRAY_LENGTH> frustumConeGradients;
shared SharedArray<f16vec4, VOXELS_IN_GROUP> workVolume;
shared SharedArray<f16vec4, VOXELS_IN_GROUP> workVolume2;
shared SharedArray<int, 2> partial_sums;

struct C{
	ivec3 atlas1 = ivec3(-1);
	ivec3 local1 = ivec3(-1);
	ivec3 global1 = ivec3(-1);
	vec4 v1 = vec4(0.0f/0.0f);

	ivec3 atlas2 = ivec3(-1);
	ivec3 local2 = ivec3(-1);
	ivec3 global2 = ivec3(-1);
	vec4 v2 = vec4(0.0f/0.0f);

	ivec3 atlas3 = ivec3(-1);
	ivec3 local3 = ivec3(-1);
	ivec3 global3 = ivec3(-1);
	vec4 v3 = vec4(0.0f/0.0f);
};

static std::vector<C> lectureAtlas(6*6*6);

struct Invocation : public InvocationBase{
	Invocation(Barrier* barrier) : InvocationBase(barrier){

	}
/*
	#undef assert

	void assert(bool b, int line, int camIndex){
		if(!b){
			int count = atomicAdd(debugArray[0].x, 1);
			if(count == 0){
				const uint tile_index = gl_WorkGroupID.x;
				TileDesc tileDescriptor = tile_descriptors[tile_index];
				const ivec3 tile_coords = ivec3(tileDescriptor.coords_index);

				debugArray[1].x = line;
				debugArray[1].y = int(gl_LocalInvocationIndex);
				debugArray[1].z = camIndex;
				debugArray[2] = ivec4(tile_coords, 0);

			}
		}
	}
*/
	//#define assert(EXPR) assert(EXPR, __LINE__, camIndex)


	int groupAdd(int v){

		int w = subgroupAdd(v);
		if(gl_SubgroupInvocationID == gl_SubgroupSize-1){
			partial_sums[gl_SubgroupID] = w;
		}
		memoryBarrierShared();
		barrier();

		if(gl_SubgroupID == 0){
			int v2 = gl_SubgroupInvocationID < gl_NumSubgroups ? partial_sums[gl_SubgroupInvocationID] : 0;
			v2 = subgroupAdd(v2);
			if(subgroupElect()){
				partial_sums[0] = v2;
			}
		}
		memoryBarrierShared();
		barrier();

		return partial_sums[0];
	}

	vec3 project(vec3 position, int camIndex){
		vec4 p = cameraData[camIndex].cameraTransform * vec4(position, 1.0);
		return vec3(p.x, p.y, p.z);
	}

	vec3 unproject(vec2 coords, int camIndex){
		vec4 p = cameraData[camIndex].cameraTransformInv * vec4(coords, 1.0, 0.0);
		return normalize(vec3(p.x, p.y, p.z));
	}

	vec3 unproject(vec2 coords, float depth, int camIndex){
		vec4 p = cameraData[camIndex].cameraTransformInv * vec4(coords * depth, depth, 1.0);
		return vec3(p);
	}

	vec3 getCamCenter(int camIndex){
		return vec3(cameraData[camIndex].camCenter);
	}

	vec2 getImageSize(int camIndex){
		vec4 distortion2 = cameraData[camIndex].distortion2;
		vec2 image_size = vec2(distortion2.z, distortion2.w);
		return image_size;
	}

	vec2 toPixels(vec2 NDCPixel, int camIndex) {
		vec4 K = cameraData[camIndex].K;
		vec4 distortion1 = cameraData[camIndex].distortion1;

		float fx = K.x;
		float fy = K.y;
		float cx = K.z;
		float cy = K.w;
		float k1 = distortion1.x;
		float k2 = distortion1.y;

		float r2 = NDCPixel.x * NDCPixel.x + NDCPixel.y * NDCPixel.y;
		float l = 1.0 + r2 * (k1 + r2 * k2);

		NDCPixel *= l;
		NDCPixel = vec2(fx * NDCPixel.x + cx, fy * NDCPixel.y + cy);
		return NDCPixel;
	}

	vec2 toNDC(vec2 Pixel, int camIndex) {
		vec4 K = cameraData[camIndex].K;
		vec4 distortion1 = cameraData[camIndex].distortion1;

		float fx = K.x;
		float fy = K.y;
		float cx = K.z;
		float cy = K.w;
		float k1 = distortion1.x;
		float k2 = distortion1.y;

		float x_distorted = (Pixel.x - cx) / fx;
		float y_distorted = (Pixel.y - cy) / fy;

		float rDist = sqrt(x_distorted*x_distorted + y_distorted*y_distorted);

		float r = rDist;
		for(int i=0; i<5; i++){//Newton's method
			float r2 = r*r;
			float f = r * ( 1.0 + r2 * (k1 + r2 * k2)  ) - rDist;
			float fprime = 1.0 + r2 * (3.0 * k1 + r2 * 5.0 * k2);
			r -= f / fprime;
		}

		float s = r / max(rDist, 1.0E-6f);
		vec2 coordsNDC = vec2(x_distorted, y_distorted) * s;
		return coordsNDC;

	};

	bool isInsideImageRange(vec2 pixelUV, vec2 image_size, float margin) {
		return pixelUV.x > margin && pixelUV.x < image_size.x-margin && pixelUV.y > margin && pixelUV.y < image_size.y - margin;
	}

	ivec3 unwind(int index, ivec3 s){
		int z = index / (s.x*s.y);
		int remainder = index - z * (s.x*s.y);
		int y = remainder / s.x;
		int x = remainder - y * s.x;
		return ivec3(x, y, z);
	}

	ivec3 getAtlasCoords(ivec3 atlas_size, int tile_index) {
		const ivec3 atlas_tiles_dims = atlas_size / (VOXELS_PER_SIDE+2);
		ivec3 atlas_coords = unwind(tile_index, atlas_tiles_dims);
		return atlas_coords;
	}

	void debug_margin(int k){
		const int margin_tile_index = int(gl_WorkGroupID.x);
		const ivec3 margin_tile_coords = ivec3(tile_descriptors[margin_tile_index].coords_index);
		const ivec3 margin_local_coords = unwind(k, ivec3(VOXELS_PER_SIDE+2)); // [0, 5]

		const ivec3 globalVoxelCoords = margin_tile_coords * VOXELS_PER_SIDE + margin_local_coords - ivec3(1);
		const ivec3 tile_coords = globalVoxelCoords / VOXELS_PER_SIDE;
		const ivec3 localCoords = globalVoxelCoords % VOXELS_PER_SIDE;


		int tile_index;
		if(any(lessThan(tile_coords, ivec3(0))) || any(greaterThanEqual(tile_coords, ivec3(TSDFTilesCount)))){
			tile_index = -1;
		}else{
			tile_index = texelFetch(TileOccupancy, tile_coords, 0).x;
		}


		ivec3 atlas_dims = textureSize(VoxelTextures, 0) / VOXELS_PER_SIDE;

		ivec3 atlas_coords;
		atlas_coords.z = tile_index / (atlas_dims.x * atlas_dims.y);
		int remainder = tile_index - atlas_coords.z * (atlas_dims.x * atlas_dims.y);
		atlas_coords.y = remainder / atlas_dims.x;
		atlas_coords.x = remainder - atlas_coords.y * atlas_dims.x;

		ivec3 atlasReadCoords = atlas_coords * VOXELS_PER_SIDE + localCoords;
		[[maybe_unused]]ivec3 atlasWriteCoords = atlas_coords * (VOXELS_PER_SIDE+2) + margin_local_coords;

		vec4 v = tile_index != -1 ? texelFetch(VoxelTextures, atlasReadCoords, 0) : vec4(1, 0, 0, 0);

		lectureAtlas[k].atlas3 = atlas_coords;
		lectureAtlas[k].local3 = localCoords;
		lectureAtlas[k].global3 = atlasReadCoords;
		lectureAtlas[k].v3 = v;

	}

	void readVoxels2(ivec3 center_tile) {

		if(gl_LocalInvocationIndex == 21){
			std::cout <<"po" <<std::endl;
		}

		const int tile_index = int(gl_WorkGroupID.x);
		const ivec3 atlas_size = textureSize(VoxelsMarginTexture, 0);
		const ivec3 atlasCoords = getAtlasCoords(atlas_size, tile_index);

	    //grab the tsdf values and store them in a work volume in shared memory
		for(int k=int(gl_LocalInvocationIndex); k<VOXELS_IN_GROUP; k+=GROUP_SIZE){
			const ivec3 local_coords = unwind(k, ivec3(VOXELS_PER_SIDE+2));
			const ivec3 atlasReadCoords = atlasCoords * (VOXELS_PER_SIDE+2) + local_coords;

			vec4 v = texelFetch(VoxelsMarginTexture, atlasReadCoords, 0);
			workVolume2[k] = f16vec4(v);
			lectureAtlas[k].atlas2 = atlasCoords;
			lectureAtlas[k].local2 = local_coords;
			lectureAtlas[k].global2 = atlasReadCoords;
			lectureAtlas[k].v2 = v;

			debug_margin(k);
		}
	    memoryBarrierShared();
	    barrier();
	}


	ivec3 computeTileIndexAtlasCoord(ivec3 tile_coords, int& tile_index) {
		tile_index = texelFetch(TileOccupancy, tile_coords, 0).x;

		ivec3 atlas_dims = textureSize(VoxelTextures, 0) / VOXELS_PER_SIDE;

		ivec3 atlas_coords = unwind(tile_index, atlas_dims);
		atlas_coords *= VOXELS_PER_SIDE;

		return atlas_coords;
	}

	void readVoxels(ivec3 center_tile) {
	    //grab the tsdf values and store them in a work volume in shared memory
		for(int k=int(gl_LocalInvocationIndex); k<VOXELS_IN_GROUP; k+=GROUP_SIZE){
			const int sz = WORK_VOLUME_SIDE * WORK_VOLUME_SIDE;
			const int sy = WORK_VOLUME_SIDE;
			int local_z = k / sz;
			int remainder = k - local_z * sz;
			int local_y = remainder / sy;
			int local_x = remainder - local_y * sy;

			const int tile_z = (local_z + VOXELS_PER_SIDE-1) / VOXELS_PER_SIDE;
			const int tile_y = (local_y + VOXELS_PER_SIDE-1) / VOXELS_PER_SIDE;
			const int tile_x = (local_x + VOXELS_PER_SIDE-1) / VOXELS_PER_SIDE;
			ivec3 tile = ivec3(tile_x, tile_y, tile_z) - ivec3(1);


			local_z = (local_z + VOXELS_PER_SIDE-1) % VOXELS_PER_SIDE;
			local_y = (local_y + VOXELS_PER_SIDE-1) % VOXELS_PER_SIDE;
			local_x = (local_x + VOXELS_PER_SIDE-1) % VOXELS_PER_SIDE;

			int tile_index = -1;
			ivec3 atlas_coords = computeTileIndexAtlasCoord(center_tile + tile, tile_index);

			if(tile_index == -1){
				workVolume[k] = f16vec4(1.0f, 0.0f, 0.0f, 0.0f);
			}else{
				ivec3 readCoords = atlas_coords + ivec3(local_x, local_y, local_z);
				vec4 v = texelFetch(VoxelTextures, readCoords, 0);
				workVolume[k] = f16vec4(v);
			}
			lectureAtlas[k].atlas1 = atlas_coords / 4;
			lectureAtlas[k].local1 = ivec3(local_x, local_y, local_z);
			lectureAtlas[k].global1 = atlas_coords + ivec3(local_x, local_y, local_z);
			lectureAtlas[k].v1 = vec4(workVolume[k]);

		}
	    memoryBarrierShared();
	    barrier();
	}

	vec4 getValueFromSharedMem(ivec3 offset){
		ivec3 coords = ivec3(gl_LocalInvocationID) + offset + ivec3(1);
		const int k = coords.x + WORK_VOLUME_SIDE * (coords.y + WORK_VOLUME_SIDE * coords.z);
		return workVolume[k];
	}

	ivec2 safeCeil(vec2 coords){
		return ivec2(floor(coords)) + mix(ivec2(0), ivec2(1), greaterThan(fract(coords), vec2(0.5f)));
	}

	ivec2 safeFloor(vec2 coords){
		return ivec2(floor(coords)) - mix(ivec2(0), ivec2(1), lessThan(fract(coords), vec2(0.5f)));
	}

	void computeTileProjections(ivec3 tile_coords){
		vec3 minTileCorner = TSDFminCorner + vec3(tile_coords) * tile_size;
		vec3 maxTileCorner = minTileCorner + tile_size;
		const float eps = 0.01f;
		minTileCorner += (0.5f-eps) * tile_size / VOXELS_PER_SIDE;
		maxTileCorner -= (0.5f-eps) * tile_size / VOXELS_PER_SIDE;
		const float tile_size_inv = 1.0f / tile_size;

		for(int camIndex = int(gl_LocalInvocationIndex / 8); camIndex < NumCameras; camIndex += GROUP_SIZE / 8){
			//each thread projects one vertex
			int cornerID = int(gl_LocalInvocationIndex % 8);
			int projID = int(gl_SubgroupInvocationID / 8);



			vec3 corner = vec3(
					(cornerID&1)==0?minTileCorner.x : maxTileCorner.x,
					(cornerID&2)==0?minTileCorner.y : maxTileCorner.y,
					(cornerID&4)==0?minTileCorner.z : maxTileCorner.z
				);

			vec3 proj = project(corner, camIndex);
			vec2 NDCCoords = vec2(proj) / proj.z;
			vec2 PixelCoords = toPixels(NDCCoords, camIndex);

			uvec4 ballot = subgroupPartitionNV(projID);
	       	vec3 minProj = subgroupPartitionedMinNV(vec3(PixelCoords, proj.z), ballot);
	       	vec3 maxProj = subgroupPartitionedMaxNV(vec3(PixelCoords, proj.z), ballot);

	       	if(cornerID == 0){
				const ivec2 image_size = ivec2(getImageSize(camIndex));
				ivec2 pixelMax = safeCeil(vec2(maxProj));
		       	ivec2 pixelMin = safeFloor(vec2(minProj));

				ivec2 dims = pixelMax - pixelMin + 1;

				if(any(greaterThanEqual(pixelMin, image_size)) || any(lessThan(pixelMax, ivec2(0)))){
					dims = ivec2(0);
				}
				dims = max(dims, ivec2(0));//just in case...


				float frustumStart = minProj.z;
				float frustumStop = maxProj.z;

				const float scaling = float(VOXELS_PER_SIDE) / STEP_SIZE;

				float a = frustumStart * tile_size_inv * scaling;
				float b = frustumStop * tile_size_inv * scaling;
				int stepsBeginFrustum = int(floor(a));
				int stepsEndFrustum = int(ceil(b));

				frustumStart = stepsBeginFrustum / (tile_size_inv * scaling);
				frustumStop = stepsEndFrustum / (tile_size_inv * scaling);

				const int requiredSlices = frustumStop - frustumStart + 1;
				if(dims.x * dims.y * requiredSlices > FRUSTUM_CONE_ARRAY_LENGTH){
					dims = ivec2(0);// The tile is too big, we must skip it !
				}

	       		allTileProjections[camIndex] = CubicTileProjection{i16vec2(pixelMin), i16vec2(dims), frustumStart, frustumStop};
	       	}
		}

	    memoryBarrierShared();
	    barrier();

	}

	void takeSample(int t, vec3 camCenter, vec3 ray, vec3& c, float& alpha) {
		vec4 value = vec4(1.0f, 0.0f, 0.0f, 0.0f);
		alpha = value.x;
		c = vec3(value.y, value.z, value.w);

		const vec3 p = camCenter * float(VOXELS_PER_SIDE) + ray * float(t);

		const vec3 tile_coords = floor(p / float(VOXELS_PER_SIDE));
		const ivec3 int_tile_coords = ivec3(tile_coords);
		const vec3 local_coords = p - tile_coords * float(VOXELS_PER_SIDE); // in [0, 4[

		if (
			any(lessThan(int_tile_coords, ivec3(0))) ||
			any(greaterThanEqual(int_tile_coords, TSDFTilesCount * VOXELS_PER_SIDE))
			)
		{
			return;
		}

		const int tile_index = texelFetch(TileOccupancy, int_tile_coords, 0).x;
		if(tile_index == -1){
			return;
		}

		const ivec3 atlas_size = textureSize(VoxelsMarginTexture, 0);
		const ivec3 atlasCoords = getAtlasCoords(atlas_size, tile_index);
		vec3 atlasReadCoords = vec3(atlasCoords * (VOXELS_PER_SIDE+2)) + local_coords + vec3(1.0f);
		atlasReadCoords /= vec3(atlas_size);

		value = texture(VoxelsMarginTexture, atlasReadCoords);

		alpha = value.x;
		c = vec3(value.y, value.z, value.w);
	}

	void writeSample(int k, vec3 color_diff, vec3 integrated_color,
		vec3& Cpartial, float& Tpartial, vec3& c, float& alpha)
	{
		float dalpha;
		if(alpha < 1.0E-3f){
			dalpha = 0.0f;
		}else{
			dalpha = dot(color_diff, - c * Tpartial + (integrated_color - Cpartial) / alpha);
		}
		vec3 dc = vec3(color_diff * Tpartial * (1.0f - alpha));
		vec4 derivative = vec4(dalpha, dc);
		frustumConeGradients[k] = f16vec4(derivative);
	}

	bool fillFrustum(int camIndex, ivec3 center_tile, CubicTileProjection tileProj) {
		const float tile_size_inv = 1.0f / tile_size;
		const ivec2 image_size = ivec2(getImageSize(camIndex));

		ivec2 sliceDims = ivec2(tileProj.dims);
		int sliceSize = sliceDims.x * sliceDims.y;

		int sliceLocalIndex = int(gl_LocalInvocationIndex);
		if(sliceLocalIndex >= sliceSize){
			return true;
		}

		int local_y = sliceLocalIndex / sliceDims.x;
		int local_x = sliceLocalIndex - local_y * sliceDims.x;
		ivec2 coordsPixel = ivec2(tileProj.pixelMinCorner) + ivec2(local_x, local_y);

		const float frustumStart = tileProj.minDepth;
		const float frustumStop = tileProj.maxDepth;
		const float scaling = float(VOXELS_PER_SIDE) / STEP_SIZE;
		const int frustumBegin = int(round(frustumStart * tile_size_inv * scaling));
		const int frustumEnd = int(round(frustumStop * tile_size_inv * scaling));

		const int requiredSlices = frustumEnd - frustumBegin + 1;
		assert(sliceSize * requiredSlices <= FRUSTUM_CONE_ARRAY_LENGTH);

		const vec2 coordsNDC = toNDC(vec2(coordsPixel)+vec2(0.5f), camIndex);
		vec3 ray = unproject(coordsNDC, camIndex);
		ray /= dot(cameraData[camIndex].cameraTransformInv[2], vec4(ray, 0.0));

		vec3 real_color;//ground truth
		vec3 integrated_color;//contains the complete integral
		RayDescriptor rayDescriptor;

		const int depthSliceLocalIndex = (coordsPixel.y % DEPTHMAP_TILE_WIDTH) * DEPTHMAP_TILE_WIDTH
								+ (coordsPixel.x % DEPTHMAP_TILE_WIDTH);

		bool outsideImage = any(greaterThanEqual(coordsPixel, image_size)) || any(lessThan(coordsPixel, ivec2(0)));

		// Fetch the depth tile index
		int depthTileIndex = texelFetch(occupancyImage, ivec3(coordsPixel / DEPTHMAP_TILE_WIDTH, camIndex), 0).x;
		if(outsideImage || depthTileIndex == -1){
			//no depth tile here
			real_color = vec3(0.0f, 0.0f, 0.0f);
			integrated_color = vec3(0.0f, 0.0f, 0.0f);
			rayDescriptor = RayDescriptor{-1, 0, -1, 0, coordsPixel.x / DEPTHMAP_TILE_WIDTH, coordsPixel.y / DEPTHMAP_TILE_WIDTH};
		}else{
			depthTileIndex &= ~(1 << 31);//Remove the occupancy flag on the msb
			real_color = vec3(texelFetch(originalImage, ivec3(coordsPixel, camIndex), 0));
			vec4 total_integral = vec4(global_integration_textures[depthTileIndex * DEPTHMAP_TILE_SIZE + depthSliceLocalIndex]);
			integrated_color = vec3(total_integral.y, total_integral.z, total_integral.w);
			rayDescriptor = ray_descriptors[depthTileIndex];
		}


		//////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////
		// March until meeting the last checkpoint before the start of the frustum



		int checkpoint = -1;
		int checkpoint_interval_index = -1;
		int checkpoint_sample = -1;
		int slices = 0;//number of slices encountered so far
		int t = 0;//number of steps taken so far
		int interval_index = -1;//index of the current interval
		Interval interval = Interval{int16_t(-100), int16_t(0)};//a dummy interval to init the march
		Interval checkpoint_interval = interval;
		while(t < frustumBegin){
			if(interval_index+1 < rayDescriptor.intervalsCount){
				interval = intervals[rayDescriptor.intervalsPtr + (++interval_index)];
				t = interval.start;
			}else{
				t = frustumBegin;
			}

			if(t >= frustumBegin){
				break;
			}

			int d = min(int(interval.end), frustumBegin) - t;
			slices += d;
			t += d;

			int overshoot = slices % checkpointsSpacing;//how far from the missed checkpoint
			if(overshoot < d){
				checkpoint_interval_index = interval_index;
				checkpoint_sample = t - overshoot;
				checkpoint = slices / checkpointsSpacing - 1;
				checkpoint_interval = interval;
			}

		}

		//////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////
		// March from the last checkpoint till the end of the frusutm


		interval = checkpoint_interval;
		interval_index = checkpoint_interval_index;

		//contains the integral up to the nearest previous checkpoint
		vec4 partial_transmittance_rgb = checkpoint < 0 ?
			vec4(1, 0, 0, 0) :
			vec4(ray_texels[rayDescriptor.texelsPtr * DEPTHMAP_TILE_SIZE + checkpoint * DEPTHMAP_TILE_SIZE + depthSliceLocalIndex]);



		const vec3 color_diff = integrated_color - real_color;
		float Tpartial = partial_transmittance_rgb.x;
		vec3  Cpartial = vec3(
				partial_transmittance_rgb.y,
				partial_transmittance_rgb.z,
				partial_transmittance_rgb.w);
		float alpha = 1.0f;
		vec3 c = vec3(0.0f);


		const vec3 camCenter = (getCamCenter(camIndex) - TSDFminCorner) * tile_size_inv;
		ray *= STEP_SIZE;


		if(checkpoint < 0){
			if(rayDescriptor.intervalsCount == 0){
				t = frustumBegin;
			}else{
				interval = intervals[rayDescriptor.intervalsPtr + (++interval_index)];
				t = interval.start;
			}
		}else{
			t = checkpoint_sample;
		}

		if(t <= interval.start){
			t = min(frustumBegin, int(interval.start));
		}

		const float TransmittanceThreshold = 1.0E-3f;
		while(t < frustumBegin){
			if(t < interval.end){
				takeSample(t, camCenter, ray, c, alpha);
				Cpartial += c * (Tpartial * (1.0f - alpha));
				if(Tpartial <= TransmittanceThreshold){
					Tpartial = 0.0f;
				}else{
					Tpartial *= alpha;
				}
				alpha = 1.0f;
				c = vec3(0.0f);
				t++;
			}else if(interval_index+1 < rayDescriptor.intervalsCount){
				//switch to the next interval
				interval = intervals[rayDescriptor.intervalsPtr + (++interval_index)];
				t = min(frustumBegin, int(interval.start));//skip to the begining of the interval
			}else{
				t = frustumBegin;//skip to the begining of the frustum
			}
		}
		while(t <= frustumEnd){
			if(Tpartial > TransmittanceThreshold && t >= interval.start){
				if(t < interval.end){
					takeSample(t, camCenter, ray, c, alpha);
					Cpartial += c * (Tpartial * (1.0f - alpha));
				}else if(interval_index+1 < rayDescriptor.intervalsCount){
					//switch to the next interval
					interval = intervals[rayDescriptor.intervalsPtr + (++interval_index)];
				}
			}
			writeSample((t - frustumBegin) * sliceSize + sliceLocalIndex, color_diff, integrated_color, Cpartial, Tpartial, c, alpha);
			if(Tpartial <= TransmittanceThreshold){
				Tpartial = 0.0f;
			}else{
				Tpartial *= alpha;
			}
			alpha = 1.0f;
			c = vec3(0.0f);
			t++;
		}

		return Tpartial <= TransmittanceThreshold;
	}

	vec4 lerpGradients(ivec3 whole_coords, vec3 fracCoords, CubicTileProjection tileProj, int camIndex) {
		ivec2 sliceDims = ivec2(tileProj.dims);
		int sliceSize = sliceDims.x * sliceDims.y;

		int maxSlices = FRUSTUM_CONE_ARRAY_LENGTH / sliceSize;
		const float tile_size_inv = 1.0f / tile_size;
		const float frustumStart = tileProj.minDepth;
		const float frustumStop = tileProj.maxDepth;
		const float scaling = float(VOXELS_PER_SIDE) / STEP_SIZE;
		const int stepsBeginFrustum = int(round(frustumStart * tile_size_inv * scaling));
		const int stepsEndFrustum = int(round(frustumStop * tile_size_inv * scaling));

		const int requiredSlices = stepsEndFrustum - stepsBeginFrustum + 1;

		assert(requiredSlices <= maxSlices);
		assert(whole_coords.z+1 < requiredSlices);

		int sliceLocalIndex = whole_coords.x + whole_coords.y * sliceDims.x;
		const int slice = whole_coords.z;
		const int baseIndex = slice * sliceSize + sliceLocalIndex;

		vec4 gradient = vec4(0.0f);
		{

			vec4 v = vec4(frustumConeGradients[baseIndex + 0]);
			vec4 vx = vec4(frustumConeGradients[baseIndex + 1]);
			v = mix(v, vx, fracCoords.x);
			vec4 vy = vec4(frustumConeGradients[baseIndex + sliceDims.x]);
			vec4 vxy = vec4(frustumConeGradients[baseIndex + sliceDims.x + 1]);
			vy = mix(vy, vxy, fracCoords.x);
			v = mix(v, vy, fracCoords.y);

			vec4 vz = vec4(frustumConeGradients[baseIndex + sliceSize + 0]);
			vec4 vxz = vec4(frustumConeGradients[baseIndex + sliceSize + 1]);
			vz = mix(vz, vxz, fracCoords.x);
			vec4 vyz = vec4(frustumConeGradients[baseIndex + sliceSize + sliceDims.x]);
			vec4 vxyz = vec4(frustumConeGradients[baseIndex + sliceSize + sliceDims.x + 1]);
			vyz = mix(vyz, vxyz, fracCoords.x);
			vz = mix(vz, vyz, fracCoords.y);

			gradient = mix(v, vz, fracCoords.z);
		}

		return gradient;
	}

	bool isCameraActive(ivec4 activeCameras, int camIndex){
		return (activeCameras[camIndex/32] & (1<<(camIndex%32))) != 0;
	}
	void setCameraInactive(ivec4& activeCameras, int camIndex){
		activeCameras[camIndex/32] &= ~(1<<(camIndex%32));
	}

	void main_shader() override{
		//One thread per voxel

		const uint tile_index = gl_WorkGroupID.x;
		TileDesc tileDescriptor = tile_descriptors[tile_index];
		const ivec3 tile_coords = ivec3(tileDescriptor.coords_index);
		ivec4 activeCameras = tileDescriptor.activeCameras;

		computeTileProjections(tile_coords);

	    const float voxel_size = tile_size / VOXELS_PER_SIDE;
	    const vec3 voxelCenter = TSDFminCorner + vec3(tile_coords) * tile_size + (vec3(gl_LocalInvocationID)+0.5f) * voxel_size;

		vec4 derivative = vec4(0.0f);// The total derivative for that voxel

		for(int camIndex=0; camIndex < NumCameras; camIndex++) {
			if(camIndex != 0){
				//break;
			}

			if(!isCameraActive(activeCameras, camIndex)){
	   			continue;
	   		}

			CubicTileProjection tileProj = allTileProjections[camIndex];
			ivec2 sliceDims = ivec2(tileProj.dims);
			int sliceSize = sliceDims.x * sliceDims.y;

			if(sliceSize == 0){
				continue;//the tile frustum is entirely outside of the view frustum of the camera
			}

		    memoryBarrierShared();
		    barrier();

			[[maybe_unused]]bool canSkip = fillFrustum(camIndex, tile_coords, tileProj);
			//if(groupAdd(int(!canSkip)) == 0){
				//if(gl_LocalInvocationIndex == 0){
				//	setCameraInactive(activeCameras, camIndex);
				//}
				//continue;
			//}

		    memoryBarrierShared();
		    barrier();


			const vec3 voxelCenterProj = project(voxelCenter, camIndex);
			const float voxelDepth = voxelCenterProj.z;
			const vec2 coordsNDC = vec2(voxelCenterProj) / voxelDepth;
			const vec2 coordsPixel = toPixels(coordsNDC, camIndex);

			const float tile_size_inv = 1.0f / tile_size;
			const float frustumStart = tileProj.minDepth;
			const float frustumStop = tileProj.maxDepth;
			const float scaling = float(VOXELS_PER_SIDE) / STEP_SIZE;
			const int stepsBeginFrustum = int(round(frustumStart * tile_size_inv * scaling));
			const int stepsEndFrustum = int(round(frustumStop * tile_size_inv * scaling));
			[[maybe_unused]]const int validSlices = (stepsEndFrustum - stepsBeginFrustum) + 1;

			vec3 frustumCoords = vec3(coordsPixel - vec2(tileProj.pixelMinCorner) - vec2(0.5f), (voxelDepth - tileProj.minDepth) * tile_size_inv * scaling );
			vec3 fracCoords = fract(frustumCoords);
			ivec3 whole_coords = ivec3(frustumCoords);

			//if(any(lessThan(frustumCoords, vec3(0))) || any(greaterThanEqual(whole_coords, ivec3(ivec2(tileProj.dims)-ivec2(1), validSlices)))){
			//	assert(false);//should not happen
			//}

			vec4 gradient = lerpGradients(whole_coords, fracCoords, tileProj, camIndex);
			derivative += gradient;

		}


		vec4 gradient = vec4(0.0f);
		gradient.x = derivative.x * (LearningRate.x / NumCameras);
		gradient.y = derivative.y * (LearningRate.y / NumCameras);
		gradient.z = derivative.z * (LearningRate.y / NumCameras);
		gradient.w = derivative.w * (LearningRate.y / NumCameras);


		readVoxels(tile_coords);
		readVoxels2(tile_coords);
		const vec4 voxel_values = getValueFromSharedMem(ivec3(0));

		vec4 dpx = getValueFromSharedMem(ivec3(1, 0, 0));
		vec4 dmx = getValueFromSharedMem(ivec3(-1, 0, 0));
		vec4 Fxx = dpx + dmx - voxel_values * 2.0f;

		vec4 dpy = getValueFromSharedMem(ivec3(0, 1, 0));
		vec4 dmy = getValueFromSharedMem(ivec3(0, -1, 0));
		vec4 Fyy = dpy + dmy - voxel_values * 2.0f;

		vec4 dpz = getValueFromSharedMem(ivec3(0, 0, 1));
		vec4 dmz = getValueFromSharedMem(ivec3(0, 0, -1));
		vec4 Fzz = dpz + dmz - voxel_values * 2.0f;

		gradient.x += (1.0f-2.0f*voxel_values.x) * LearningRate.z;
		//gradient.x += (1.0-maxTpartial)*0.001;

		vec4 Laplacian = Fxx + Fyy + Fzz;
		gradient -= Laplacian * vec4(LearningRate2.x, vec3(LearningRate2.y));



		const uint gradient_index = VOXELS_PER_TILE * tile_index +
							gl_LocalInvocationID.x +
							gl_LocalInvocationID.y * VOXELS_PER_SIDE +
							gl_LocalInvocationID.z * VOXELS_PER_SIDE * VOXELS_PER_SIDE;


		if(ADAM == 0){
			gradients[gradient_index] = f16vec4(gradient);
		}else{
			const float beta1 = beta.x;
			const float beta2 = beta.y;
			gradients[gradient_index] = f16vec4(mix(gradient, vec4(gradients[gradient_index]), beta1));
			gradients_squared[gradient_index] = f16vec4(mix(gradient*gradient, vec4(gradients_squared[gradient_index]), beta2));
		}

		//if(any(isnan(gradient)) || any(isinf(gradient))){
		//	int camIndex = -1;
		//	assert(false);
		//}

		// Deactivate unused cameras
		//if(gl_LocalInvocationIndex == 0){
		//	tile_descriptors[tile_index].activeCameras = activeCameras;
		//}

	}



};

class ComputeGradientShader : protected ShaderBase{
public:

	ComputeGradientShader() : ShaderBase(VOXELS_PER_SIDE, VOXELS_PER_SIDE, VOXELS_PER_SIDE)
	{

	};
	virtual ~ComputeGradientShader(){

	};


	void dispatchCompute(int gx, int gy, int gz) override {
		Barrier group_barrier(local_size_x * local_size_y * local_size_z);

		std::vector<std::unique_ptr<InvocationBase>> group;
		for(int e=0; e<local_size_x * local_size_y * local_size_z; e++){
			group.emplace_back(std::make_unique<Invocation>(&group_barrier));
		}

		dispatchComputeInternal(gx, gy, gz, group);
	}

	void runGroup(ivec3 gl_WorkGroupID, std::vector<std::unique_ptr<InvocationBase>>& group) override {

//		if(ivec3(tile_descriptors[gl_WorkGroupID.x].coords_index) != ivec3(params.getVec3("DDATest#Tile"))){
//			return;
//		}

		if(ivec3(tile_descriptors[gl_WorkGroupID.x].coords_index) != ivec3(199, 223, 12)){
			return;
		}

		std::cout <<"Computing tile: " << glm::to_string(ivec3(tile_descriptors[gl_WorkGroupID.x].coords_index)) <<std::endl;

		std::vector<std::thread> threads;
		for (std::unique_ptr<InvocationBase>& e : group) {
			Invocation& invoc = dynamic_cast<Invocation&>(*e);
			threads.emplace_back([&]{
				invoc.main_shader();
			});
		}
		for (std::thread& thread : threads) {
			thread.join();
		}

	}

	void setUniforms(
			GL_PTR& ray_descriptorsPTR,
			GL_PTR& intervalsPTR,
			GL_PTR& ray_texelsPTR,
			GL_PTR& global_integration_texturesPTR,
			GL_PTR& tile_descriptorsPTR,
			GL_PTR& gradientsPTR,
			GL_PTR& gradients_squaredPTR,
			GL_PTR& debugArrayPTR,
			GL_PTR& cameraDataPTR,
			sampler2DArray& originalImageSampler,
			isampler2DArray& occupancyImageSampler,
			sampler3D& VoxelTexturesSampler,
			isampler3D& TileOccupancySampler,
			sampler3D& _VoxelsMarginTexture,
			vec3 _TSDFminCorner,
			vec3 _TSDFmaxCorner,
			ivec3 _TSDFTilesCount,
			float _tile_size,
			int _NumCameras,
			int _NumTiles,
			vec4 _LearningRate,
			vec4 _LearningRate2,
			int _checkpointsSpacing){

		ray_descriptors.set((RayDescriptor*)ray_descriptorsPTR.data, ray_descriptorsPTR.size);
		intervals.set((Interval*)intervalsPTR.data, intervalsPTR.size);
		ray_texels.set((f16vec4*)ray_texelsPTR.data, ray_texelsPTR.size);
		global_integration_textures.set((f16vec4*)global_integration_texturesPTR.data, global_integration_texturesPTR.size);
		tile_descriptors.set((TileDesc*)tile_descriptorsPTR.data, tile_descriptorsPTR.size);
		gradients.set((f16vec4*)gradientsPTR.data, gradientsPTR.size);
		gradients_squared.set((f16vec4*)gradients_squaredPTR.data, gradients_squaredPTR.size);
		cameraData.set((CameraData*)cameraDataPTR.data, cameraDataPTR.size);

		originalImage = &originalImageSampler;
		occupancyImage = &occupancyImageSampler;
		VoxelTextures = &VoxelTexturesSampler;
		TileOccupancy = &TileOccupancySampler;
		VoxelsMarginTexture = &_VoxelsMarginTexture;

		TSDFminCorner = _TSDFminCorner;
		TSDFTilesCount = _TSDFTilesCount;
		tile_size = _tile_size;
		NumCameras = _NumCameras;
		NumTiles = _NumTiles;
		LearningRate = _LearningRate;
		LearningRate2 = _LearningRate2;
		checkpointsSpacing = _checkpointsSpacing;
	}



};



}

#endif /* SEQUENCE_EMULATOR_COMPUTEGRADIENTSHADER_H_ */
