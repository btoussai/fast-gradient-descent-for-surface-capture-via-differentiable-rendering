/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * BackgroundFilter.h
 *
 *  Created on: 7 oct. 2021
 *      Author: Briac
 */

#ifndef SEQUENCE_BACKGROUNDS_H_
#define SEQUENCE_BACKGROUNDS_H_

#include <functional>

#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Parameters.h"
#include "../rendering_base/Camera.h"
#include "CommonTypes.h"

#include "AsyncLoader.h"
#include "Texture2DArray.h"
#include "Depthmap.h"
#include "DownScale.h"

class Backgrounds {
public:
	Backgrounds(const std::vector<ImageLOD>& imageLODs);
	virtual ~Backgrounds();

	bool updateGUI(const RenderingBase::Camera& camera, bool& generateFilters, bool& loadMasks);
	bool displayStatusMsg();

	void loadFromFile(const std::string& backgroundsDir, AsyncLoader<ImageLoadingTask>& imageLoader, const DownScale& downscale);
	void computeFromSequence(const std::string& saveDir, const std::string& sequenceDir, AsyncLoader<ImageLoadingTask>& imageLoader, const DownScale& downscale);

	void applySegmentation(AllDepthmaps& depthmaps, int viewpoint);

	void recombineSilhouettes(Texture2DArray& allviewpoints);

	void editMask(int viewpoint, glm::vec2 cursorNDCPos, float brushRadius, bool remove);

	const Texture2DArray& getImages(int imagePyramidLevel) const {
		return background_images.at(imagePyramidLevel);
	}
	
	std::unique_ptr<Texture2DArray>& getSilhouettes() {
		return silhouettes;
	}

	const std::string& getBackgroundsDir() const{
		return params.getString("Window#BackgroundsDir");
	}

	/**
	 * Initilizes the texture storage
	 */
	void setNbCameras(int nbCameras, int width, int height, bool hasSilhouettes) {
		this->nbCameras = nbCameras;
		for(int imagePyramidLevel=0; imagePyramidLevel < imagePyramidLevels; imagePyramidLevel++){
			const int s = 1 << imagePyramidLevel;
			background_images.at(imagePyramidLevel).init(width / s, height / s, nbCameras, GL_RGBA8);
		}
		if(hasSilhouettes){
			silhouettes = std::make_unique<Texture2DArray>();
			silhouettes->init(width, height, nbCameras, GL_R8);
		}else{
			silhouettes.reset();
		}

		moddifiedImages = std::vector<bool>(nbCameras, false);
	}

	bool Loaded() const{
		return loaded;
	}

	int getNumCameras() const{
		return nbCameras;
	}

	size_t memUsage(bool showGui);

private:
	Backgrounds(const Backgrounds&) = delete;
	Backgrounds(Backgrounds&&) = delete;
	Backgrounds& operator=(const Backgrounds&) = delete;
	Backgrounds& operator=(Backgrounds&&) = delete;

	void openDialog();

	const std::vector<ImageLOD>& imageLODs;
	RenderingBase::Parameters params = RenderingBase::Parameters("BackgroundFilter");

	int processedImages = 0;
	int nbCameras = 0;
	std::vector<Texture2DArray> background_images = std::vector<Texture2DArray>(imagePyramidLevels);
	std::unique_ptr<Texture2DArray> silhouettes;
	std::vector<RenderingBase::QueryBuffer> timers;
	bool loaded = false;

	RenderingBase::Shader backgroundEditShader = RenderingBase::Shader("background/background_edit.cp");
	std::vector<bool> moddifiedImages;

	RenderingBase::Shader backgroundApplyShader = RenderingBase::Shader("background/background_apply.cp");
	RenderingBase::Shader backgroundSmoothShader = RenderingBase::Shader("background/background_smooth.cp");
	RenderingBase::Shader backgroundDownscaleShader = RenderingBase::Shader("background/background_downscale.cp");
	RenderingBase::Shader backgroundUpscaleShader = RenderingBase::Shader("background/background_upscale.cp");
	RenderingBase::Shader backgroundCopyShader = RenderingBase::Shader("background/background_copy.cp");
	RenderingBase::Shader writeOccupancyShader = RenderingBase::Shader("background/write2DOccupancyTexture.cp");

	RenderingBase::Shader recombineSilhouettesShader = RenderingBase::Shader("background/recombineSilhouettes.cp");

	const DownScale* downscale = nullptr;
};

#endif /* SEQUENCE_BACKGROUNDS_H_ */
