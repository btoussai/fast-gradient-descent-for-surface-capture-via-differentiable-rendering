/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Evaluation.h
 *
 *  Created on: 16 mai 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_EVALUATIONNERF_H_
#define SEQUENCE_EVALUATIONNERF_H_

#include "../rendering_base/Parameters.h"
#include "../rendering_base/Camera.h"

#include "SparseGrid.h"
#include "DifferentialRendering.h"
#include "Backgrounds.h"

#include <functional>

class EvaluationNERF {
public:
	EvaluationNERF();
	virtual ~EvaluationNERF();

	void updateGUIs(RenderingBase::Camera& camera,
			Backgrounds& backgrounds,
			SparseGrid& sparseGrid,
			DifferentialRendering& differentialRendering,
			AsyncLoader<ImageLoadingTask>& imageLoader);

	void eval(RenderingBase::Camera& camera,
			Backgrounds& backgrounds,
			SparseGrid& sparseGrid,
			DifferentialRendering& differentialRendering,
			AsyncLoader<ImageLoadingTask>& imageLoader);
private:
	RenderingBase::Parameters params = RenderingBase::Parameters("EvaluationNERF");
};

#endif /* SEQUENCE_EVALUATIONNERF_H_ */
