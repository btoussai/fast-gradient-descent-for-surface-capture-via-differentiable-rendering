/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * DifferentialRendering.h
 *
 *  Created on: 28 janv. 2022
 *      Author: Briac
 */

#ifndef SRC_SEQUENCE_DIFFERENTIALRENDERING_H_
#define SRC_SEQUENCE_DIFFERENTIALRENDERING_H_

#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Parameters.h"
#include "Backgrounds.h"
#include "Depthmap.h"
#include "CommonTypes.h"
#include "BindlessTexture2DArray.h"

#include <chrono>
#include "SparseGrid.h"

class DifferentialRendering {
public:
	DifferentialRendering(const std::vector<ImageLOD>& imageLODs, SparseGrid &sparseGrid);
	virtual ~DifferentialRendering();

	bool displayStatusMsg();
	void manageActiveCameras(SparseGrid &sparseGrid);
	void updateGUI(RenderingBase::Camera &camera, Backgrounds& backgrounds, SparseGrid& sparseGrid);

	void step(Backgrounds& backgrounds, SparseGrid &sparseGrid, bool autoCoarseToFine,
			const std::function<void()>& exportMesh);

	RenderingBase::VBO& getGlobalIntegrationTextureBlock() {
		return GlobalIntegrationTextureBlock;
	}

	RenderingBase::VBO& getRayDescriptorsBlock() {
		return RayDescriptorsBlock;
	}

	RenderingBase::VBO& getIntervalsBlock() {
		return IntervalsBlock;
	}

	BindlessTexture2DArray& getRayTexels() {
		return RayTexels;
	}

	int getTotalIterations() const{
		int s = 0;
		for(int i=0; i<(int)iterations.size(); i++){
			s += iterations.at(i);
		}
		return s;
	}

	const std::vector<int>& getIterations() const{
		return iterations;
	}

	glm::ivec4 getMaxIterations() const{
		glm::ivec4 iterationsPerLOD = params.getiVec4("IterationsPerLod");
		return iterationsPerLOD;
	}

	glm::ivec4 getActiveCameras() const{
		return params.getiVec4("activeCameras");
	}

	void setFinished(bool finished) {
		this->finished = finished;
	}

	bool hasFinished() const{
		return finished;
	}

	float getElapsedTime() const{
		return elapsedTime.count() / 1000.0f;
	}

	void resetElapsedTime(){
		elapsedTime = std::chrono::milliseconds(0);
	}

	void renderRayTexels();

	void resetExposureCorrection(int nbCameras);
	void allocateIntervals(Backgrounds& backgrounds, SparseGrid& sparseGrid, bool resetExposure = true);
	void rayIntegration(Backgrounds& backgrounds, SparseGrid& sparseGrid);
	void rayDerivatives(Backgrounds& backgrounds, SparseGrid& sparseGrid);
	void voxelsDerivatives(Backgrounds& backgrounds, SparseGrid& sparseGrid);
	void applyGradient(SparseGrid& sparseGrid);
	void cullUnusedCameras(Backgrounds& backgrounds, SparseGrid& sparseGrid);
	void writeAccumulatedTransmittance(Backgrounds& backgrounds, SparseGrid& sparseGrid);
	void computeTileFlags(SparseGrid& sparseGrid);
	void cullUnusedTiles(Backgrounds& backgrounds, SparseGrid& sparseGrid);
	void nextLevelOfDetail(Backgrounds& backgrounds, SparseGrid& sparseGrid);

	size_t memUsage(bool showGui);
	void printStats();
private:

	RenderingBase::Parameters params = RenderingBase::Parameters("DifferentialRendering");
	const std::vector<ImageLOD>& imageLODs;

	//Init
	RenderingBase::Shader countIntervalsShader = RenderingBase::Shader("differentialRendering/init/countIntervals.cp");

	//Optimize
	RenderingBase::Shader rayIntegrationShader = RenderingBase::Shader("differentialRendering/rayIntegration.cp");
	RenderingBase::Shader rayDerivativesShader = RenderingBase::Shader("differentialRendering/rayDerivatives.cp");
	RenderingBase::Shader voxelDerivativesShader = RenderingBase::Shader("differentialRendering/voxelDerivatives.cp");
	RenderingBase::Shader applyGradientShader = RenderingBase::Shader("differentialRendering/applyGradient.cp");

	//Coarse to fine
	RenderingBase::Shader writeAccumulatedTransmittanceShader = RenderingBase::Shader("differentialRendering/coarse_to_fine/writeAccumulatedTransmittance.cp");
	RenderingBase::Shader cullUnusedCamerasShader = RenderingBase::Shader("differentialRendering/coarse_to_fine/cullUnusedCameras.cp");
	RenderingBase::Shader computeTileFlagsShader = RenderingBase::Shader("differentialRendering/coarse_to_fine/computeTileFlags.cp");
	RenderingBase::Shader cullUnusedTilesShader = RenderingBase::Shader("differentialRendering/coarse_to_fine/cullUnusedTiles.cp");
	RenderingBase::Shader moveVoxelsTextureShader = RenderingBase::Shader("differentialRendering/coarse_to_fine/moveVoxelsTexture.cp");
	RenderingBase::Shader upscaleOccupancyShader = RenderingBase::Shader("differentialRendering/coarse_to_fine/upscaleOccupancy.cp");
	RenderingBase::Shader upscaleVoxelsShader = RenderingBase::Shader("differentialRendering/coarse_to_fine/upscaleVoxels.cp");
	RenderingBase::Shader removeOldTileIndicesShader = RenderingBase::Shader("differentialRendering/coarse_to_fine/removeOldTileIndices.cp");


	//Visualize
	RenderingBase::Shader showRayTexelsShader = RenderingBase::Shader("copyBindlessTexture/copyBindlessTexture.vs",
				"copyBindlessTexture/copyBindlessTexture.fs");

	BindlessTexture2DArray RayTexels;
	RenderingBase::VBO GlobalIntegrationTextureBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);

	std::vector<int> iterations;
	bool finished = false;
	std::chrono::system_clock::time_point startTime;
	std::chrono::system_clock::time_point endTime;
	std::chrono::milliseconds elapsedTime = std::chrono::milliseconds(0);

	int totalIntervals = 0;
	int totalIntervalLengths = 0;
	int64_t totalRaySamples = 0;
	RenderingBase::VBO RayDescriptorsBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
	RenderingBase::VBO IntervalsBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);

	RenderingBase::VBO exposureGradVBO = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);

	std::vector<RenderingBase::QueryBuffer> timers;

	std::vector<int> tileFlagsVec;

	std::vector<double> all_losses;
	std::vector<double> pixel_errors;
	std::vector<double> elapsed_times;
	std::vector<Stats> stats;

	double lossDiff = 0;

	struct ExposureCorrection{
		double gain;
		double bias;
		glm::dvec2 grad;
		glm::dvec2 grad2;
		glm::dvec2 newGrad;
	};

	double exposureRegLoss = 0.0;
	std::vector<ExposureCorrection> exposureCorrections;
	void manageExposureCorrection();
	void updateExposureCorrection(int nbCameras, const std::vector<glm::dvec2>& exposureGrads);
	bool evaluateConvergence(SparseGrid& sparseGrid);
	void error_plot();

};

#endif /* SRC_SEQUENCE_DIFFERENTIALRENDERING_H_ */
