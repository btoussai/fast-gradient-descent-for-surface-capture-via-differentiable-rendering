/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Depthmap.cpp
 *
 *  Created on: 11 déc. 2021
 *      Author: Briac
 */

#include "Depthmap.h"
#include <mutex>

#include "FastBVH.h"

#include "../glm/gtx/string_cast.hpp"

static std::mutex printMtx;

Depthmap::Depthmap(BorderData borderData, PixelData pixelData) : borderData(borderData), pixelData(pixelData) {

}

Depthmap::~Depthmap() {
	// TODO Auto-generated destructor stub
}


Depthmap::Depthmap(Depthmap&& other) : borderData(other.borderData), pixelData(other.pixelData) {

}

static std::function<AABB(const segment&)> makeBox = [](const segment& s){
	auto box = AABB{glm::min(s.a, s.b), glm::max(s.a, s.b)};

	const float eps = 1.0E-4;
	box.minCorner -= glm::vec2(eps);
	box.maxCorner += glm::vec2(eps);

	return box;
};

void Depthmap::buildBorderBVH(AsyncLoader<ImageLoadingTask> &imageLoader, const CameraInfo& camData, const std::vector<BorderPoint>& borders) {

	fastBVH = nullptr;//We must delete it on the main thread
	finished_bvh = false;
	std::function<void()> f = [&](){

//		auto t0 = std::chrono::system_clock::now();

		std::unique_lock < std::mutex > lock(mutex);

		std::vector<segment> segments;
		for (int i = 0; i < this->borderData.BorderPixels; i++) {
			glm::vec4 uv_viewpoint_type = borders.at(i + this->borderData.BorderPixelOffset).uv_viewpoint_type;

			glm::vec2 c = glm::vec2(uv_viewpoint_type.x, uv_viewpoint_type.y);

			const float d = 1.0f/4.0f;

			glm::vec2 c0 = c + glm::vec2(-0.5, +0.5) + glm::vec2(0, -d);
			glm::vec2 c1 = c + glm::vec2(-0.5, -0.5) + glm::vec2(0, +d);
			glm::vec2 c2 = c + glm::vec2(-0.5, -0.5) + glm::vec2(+d, 0);
			glm::vec2 c3 = c + glm::vec2(+0.5, -0.5) + glm::vec2(-d, 0);
			glm::vec2 c4 = c + glm::vec2(+0.5, -0.5) + glm::vec2(0, +d);
			glm::vec2 c5 = c + glm::vec2(+0.5, +0.5) + glm::vec2(0, -d);
			glm::vec2 c6 = c + glm::vec2(+0.5, +0.5) + glm::vec2(-d, 0);
			glm::vec2 c7 = c + glm::vec2(-0.5, +0.5) + glm::vec2(+d, 0);


			glm::vec2 c8  = c + glm::vec2(-0.5, +0.5) + glm::vec2(+d, -d);
			glm::vec2 c9  = c + glm::vec2(-0.5, -0.5) + glm::vec2(+d, +d);
			glm::vec2 c10 = c + glm::vec2(+0.5, -0.5) + glm::vec2(-d, +d);
			glm::vec2 c11 = c + glm::vec2(+0.5, +0.5) + glm::vec2(-d, -d);

//			bool borderPoint = (int)c.x == 0 || (int)c.x == camData.width-1 || (int)c.y == 0 || (int)c.y == camData.height-1;

			const int code = (int) uv_viewpoint_type.w;

			enum side{
				L=0, R, T, B, TL, TR, BR, BL
			};

			const auto& air = [&](side s){
				return (code & (1<<s)) != 0;
			};

			//Horizontal segments
			if(air(T) && !air(L) && !air(R)){
				segments.push_back( { c1, c4 });
			}
			if(air(B) && !air(L) && !air(R)){
				segments.push_back( { c5, c0 });
			}
			//Vertical segments
			if(air(L) && !air(T) && !air(B)){
				segments.push_back( { c7, c2 });
			}
			if(air(R) && !air(T) && !air(B)){
				segments.push_back( { c3, c6 });
			}

			//Small Corner segments
			if(air(TL) && !air(T) && !air(L)){
				segments.push_back( { c1, c2 });
			}
			if(air(TR) && !air(T) && !air(R)){
				segments.push_back( { c3, c4 });
			}
			if(air(BR) && !air(B) && !air(R)){
				segments.push_back( { c5, c6 });
			}
			if(air(BL) && !air(B) && !air(L)){
				segments.push_back( { c7, c0 });
			}

			//Long Corner segments
			if(air(T) && air(L) && !air(R) && !air(B)){
				segments.push_back( { c7, c4 });
//				segments.push_back( { c7, c9 });
//				segments.push_back( { c9, c4 });
			}
			if(!air(T) && air(L) && !air(R) && air(B)){
				segments.push_back( { c5, c2 });
//				segments.push_back( { c5, c8 });
//				segments.push_back( { c8, c2 });
			}
			if(air(T) && !air(L) && air(R) && !air(B)){
				segments.push_back( { c1, c6 });
//				segments.push_back( { c1, c10 });
//				segments.push_back( { c10, c6 });
			}
			if(!air(T) && !air(L) && air(R) && air(B)){
				segments.push_back( { c3, c0 });
//				segments.push_back( { c3, c11 });
//				segments.push_back( { c11, c0 });
			}

			//3 segments cases
			if(!air(L) && air(R) && air(T) && air(B)){
				segments.push_back( { c1, c10 });
				segments.push_back( { c10, c11 });
				segments.push_back( { c11, c0 });
			}
			if(air(L) && !air(R) && air(T) && air(B)){
				segments.push_back( { c5, c8 });
				segments.push_back( { c8, c9 });
				segments.push_back( { c9, c4 });
			}
			if(air(L) && air(R) && !air(T) && air(B)){
				segments.push_back( { c3, c11 });
				segments.push_back( { c11, c8 });
				segments.push_back( { c8, c2 });
			}
			if(air(L) && air(R) && air(T) && !air(B)){
				segments.push_back( { c7, c9 });
				segments.push_back( { c9, c10 });
				segments.push_back( { c10, c6 });
			}
		}


		for(segment& segment : segments){
			segment.a = camData.toNDC(segment.a);
			segment.b = camData.toNDC(segment.b);
		}

		fastBVH = std::make_unique<FastBVH>(segments, makeBox);

//		auto t1 = std::chrono::system_clock::now();
//		auto time = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);

//		{
//			std::lock_guard<std::mutex> lock(printMtx);
//			std::cout <<"Segments: " <<fastBVH->getSegments().size() <<std::endl;
//			std::cout <<"Nodes: " <<fastBVH->getNodes().size() <<std::endl;
//			std::cout << "Built border in " << time.count() << "ms for view " << camData.ID-1 << std::endl;
//		}

		finished_bvh = true;
	};

//	f();
	imageLoader.exec(std::move(f));
}

void Depthmap::waitForBVH(){
	std::unique_lock < std::mutex > lock(mutex);
	const auto timeout = std::chrono::milliseconds(1);

	bool success = false;
	while(!success){
		success = condition.wait_for(lock, timeout, [this]() {
			return finished_bvh == true;
		});
	}
}

bool AllDepthmaps::buildVAOs() {


	const int* ptr = (const int*)glMapNamedBuffer(AtomicBlock.getID(), GL_READ_ONLY);
	TotalForegroundPixels = ptr[0];
	TotalBorderPixels = ptr[1];
	TotalForegroundPixelTiles = TotalForegroundPixels / DEPTHMAP_TILE_SIZE;
	bool overflow = TotalForegroundPixels > maxExpectedForegroundPixels || TotalBorderPixels > maxExpectedBorderPixels;


	int currentForegroundPixelsOffset = 0;
	int currentBorderPixelsOffset = 0;
	for(int i=0; i<(int)depthmaps.size(); i++){
		int foregroundPixels = std::min(ptr[2*(i+1)+0], maxExpectedForegroundPixels);
		int borderPixels = std::min(ptr[2*(i+1)+1], maxExpectedBorderPixels);

		auto& depthmap = depthmaps.at(i);
		auto& pixelData = depthmap.pixelData;
		auto& borderData = depthmap.borderData;


		pixelData.ForegroundPixelsOffset = currentForegroundPixelsOffset;
		pixelData.ForegroundPixels = std::max(foregroundPixels - currentForegroundPixelsOffset, 0);
		pixelData.DepthTilesOffset = pixelData.ForegroundPixelsOffset / DEPTHMAP_TILE_SIZE;
		pixelData.DepthTiles = pixelData.ForegroundPixels / DEPTHMAP_TILE_SIZE;

		borderData.BorderPixelOffset = currentBorderPixelsOffset;
		borderData.BorderPixels = std::max(borderPixels - currentBorderPixelsOffset, 0);



		currentBorderPixelsOffset = borderPixels;
		currentForegroundPixelsOffset = foregroundPixels;

	}
	glUnmapNamedBuffer(AtomicBlock.getID());


	std::vector<BorderPoint> borders;
	borders.reserve(TotalBorderPixels);
	for (int i = 0; i < std::min(TotalBorderPixels, maxExpectedBorderPixels); i++) {
		borders.push_back(((BorderPoint*) borderPtr)[i]);
	}


	for(int i=0; i<(int)depthmaps.size(); i++){
		Depthmap& depthmap = depthmaps.at(i);
		depthmap.buildBorderBVH(asyncLoader, imageLODs.at(segmentationPyramidLevel).cameras.at(i), borders);
	}
	for(Depthmap& depthmap : depthmaps){
		depthmap.waitForBVH();
	}

	struct Offset{
		int NodeOffset=0;//Number of nodes to skip at the start of the buffer
		int NodeCount=0;//Number of nodes for that depthmap
		int SegmentOffset=0;//Number of segments to skip at the start of the buffer
		int SegmentCount=0;//Number of segments for that depthmap
	};

	std::vector<Offset> offsets;
	offsets.reserve(depthmaps.size());

	int totalNodes = 0;
	int totalSegments = 0;
	for(Depthmap& depthmap : depthmaps){
		totalNodes += depthmap.fastBVH->getNodes().size();
		totalSegments += depthmap.fastBVH->getSegments().size();

	}

	std::vector<segment> segments;
	segments.reserve(totalSegments);
	std::vector<BVHNode> nodes;
	nodes.reserve(totalNodes);

	for(Depthmap& depthmap : depthmaps){
		const auto& bvh = depthmap.fastBVH;
		offsets.emplace_back(Offset{(int)nodes.size(), (int)bvh->getNodes().size(), (int)segments.size(), (int)bvh->getSegments().size()});
		nodes.insert(nodes.end(), bvh->getNodes().begin(), bvh->getNodes().end());
		segments.insert(segments.end(), bvh->getSegments().begin(), bvh->getSegments().end());
	}

	nodesVBO = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
	nodesVBO.bind();
	nodesVBO.storeData(nodes.data(), nodes.size() * sizeof(BVHNode), GL_STATIC_DRAW);
	nodesVBO.unbind();

	segmentsVBO = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
	segmentsVBO.bind();
	segmentsVBO.storeData(segments.data(), segments.size() * sizeof(segment), GL_STATIC_DRAW);
	segmentsVBO.unbind();


	for(int i=0; i<(int)depthmaps.size(); i++){
		auto& borderData = depthmaps.at(i).borderData;
		borderData.NodeOffset = offsets.at(i).NodeOffset;
		borderData.NodeCount = offsets.at(i).NodeCount;
		borderData.SegmentOffset = offsets.at(i).SegmentOffset;
		borderData.SegmentCount = offsets.at(i).SegmentCount;
		depthmaps.at(i).buildVAOs();
	}

	return overflow;
}

void Depthmap::buildVAOs() {
	bvhNodesVAO = RenderingBase::VAO();
	bvhNodesVAO.bind();
	borderData.nodesVBO.bindAs(GL_ARRAY_BUFFER);
	int64_t baseNodeOffset = borderData.NodeOffset * sizeof(BVHNode);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(BVHNode), (GLvoid*) (baseNodeOffset + 0*sizeof(float)));
	glVertexAttribIPointer(1, 4, GL_INT, sizeof(BVHNode), (GLvoid*) (baseNodeOffset + 4*sizeof(float)));
	borderData.nodesVBO.unbindAs(GL_ARRAY_BUFFER);
	bvhNodesVAO.unbind();

	borderSegmentsVAO = RenderingBase::VAO();
	borderSegmentsVAO.bind();
	borderData.segmentsVBO.bindAs(GL_ARRAY_BUFFER);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (GLvoid*) (borderData.SegmentOffset * sizeof(segment)));
	borderData.segmentsVBO.unbindAs(GL_ARRAY_BUFFER);
	borderSegmentsVAO.unbind();

	foregroundPixelsVAO = RenderingBase::VAO();
	foregroundPixelsVAO.bind();
	pixelData.depthPixelsBlock.bindAs(GL_ARRAY_BUFFER);
	size_t basePixelOffset = pixelData.ForegroundPixelsOffset * sizeof(DepthPixel);
	glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, 0, (GLvoid*) (basePixelOffset + 0));
	pixelData.depthPixelsBlock.unbindAs(GL_ARRAY_BUFFER);
	foregroundPixelsVAO.unbind();
}

AllDepthmaps::AllDepthmaps(const std::vector<ImageLOD>& imageLODs,
		AsyncLoader<ImageLoadingTask> &asyncLoader) : imageLODs(imageLODs), asyncLoader(asyncLoader) {
}

AllDepthmaps::~AllDepthmaps() {
}

void AllDepthmaps::init(){
	BorderData borderData {nodesVBO, segmentsVBO, borderPixelsBlock, 0, 0, 0, 0, 0, 0};
	PixelData pixelData{depthTileDescriptorsBlock, depthPixelsBlock, 0, 0, 0, 0};

	nbCameras = (int)imageLODs.at(0).cameras.size();
	depthmaps.reserve(nbCameras);
	for(int i=0; i<nbCameras; i++){
		depthmaps.emplace_back(borderData, pixelData);
	}

}

void AllDepthmaps::allocSpace(int expectedBorderPixels,
		int expectedDepthTiles) {

	assert(nbCameras > 0);

	maxExpectedDepthTiles = expectedDepthTiles;
	maxExpectedForegroundPixels = expectedDepthTiles * DEPTHMAP_TILE_SIZE;
	maxExpectedBorderPixels = expectedBorderPixels;

	depthPixelsBlock.bind();
	depthPixelsBlock.storeData(nullptr, expectedDepthTiles * DEPTHMAP_TILE_SIZE * sizeof(DepthPixel),
	GL_STATIC_DRAW);
	depthPixelsBlock.unbind();

	depthTileDescriptorsBlock.bind();
	depthTileDescriptorsBlock.storeData(nullptr, expectedDepthTiles * sizeof(DepthTileDescriptor),
			GL_STATIC_DRAW);
	depthTileDescriptorsBlock.unbind();


	if (borderPtr != nullptr) {
		borderPtr = nullptr;
		glUnmapNamedBuffer(borderPixelsBlock.getID());
	}

	borderPixelsBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
	borderPixelsBlock.bind();
	GLuint flags = GL_MAP_READ_BIT | GL_MAP_PERSISTENT_BIT;
	glBufferStorage(GL_SHADER_STORAGE_BUFFER,
			expectedBorderPixels * sizeof(BorderPoint), nullptr, flags);
	borderPixelsBlock.unbind();
	borderPtr = glMapNamedBuffer(borderPixelsBlock.getID(), GL_READ_ONLY);

	AtomicBlock.bind();
	std::vector<int> v = std::vector<int>(2*(depthmaps.size()+1), 0);
	AtomicBlock.storeData(v.data(), v.size()*sizeof(int), GL_STATIC_DRAW);
	AtomicBlock.unbind();

	const int width = imageLODs.at(segmentationPyramidLevel).allViewpoints.getWidth() / DEPTHMAP_TILE_WIDTH;
	const int height = imageLODs.at(segmentationPyramidLevel).allViewpoints.getHeight() / DEPTHMAP_TILE_WIDTH;
	const int negative_one = -1;
	occupancyTexture.init(width, height, nbCameras, GL_R32I);
	glClearTexImage(occupancyTexture.getID(), 0, GL_RED_INTEGER, GL_INT, (GLvoid*)&negative_one);

}

size_t AllDepthmaps::memUsage(bool showGui) {

	size_t s1 = segmentsVBO.getDatasize();
	size_t s2 = borderPixelsBlock.getDatasize();
	size_t s3 = nodesVBO.getDatasize();
	size_t s4 = depthPixelsBlock.getDatasize();
	size_t s5 = depthTileDescriptorsBlock.getDatasize();
	size_t s6 = occupancyTexture.memUsage(4);

	size_t s = s1 + s2 + s3 + s4 + s5 + s6;

	if(showGui && ImGui::TreeNode("Depthmaps", "Depthmaps (%s)", prettyPrintMem(s).c_str())){
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Border segments: %s", prettyPrintMem(s1).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Border pixels: %s", prettyPrintMem(s2).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "BVH nodes: %s", prettyPrintMem(s3).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Depth pixels: %s", prettyPrintMem(s4).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Depth tile descriptors: %s", prettyPrintMem(s5).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Occupancy texture: %s", prettyPrintMem(s6).c_str());
		ImGui::TreePop();
	}

	return s;
}
