/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * DifferentialRendering.cpp
 *
 *  Created on: 28 janv. 2022
 *      Author: Briac
 */

#include "DifferentialRendering.h"

#include "../implot/implot.h"
#include "../renderdoc/InitRenderDoc.h"

#include <chrono>

enum OPS{
	ALLOCATE_INTERVALS,
	WRITE_ACCUMULATED_TRANSMITTANCE,
	CULL_UNUSED_CAMERAS,
	COMPUTE_TILE_FLAGS,
	CULL_UNUSED_TILES,
	MOVE_VOXELS,
	SPLIT_TILE_DESCRIPTORS,
	UPSCALE_OCCUPANCY,
	REMOVE_OLD_TILE_INDICES,
	UPSCALE_VOXELS,
	RAY_INTEGRATION,
	RAY_DERIVATIVES,
	VOXEL_DERIVATIVES,
	APPLY_GRADIENT,
	NUM_OPS
};

DifferentialRendering::DifferentialRendering(const std::vector<ImageLOD>& imageLODs, SparseGrid &sparseGrid) : imageLODs(imageLODs) {
	countIntervalsShader.finishInit();
	countIntervalsShader.init_uniforms({"TSDFminCorner", "TSDFmaxCorner", "TSDFTilesCount",
		"tile_size", "STEP_SIZE", "NumCameras", "NumTiles", "activeCameras"});

	std::vector<std::string> samplerNames(sparseGrid.getVoxelsTexturesCount());
	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		samplerNames.push_back("VoxelsTexture[" + std::to_string(i) + "]");
	}

	rayIntegrationShader.finishInit();
	rayIntegrationShader.init_uniforms({"TSDFminCorner",
		"TSDFTilesCount", "tile_size", "STEP_SIZE", "NumCameras", "NumTiles",
		"skipInactiveCamerasIntegration", "skipInactiveCamerasError", "activeCameras", "MaskReg"});

	rayIntegrationShader.init_uniforms(samplerNames);
	rayIntegrationShader.start();
	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		rayIntegrationShader.connectTextureUnit(samplerNames[i], i);
	}
	rayIntegrationShader.stop();


	rayDerivativesShader.finishInit();
	rayDerivativesShader.init_uniforms({"TSDFminCorner", "TSDFmaxCorner",
		"TSDFTilesCount", "tile_size", "STEP_SIZE", "NumCameras", "NumTiles",
		"skipInactiveCamerasIntegration", "skipInactiveCamerasError", "activeCameras",
		"backgroundEntropyReg", "colorDiscrepancyReg", "NoColorSpilling", "MaskReg"});

	voxelDerivativesShader.finishInit();
	voxelDerivativesShader.init_uniforms({"TSDFminCorner", "TSDFTilesCount", "tile_size", "STEP_SIZE",
		"NumCameras", "NumTiles",
		"opacityLaplacianReg", "baseColorLaplacianReg", "SHLaplacianReg", "SHParcimonyReg", "ballooningTerm",
		"beta"});

	voxelDerivativesShader.init_uniforms(samplerNames);
	voxelDerivativesShader.start();
	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		voxelDerivativesShader.connectTextureUnit(samplerNames[i], i);
	}
	voxelDerivativesShader.stop();


	applyGradientShader.finishInit();
	applyGradientShader.init_uniforms({"Biases", "GlobalLearningRate"});
	applyGradientShader.init_uniforms(samplerNames);
	applyGradientShader.start();
	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		applyGradientShader.connectTextureUnit(samplerNames[i], i);
	}
	applyGradientShader.stop();


	writeAccumulatedTransmittanceShader.finishInit();
	writeAccumulatedTransmittanceShader.init_uniforms({"TSDFminCorner",
		"TSDFTilesCount", "tile_size", "STEP_SIZE", "NumCameras"});

	cullUnusedCamerasShader.finishInit();
	cullUnusedCamerasShader.init_uniforms({"TSDFminCorner", "TSDFTilesCount", "tile_size", "STEP_SIZE",
		"NumCameras", "GlobalActiveCameras"});

	cullUnusedTilesShader.finishInit();
	computeTileFlagsShader.finishInit();

	moveVoxelsTextureShader.finishInit();
	moveVoxelsTextureShader.init_uniforms({"WriteDescriptors"});

	upscaleOccupancyShader.finishInit();
	upscaleVoxelsShader.finishInit();
	upscaleVoxelsShader.init_uniforms({"OldTilesDim", "WriteDescriptors"});
	removeOldTileIndicesShader.finishInit();


	showRayTexelsShader.bindVertexAttribute(0, "position");
	showRayTexelsShader.finishInit();
	showRayTexelsShader.init_uniforms({"transform", "layer"});

	timers.reserve(NUM_OPS);
	for(int i=0; i<NUM_OPS; i++){
		timers.emplace_back(GL_TIME_ELAPSED);
	}
}

DifferentialRendering::~DifferentialRendering() {
	params.getBool("RepeatMode") = false;
}

bool DifferentialRendering::evaluateConvergence(SparseGrid& sparseGrid){
	const int avgN = 2;
	if(all_losses.size() >= 2*avgN+1 && iterations.at(sparseGrid.getCurrentLOD()) >= 2*avgN+1){
		double ybar = 0;
		double maxy = 0;
		for(int k=-avgN; k<=avgN; k++){
			double y = all_losses[all_losses.size() + k - avgN - 1];
			ybar += y;
			maxy = std::max(maxy, y);
		}
		ybar /= 2*avgN+1;

		double num = 0;
		double denom = 0;
		for(int k=-avgN; k<=avgN; k++){
			double y = all_losses[all_losses.size() + k - avgN - 1];

			num += (k - avgN) * (y - ybar);
			denom += (k - avgN) * (k - avgN);
		}
		double beta = num / denom / ybar;
		lossDiff = glm::mix(log10(abs(beta)), lossDiff, 0.9);
	}else{
		lossDiff = 0;
	}

	return lossDiff < params.getFloat("ConvergenceThreshold");
}

void DifferentialRendering::printStats() {

	for(int i=0; i<(int)stats.size(); i++){
		std::cout <<"#################" <<std::endl;
		std::cout <<"Stats for level of detail " <<imagePyramidLevels - 1 - i <<": " <<std::endl;
		std::cout <<"Total elapsed time: " <<stats.at(i).elapsedTime*0.001 <<" s" <<std::endl;
		std::cout <<"Total iterations: " <<stats.at(i).iterations <<std::endl;
		std::cout <<"VRAM Usage: " <<stats.at(i).vramUsage <<" GB" <<std::endl;
	}

}

void DifferentialRendering::error_plot(){
	ImGui::TextColored(ImVec4(1, 0, 1, 1), "Photometric error: %5.3f", *pixel_errors.rbegin());

	const int avgN = 2;
	if(all_losses.size() >= 2*avgN+1){
		ImGui::TextColored(ImVec4(1, 0, 1, 1), "loss diff: %2.2f, loss diff threshold: %2.2f", lossDiff, params.getFloat("ConvergenceThreshold"));
	}

	bool& XaxisTime = params.getBool("plotXAxisTime");

	ImGui::TextColored(ImVec4(1, 1, 1, 1), "X-axis units:");
	ImGui::SameLine();
	if(ImGui::RadioButton("# of iterations", !XaxisTime)){
		params.getBool("plotXAxisTime") = false;
	}
	ImGui::SameLine();
	if(ImGui::RadioButton("time", XaxisTime)){
		params.getBool("plotXAxisTime") = true;
	}

	int N = std::min(pixel_errors.size(), std::min(all_losses.size(), elapsed_times.size()));


	std::vector<double> xs(N);
	if(XaxisTime){
		for (int i = 0; i < N; i++) {
			xs[i] = elapsed_times.at(i) * 0.001;
		}
	}else{
		for (int i = 0; i < N; i++) {
			xs[i] = i;
		}
	}


	double y_max = *std::max_element(begin(all_losses), end(all_losses));
	double y_max2 = *std::max_element(begin(pixel_errors), end(pixel_errors));

	ImVec2 plotSize = ImVec2(ImGui::GetContentRegionAvailWidth() - 20, 250);
	ImPlotFlags flags = ImPlotFlags_YAxis2;
	ImPlotAxisFlags X_flags = ImPlotAxisFlags_AutoFit;
	ImPlotAxisFlags Y_flags = ImPlotAxisFlags_Lock;

	ImPlot::SetNextPlotLimitsY(0, y_max, ImGuiCond_Always, ImPlotYAxis_1);
	ImPlot::SetNextPlotLimitsY(0, y_max2, ImGuiCond_Always, ImPlotYAxis_2);
	bool ok = ImPlot::BeginPlot("Loss", XaxisTime ? "elapsed time (s)" : "# of iterations", "Total loss", plotSize, flags, X_flags, Y_flags, Y_flags, Y_flags, "Photometric loss");
	if(ok){
		ImPlot::SetNextLineStyle(ImVec4(0, 1, 1, 1), 1);

		ImPlot::SetLegendLocation(ImPlotLocation_North, ImPlotOrientation_Horizontal, false);

        ImPlot::SetPlotYAxis(ImPlotYAxis_1);
		ImPlot::PlotLine("All losses", xs.data(), all_losses.data(), N);

		ImPlot::SetNextLineStyle(ImVec4(0, 1, 0, 1), 1);
        ImPlot::SetPlotYAxis(ImPlotYAxis_2);
		ImPlot::PlotLine("Photmetric loss", xs.data(), pixel_errors.data(), N);

		ImPlot::EndPlot();
	}

}

bool DifferentialRendering::displayStatusMsg() {
	static bool overflow = false;
	overflow = totalIntervals > params.getInt("ExpectedIntervals")*1000;

	if(overflow)ImGui::SetNextItemOpen(true, ImGuiCond_Always);
	if(ImGui::TreeNode("Differential rendering status")){

		float A0 = timers.at(ALLOCATE_INTERVALS).getLastResult(true)*1.0E-6f;

		if(overflow)ImGui::SetNextItemOpen(true, ImGuiCond_Always);
		if(ImGui::TreeNode("Allocate intervals", "Allocate intervals [%.3f ms]", A0)){
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Allocate intervals: %.3f ms", A0);
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Total intervals: %d ", totalIntervals);
			if(overflow){
				ImGui::SameLine();
				ImGui::TextColored(ImVec4(0, 1, 0, 1), "(Overflow!)");
			}
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Ray samples: %.3f M", totalRaySamples*1.0E-6f);
			ImGui::TextColored(ImVec4(0, 1, 0, 1), "Interval lengths: %.3f M", totalIntervalLengths*1.0E-6f);
			ImGui::TreePop();
		}

		static float A = 0;
		float a1 = timers.at(WRITE_ACCUMULATED_TRANSMITTANCE).getLastResult(true)*1.0E-6f;
		float b1 = timers.at(CULL_UNUSED_CAMERAS).getLastResult(true)*1.0E-6f;
		float c1 = timers.at(COMPUTE_TILE_FLAGS).getLastResult(true)*1.0E-6f;
		A = a1 + b1 + c1;
		if(ImGui::TreeNode("Cull unused cameras", "Cull unused cameras [%.3f ms]", A)){
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Write accumulated transmittance: %.3f ms", a1);
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Cull unused cameras: %.3f ms", b1);
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Compute tile flags: %.3f ms", c1);
			ImGui::TreePop();
		}

		static float B = 0;
		float a2 = timers.at(CULL_UNUSED_TILES).getLastResult(true)*1.0E-6f;
		float b2 = timers.at(MOVE_VOXELS).getLastResult(true)*1.0E-6f;
		B = a2 + b2;
		if(ImGui::TreeNode("Cull unused tiles", "Cull unused tiles [%.3f ms]", B)){
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Cull unused tiles: %.3f ms", a2);
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Move voxels: %.3f ms", b2);
			ImGui::TreePop();
		}

		static float C = 0;
		float a3 = timers.at(SPLIT_TILE_DESCRIPTORS).getLastResult(true)*1.0E-6f;
		float b3 = timers.at(UPSCALE_OCCUPANCY).getLastResult(true)*1.0E-6f;
		float c3 = timers.at(REMOVE_OLD_TILE_INDICES).getLastResult(true)*1.0E-6f;
		float d3 = timers.at(UPSCALE_VOXELS).getLastResult(true)*1.0E-6f;
		C = a3 + b3 + c3 + d3;
		if(ImGui::TreeNode("Next level of detail", "Next level of detail [%.3f ms]", C)){
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Split tile descriptors: %.3f ms", a3);
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Upscale occupancy: %.3f ms", b3);
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Remove old tile indices: %.3f ms", c3);
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Upscale voxels: %.3f ms", d3);
			ImGui::TreePop();
		}

		static float D = 0;
		float a4 = timers.at(RAY_INTEGRATION).getLastResult(true)*1.0E-6f;
		float b4 = timers.at(RAY_DERIVATIVES).getLastResult(true)*1.0E-6f;
		float c4 = timers.at(VOXEL_DERIVATIVES).getLastResult(true)*1.0E-6f;
		float d4 = timers.at(APPLY_GRADIENT).getLastResult(true)*1.0E-6f;
		D = a4 + b4 + c4 + d4;
		if(ImGui::TreeNode("Step", "Step [%.3f ms]", D)){
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Ray integration: %.3f ms", a4);
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Ray derivatives: %.3f ms", b4);
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Voxel derivatives: %.3f ms", c4);
			ImGui::TextColored(ImVec4(1, 0, 1, 1), "Apply Gradient: %.3f ms", d4);
			ImGui::TreePop();
		}


		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Total iterations: %d", getTotalIterations());

		ImGui::Separator();
		if(!pixel_errors.empty()){
			error_plot();
		}


		ImGui::TreePop();
	}

	return overflow;
}

void DifferentialRendering::manageExposureCorrection(){

	if(ImGui::TreeNode("Exposure Correction")){
		ImGui::SliderFloat2("Adam Beta", &params.getVec2("ExposureAdamBeta").x, 0, 1, "%.3f");
		ImGui::SliderFloat("Learning rate", &params.getFloat("ExposureLearningRate"), 0, 1, "%.5f", ImGuiSliderFlags_Logarithmic);
		ImGui::SliderFloat2("Gain & Bias Regularization", &params.getVec2("ExposureRegularization").x, 0, 10, "%.5f", ImGuiSliderFlags_Logarithmic);

		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Exposure Regularization loss: %.5f", exposureRegLoss);
		int selectedCam = params.getInt("Sequence#SelectedCamera");
		const auto& correction = exposureCorrections.at(selectedCam);
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Selected cam gain: %.3f, bias: %.3f", correction.gain, correction.bias);

		ImGui::TreePop();
	}

}
void DifferentialRendering::resetExposureCorrection(int nbCameras){

	std::vector<float> lutData(nbCameras * 2);


	exposureCorrections = std::vector<ExposureCorrection>(nbCameras);
	for(int i=0; i<nbCameras; i++){
		auto& correction = exposureCorrections.at(i);
		correction.gain = 1.0f;
		correction.bias = 0.0f;
		correction.grad = correction.grad2 = correction.newGrad = glm::dvec2(0.0);
	}

	//Clear the exposure LUT
	const glm::vec2 defaultExposureCorrection = glm::vec2(1.0f, 0.0f);
	glClearTexImage(imageLODs.at(0).ExposureLUT->getID(), 0, GL_RG, GL_FLOAT, &defaultExposureCorrection);

	//clear the gradients
	exposureGradVBO = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
	exposureGradVBO.bind();
	const std::vector<double> zeros(2 + 2*nbCameras);
	exposureGradVBO.storeData(zeros.data(), zeros.size() * sizeof(double), GL_STATIC_DRAW);
	exposureGradVBO.unbind();
}

size_t DifferentialRendering::memUsage(bool showGui) {
	size_t s = 0;

	size_t s1 = IntervalsBlock.getDatasize();
	size_t s2 = RayDescriptorsBlock.getDatasize();
	size_t s3 = RayTexels.memUsage(4);
	size_t s4 = GlobalIntegrationTextureBlock.getDatasize();

	s = s1 + s2 + s3 + s4;

	if(showGui && ImGui::TreeNode("Differential Rendering", "Differential Rendering (%s)", prettyPrintMem(s).c_str())){
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Intervals: %s", prettyPrintMem(s1).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Ray descriptors: %s", prettyPrintMem(s2).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Ray texels: %s", prettyPrintMem(s3).c_str());
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Final integration buffer: %s", prettyPrintMem(s4).c_str());
		ImGui::TreePop();
	}

	return s;
}

void DifferentialRendering::renderRayTexels() {
	if(RayTexels.getLayers() == 0 || !params.getBool("DisplayRayTexels")){
		return;
	}

	showRayTexelsShader.start();

	glm::mat4 transform = glm::mat4(1.0f);
	transform[0][0] = 0.25;
	transform[1][1] = 0.25;
	transform[3][0] = 0.75;
	transform[3][1] = -0.75;

	int& layer = params.getInt("RayTexelLayerToDisplay");
	layer = std::max(std::min(layer, RayTexels.getLayers()-1), 0);

	showRayTexelsShader.loadMat4("transform", transform);
	showRayTexelsShader.loadInt("layer", params.getInt("RayTexelLayerToDisplay"));

	glBindBufferBase(GL_UNIFORM_BUFFER, 1, RayTexels.getImageHandlesVBO().getID());
	RayTexels.makeImageHandlesResident(true, GL_READ_ONLY);

	RenderingBase::VAO quad;
	quad.bind();
	float positions[] = { -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f };
	quad.createFloatAttribute(0, positions, 8, 2, 0, GL_STATIC_DRAW);
	quad.unbind();


	quad.bind();
	quad.bindAttribute(0);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	quad.unbindAttribute(0);
	quad.unbind();

	glBindBufferBase(GL_UNIFORM_BUFFER, 1, 0);
	RayTexels.makeImageHandlesResident(false, GL_READ_ONLY);
	showRayTexelsShader.stop();
}

void DifferentialRendering::updateExposureCorrection(int nbCameras, const std::vector<glm::dvec2>& exposureGrads){

	const float LearningRate = params.getFloat("ExposureLearningRate");
	const glm::vec2 Regularization = params.getVec2("ExposureRegularization");
	const glm::vec2 beta = params.getVec2("ExposureAdamBeta");

	std::vector<float> data(nbCameras * 2);

	exposureRegLoss = 0.0f;
	double averageGain = 1.0f;
	for(int i=0; i<nbCameras; i++){
		auto& c = exposureCorrections.at(i);
		c.newGrad = exposureGrads.at(i) + 2.0 / double(nbCameras) * glm::dvec2(log(c.gain) / c.gain, c.bias) * glm::dvec2(Regularization);

		c.grad = glm::mix(c.grad, c.newGrad, beta.x);
		c.grad2 = glm::mix(c.grad2, c.newGrad * c.newGrad, beta.y);

		const float epsilon = 1.0E-8f;
		c.gain -= LearningRate * c.grad.x / sqrt(c.grad2.x + epsilon);
		c.bias -= LearningRate * c.grad.y / sqrt(c.grad2.y + epsilon);

		exposureRegLoss += (Regularization.x * log(c.gain)*log(c.gain) + Regularization.y * c.bias * c.bias);

		averageGain *= c.gain;
	}
	exposureRegLoss /= nbCameras;

	for(int i=0; i<nbCameras; i++){
		auto& c = exposureCorrections.at(i);
		c.gain /= pow(averageGain, 1.0 / nbCameras);
		data[2*i+0] = c.gain;
		data[2*i+1] = c.bias;
	}

	glTextureSubImage1D(imageLODs.at(0).ExposureLUT->getID(), 0, 0, nbCameras, GL_RG, GL_FLOAT, data.data());
}

void DifferentialRendering::manageActiveCameras(SparseGrid &sparseGrid){

	const std::vector<CameraInfo>& cameras = imageLODs.at(0).cameras;

	if(ImGui::TreeNode("Active cameras")){
		const int nCams = cameras.size();

		glm::ivec4 activeCameras = params.getiVec4("activeCameras");

		static std::array<bool, MAX_CAMERAS> states = {};
		for(int i=0; i<nCams; i++){
			states[i] = isCameraActive(activeCameras, i);
		}

		const int COLUMNS_COUNT = 5;
		if (ImGui::BeginTable("table_activeCameras", COLUMNS_COUNT, ImGuiTableFlags_Borders))
		{

		int i = 0;
		for (int row = 0; row < (nCams + COLUMNS_COUNT-1) / COLUMNS_COUNT; row++)
		{
			ImGui::TableNextRow();
			for (int column = 0; column < COLUMNS_COUNT; column++)
			{
				ImGui::TableSetColumnIndex(column);
				if(i < nCams){
					char buf[32];
					sprintf(buf, "Cam %d   ", i);
					ImGui::Checkbox(buf, &states[i]);
				}
				i++;
			}
		}
		ImGui::EndTable();
		}
		for(int i=0; i<nCams; i++){
			setCameraActive(activeCameras, i, states[i]);
		}

		ImVec2 buttonSize =  ImVec2(ImGui::GetContentRegionAvailWidth() / 4, ImGui::GetTextLineHeight()*1.5f);
		if(ImGui::Button("Select all", buttonSize)){
			for(int i=0; i<nCams; i++){
				setCameraActive(activeCameras, i, true);
			}
		}
		ImGui::SameLine();
		if(ImGui::Button("De-select all", buttonSize)){
			for(int i=0; i<nCams; i++){
				setCameraActive(activeCameras, i, false);
			}
		}

		ImGui::SameLine();
		if(ImGui::Button("Short focals", buttonSize)){
			for(int i=0; i<nCams; i++){
				float r = 2.0 * cameras.at(i).K[0][0] / cameras.at(i).width;
				setCameraActive(activeCameras, i, r < 1.5);
			}
		}
		ImGui::SameLine();
		if(ImGui::Button("Long focals", buttonSize)){
			for(int i=0; i<nCams; i++){
				float r = 2.0 * cameras.at(i).K[0][0] / cameras.at(i).width;
				setCameraActive(activeCameras, i, r > 1.5);
			}
		}
		if(params.getiVec4("activeCameras") != activeCameras){
			params.getiVec4("activeCameras") = activeCameras;
			for(int i=sparseGrid.getNbCameras(); i<MAX_CAMERAS; i++){
				setCameraActive(activeCameras, i, false);//disable the non-existing cameras
			}

			if(sparseGrid.getTileDescriptorsBlock().getDatasize() > 0){
				TileDesc* descPtr = (TileDesc*)glMapNamedBuffer(sparseGrid.getTileDescriptorsBlock().getID(), GL_READ_WRITE);
				for(int i=0; i<sparseGrid.getTotalTiles(); i++){
					descPtr[i].activeCameras = activeCameras;
				}
				glUnmapNamedBuffer(sparseGrid.getTileDescriptorsBlock().getID());
			}
		}

		ImGui::Checkbox("Skip inactive cameras integration", &params.getBool("skipInactiveCamerasIntegration"));
		ImGui::SameLine();
		ImGui::Checkbox("Skip inactive cameras error", &params.getBool("skipInactiveCamerasError"));

		ImGui::TreePop();
	}

}


void DifferentialRendering::step(Backgrounds& backgrounds, SparseGrid &sparseGrid, bool autoCoarseToFine, const std::function<void()>& exportMesh){

	{
		auto& q = timers.at(RAY_INTEGRATION).push_back();
		q.begin();
		rayIntegration(backgrounds, sparseGrid);
		q.end();
	}
	{
		auto& q = timers.at(RAY_DERIVATIVES).push_back();
		q.begin();
		rayDerivatives(backgrounds, sparseGrid);
		q.end();
	}
	{
		auto& q = timers.at(VOXEL_DERIVATIVES).push_back();
		q.begin();
		voxelsDerivatives(backgrounds, sparseGrid);
		q.end();
	}
	{
		auto& q = timers.at(APPLY_GRADIENT).push_back();
		q.begin();
		applyGradient(sparseGrid);
		q.end();
	}

	iterations.at(sparseGrid.getCurrentLOD())++;

	sparseGrid.extractMesh();

	glm::ivec4 IterationsPerLod = params.getiVec4("IterationsPerLod");
	bool tooManyIterations = iterations[sparseGrid.getCurrentLOD()] >= IterationsPerLod[sparseGrid.getCurrentLOD()];

	bool hasConverged = evaluateConvergence(sparseGrid);

	if(autoCoarseToFine && (tooManyIterations || hasConverged)){
		endTime = std::chrono::system_clock::now();
		elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);

		stats.push_back(Stats{getTotalIterations(), (double)elapsedTime.count(), params.getFloat("Sequence#VRAMUsage")});

		if(sparseGrid.getCurrentLOD() > 0){
			exportMesh();
			cullUnusedCameras(backgrounds, sparseGrid);
//			cullUnusedTiles(backgrounds, sparseGrid);
			nextLevelOfDetail(backgrounds, sparseGrid);
		}else if(!finished){
			exportMesh();
			setFinished(true);
			params.getBool("RepeatMode") = false;

			printStats();
		}

	}

	if(sparseGrid.getTotalTiles() == 0){
		setFinished(true);
		params.getBool("RepeatMode") = false;
	}

	if(!finished){
		endTime = std::chrono::system_clock::now();
		elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);
		elapsed_times.push_back(elapsedTime.count());
	}

}

void DifferentialRendering::updateGUI(RenderingBase::Camera &camera, Backgrounds& backgrounds, SparseGrid &sparseGrid) {
	static bool overflow = false;


	ImGui::SliderInt("Expected intervals:", &params.getInt("ExpectedIntervals"), 0, 500, "%d K");

	ImGui::Separator();
	ImVec2 buttonSize =  ImVec2(ImGui::GetContentRegionAvailWidth(), ImGui::GetTextLineHeight()*1.5f);

	ImGui::Checkbox("Step repeat", &params.getBool("RepeatMode"));
	ImGui::SameLine();
	ImGui::Checkbox("Automatic coarse to fine", &params.getBool("AutoCoarseToFine"));
//	ImGui::Checkbox("Shader Emulator", &params.getBool("ShaderEmulator"));

	bool f10 = glfwGetKey(camera.getWindow(), GLFW_KEY_F10);

	if(f10){
		InitRenderDoc::startFrame();
		rayIntegration(backgrounds, sparseGrid);
		InitRenderDoc::stopFrame();
	}

	if(!params.getBool("AutoCoarseToFine")){
		if(ImGui::Button("Cull unused cameras", buttonSize)){
			cullUnusedCameras(backgrounds, sparseGrid);
		}
		if(ImGui::Button("Cull unused tiles", buttonSize)){
			cullUnusedTiles(backgrounds, sparseGrid);
		}
		if(ImGui::Button("Next level of detail", buttonSize)){
			nextLevelOfDetail(backgrounds, sparseGrid);
		}
	}else{
		ImGui::SliderFloat("Convergence threshold", &params.getFloat("ConvergenceThreshold"), -6.0f, -1.0f);
		ImGui::SliderInt4("Max iterations per LOD (finest to coarsest)", &params.getiVec4("IterationsPerLod").x, 5, 100);
	}



	bool repeatMode = params.getBool("RepeatMode");
	bool doStep = false;
	if(repeatMode){
		doStep = true;
	}else{
		ImGui::PushButtonRepeat(true);
		doStep = ImGui::Button("Step", buttonSize);
		ImGui::PopButtonRepeat();
		ImGui::Separator();

	}

	camera.setVsync(!repeatMode);

	if(doStep){
		step(backgrounds, sparseGrid, params.getBool("AutoCoarseToFine"), [](){});
	}


	ImGui::Separator();

	if(ImGui::TreeNode("Regularizations")){

		ImGui::SliderFloat("Opacity laplacian", &params.getFloat("opacityLaplacianReg"), 0, 0.25f, "%.5f", ImGuiSliderFlags_Logarithmic);
		ImGui::SliderFloat("Base color laplacian", &params.getFloat("baseColorLaplacianReg"), 0, 0.25f, "%.5f", ImGuiSliderFlags_Logarithmic);
		ImGui::SliderFloat("SH laplacian", &params.getFloat("SHLaplacianReg"), 0, 0.25f, "%.5f", ImGuiSliderFlags_Logarithmic);
		ImGui::SliderFloat("SH parcimony", &params.getFloat("SHParcimonyReg"), 0, 0.25f, "%.5f", ImGuiSliderFlags_Logarithmic);
		ImGui::SliderFloat("Ballooning", &params.getFloat("ballooningTerm"), 0, 0.25f, "%.5f", ImGuiSliderFlags_Logarithmic);
		ImGui::SliderFloat("Background entropy", &params.getFloat("backgroundEntropyReg"), 0, 0.25f, "%.5f", ImGuiSliderFlags_Logarithmic);
		ImGui::SliderFloat("Color discrepancy", &params.getFloat("colorDiscrepancyReg"), 0, 10.0f, "%.5f", ImGuiSliderFlags_Logarithmic);
		ImGui::SliderFloat("Mask regularization", &params.getFloat("MaskReg"), 0, 0.25f, "%.5f", ImGuiSliderFlags_Logarithmic);
		HelpMarker("Should be non-zero only when segmentation masks are used.");

		ImGui::TreePop();
	}

	ImGui::Separator();
	ImGui::SliderFloat("Global learning rate", &params.getFloat("GlobalLearningRate"), 0, 10, "%.5f", ImGuiSliderFlags_Logarithmic);

	ImGui::Checkbox("No color spilling", &params.getBool("NoColorSpilling"));

	ImGui::SliderFloat2("ADAM beta", &params.getVec2("beta").x, 0, 1, "%.3f");

	ImGui::Separator();
	overflow = displayStatusMsg();

	if(overflow){
		params.getBool("RepeatMode") = false;
	}

	manageActiveCameras(sparseGrid);
	manageExposureCorrection();

	if(RayTexels.getLayers() > 0){
		ImGui::Checkbox("Display ray texels", &params.getBool("DisplayRayTexels"));
		if(params.getBool("DisplayRayTexels")){
			ImGui::SliderInt("Ray Texel Layer To Display", &params.getInt("RayTexelLayerToDisplay"), 0, RayTexels.getLayers()-1);
		}
	}

}

void DifferentialRendering::allocateIntervals(Backgrounds& backgrounds, SparseGrid& sparseGrid, bool resetExposure){
	if(sparseGrid.getTotalTiles() == 0){
		return;
	}

	auto& q = timers.at(ALLOCATE_INTERVALS).push_back();
	q.begin();


	const auto& imageLOD = imageLODs.at(sparseGrid.getCurrentLOD());

	if(resetExposure){
		stats.clear();
		all_losses.clear();
		pixel_errors.clear();
		elapsed_times.clear();
		iterations = std::vector<int>(imagePyramidLevels, 0);
		startTime = std::chrono::system_clock::now();
		resetExposureCorrection(sparseGrid.getNbCameras());
	}

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLOD.CameraDataBlock.getID());

	RenderingBase::VBO atomics = RenderingBase::VBO(GL_ATOMIC_COUNTER_BUFFER);
	uint32_t zeros[12] = {};
	atomics.bind();
	atomics.storeData(zeros, sizeof(zeros), GL_STATIC_DRAW);
	atomics.unbind();

	RayDescriptorsBlock.bind();
	RayDescriptorsBlock.storeData(nullptr, imageLOD.TotalDepthTiles * sizeof(RayDescriptor), GL_STATIC_DRAW);
	RayDescriptorsBlock.unbind();

	IntervalsBlock.bind();
	IntervalsBlock.storeData(nullptr, params.getInt("ExpectedIntervals") * 1000 * sizeof(Interval), GL_STATIC_DRAW);
	IntervalsBlock.unbind();

	sparseGrid.getCompressedTileOccupancy()->bindToTextureUnit(0);
	sparseGrid.getTileOccupancy()->bindToTextureUnit(1);
	sparseGrid.getVoxelsTextures()[0]->bindToTextureUnit(2);//we only need the opacity

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, imageLOD.depthTileDescriptorsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, atomics.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, RayDescriptorsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, IntervalsBlock.getID());

	countIntervalsShader.start();
	countIntervalsShader.loadVec3("TSDFminCorner", sparseGrid.getTSDFMinCorner());
	countIntervalsShader.loadVec3("TSDFmaxCorner", sparseGrid.getTSDFMaxCorner());
	countIntervalsShader.loadiVec3("TSDFTilesCount", sparseGrid.getTilesDim());
	countIntervalsShader.loadFloat("tile_size", sparseGrid.getTileSize());
	countIntervalsShader.loadFloat("STEP_SIZE", sparseGrid.getStepSize());
	countIntervalsShader.loadInt("NumCameras", backgrounds.getNumCameras());
	countIntervalsShader.loadInt("NumTiles", sparseGrid.getTotalTiles());
	countIntervalsShader.loadiVec4("activeCameras", params.getiVec4("activeCameras"));

	glDispatchCompute(imageLOD.TotalDepthTiles, 1, 1);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	countIntervalsShader.stop();

	uint32_t* ptr = (uint32_t*)glMapNamedBuffer(atomics.getID(), GL_READ_ONLY);
	totalIntervals = ptr[0];
	totalRaySamples = ((int64_t)ptr[1]) * DEPTHMAP_TILE_SIZE;
	totalIntervalLengths = ptr[2];
	glUnmapNamedBuffer(atomics.getID());

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, 0);

	sparseGrid.getVoxelsTextures()[0]->unbindFromTextureUnit(2);
	sparseGrid.getTileOccupancy()->unbindFromTextureUnit(1);
	sparseGrid.getCompressedTileOccupancy()->unbindFromTextureUnit(0);

	if(totalRaySamples > INT32_MAX){
		throw "Too many ray texels: " + std::to_string(totalRaySamples);
	}

	int64_t slices = totalRaySamples / DEPTHMAP_TILE_SIZE;
	int64_t dimsZ = (int)ceil(double(slices) / (256 * 256));
	int64_t dimsXY = 256 * DEPTHMAP_TILE_WIDTH;

	std::cout <<"Allocating RayTexels bindless textures with " <<dimsZ <<" layers" <<std::endl;
	RayTexels.init(dimsXY, dimsXY, dimsZ, GL_RGBA8UI);

	GlobalIntegrationTextureBlock.bind();
	GlobalIntegrationTextureBlock.storeData(nullptr, imageLOD.TotalDepthTiles * DEPTHMAP_TILE_SIZE * sizeof(GlobalIntegrationTexel), GL_STATIC_DRAW);
	GlobalIntegrationTextureBlock.unbind();

//	if(imageLOD.TotalDepthTiles > 0){
//		RayDescriptor* rayDescriptors = (RayDescriptor*)glMapNamedBuffer(RayDescriptorsBlock.getID(), GL_READ_ONLY);
//		std::vector<int> histogram(100, 0);
//		for(int i=0; i<imageLOD.TotalDepthTiles; i++){
//			RayDescriptor& rayDescriptor = rayDescriptors[i];
//			int bucket = std::min(rayDescriptor.intervalsCount, (int)histogram.size()-1);
//			histogram.at(bucket)++;
//		}
//		for(int i=0; i<(int)histogram.size(); i++){
//			std::cout <<i <<" intervals: " << histogram.at(i) <<" -- " << 100.0f * histogram.at(i) / float(imageLOD.TotalDepthTiles) <<"%" <<std::endl;
//		}
//		glUnmapNamedBuffer(RayDescriptorsBlock.getID());
//	}

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
	q.end();

}

void DifferentialRendering::rayIntegration(Backgrounds& backgrounds,
		SparseGrid &sparseGrid) {
	if(sparseGrid.getTotalTiles() == 0){
		return;
	}

	if(iterations.at(imagePyramidLevels-1) > 0){
		//Update the exposure texture from the results of the previous iteration
		std::vector<glm::dvec2> exposureGrads = std::vector<glm::dvec2>(sparseGrid.getNbCameras());
		double* atomics = (double*)glMapNamedBuffer(exposureGradVBO.getID(), GL_READ_ONLY);
		double error = atomics[0];
		double N = atomics[1];
		pixel_errors.push_back(255.0f * sqrt(error / N));
		for(int i=0; i<sparseGrid.getNbCameras(); i++){
			exposureGrads[i].x = atomics[2*i+2] / N;
			exposureGrads[i].y = atomics[2*i+3] / N;
		}
		glUnmapNamedBuffer(exposureGradVBO.getID());
		updateExposureCorrection(sparseGrid.getNbCameras(), exposureGrads);
	}

	exposureGradVBO = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
	exposureGradVBO.bind();
	const std::vector<double> zeros(2 + 2*sparseGrid.getNbCameras());
	exposureGradVBO.storeData(zeros.data(), zeros.size() * sizeof(double), GL_STATIC_DRAW);
	exposureGradVBO.unbind();



	const auto& imageLOD = imageLODs.at(sparseGrid.getCurrentLOD());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, sparseGrid.getTileDescriptorsBlock().getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, IntervalsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, imageLOD.depthTileDescriptorsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, GlobalIntegrationTextureBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, exposureGradVBO.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, imageLOD.depthTilesBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, RayDescriptorsBlock.getID());

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLOD.CameraDataBlock.getID());

	glBindBufferBase(GL_UNIFORM_BUFFER, 1, RayTexels.getImageHandlesVBO().getID());
	RayTexels.makeImageHandlesResident(true, GL_WRITE_ONLY);

	rayIntegrationShader.start();
	rayIntegrationShader.loadVec3("TSDFminCorner", sparseGrid.getTSDFMinCorner());
	rayIntegrationShader.loadiVec3("TSDFTilesCount", sparseGrid.getTilesDim());
	rayIntegrationShader.loadFloat("tile_size", sparseGrid.getTileSize());
	rayIntegrationShader.loadFloat("STEP_SIZE", sparseGrid.getStepSize());
	rayIntegrationShader.loadInt("NumCameras", backgrounds.getNumCameras());
	rayIntegrationShader.loadInt("NumTiles", sparseGrid.getTotalTiles());
	rayIntegrationShader.loadiVec4("activeCameras", params.getiVec4("activeCameras"));
	rayIntegrationShader.loadInt("skipInactiveCamerasIntegration", params.getBool("skipInactiveCamerasIntegration"));
	rayIntegrationShader.loadInt("skipInactiveCamerasError", params.getBool("skipInactiveCamerasError"));
	rayIntegrationShader.loadFloat("MaskReg", params.getFloat("MaskReg"));

	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		sparseGrid.getVoxelsTextures()[i]->bindToTextureUnit(i);
	}
	sparseGrid.getTileOccupancy()->bindToTextureUnit(7);
	backgrounds.getImages(sparseGrid.getCurrentLOD()).bindToTextureUnit(8);
	imageLOD.ExposureLUT->bindToTextureUnit(9);


	glDispatchCompute(imageLOD.TotalDepthTiles, 1, 1);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	rayIntegrationShader.stop();

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, 0);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, 0);
	RayTexels.makeImageHandlesResident(false, GL_READ_ONLY);

	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		sparseGrid.getVoxelsTextures()[i]->unbindFromTextureUnit(i);
	}
	sparseGrid.getTileOccupancy()->unbindFromTextureUnit(7);
	backgrounds.getImages(sparseGrid.getCurrentLOD()).unbindFromTextureUnit(8);
	imageLOD.ExposureLUT->unbindFromTextureUnit(9);

}

void DifferentialRendering::rayDerivatives(Backgrounds& backgrounds, SparseGrid& sparseGrid){
	if(sparseGrid.getTotalTiles() == 0){
		return;
	}

	RenderingBase::VBO atomicsVBO = RenderingBase::VBO(GL_ATOMIC_COUNTER_BUFFER);
	double zeros[1] = {0.0};
	atomicsVBO.bind();
	atomicsVBO.storeData(zeros, sizeof(zeros), GL_STATIC_DRAW);
	atomicsVBO.unbind();

	const auto& imageLOD = imageLODs.at(sparseGrid.getCurrentLOD());

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, RayDescriptorsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, IntervalsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, imageLOD.depthTileDescriptorsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, GlobalIntegrationTextureBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, imageLOD.depthTilesBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, atomicsVBO.getID());

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLOD.CameraDataBlock.getID());
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, RayTexels.getImageHandlesVBO().getID());
	RayTexels.makeImageHandlesResident(true, GL_READ_WRITE);

	rayDerivativesShader.start();
	rayDerivativesShader.loadVec3("TSDFminCorner", sparseGrid.getTSDFMinCorner());
	rayDerivativesShader.loadVec3("TSDFmaxCorner", sparseGrid.getTSDFMaxCorner());
	rayDerivativesShader.loadiVec3("TSDFTilesCount", sparseGrid.getTilesDim());
	rayDerivativesShader.loadFloat("tile_size", sparseGrid.getTileSize());
	rayDerivativesShader.loadFloat("STEP_SIZE", sparseGrid.getStepSize());
	rayDerivativesShader.loadInt("NumCameras", backgrounds.getNumCameras());
	rayDerivativesShader.loadInt("NumTiles", sparseGrid.getTotalTiles());
	rayDerivativesShader.loadiVec4("activeCameras", params.getiVec4("activeCameras"));
	rayDerivativesShader.loadInt("skipInactiveCamerasIntegration", params.getBool("skipInactiveCamerasIntegration"));
	rayDerivativesShader.loadInt("skipInactiveCamerasError", params.getBool("skipInactiveCamerasError"));
	rayDerivativesShader.loadFloat("MaskReg", params.getFloat("MaskReg"));


//	if(sparseGrid.getCurrentLOD() == imagePyramidLevels-1 && imagePyramidLevels > 1){
//		rayDerivativesShader.loadFloat("colorDiscrepancyReg", 0.0f);
//		rayDerivativesShader.loadFloat("backgroundEntropyReg", 0.0f);
//	}else{
	rayDerivativesShader.loadFloat("colorDiscrepancyReg", params.getFloat("colorDiscrepancyReg"));
	rayDerivativesShader.loadFloat("backgroundEntropyReg", params.getFloat("backgroundEntropyReg"));
//	}

	rayDerivativesShader.loadInt("NoColorSpilling", params.getBool("NoColorSpilling"));

	if(params.getFloat("MaskReg") > 0){
		backgrounds.getImages(sparseGrid.getCurrentLOD()).bindToTextureUnit(0);
	}
	imageLOD.ExposureLUT->bindToTextureUnit(1);

	glDispatchCompute(imageLOD.TotalDepthTiles, 1, 1);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	imageLOD.occupancyTexture.unbindFromTextureUnit(0);
	backgrounds.getImages(sparseGrid.getCurrentLOD()).unbindFromTextureUnit(0);

	rayDerivativesShader.stop();

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, 0);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, 0);
	RayTexels.makeImageHandlesResident(false, GL_READ_ONLY);

	imageLOD.ExposureLUT->unbindFromTextureUnit(1);

	double* atomics = (double*)glMapNamedBuffer(atomicsVBO.getID(), GL_READ_ONLY);
	double loss = atomics[0];
	glUnmapNamedBuffer(atomicsVBO.getID());
	all_losses.push_back(loss);
}

void DifferentialRendering::voxelsDerivatives(Backgrounds& backgrounds, SparseGrid& sparseGrid){
	if(sparseGrid.getTotalTiles() == 0){
		return;
	}
	const auto& imageLOD = imageLODs.at(sparseGrid.getCurrentLOD());

	voxelDerivativesShader.start();
	voxelDerivativesShader.loadVec3("TSDFminCorner", sparseGrid.getTSDFMinCorner());
	voxelDerivativesShader.loadiVec3("TSDFTilesCount", sparseGrid.getTilesDim());
	voxelDerivativesShader.loadFloat("tile_size", sparseGrid.getTileSize());
	voxelDerivativesShader.loadFloat("STEP_SIZE", sparseGrid.getStepSize());
	voxelDerivativesShader.loadInt("NumCameras", backgrounds.getNumCameras());
	voxelDerivativesShader.loadInt("NumTiles", sparseGrid.getTotalTiles());
	voxelDerivativesShader.loadFloat("opacityLaplacianReg", params.getFloat("opacityLaplacianReg"));
	voxelDerivativesShader.loadFloat("baseColorLaplacianReg", params.getFloat("baseColorLaplacianReg"));
	voxelDerivativesShader.loadFloat("SHLaplacianReg", params.getFloat("SHLaplacianReg"));
	voxelDerivativesShader.loadFloat("SHParcimonyReg", params.getFloat("SHParcimonyReg"));
	voxelDerivativesShader.loadFloat("ballooningTerm", params.getFloat("ballooningTerm"));
	voxelDerivativesShader.loadVec2("beta", params.getVec2("beta"));

	RenderingBase::VBO atomicsVBO = RenderingBase::VBO(GL_ATOMIC_COUNTER_BUFFER);
	double zeros[1] = {0.0};
	atomicsVBO.bind();
	atomicsVBO.storeData(zeros, sizeof(zeros), GL_STATIC_DRAW);
	atomicsVBO.unbind();

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, RayDescriptorsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, IntervalsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, sparseGrid.getTileDescriptorsBlock().getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, sparseGrid.getGradientBlock().getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, sparseGrid.getGradientSquaredBlock().getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, atomicsVBO.getID());

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLOD.CameraDataBlock.getID());
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, RayTexels.getImageHandlesVBO().getID());
	RayTexels.makeImageHandlesResident(true, GL_READ_ONLY);


	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		sparseGrid.getVoxelsTextures()[i]->bindToTextureUnit(i);
	}
	imageLOD.occupancyTexture.bindToTextureUnit(7);
	sparseGrid.getTileOccupancy()->bindToTextureUnit(8);

	glDispatchCompute(sparseGrid.getTotalTiles(), 1, 1);

	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		sparseGrid.getVoxelsTextures()[i]->unbindFromTextureUnit(i);
	}
	imageLOD.occupancyTexture.unbindFromTextureUnit(7);
	sparseGrid.getTileOccupancy()->unbindFromTextureUnit(8);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, 0);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, 0);
	RayTexels.makeImageHandlesResident(false, GL_READ_ONLY);

	voxelDerivativesShader.stop();

	double* atomics = (double*)glMapNamedBuffer(atomicsVBO.getID(), GL_READ_ONLY);
	double loss = atomics[0];
	glUnmapNamedBuffer(atomicsVBO.getID());
	all_losses.rbegin() += loss;
}

void DifferentialRendering::applyGradient(SparseGrid& sparseGrid){
	if(sparseGrid.getTotalTiles() == 0){
		return;
	}

	glm::vec2 biases = glm::vec2(1.0f);
	float l = params.getFloat("GlobalLearningRate");

	applyGradientShader.start();
	applyGradientShader.loadVec2("Biases", biases);
	applyGradientShader.loadFloat("GlobalLearningRate", l);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, sparseGrid.getGradientBlock().getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, sparseGrid.getGradientSquaredBlock().getID());

	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		glBindImageTexture(i, sparseGrid.getVoxelsTextures()[i]->getID(), 0, GL_FALSE, 0, GL_READ_WRITE, voxelInternalFormat);
	}

	glDispatchCompute(sparseGrid.getTotalTiles(), 1, 1);

	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		glBindImageTexture(i, 0, 0, GL_FALSE, 0, GL_READ_WRITE, voxelInternalFormat);
	}

	applyGradientShader.stop();

}

void DifferentialRendering::writeAccumulatedTransmittance(Backgrounds& backgrounds, SparseGrid& sparseGrid){
	auto& q1 = timers.at(WRITE_ACCUMULATED_TRANSMITTANCE).push_back();
	q1.begin();

	const auto& imageLOD = imageLODs.at(sparseGrid.getCurrentLOD());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, sparseGrid.getTileDescriptorsBlock().getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, RayDescriptorsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, IntervalsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, imageLOD.depthTileDescriptorsBlock.getID());

	writeAccumulatedTransmittanceShader.start();
	writeAccumulatedTransmittanceShader.loadVec3("TSDFminCorner", sparseGrid.getTSDFMinCorner());
	writeAccumulatedTransmittanceShader.loadiVec3("TSDFTilesCount", sparseGrid.getTilesDim());
	writeAccumulatedTransmittanceShader.loadFloat("tile_size", sparseGrid.getTileSize());
	writeAccumulatedTransmittanceShader.loadFloat("STEP_SIZE", sparseGrid.getStepSize());
	writeAccumulatedTransmittanceShader.loadInt("NumCameras", backgrounds.getNumCameras());

	sparseGrid.getTileOccupancy()->bindToTextureUnit(0);
	sparseGrid.getVoxelsTexture()->bindToTextureUnit(1);

	glDispatchCompute(imageLOD.TotalDepthTiles, 1, 1);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	sparseGrid.getTileOccupancy()->unbindFromTextureUnit(0);
	sparseGrid.getVoxelsTexture()->unbindFromTextureUnit(1);

	writeAccumulatedTransmittanceShader.stop();

	q1.end();
}

void DifferentialRendering::cullUnusedCameras(Backgrounds& backgrounds, SparseGrid& sparseGrid){
	if(sparseGrid.getTotalTiles() == 0){
		return;
	}

	const auto& imageLOD = imageLODs.at(sparseGrid.getCurrentLOD());
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLOD.CameraDataBlock.getID());
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, RayTexels.getImageHandlesVBO().getID());
	RayTexels.makeImageHandlesResident(true, GL_WRITE_ONLY);

	writeAccumulatedTransmittance(backgrounds, sparseGrid);

	auto& q2 = timers.at(CULL_UNUSED_CAMERAS).push_back();
	q2.begin();

	sparseGrid.getTileFlags().bind();
	sparseGrid.getTileFlags().storeData(nullptr, sparseGrid.getTotalTiles() * sizeof(int), GL_STATIC_DRAW);
	sparseGrid.getTileFlags().unbind();
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, sparseGrid.getTileFlags().getID());

	RayTexels.makeImageHandlesResident(false, GL_READ_ONLY);
	RayTexels.makeImageHandlesResident(true, GL_READ_ONLY);

	imageLOD.occupancyTexture.bindToTextureUnit(0);
	cullUnusedCamerasShader.start();
	cullUnusedCamerasShader.loadVec3("TSDFminCorner", sparseGrid.getTSDFMinCorner());
	cullUnusedCamerasShader.loadiVec3("TSDFTilesCount", sparseGrid.getTilesDim());
	cullUnusedCamerasShader.loadFloat("tile_size", sparseGrid.getTileSize());
	cullUnusedCamerasShader.loadFloat("STEP_SIZE", sparseGrid.getStepSize());
	cullUnusedCamerasShader.loadInt("NumCameras", backgrounds.getNumCameras());
	cullUnusedCamerasShader.loadiVec4("GlobalActiveCameras", params.getiVec4("activeCameras"));


	glDispatchCompute(sparseGrid.getTotalTiles(), 1, 1);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);
	cullUnusedCamerasShader.stop();
	imageLOD.occupancyTexture.unbindFromTextureUnit(0);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, 0);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, 0);
	RayTexels.makeImageHandlesResident(false, GL_READ_ONLY);

	q2.end();

	computeTileFlags(sparseGrid);

}

void DifferentialRendering::computeTileFlags(SparseGrid& sparseGrid){
	auto& q = timers.at(COMPUTE_TILE_FLAGS).push_back();
	q.begin();

	RenderingBase::VBO newTileFlags = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
	newTileFlags.bind();
	newTileFlags.storeData(nullptr, sparseGrid.getTotalTiles() * sizeof(int), GL_STATIC_DRAW);
	newTileFlags.unbind();
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, sparseGrid.getTileDescriptorsBlock().getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, sparseGrid.getTileFlags().getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, newTileFlags.getID());

	computeTileFlagsShader.start();
	sparseGrid.getTileOccupancy()->bindToTextureUnit(0);
	glDispatchCompute((sparseGrid.getTotalTiles() + 32 - 1) / 32, 1, 1);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);
	sparseGrid.getTileOccupancy()->unbindFromTextureUnit(0);
	computeTileFlagsShader.stop();

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, 0);

	sparseGrid.getTileFlags() = std::move(newTileFlags);

	q.end();
}

void DifferentialRendering::cullUnusedTiles(Backgrounds& backgrounds, SparseGrid& sparseGrid){
	if(sparseGrid.getTotalTiles() == 0){
		return;
	}

	auto& q = timers.at(CULL_UNUSED_TILES).push_back();
	q.begin();

	//Mark the empty tiles as such in the occupancy volume

	cullUnusedTilesShader.start();
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, sparseGrid.getTileDescriptorsBlock().getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, sparseGrid.getTileFlags().getID());
	glBindImageTexture(0, sparseGrid.getTileOccupancy()->getID(), 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32I);
	glDispatchCompute((sparseGrid.getTotalTiles() + 32 - 1) / 32, 1, 1);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);
	glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32I);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	cullUnusedTilesShader.stop();

	const auto t0 = std::chrono::system_clock::now();

	{
		const int* tileflagsPtr = (int*)glMapNamedBuffer(sparseGrid.getTileFlags().getID(), GL_READ_ONLY);
		tileFlagsVec = std::vector(tileflagsPtr, tileflagsPtr + sparseGrid.getTotalTiles());
		glUnmapNamedBuffer(sparseGrid.getTileFlags().getID());
	}

	//Remove the empty tiles and reorder the remaining ones in morton order
	const TileDesc* descPtr = (TileDesc*)glMapNamedBuffer(sparseGrid.getTileDescriptorsBlock().getID(), GL_READ_ONLY);
	std::vector<TileDesc> vec;
	vec.reserve(sparseGrid.getTotalTiles());
	for(int i=0; i<sparseGrid.getTotalTiles(); i++){
		TileDesc desc = descPtr[i];
		const int flag = tileFlagsVec[i];
		bool isEmptyOrFull = (flag & (1<<0)) > 0;
		bool shouldRemain = (flag & (1<<7)) > 0;
		bool isRemovable = isEmptyOrFull && !shouldRemain;

		if(!isRemovable){
			vec.push_back(desc);
		}

	}
	glUnmapNamedBuffer(sparseGrid.getTileDescriptorsBlock().getID());

	std::sort(std::begin(vec), std::end(vec), [](const TileDesc& a, const TileDesc& b){
		uint32_t codeA = m3D_e_magicbits<uint32_t, uint32_t>(a.coords_index.x, a.coords_index.y, a.coords_index.z);
		uint32_t codeB = m3D_e_magicbits<uint32_t, uint32_t>(b.coords_index.x, b.coords_index.y, b.coords_index.z);
		return codeA < codeB;
	});
	const auto t1 = std::chrono::system_clock::now();
	const auto time = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);

	std::cout <<"Removed unused tiles and sorted remaining ones in " <<time.count() <<" ms" <<std::endl;

	{
		//Transfer to a new buffer
		RenderingBase::VBO TileDescriptorsBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
		TileDescriptorsBlock.bind();
		TileDescriptorsBlock.storeData(vec.data(), sizeof(TileDesc)*vec.size(), GL_STATIC_DRAW);
		TileDescriptorsBlock.unbind();

		//swap the new and old buffers
		sparseGrid.swapTileDescriptors((int)vec.size(), std::move(TileDescriptorsBlock));

		if(sparseGrid.getTotalTiles() == 0){
			return;
		}
	}

	{
		//reorder the flags
		std::vector<int> tileFlagsVec2;
		tileFlagsVec2.reserve(sparseGrid.getTotalTiles());
		for(int i=0; i<sparseGrid.getTotalTiles(); i++){
			tileFlagsVec2.push_back(tileFlagsVec[vec[i].coords_index.w]);
		}
		tileFlagsVec = std::move(tileFlagsVec2);
		sparseGrid.getTileFlags().bind();
		sparseGrid.getTileFlags().storeData(tileFlagsVec.data(), sparseGrid.getTotalTiles() * sizeof(int), GL_STATIC_DRAW);
		sparseGrid.getTileFlags().unbind();
	}

	//build a new texture for the voxels

	q.end();

	auto& q2 = timers.at(MOVE_VOXELS).push_back();
	q2.begin();


	moveVoxelsTextureShader.start();
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, sparseGrid.getTileDescriptorsBlock().getID());
	glBindImageTexture(0, sparseGrid.getTileOccupancy()->getID(), 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);


	sparseGrid.allocateVoxels(false);
	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		std::unique_ptr<Texture3D> oldVoxelsTexture = std::move(sparseGrid.getVoxelsTextures()[i]);
		sparseGrid.getVoxelsTextures()[i] = std::make_unique<Texture3D>(
				sparseGrid.getVoxelsTexturesSize().x,
				sparseGrid.getVoxelsTexturesSize().y,
				sparseGrid.getVoxelsTexturesSize().z,
				voxelInternalFormat);

		glBindImageTexture(1, oldVoxelsTexture->getID(), 0, GL_FALSE, 0, GL_READ_ONLY, voxelInternalFormat);
		glBindImageTexture(2, sparseGrid.getVoxelsTextures()[i]->getID(), 0, GL_FALSE, 0, GL_WRITE_ONLY, voxelInternalFormat);
		moveVoxelsTextureShader.loadInt("WriteDescriptors", (i+1) == sparseGrid.getVoxelsTexturesCount());
		glDispatchCompute(sparseGrid.getTotalTiles(), 1, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);
	}

	glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);
	glBindImageTexture(1, 0, 0, GL_FALSE, 0, GL_READ_ONLY, voxelInternalFormat);
	glBindImageTexture(2, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, voxelInternalFormat);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	moveVoxelsTextureShader.stop();

	q2.end();

	sparseGrid.compressOccupancy();
	allocateIntervals(backgrounds, sparseGrid, false);
}

void DifferentialRendering::nextLevelOfDetail(Backgrounds& backgrounds, SparseGrid& sparseGrid){
	if(sparseGrid.getTotalTiles() == 0){
		return;
	}

	auto& q = timers.at(SPLIT_TILE_DESCRIPTORS).push_back();
	q.begin();

	assert(sparseGrid.getCurrentLOD() > 0);


//	const auto t0 = std::chrono::system_clock::now();

	{
		const int* tileflagsPtr = (int*)glMapNamedBuffer(sparseGrid.getTileFlags().getID(), GL_READ_ONLY);
		tileFlagsVec = std::vector(tileflagsPtr, tileflagsPtr + sparseGrid.getTotalTiles());
		glUnmapNamedBuffer(sparseGrid.getTileFlags().getID());
	}

	const TileDesc* descPtr = (TileDesc*)glMapNamedBuffer(sparseGrid.getTileDescriptorsBlock().getID(), GL_READ_ONLY);
	std::vector<TileDesc> newDescriptors;
	newDescriptors.reserve(8*sparseGrid.getTotalTiles());
	std::vector<glm::ivec4> tilesToRemove;
	tilesToRemove.reserve(8*sparseGrid.getTotalTiles());
	for(int i=0; i<sparseGrid.getTotalTiles(); i++){
		const TileDesc desc = descPtr[i];

		int flag = tileFlagsVec[i];
		bool isEmptyOrFull = (flag & 1) > 0;

		for(int j=0; j<8; j++){
			glm::ivec3 offset = glm::ivec3(j&1, (j>>1)&1, (j>>2)&1);
			const glm::ivec3 c = 2*glm::ivec3(desc.coords_index) + offset;

			bool hasNeighborX = (flag & (1<<(1+0+offset.x))) > 0;
			bool hasNeighborY = (flag & (1<<(1+2+offset.y))) > 0;
			bool hasNeighborZ = (flag & (1<<(1+4+offset.z))) > 0;

			bool isNecessary = int(hasNeighborX) + int(hasNeighborY) + int(hasNeighborZ) > 0;

			if(!isEmptyOrFull || isNecessary){
				newDescriptors.push_back(TileDesc{glm::ivec4(c, desc.coords_index.w), desc.activeCameras});
			}else{
				tilesToRemove.push_back(glm::ivec4(c, int(desc.activeCameras == glm::ivec4(0))));
			}
		}

	}
	glUnmapNamedBuffer(sparseGrid.getTileDescriptorsBlock().getID());


//	std::sort(std::begin(newDescriptors), std::end(newDescriptors), [](const TileDesc& a, const TileDesc& b){
//		uint32_t codeA = m3D_e_magicbits<uint32_t, uint32_t>(a.coords_index.x, a.coords_index.y, a.coords_index.z);
//		uint32_t codeB = m3D_e_magicbits<uint32_t, uint32_t>(b.coords_index.x, b.coords_index.y, b.coords_index.z);
//		return codeA < codeB;
//	});


	sparseGrid.nextLOD();

//	std::cout <<"Next lod: " <<sparseGrid.getTotalTiles() <<" -> " <<newDescriptors.size() <<" tiles " <<std::endl;

//	const auto t1 = std::chrono::system_clock::now();
//	const auto time = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);

//	std::cout <<"Split tiles: " <<time.count() <<" ms" <<std::endl;


	{
		//Transfer to a new buffer
		RenderingBase::VBO TileDescriptorsBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
		TileDescriptorsBlock.bind();
		TileDescriptorsBlock.storeData(newDescriptors.data(), sizeof(TileDesc)*newDescriptors.size(), GL_STATIC_DRAW);
		TileDescriptorsBlock.unbind();
		sparseGrid.swapTileDescriptors((int)newDescriptors.size(), std::move(TileDescriptorsBlock));

		if(sparseGrid.getTotalTiles() == 0){
			return;
		}
	}

	q.end();

	//build new textures
	std::unique_ptr<Texture3D> oldOccupancy = std::move(sparseGrid.getTileOccupancy());
	sparseGrid.allocateOccupancy(false);
	sparseGrid.allocateVoxels(false);

	//necessary in order to transfer the solid tiles (with index -2) to the next level
	glBindImageTexture(0, sparseGrid.getTileOccupancy()->getID(), 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);

	auto& q2 = timers.at(UPSCALE_OCCUPANCY).push_back();
	q2.begin();

	upscaleOccupancyShader.start();
	oldOccupancy->bindToTextureUnit(0);
	glm::ivec3 groups = glm::ivec3(sparseGrid.getTilesDim().x, sparseGrid.getTilesDim().y, sparseGrid.getTilesDim().z);
	groups = (groups + 3) / 4;
	glDispatchCompute(groups.x, groups.y, groups.z);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);
	oldOccupancy->unbindFromTextureUnit(0);
	upscaleOccupancyShader.stop();

	q2.end();

	{
		auto& q3 = timers.at(REMOVE_OLD_TILE_INDICES).push_back();
		q3.begin();

		RenderingBase::VBO vbo = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
		vbo.bind();
		vbo.storeData(tilesToRemove.data(), tilesToRemove.size() * sizeof(glm::ivec4), GL_STATIC_DRAW);
		vbo.unbind();
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, vbo.getID());
		removeOldTileIndicesShader.start();
		glDispatchCompute((tilesToRemove.size() + 63) / 64, 1, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);
		removeOldTileIndicesShader.stop();
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);

		q3.end();
	}

	auto& q4 = timers.at(UPSCALE_VOXELS).push_back();
	q4.begin();

	upscaleVoxelsShader.start();
	upscaleVoxelsShader.loadiVec3("OldTilesDim", sparseGrid.getTilesDim()/2);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, sparseGrid.getTileDescriptorsBlock().getID());
	glBindImageTexture(0, sparseGrid.getTileOccupancy()->getID(), 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);

	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		std::unique_ptr<Texture3D> oldVoxelsTexture = std::move(sparseGrid.getVoxelsTextures()[i]);
		sparseGrid.getVoxelsTextures()[i] = std::make_unique<Texture3D>(
				sparseGrid.getVoxelsTexturesSize().x,
				sparseGrid.getVoxelsTexturesSize().y,
				sparseGrid.getVoxelsTexturesSize().z,
				voxelInternalFormat);
		oldOccupancy->bindToTextureUnit(0);
		oldVoxelsTexture->bindToTextureUnit(1);
		glBindImageTexture(1, sparseGrid.getVoxelsTextures()[i]->getID(), 0, GL_FALSE, 0, GL_WRITE_ONLY, voxelInternalFormat);
		upscaleVoxelsShader.loadInt("WriteDescriptors", (i+1) == sparseGrid.getVoxelsTexturesCount());
		glDispatchCompute(sparseGrid.getTotalTiles(), 1, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);
		oldVoxelsTexture->unbindFromTextureUnit(1);
		oldOccupancy->unbindFromTextureUnit(0);
	}

	glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);
	glBindImageTexture(1, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, voxelInternalFormat);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	upscaleVoxelsShader.stop();

	q4.end();

	sparseGrid.compressOccupancy();
	allocateIntervals(backgrounds, sparseGrid, false);


}


