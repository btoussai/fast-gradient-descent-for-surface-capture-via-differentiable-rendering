/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * TSDF.h
 *
 *  Created on: 5 janv. 2022
 *      Author: Briac
 */

#ifndef SRC_SEQUENCE_TSDF_H_
#define SRC_SEQUENCE_TSDF_H_

#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Camera.h"
#include "../rendering_base/Parameters.h"
#include "Depthmap.h"
#include "MarchingCubes.h"
#include "CommonTypes.h"
#include "Texture3D.h"

#include <vector>
#include <functional>

class SparseGrid {
public:
	SparseGrid(const std::vector<ImageLOD>& imageLODs);
	virtual ~SparseGrid();

	bool displayStatusMsg();
	void updateGUI(AllDepthmaps& depthmaps);
	void render(int selectedCameraIndex, const RenderingBase::Camera& camera, bool RenderMeshAlone, bool SideBySideViews);
	void render(int selectedCameraIndex, const glm::mat4& canvas_transform, bool RemoveDistortion, bool RenderMesh, bool NoVolumeLines);
	void renderTestCamera(int selectedCameraIndex, const glm::mat4& canvas_transform,
			std::vector<CameraInfo> cameras, RenderingBase::VBO& TestCameraDataBlock, bool useColors);
	void build(AllDepthmaps& allDepthmaps);
	void build();

	glm::vec3 getTSDFMinCorner(bool reducedVolume = true) const{
		if(reducedVolume){
			return reducedVolumeMinCorner;
		}else{
			return params.getVec3("VolumeMinCorner");
		}
	}
	glm::vec3 getTSDFMaxCorner(bool reducedVolume = true) const{
		return getTSDFMinCorner(reducedVolume) + glm::vec3(getTilesDim(reducedVolume)) * getTileSize();
	}
	/**
	 * The number of tiles along each axis
	 */
	glm::ivec3 getTilesDim(bool reducedVolume = true, int lod = -1) const{
		if(reducedVolume){
			return reducedVolumeTilesDim;
		}else{
			if(lod == -1) lod = currentLOD;
			return glm::ivec3(params.getVec3("TSDFTilesDim")) / (1<<lod);
		}
	}

	float getStepSize(int lod = -1) const{
		return params.getFloat("StepSize");
	}

	float getTileSize(int lod = -1) const{
		if(lod == -1) lod = currentLOD;
		return params.getFloat("VoxelSize") * VOXELS_PER_SIDE * (1<<lod) / 1000.0f;//mm to m
	}
	int getTotalTiles() const{
		return TilesCounter;
	}

	RenderingBase::VBO& getTileDescriptorsBlock(){
		return TileDescriptorsBlock;
	}

	RenderingBase::VBO& getTileFlags() {
		return TileFlags;
	}

	std::unique_ptr<Texture3D>& getTileOccupancy(){
		return tileOccupancy;
	}

	const RenderingBase::VBO& getGradientBlock() const{
		return GradientBlock;
	}

	const RenderingBase::VBO& getGradientSquaredBlock() const{
		return GradientSquaredBlock;
	}

	std::unique_ptr<Texture3D>& getVoxelsTexture() {
		return VoxelsTexture[0];
	}

	std::vector<std::unique_ptr<Texture3D>>& getVoxelsTextures() {
		return VoxelsTexture;
	}

	int getSHBands() const{
		return params.getInt("Window#SHBands");
	}

	int getParamsPerVoxel() const{
		return 1 + 3*(getSHBands()+1)*(getSHBands()+1);
	}

	int getVoxelsTexturesCount() const{
		return (int)ceil(getParamsPerVoxel() / 4.0);
	}

	const std::unique_ptr<Texture3D>& getCompressedTileOccupancy() const{
		return compressedTileOccupancy;
	}

	const glm::ivec3 getVoxelsTexturesSize() const{
		return voxelsTextureSize;
	}

	int getTriangleCount() const{
		return TotalVertices / 3;
	}

	RenderingBase::Shader& getWireframeTileShader() {
		return wireframeTileShader;
	}

	void extractMesh();

	int getCurrentLOD() const{
		return currentLOD;
	}

	void nextLOD() {
		currentLOD--;
		reducedVolumeTilesDim *= 2;
	}

	int getNbCameras() const{
		return (int)imageLODs.at(0).cameras.size();
	}

	std::function<std::function<void()>()> exportMeshTo(const std::string& path, const std::string& format, bool exportOnlyBinaryMeshes);

	void swapTileDescriptors(int NumTiles, RenderingBase::VBO&& TileDescriptorsBlock){
		this->TileDescriptorsBlock = std::move(TileDescriptorsBlock);
		this->TilesCounter = NumTiles;
	}

	void writeOccupancy();
	void compressOccupancy();

	void allocateOccupancy(bool clear);
	void allocateVoxels(bool allocateVoxelsTextures = true);

	size_t memUsage(bool showGui);

	const std::vector<ImageLOD>& getImageLoDs() const {
		return imageLODs;
	}

private:
	RenderingBase::Parameters params = RenderingBase::Parameters("SparseGrid");

	int currentLOD = imagePyramidLevels-1;
	glm::vec3 reducedVolumeMinCorner = getTSDFMinCorner(false);
	glm::ivec3 reducedVolumeTilesDim = getTilesDim(false);

	const std::vector<ImageLOD>& imageLODs;
	RenderingBase::Shader reserveTilesShader = RenderingBase::Shader("sparseGrid/reserveTilesShader.cp");
	RenderingBase::Shader writeOccupancyShader = RenderingBase::Shader("sparseGrid/write3DOccupancyTexture.cp");
	RenderingBase::Shader fillVoxelsShader = RenderingBase::Shader("sparseGrid/fillVoxels.cp");
	RenderingBase::Shader compressOccupancyShader = RenderingBase::Shader("sparseGrid/compressOccupancy.cp");
	RenderingBase::Shader wireframeTileShader = RenderingBase::Shader("sparseGrid/tile.vs", "sparseGrid/tile.gs", "sparseGrid/tile.fs");
	RenderingBase::Shader meshShader = RenderingBase::Shader("mesh/mesh.vs", "mesh/mesh.fs");

	bool IsPlaneCutActive = false;
	glm::vec4 clipEquation = glm::vec4(0.0);
	RenderingBase::Shader planeCutShader = RenderingBase::Shader("planeCut/drawPlaneCut.vs", "planeCut/drawPlaneCut.fs");

	RenderingBase::VAO quad = RenderingBase::VAO();


	int TilesCounter = 0;
	RenderingBase::VBO TileDescriptorsBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
	RenderingBase::VBO TileFlags = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);

	std::unique_ptr<Texture3D> tileOccupancy;
	std::unique_ptr<Texture3D> compressedTileOccupancy;

	std::vector<std::unique_ptr<Texture3D>> VoxelsTexture = std::vector<std::unique_ptr<Texture3D>>(getVoxelsTexturesCount());
	glm::ivec3 voxelsTextureSize = glm::ivec3(0);
	RenderingBase::VBO GradientBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
	RenderingBase::VBO GradientSquaredBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);


	int TotalVertices = 0;
	RenderingBase::VBO VerticesBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);

	//Buffer in pinned memory for vertex readback
	std::atomic_bool isTransferBufferAvailable = true;
	RenderingBase::VBO TransferBuffer = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
	const char* mappedPtr = nullptr;
	GLsync copySync = {};
	mutable std::mutex transfer_mutex;
	std::condition_variable transfer_condition;


	std::vector<RenderingBase::QueryBuffer> timers;

	void reserveTiles();
	void fillVoxels();
	void exportMeshToTemp();


	MarchingCubes marchingCubes;
};

#endif /* SRC_SEQUENCE_TSDF_H_ */
