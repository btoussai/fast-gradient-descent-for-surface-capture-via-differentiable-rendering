/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Depthmap.h
 *
 *  Created on: 11 déc. 2021
 *      Author: Briac
 */

#ifndef SRC_SEQUENCE_DEPTHMAP_H_
#define SRC_SEQUENCE_DEPTHMAP_H_

#include "../rendering_base/openglObjects.h"
#include "CommonTypes.h"
#include "AsyncLoader.h"
#include "Texture2DArray.h"
#include "FastBVH.h"

#include <mutex>
#include <condition_variable>
#include <atomic>

class Depthmap;

struct BorderData{
	//The following buffers are shared among all the depthmaps
	RenderingBase::VBO& nodesVBO;
	RenderingBase::VBO& segmentsVBO;
	RenderingBase::VBO& borderPixelsBlock;//An array of BorderPoint

	int NodeOffset=0;//Number of nodes to skip at the start of the buffer
	int NodeCount=0;//Number of nodes for that depthmap
	int SegmentOffset=0;//Number of segments to skip at the start of the buffer
	int SegmentCount=0;//Number of segments for that depthmap
	int BorderPixelOffset=0;//Number of BorderPoints to skip at the start of the buffer;
	int BorderPixels=0;//Number of BorderPoints for the depthmap

};

struct PixelData{
	//The following buffers are shared among all the depthmaps
	RenderingBase::VBO& depthTileDescriptorsBlock;//An array of DepthTileDescriptors
	RenderingBase::VBO& depthPixelsBlock;//An array of DepthPixels
	int ForegroundPixels=0;//Number of DepthPixels for the depthmap
	int ForegroundPixelsOffset=0;//Number of DepthPixels to skip at the start of the foregroundPixelsBlock buffer
	int DepthTiles=0;//How many depth tiles in that image
	int DepthTilesOffset=0;//How many depth tiles in the previous images
};

/**
 * Contains data necessary to compute the visual hull.
 * Once computed, it can be discarded.
 */
class AllDepthmaps {
public:
	AllDepthmaps(const std::vector<ImageLOD>& imageLODs, AsyncLoader<ImageLoadingTask>& asyncLoader);
	virtual ~AllDepthmaps();
	void init();
	void allocSpace(int maxExpectedBorderPixels, int maxExpectedDepthTiles);
	bool buildVAOs();

	std::vector<Depthmap> depthmaps;

	Texture2DArray occupancyTexture;
	RenderingBase::VBO depthTileDescriptorsBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);//An array of DepthTileDescriptors
	RenderingBase::VBO depthPixelsBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);//An array of DepthPixels
	RenderingBase::VBO borderPixelsBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);//An array of segments

	RenderingBase::VBO nodesVBO = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);//Contains all the BVH nodes for all the depthmaps
	RenderingBase::VBO segmentsVBO = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);//Contains all the segments for all the depthmaps

	RenderingBase::VBO AtomicBlock = RenderingBase::VBO(GL_ATOMIC_COUNTER_BUFFER);//2 values per depthmap {ForegroundPixelOffset, BorderPixelOffset}

	int TotalForegroundPixels=0;
	int TotalForegroundPixelTiles=0;
	int TotalBorderPixels=0;

	int maxExpectedBorderPixels = 0;
	int maxExpectedForegroundPixels = 0;
	int maxExpectedDepthTiles = 0;

	int getNbCameras() const{
		return nbCameras;
	}

	size_t memUsage(bool showGui);

private:
	int nbCameras=0;
	const std::vector<ImageLOD>& imageLODs;
	AsyncLoader<ImageLoadingTask>& asyncLoader;

	void* borderPtr = nullptr;

};

class Depthmap {
public:

	Depthmap(BorderData borderData, PixelData pixelData);
	virtual ~Depthmap();

	Depthmap(const Depthmap&) = delete;
	Depthmap& operator=(const Depthmap&) = delete;

	Depthmap(Depthmap&&);
	Depthmap& operator=(Depthmap&&) = delete;

	void buildBorderBVH(AsyncLoader<ImageLoadingTask>& imageLoader, const CameraInfo& camData, const std::vector<BorderPoint>& borders);
	void waitForBVH();
	void buildVAOs();

	std::atomic_bool finished_bvh = false;
	//std::unique_ptr<BVH<segment>> bvh = nullptr;
	std::unique_ptr<FastBVH> fastBVH;

	BorderData borderData;
	PixelData pixelData;

	RenderingBase::VAO bvhNodesVAO;
	RenderingBase::VAO borderSegmentsVAO;
	RenderingBase::VAO foregroundPixelsVAO;
private:
	mutable std::mutex mutex;
	std::condition_variable condition;



};

#endif /* SRC_SEQUENCE_DEPTHMAP_H_ */
