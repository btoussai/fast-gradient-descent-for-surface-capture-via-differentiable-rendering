/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * AnimatedReconstruction.cpp
 *
 *  Created on: 15 nov. 2021
 *      Author: Briac
 */

#include "AnimatedReconstruction.h"
#include <filesystem>
#include <iostream>
#include <vector>

#include "../glm/gtx/string_cast.hpp"

#include "EigenPrecompiled.h"

#include "../cnpy/cnpy.h"

#include "../imgui/imgui.h"

AnimatedReconstruction::AnimatedReconstruction(const std::string &path) {

	N_verts = 6890;
	N_meshes = 0;
	std::vector<double> data_cano;
	std::vector<double> data_posed;

	std::cout << "Loading " << path << std::endl;

	using recursive_directory_iterator = std::filesystem::recursive_directory_iterator;
	for (const auto &dirEntry : recursive_directory_iterator(path)) {
		N_meshes++;
//		std::cout << dirEntry << std::endl;

		cnpy::npz_t arrays = cnpy::npz_load(dirEntry.path().string());

		[[maybe_unused]] auto &pose = arrays["pose"]; //72 doubles
		[[maybe_unused]] auto &transl = arrays["transl"]; // 3 doubles ??
		[[maybe_unused]] auto &v_cano = arrays["v_cano"]; //6890 x 3 doubles
		[[maybe_unused]] auto &v_posed = arrays["v_posed"]; //6890 x 3 doubles

		double *tab_cano = v_cano.data<double>();
		double *tab_posed = v_posed.data<double>();
//		std::cout << "shape: " << v_cano.shape[0] << " x " << v_cano.shape[1]
//				<< std::endl;

		int N = (int) v_cano.shape[0] * v_cano.shape[1];
		for (int index = 0; index < N; index++) {
			data_cano.push_back(tab_cano[index]);
			data_posed.push_back(tab_posed[index]);
		}

	}

//	vao_cano.bind();
//	vao_cano.createFloatAttribute(0, data_cano.data(), data_cano.size(), 3, 0,
//			GL_STATIC_DRAW);
//	vao_cano.unbind();
//
//	vao_posed.bind();
//	vao_posed.createFloatAttribute(0, data_posed.data(), data_posed.size(), 3,
//			0, GL_STATIC_DRAW);
//	vao_posed.unbind();

	shader.bindVertexAttribute(0, "position");
	shader.bindVertexAttribute(1, "error");
	shader.finishInit();
	shader.init_uniforms( { "projectionView", "transform", "ErrorMagnification" });

	mesh_cano = Eigen::MatrixXd(N_meshes, N_verts * 3);
	mesh_posed = Eigen::MatrixXd(N_meshes, N_verts * 3);
	for (int t = 0; t < N_meshes; t++) {
		for (int n = 0; n < N_verts; n++) {
			mesh_cano(t, 3 * n + 0) = data_cano[t * N_verts * 3 + 3 * n + 0];
			mesh_cano(t, 3 * n + 1) = data_cano[t * N_verts * 3 + 3 * n + 1];
			mesh_cano(t, 3 * n + 2) = data_cano[t * N_verts * 3 + 3 * n + 2];
			mesh_posed(t, 3 * n + 0) = data_posed[t * N_verts * 3 + 3 * n + 0];
			mesh_posed(t, 3 * n + 1) = data_posed[t * N_verts * 3 + 3 * n + 1];
			mesh_posed(t, 3 * n + 2) = data_posed[t * N_verts * 3 + 3 * n + 2];
		}
	}

	svd_cano = SVDdata::computeSVD(mesh_cano,
			Eigen::ComputeThinU | Eigen::ComputeThinV);
	svd_posed = SVDdata::computeSVD(mesh_posed,
			Eigen::ComputeThinU | Eigen::ComputeThinV);

	MaxSingularValues = N_meshes;
	if(params.getBool("PosedMesh")){
		recombine(*svd_posed, mesh_posed);
	}else{
		recombine(*svd_cano, mesh_cano);
	}
}

AnimatedReconstruction::~AnimatedReconstruction() {
	delete svd_cano;
	delete svd_posed;
}

void AnimatedReconstruction::updateUI() {
	bool changed = false;

	ImGui::Checkbox("Animate", &params.getBool("Animate"));
	ImGui::SliderInt("Timestamp", &Timestamp, 0, N_meshes - 1);
	changed |= ImGui::Checkbox("Posed mesh", &params.getBool("PosedMesh"));
	changed |= ImGui::SliderInt("Singular values", &MaxSingularValues, 1,
			N_meshes - 1);
	ImGui::SliderFloat("Error magnification", &params.getFloat("ErrorMagnification"), 1, 10);

	ImGui::Separator();
	ImGui::TextColored(ImVec4(1, 0, 1, 1), "Max Error: %0.3f", maxDist);

	if (params.getBool("Animate")) {
		static float dt = 0;
		const float fps = 30.0;
		dt += 1.0 / 120.0 * fps;
		if (dt > 1.0) {
			dt -= 1.0;
			Timestamp = (Timestamp + 1) % N_meshes;
		}
	}

	if (changed) {
		if(params.getBool("PosedMesh")){
			recombine(*svd_posed, mesh_posed);
		}else{
			recombine(*svd_cano, mesh_cano);
		}
	}

}

void AnimatedReconstruction::recombine(SVDdata &svd, Eigen::MatrixXd& mesh) {

	Eigen::VectorXd s = svd.singularValues();
	for (int i = MaxSingularValues; i < s.size(); i++) {
		s[i] = 0.0;
	}

	Eigen::MatrixXd mesh_recombined = svd.matrixU() * s.asDiagonal()
			* svd.matrixV().transpose();
	std::vector<float> v;
	std::vector<float> c;
	v.reserve(N_meshes * N_verts * 3);
	c.reserve(N_meshes * N_verts);
	maxDist = 0;

	glm::vec3 minA = glm::vec3(0.0);
	glm::vec3 maxA = glm::vec3(0.0);
	for (int t = 0; t < N_meshes; t++) {
		for (int n = 0; n < N_verts; n++) {
			glm::vec3 a, b;
			a = glm::vec3(mesh_recombined(t, 3 * n + 0), mesh_recombined(t, 3 * n + 1), mesh_recombined(t, 3 * n + 2));
			b = glm::vec3(mesh(t, 3 * n + 0), mesh(t, 3 * n + 1), mesh(t, 3 * n + 2));

			v.push_back(a.x);
			v.push_back(a.y);
			v.push_back(a.z);

			glm::vec3 diff = a-b;
			float d = sqrt(glm::dot(diff, diff));
			c.push_back(d);
			maxDist = std::max(maxDist, d);

			maxA = glm::max(a, maxA);
			minA = glm::min(a, minA);
		}
	}

	for(int i=0; i<(int)c.size(); i++){
		c[i] /= maxDist;
	}

	vao = RenderingBase::VAO();
	vao.bind();
	vao.createFloatAttribute(0, v.data(), v.size(), 3, 0,
	GL_STATIC_DRAW);
	vao.createFloatAttribute(1, c.data(), c.size(), 1, 0,
	GL_STATIC_DRAW);
	vao.unbind();

}

void AnimatedReconstruction::render(const RenderingBase::Camera &camera) {
	shader.start();
	shader.loadMat4("projectionView", camera.getProjectionViewMatrix());

	float s = 1;
	glm::mat4 transform = glm::mat4(s);
	transform[3][3] = 1.0;
	shader.loadMat4("transform", transform);
	shader.loadFloat("ErrorMagnification", params.getFloat("ErrorMagnification"));

	//glPointSize(3.0);
	glEnable(GL_PROGRAM_POINT_SIZE);

	vao.bind();
	vao.bindAttributes({0, 1});
	glDrawArrays(GL_POINTS, Timestamp * N_verts, N_verts);
	vao.unbindAttributes({0, 1});
	vao.unbind();

	glDisable(GL_PROGRAM_POINT_SIZE);

	shader.stop();
}
