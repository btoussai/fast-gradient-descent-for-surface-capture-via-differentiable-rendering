/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * CustomBarrier.h
 *
 *  Created on: 22 févr. 2022
 *      Author: Briac
 */

#ifndef SRC_SEQUENCE_CUSTOMBARRIER_H_
#define SRC_SEQUENCE_CUSTOMBARRIER_H_

#include <barrier>
#include <mutex>
#include <condition_variable>

//There appears to be a bug with gcc 11.2.0 and earlier with barriers which create deadlocks
//see  https://stackoverflow.com/questions/69310487/barrier-deadlocks-in-gcc-11-1-0
//this implementation does not seem to have that problem.
//from https://stackoverflow.com/questions/24465533/implementing-boostbarrier-in-c11
class Barrier {
public:
	explicit Barrier(int size) :
			mThreshold(size), mCount(size), mGeneration(0), arrivals(size, {-1, -1}) {
	}

	void arrive_and_wait(int k) {
		std::unique_lock<std::mutex> lLock { mMutex };
		auto lGen = mGeneration;

//		arrivals.at(k) = {int(lGen), int(mCount)};

		if (!--mCount) {
			mGeneration++;
			mCount = mThreshold;
//			arrivals = std::vector<S> (mThreshold, {-1, -1});
			mCond.notify_all();
		} else {
			mCond.wait(lLock, [this, lGen] {
				return lGen != mGeneration;
			});
		}
	}

	static void Test() {
		//From https://stackoverflow.com/questions/69310487/barrier-deadlocks-in-gcc-11-1-0
		constexpr unsigned N = 2;
		//  std::barrier barrier(N); //Fails with this
		Barrier barrier(N);
		auto const run = [&barrier]() {
			std::string buff;
			for (unsigned i = 0; i < 1000; i++) {
				std::cout << ((buff = "A ") + std::to_string(i) + "\n")
						<< std::endl;
				barrier.arrive_and_wait(0);
				std::cout << ((buff = "B ") + std::to_string(i) + "\n")
						<< std::endl;
				barrier.arrive_and_wait(0);
				std::cout << ((buff = "C ") + std::to_string(i) + "\n")
						<< std::endl;
			}
		};

		std::thread ts[N];
		for (unsigned j = 0; j < N; j++)
			ts[j] = std::thread(run);
		for (unsigned j = 0; j < N; j++)
			ts[j].join();

	}

	std::mutex mMutex;
private:
	struct S{
		int gen;
		int order;
	};
	std::condition_variable mCond;
	std::size_t mThreshold;
	std::size_t mCount;
	std::size_t mGeneration;
	std::vector<S> arrivals;
};

#endif /* SRC_SEQUENCE_CUSTOMBARRIER_H_ */
