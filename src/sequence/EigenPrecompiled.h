/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * EigenPrecompiled.h
 *
 *  Created on: 16 nov. 2021
 *      Author: Briac
 */

#ifndef SEQUENCE_EIGENPRECOMPILED_H_
#define SEQUENCE_EIGENPRECOMPILED_H_

#include "eigen3/Eigen/Core"
#include "eigen3/Eigen/src/SVD/SVDBase.h"

#include <memory>

struct SVDdata{
	static SVDdata* computeSVD(const Eigen::MatrixXd& m, int flags);

	~SVDdata();
	const Eigen::VectorXd& singularValues() const;
	const Eigen::MatrixXd& matrixU() const;
	const Eigen::MatrixXd& matrixV() const;
private:
	SVDdata(const Eigen::MatrixXd& m, int flags);
	struct impl;
	std::unique_ptr<impl> pImpl;
};

#endif /* SEQUENCE_EIGENPRECOMPILED_H_ */
