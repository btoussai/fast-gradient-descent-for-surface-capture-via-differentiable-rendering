/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * AsyncTextureUploader.h
 *
 *  Created on: 4 mai 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_ASYNCTEXTURELOADER_H_
#define SEQUENCE_ASYNCTEXTURELOADER_H_

#include <unordered_map>
#include <list>
#include <vector>
#include <functional>
#include <string>
#include <mutex>
#include <memory>

#include "CommonTypes.h"
#include "AsyncLoader.h"
#include "SharedContext.h"
#include "Backgrounds.h"
#include "../rendering_base/openglObjects.h"

struct Frame {
	const int timestamp;
	const int count;
	bool hasSilhouettes;
	std::list<ImageLoadingTask> images;
	std::list<ImageLoadingTask> silhouettes;

	mutable std::mutex mtx;
	std::atomic_bool uploaded = false;

	Frame(int timestamp, int count, bool hasSilhouettes) :
			timestamp(timestamp), count(count), hasSilhouettes(hasSilhouettes) {
	}
	~Frame() {
	}
	bool isCompleted() const {
		std::unique_lock<std::mutex> lock(mtx);
		return (int) images.size() == count && (!hasSilhouettes || (int)silhouettes.size() == count);
	}
	bool isUploaded() const {
		return uploaded;
	}
	void setUploaded(bool uploaded) {
		this->uploaded = uploaded;
	}

	void appendImage(ImageLoadingTask &&task) {
		std::unique_lock<std::mutex> lock(mtx);
		images.push_back(std::move(task));
		if ((int) images.size() == count) {
			images.sort(
					[](const ImageLoadingTask &a, const ImageLoadingTask &b) {
						return a.camIndex < b.camIndex;
					});
		}
	}
	void appendSilhouette(ImageLoadingTask &&task) {
		std::unique_lock<std::mutex> lock(mtx);
		silhouettes.push_back(std::move(task));
		if ((int) silhouettes.size() == count) {
			silhouettes.sort(
					[](const ImageLoadingTask &a, const ImageLoadingTask &b) {
						return a.camIndex < b.camIndex;
					});
		}
	}

};

class FrameRequests {
public:
	bool exists(int timestamp);
	std::shared_ptr<Frame>& get(int timestamp);
	std::shared_ptr<Frame>& add(int timestamp, int count, bool hasSilhouettes);
	void remove(int timestamp);
	void removeOldRequests(const std::vector<int> &timestamps);
private:
	std::unordered_map<int, std::shared_ptr<Frame>> requests;

};

class AsyncTextureLoader {
public:
	AsyncTextureLoader(std::vector<ImageLOD> &imageLODs,
			AsyncLoader<ImageLoadingTask> &imageLoader,
			std::unique_ptr<SharedContext> &sharedContext,
			Backgrounds& backgrounds);
	virtual ~AsyncTextureLoader();

	void update(std::vector<int> timestamps, int selectedTimeStamp,
			int selectedCameraIndex, bool loadAllViewPoints, int nbCameras,
			const std::string &sequenceDir,
			const std::string& silhouettesDir,
			bool loadSilhouettes);

	bool isFrameComplete(int selectedTimeStamp) {
		return multiviewLoadingRequests.exists(selectedTimeStamp)
				&& multiviewLoadingRequests.get(selectedTimeStamp)->uploaded;
	}
private:

	void async_upload(std::shared_ptr<Frame> &frame);

	struct TransferBuffer{
		std::atomic_bool isTransferBufferAvailable = true;
		void* mappedPtr = nullptr;
		RenderingBase::VBO buffer = RenderingBase::VBO(GL_PIXEL_UNPACK_BUFFER);
		GLsync copySync = {};

		void realloc(uint32_t size){
			if(buffer.getDatasize() < size){
				if(mappedPtr != nullptr){
					glUnmapNamedBuffer(buffer.getID());
				}
				buffer = RenderingBase::VBO(GL_PIXEL_UNPACK_BUFFER);
				glNamedBufferStorage(buffer.getID(), size, nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
				buffer.setDataSize(size);
				mappedPtr = glMapNamedBufferRange(buffer.getID(), 0, size, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
			}
		}

		void sync(){
			copySync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
			glFlush();
		}

		void waitForSync(){
			if(copySync == nullptr){
				return;
			}
			while(glClientWaitSync(copySync, 0, 10000) != GL_ALREADY_SIGNALED);
			glDeleteSync(copySync);
			copySync = nullptr;
		}
	};

	const int buffers = 1;
	std::vector<TransferBuffer> transfer_buffers = std::vector<TransferBuffer>(buffers);
	std::atomic_bool allTransferBuffersAvailable = true;


	FrameRequests multiviewLoadingRequests;
	FrameRequests singleviewLoadingRequests;
	std::vector<ImageLOD> &imageLODs;
	AsyncLoader<ImageLoadingTask> &imageLoader;
	std::unique_ptr<SharedContext> &sharedContext;
	Backgrounds& backgrounds;
};

#endif /* SEQUENCE_ASYNCTEXTURELOADER_H_ */
