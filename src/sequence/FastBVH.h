/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * FastBVH.h
 *
 *  Created on: 10 janv. 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_FASTBVH_H_
#define SEQUENCE_FASTBVH_H_

#include <vector>
#include <functional>
#include <bitset>

#include "../glm/gtx/string_cast.hpp"
#include "CommonTypes.h"
#include "Morton.h"

class FastBVH {
public:

	FastBVH(){

	}

	FastBVH(const std::vector<segment> &elems,
			const std::function<AABB(const segment&)> &makebox) {

		if(elems.empty()){
			BVHNode n{AABB{glm::vec2(-10, -10), glm::vec2(-9, -9)}, -1, -1, -1, -1};
			nodes.push_back(n);
			segments = elems;
			return;
		}

		struct triplet {
			AABB box;
			uint32_t mortonCode;
			segment s;
		};
		std::vector<triplet> boxes;
		boxes.reserve(elems.size());

		//Compute all the leaf boxes
		for (int i = 0; i < (int) elems.size(); i++) {
			AABB box = makebox(elems[i]);
			boxes.push_back(triplet{box, 0, elems[i]});
		}
		//Compute the total extent
		AABB big_box { glm::vec2(+INFINITY), glm::vec2(-INFINITY) };
		for (int i = 0; i < (int) elems.size(); i++) {
			big_box = AABB::unionAABB(big_box, boxes[i].box);
		}

		//Compute the morton codes
		for (int i = 0; i < (int) elems.size(); i++) {
			AABB box = boxes[i].box;
			glm::vec2 center = (box.maxCorner + box.minCorner) / 2.0f;
			glm::vec2 coords = (center - big_box.minCorner) / (big_box.maxCorner - big_box.minCorner);
			int x = (int) (coords.x * 8192);
			int y = (int) (coords.y * 8192);
			boxes[i].mortonCode = m2D_e_magicbits<uint32_t, uint32_t>(x, y);
		}
		std::sort(boxes.begin(), boxes.end(),
				[](const triplet &a, const triplet &b) {
					return a.mortonCode < b.mortonCode;
				});

		struct split{
			int index;
			int level;
		};
		std::vector<split> splits;
		for(int k=0; k<(int)boxes.size()-1; k++){
			uint32_t c1 = boxes.at(k).mortonCode;
			uint32_t c2 = boxes.at(k+1).mortonCode;
			uint32_t x = c1 ^ c2;

			assert(x != 0); //Two segments have the same morton code otherwise

//			std::cout <<"k:" <<k <<": " <<c1 <<" | " <<c2 <<"  or   " <<std::bitset<32>(c1) <<" | " <<std::bitset<32>(c2) <<std::endl;
//			std::cout <<"xor: " <<std::bitset<32>(x) <<std::endl;

			int r = 32;
			while (x) {
			    r--;
			    x = x >> 1;
			}
			splits.push_back(split{k+1, r});

		}

		std::sort(splits.begin(), splits.end(),
				[](const split &a, const split &b) {
					return a.level < b.level || (a.level == b.level && a.index < b.index);
				});

//		std::cout <<std::endl;
//
//		for(int i=0; i<(int)splits.size(); i++){
//			std::cout <<splits.at(i).level << ": " << splits.at(i).index <<std::endl;
//		}

//		std::cout <<splits.size() <<" splits."<< std::endl;

		struct TempNode{
			AABB bounds;
			int childL;
			int childR;
			int parent;
			int index;
			int leftBracket;
			int rightBracket;
			int level;
		};


		assert(splits.at(0).level != 0);//Too many nodes

		int currentLevel = splits.at(0).level;
		std::vector<TempNode> tempNodes;
		tempNodes.push_back(TempNode{AABB(), -1, -1, -1, 0, 0, (int)boxes.size(), currentLevel-1});


		std::vector<int> parents;
		std::vector<int> children;
		parents.push_back(0);


		int splitIndex = 0;
		std::vector<int>::iterator parentIt = parents.begin();
		TempNode parent = tempNodes[*parentIt];
		while(splitIndex<(int)splits.size()){
			auto& split = splits.at(splitIndex);
//			std::cout <<split.level << ": " << split.index <<std::endl;

			while(!(split.index > parent.leftBracket && split.index < parent.rightBracket)){
				children.push_back(parent.index);
				parentIt++;
				if(parentIt == parents.end()){
					std::swap(children, parents);
					children.clear();
					parentIt = parents.begin();
				}
				parent = tempNodes.at(*parentIt);
			}

			{
				parent.childL = tempNodes.size();
				TempNode& n = tempNodes.emplace_back(TempNode{AABB(), -1, -1, parent.index, parent.childL, parent.leftBracket, split.index, split.level});
				if(n.rightBracket-n.leftBracket > 1)
					children.push_back(n.index);
			}
			{
				parent.childR = tempNodes.size();
				TempNode& n = tempNodes.emplace_back(TempNode{AABB(), -1, -1, parent.index, parent.childR, tempNodes.at(parent.childL).rightBracket, parent.rightBracket, split.level});
				if(n.rightBracket-n.leftBracket > 1)
					children.push_back(n.index);
			}

			tempNodes.at(parent.index) = parent;//Write back the parent
			parentIt++;//next parent
			if(parentIt == parents.end()){
				std::swap(children, parents);
				children.clear();
				parentIt = parents.begin();
			}
			parent = tempNodes.at(*parentIt);

			splitIndex++;
		}


		segments = std::vector<segment>(boxes.size());
		for(int i=0; i<(int)boxes.size(); i++){
			segments.at(i) = boxes.at(i).s;
		}

		nodes = std::vector<BVHNode>(tempNodes.size());
		for(int i=(int)tempNodes.size()-1; i>= 0; i--){
			auto& tmp = tempNodes[i];
			//std::cout <<i <<" Level:" <<tmp.level <<": [" <<tmp.leftBracket <<" " <<tmp.rightBracket <<"[ children: " <<tmp.childL <<", " <<tmp.childR <<std::endl;


			if(tmp.rightBracket-tmp.leftBracket == 1){//that's a leaf node
				tmp.bounds = boxes.at(tmp.leftBracket).box;
				BVHNode n = BVHNode{tmp.bounds, tmp.childL, tmp.childR, tmp.parent, tmp.leftBracket};
				nodes[i] = n;
			}else{
				assert(tmp.childL != -1);
				assert(tmp.childR != -1);
				tmp.bounds = AABB::unionAABB(tempNodes.at(tmp.childL).bounds, tempNodes.at(tmp.childR).bounds);
				BVHNode n = BVHNode{tmp.bounds, tmp.childL, tmp.childR, tmp.parent, -1};
				nodes[i] = n;
			}
			//std::cout <<glm::to_string(tmp.bounds.minCorner) <<" " << glm::to_string(tmp.bounds.maxCorner) <<std::endl;
		}



		std::function<void(BVHNode&, int)> rec;
		rec = [&](BVHNode& n, int level){

			if((n.childL == -1) != (n.childR == -1)){
				throw "Error in the BVH";
			}

			if(n.childL != -1){
				auto& c = nodes.at(n.childL);
				if(&nodes.at(c.parent) != &n){
					throw "Error in the BVH";
				}
				rec(c, level+1);
			}
			if(n.childR != -1){
				auto& c = nodes.at(n.childR);
				if(&nodes.at(c.parent) != &n){
					throw "Error in the BVH";
				}
				rec(c, level+1);
			}
			n.parent = level;


		};
		rec(nodes[0], 0);


	}

	~FastBVH() {

	}

	const std::vector<segment>& getSegments() const {
		return segments;
	}

	const std::vector<BVHNode>& getNodes() const{
		return nodes;
	}

private:
	std::vector<BVHNode> nodes;
	std::vector<segment> segments;

};

#endif /* SEQUENCE_FASTBVH_H_ */
