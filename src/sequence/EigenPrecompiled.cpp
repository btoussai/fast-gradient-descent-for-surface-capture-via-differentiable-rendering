/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * EigenPrecompiled.cpp
 *
 *  Created on: 16 nov. 2021
 *      Author: Briac
 */

#include "EigenPrecompiled.h"

#include "eigen3/Eigen/SVD"

//SVDdata::impl{
//	Eigen::BDCSVD<Eigen::MatrixXd> svd;
//};
//
//struct SVDdata{
//	SVDdata(const Eigen::MatrixXd &m, int flags) : svd(m, Eigen::ComputeThinU | Eigen::ComputeThinV){
//
//	};
//	Eigen::BDCSVD<Eigen::MatrixXd> svd;
//};
//
//SVDdata::~SVDdata(){
//
//}
//
//Eigen::VectorXd& SVDdata::singularValues(){
//	return svd.singularValues();
//}

struct SVDdata::impl{
	impl(const Eigen::MatrixXd &m, int flags) : svd(m, flags){

	}
	~impl(){

	}
	Eigen::BDCSVD<Eigen::MatrixXd> svd;
};

SVDdata::SVDdata(const Eigen::MatrixXd &m, int flags){
	pImpl = std::make_unique<SVDdata::impl>(m, flags);
}

SVDdata::~SVDdata(){

}

const Eigen::VectorXd& SVDdata::singularValues() const{
	return pImpl->svd.singularValues();
}
const Eigen::MatrixXd& SVDdata::matrixU() const{
	return pImpl->svd.matrixU();
}
const Eigen::MatrixXd& SVDdata::matrixV() const{
	return pImpl->svd.matrixV();
}

SVDdata* SVDdata::computeSVD(
		const Eigen::MatrixXd &m, int flags) {
	return new SVDdata(m, flags);
}
