/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * SharedContext.cpp
 *
 *  Created on: 4 mai 2022
 *      Author: Briac
 */

#include "SharedContext.h"

#include <string>

SharedContext::SharedContext(GLFWwindow *mainWindow) {

	glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
	w = glfwCreateWindow(640, 480, "SharedContext", NULL, mainWindow);
	if (!w) {
		glfwTerminate();
		throw std::string("Error while creating the shared context window");
	}

	thread = std::make_unique<std::thread>([this](){
		thread_loop();
	});
}

SharedContext::~SharedContext() {
	should_exit = true;
	thread->join();

	std::cout << "Shared context deleted." << std::endl;
}

void SharedContext::scheduleTask(std::function<void()> &&task) {
	tasks.push(std::move(task));
}

void SharedContext::thread_loop() {

	glfwMakeContextCurrent(w);

	std::chrono::milliseconds timeout(1000);
	while (!should_exit) {
		std::function<void()> task;
		bool success = false;
		tasks.get(task, timeout, success);

		if (should_exit) {
			break;
		}

		if (success) {
			try {
				task();
			} catch (std::string &s) {
				std::stringstream ss;
				ss << "An exception occurred in the shared context thread."
						<< std::endl;
				ss << s << std::endl;
				std::cout << ss.str() << std::endl;
			}
		}

	}
}
