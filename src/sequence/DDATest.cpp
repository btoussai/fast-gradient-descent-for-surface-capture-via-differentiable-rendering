/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * DDATest.cpp
 *
 *  Created on: 2 févr. 2022
 *      Author: Briac
 */

#include "DDATest.h"
#include "../implot/implot.h"
#include "CommonTypes.h"

using namespace glm;

static const GLEmulator::SizedArray<DepthTileDescriptor> occupancyDescriptors;
static const GLEmulator::SizedArray<RayDescriptor> ray_descriptors;
static const GLEmulator::SizedArray<Interval> intervals;
static const GLEmulator::SizedArray<f16vec4> global_integration_texture;
static const GLEmulator::SizedArray<CameraData> cameraData;

static vec3 TSDFminCorner;
static vec3 TSDFmaxCorner;
static vec3 TSDFTilesCount;
static float tile_size;
static int NumCameras;
static int NumTiles;
static GLEmulator::isampler2DArray* occupancyImage;
static GLEmulator::usampler3D* CompressedTileOccupancy;
static GLEmulator::isampler3D* TileOccupancy;
static std::vector<GLEmulator::uimage2D>* RayTexels;


ivec3 getRayTexelCoords(int slice, ivec2 localCoords){
	int x = bitfieldExtract(slice, 0, 8);
	int y = bitfieldExtract(slice, 8, 8);
	int z = bitfieldExtract(slice, 16, 16);
	return ivec3(x * DEPTHMAP_TILE_WIDTH + localCoords.x, y * DEPTHMAP_TILE_WIDTH + localCoords.y, z);
}

static float STEP_SIZE = sqrt(3.0);// relative to the size of a voxel

DDATest::DDATest() {
	rayShader.bindVertexAttribute(0, "pos");
	rayShader.finishInit();
	rayShader.init_uniforms({"projectionView", "transform", "color"});
}

DDATest::~DDATest() {
	// TODO Auto-generated destructor stub
}

void DDATest::updateGUIs(const RenderingBase::Camera& camera, std::vector<ImageLOD>& imageLODs, AllDepthmaps &depthmaps, SparseGrid &sparseGrid,
		DifferentialRendering &differentialRendering, Backgrounds& backgrounds,
		glm::ivec2 selectedPixel, glm::ivec2 selectedDepthTile) {

	ImVec2 buttonSize =  ImVec2(ImGui::GetContentRegionAvailWidth(), ImGui::GetTextLineHeight()*1.5f);
	if(ImGui::Button("Copy Buffers", buttonSize)){

		buffs = std::make_unique<buffers>(imageLODs, depthmaps, sparseGrid, backgrounds, differentialRendering);

		occupancyDescriptors.set((DepthTileDescriptor*)buffs->occupancyDescriptorsPTR.data, buffs->occupancyDescriptorsPTR.size);
		cameraData.set((CameraData*)buffs->cameraDataPTR.data, buffs->cameraDataPTR.size);

		ray_descriptors.set((RayDescriptor*)buffs->ray_descriptorsPTR.data, buffs->ray_descriptorsPTR.size);
		intervals.set((Interval*)buffs->intervalsPTR.data, buffs->intervalsPTR.size);

		RayTexels = &buffs->RayTexels;
		global_integration_texture.set((f16vec4*)buffs->global_integration_texturePTR.data, buffs->global_integration_texturePTR.size);

		TSDFminCorner = sparseGrid.getTSDFMinCorner();
		TSDFmaxCorner =	sparseGrid.getTSDFMaxCorner();
		TSDFTilesCount = sparseGrid.getTilesDim();
		tile_size =	sparseGrid.getTileSize();
		NumCameras = 68;
		NumTiles = sparseGrid.getTotalTiles();

		CompressedTileOccupancy = &buffs->CompressedTileOccupancy;
		TileOccupancy = &buffs->TileOccupancy;
		occupancyImage = &buffs->occupancyImageSampler;

		STEP_SIZE = sparseGrid.getStepSize();
	}

	if(!buffs){
		return;
	}
	const int camIndex = params.getInt("Sequence#SelectedCamera");

	bool takeSelectedCameraViewpoint = params.getBool(
			"Sequence#TakeSelectedCameraVewpoint");

	if(takeSelectedCameraViewpoint && camera.is_ctrl_pressed() && camera.is_lmb_pressed()){
		params.getVec2("DepthTile").x = selectedDepthTile.x;
		params.getVec2("DepthTile").y = selectedDepthTile.y;
		params.getVec3("Froxel").x = selectedPixel.x % 8;
		params.getVec3("Froxel").y = selectedPixel.y % 8;
	}

//	const auto& update = [&](){
//		glm::ivec3 selectedTile = glm::ivec3(params.getVec3("Tile"));
//		searchIntersectionWithSelectedTile(selectedTile);
//		computeTileFrustum(selectedTile, camIndex);
//
//		if(ray_texels.size > 0){
//			globalIntegration();
//		}
//
//	};

	glm::ivec2 imgSize = imageLODs.at(sparseGrid.getCurrentLOD()).occupancyTexture.getSize();
	ImGui::SliderFloat2("Depth Tile", &params.getVec2("DepthTile").x, 0, max(imgSize.x, imgSize.y)-1, "%.0f", ImGuiSliderFlags_AlwaysClamp);
	ImGui::SliderFloat3("Tile", &params.getVec3("Tile").x, 0, sparseGrid.getTilesDim().x-1, "%.0f", ImGuiSliderFlags_AlwaysClamp);
	ImGui::SliderFloat3("Voxel", &params.getVec3("Voxel").x, 0, VOXELS_PER_SIDE-1, "%.0f", ImGuiSliderFlags_AlwaysClamp);

	params.getVec2("DepthTile") = clamp(params.getVec2("DepthTile"), glm::vec2(0), glm::vec2(imgSize-1));
	params.getVec3("Froxel").x = clamp(params.getVec3("Froxel").x, 0.0f, DEPTHMAP_TILE_WIDTH-1.0f);
	params.getVec3("Froxel").y = clamp(params.getVec3("Froxel").y, 0.0f, DEPTHMAP_TILE_WIDTH-1.0f);

//	ImGui::Separator();
//
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "Non-empty tiles hit: %d", nonEmptyTiles);
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "Total tiles hit: %d", tilesIntersected);
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "intervalsCount: %d", intervalsCount);
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "maxIntervalLength: %d", maxIntervalLength);
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "totalIntervalLengths: %d", totalIntervalLengths);
//
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "Transmittance: %.2f", final_transmittance_rgb.x);
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "RGB: %.2f %.2f %.2f", final_transmittance_rgb.y, final_transmittance_rgb.z, final_transmittance_rgb.w);
//

	ImGui::Separator();

	depthTileCoords = ivec2(params.getVec2("DepthTile"));
	depthTileIndex = GLEmulator::texelFetch(occupancyImage, ivec3(depthTileCoords, camIndex), 0).x;
	gpuIntervals.clear();
	outSamplePoints.clear();
	inSamplePoints.clear();
	accumulatedTransmittanceColor.clear();
	samplesLocation.clear();
	if(depthTileIndex != -1){
		ImGui::TextColored(ImVec4(1, 0, 0, 1), "depthTileIndex: %d", depthTileIndex);

		rayDescriptor = ray_descriptors[depthTileIndex];

		assert(rayDescriptor.depth_tile_x == depthTileCoords.x);
		assert(rayDescriptor.depth_tile_y == depthTileCoords.y);

		const float tile_size_inv = 1.0f / tile_size;
		const float scaling = tile_size_inv * float(VOXELS_PER_SIDE);

		ivec2 localCoords = ivec2(params.getVec3("Froxel"));
		ivec2 pixelCoords = depthTileCoords * DEPTHMAP_TILE_WIDTH + localCoords;
		vec2 uv = vec2(pixelCoords) + vec2(0.5f);
		vec2 undistorted_uv = toNDC(uv, camIndex);

		vec3 camCenter = (getCamCenter(camIndex) - TSDFminCorner) * scaling;
		vec3 ray = unproject(undistorted_uv, camIndex);
		ray /= dot(cameraData[camIndex].cameraTransformInv[2], vec4(ray, 0.0));
		ray *= STEP_SIZE;

		float Tpartial = 1.0f;
		vec3 Cpartial = vec3(0.0);
		ImGui::TextColored(ImVec4(1, 0, 0, 1), "intervalsCount: %d", rayDescriptor.intervalsCount);
		int texelIndex = rayDescriptor.texelsPtr;
		for(int i=0; i<rayDescriptor.intervalsCount; i++){
			int ptr = rayDescriptor.intervalsPtr + i;
			ImGui::TextColored(ImVec4(1, 0, 0, 1), "[%d - %d[", intervals[ptr].start, intervals[ptr].end);
			gpuIntervals.push_back(intervals[ptr]);

			for(int t=intervals[ptr].start; t<intervals[ptr].end; t++){
				const vec3 p = camCenter + ray * float(t);
				const ivec3 tile_coords = ivec3(p / float(VOXELS_PER_SIDE));
				const ivec3 compressedCoord = tile_coords / 4;
				const ivec3 localCompressedCoord = tile_coords % 4;
				const uvec2 bitmask = uvec2(texelFetch(CompressedTileOccupancy, ivec3(compressedCoord), 0));
				const int bitPos = localCompressedCoord.x + 4*localCompressedCoord.y + 16 * (localCompressedCoord.z%2);
				const bool isEmpty = ( ((localCompressedCoord.z/2)==0 ? bitmask.x : bitmask.y) & (1<<bitPos)) == 0;


				if(isEmpty){
					outSamplePoints.push_back(vec4(p / scaling + TSDFminCorner, 1.0));
				}else{
					inSamplePoints.push_back(vec4(p / scaling + TSDFminCorner, 1.0));
				}

//				int64_t k = (int64_t)texelIndex * DEPTHMAP_TILE_SIZE + localCoords.y * DEPTHMAP_TILE_WIDTH + localCoords.x;

//				vec4 v = glm::vec4(ray_texels[k]) / 255.0f;
//				float alpha = v.x;
//				vec3 c = vec3(v.y, v.z, v.w);

				const ivec3 bindless_coords = getRayTexelCoords(texelIndex, localCoords);
				GLEmulator::uimage2D* img = &(*RayTexels)[bindless_coords.z];

				float alpha = GLEmulator::imageLoad(img, ivec2(bindless_coords)).x / 255.0f;

				//float alpha = ray_texels[k].x / 255.0f;
				vec3 c = vec3(0);

				Cpartial += c * (Tpartial * (1.0f - alpha));
				Tpartial *= alpha;
				accumulatedTransmittanceColor.push_back(vec4(Tpartial, Cpartial));
				texelIndex++;
				samplesLocation.push_back(t);
			}
		}

		if(rayDescriptor.intervalsCount > 0){
			ImGui::SliderFloat2("Froxel XY", &params.getVec3("Froxel").x, 0, DEPTHMAP_TILE_WIDTH-1, "%.0f", ImGuiSliderFlags_AlwaysClamp);

			ImGui::SliderInt("Interval", &params.getInt("Interval"), 0, (int)gpuIntervals.size()-1, "%.0f", ImGuiSliderFlags_AlwaysClamp);
			params.getInt("Interval") = max(0, min(params.getInt("Interval"), (int)gpuIntervals.size()-1));

			int maxz = gpuIntervals[params.getInt("Interval")].end-gpuIntervals[params.getInt("Interval")].start-1;

			ImGui::SliderFloat("Froxel Z", &params.getVec3("Froxel").z, 0, maxz, "%.0f", ImGuiSliderFlags_AlwaysClamp);
			params.getVec3("Froxel").z = max(0, min((int)params.getVec3("Froxel").z, maxz));

			int z = (int)params.getVec3("Froxel").z;
			if(z < (int)samplesLocation.size()){
				ImGui::TextColored(ImVec4(1, 0, 0, 1), "t = %d, transmittance = %.3f", samplesLocation.at(z), accumulatedTransmittanceColor.at(z).x);
			}
		}

	}else{
		ImGui::TextColored(ImVec4(1, 0, 0, 1), "depthTileIndex: %d", depthTileIndex);
	}


	if(samplesLocation.size() > 0){
		ImGui::Separator();
		std::vector<double> xs;
		std::vector<double> ys;

		for(int i=0; i<(int)samplesLocation.size(); i++){
			xs.push_back(samplesLocation.at(i));
			float T = accumulatedTransmittanceColor.at(i).x;
			ys.push_back(T);
		}

		ImVec2 plotSize = ImVec2(ImGui::GetContentRegionAvailWidth() - 20, 300);
		ImPlotFlags flags = ImPlotAxisFlags_None;
		ImPlotAxisFlags X_flags = ImPlotAxisFlags_AutoFit;
		ImPlotAxisFlags Y_flags = ImPlotAxisFlags_AutoFit;

		ImPlot::SetNextPlotLimitsY(0.0F, 1.0f, ImGuiCond_Always, ImPlotYAxis_1);
		bool ok = ImPlot::BeginPlot("Transmittance", "x", "y", plotSize,
				flags, X_flags, Y_flags);
		if(ok){
			ImPlot::SetNextLineStyle(ImVec4(0, 1, 1, 1), 1);


			ImPlot::PlotLine("Accumulated Transmittance", xs.data(), ys.data(), ys.size());

			static double marker = 0;

			int z = (int)params.getVec3("Froxel").z;
			if(z < (int)samplesLocation.size()){
				marker = samplesLocation.at(z);
				if (ImPlot::DragLineX("sample", &marker,
						true,
						ImVec4(1, 0, 0, 1), 2)) {
					const auto it = std::find(samplesLocation.begin(), samplesLocation.end(), (int)marker);
					params.getVec3("Froxel").z = it-samplesLocation.begin();
				}
			}

			ImPlot::SetLegendLocation(ImPlotLocation_South,
					ImPlotOrientation_Horizontal, true);
			ImPlot::EndPlot();

		}
	}

//
//	if(hitTiles.size() > 0){
//		ImGui::TextColored(ImVec4(0, 1, 0, 1), "start tile: %d %d %d", hitTiles[0].x, hitTiles[0].y, hitTiles[0].z);
//		for(int i=0; i<intervalsCount; i++){
//			int start = hitIntervals.at(i).start;
//			int end = hitIntervals.at(i).end;
//			ImGui::TextColored(ImVec4(0, 1, 0, 1), "[%d - %d[", start, end);
//		}
//		ImGui::Separator();
//		ImGui::TextColored(ImVec4(1, 0, 0, 1), "gpu start tile: %d %d %d", gpuStartTile.x, gpuStartTile.y, gpuStartTile.z);
//		for(int i=0; i<(int)gpuIntervals.size(); i++){
//			int start = gpuIntervals.at(i).start;
//			int end = gpuIntervals.at(i).end;
//			ImGui::TextColored(ImVec4(1, 0, 0, 1), "[%d - %d[", start, end);
//		}
//
//	}



//	ImGui::Separator();
//
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "global_transmittance_rgb");
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "Transmittance: %.2f RGB: %.2f %.2f %.2f", global_transmittance_rgb.x,
//			global_transmittance_rgb.y, global_transmittance_rgb.z, global_transmittance_rgb.w);
//
//	ImGui::Separator();
//
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "emulated_transmittance_rgb");
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "Transmittance: %.2f RGB: %.2f %.2f %.2f", emulated_transmittance_rgb.x,
//			emulated_transmittance_rgb.y, emulated_transmittance_rgb.z, emulated_transmittance_rgb.w);
//
//
//	ImGui::Separator();
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "Tile proj: %d %d %d", tileProjection.dims.x, tileProjection.dims.y, tileProjection.stepsEndFrustum - tileProjection.stepsBeginFrustum + 1);
//
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "sampleDistances: %d %d %d", sampleDistances.x, sampleDistances.y, sampleDistances.z);
//	ImGui::TextColored(ImVec4(0, 1, 0, 1), "froxelRatio: %2.2f %2.2f", froxelRatio.x, froxelRatio.y);
//




}

void DDATest::render(const RenderingBase::Camera& camera, SparseGrid &sparseGrid, const std::vector<ImageLOD>& imageLODs,
		RENDER_PROJECTION_TYPE PROJECTION_TYPE, glm::mat4 canvas_transform, bool RemoveDistortion){
	auto& wireframeTileShader = sparseGrid.getWireframeTileShader();
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLODs.at(sparseGrid.getCurrentLOD()).CameraDataBlock.getID());


	glm::mat4 transform = glm::mat4(1.0);

	wireframeTileShader.start();
	wireframeTileShader.loadInt("PROJECTION_TYPE", PROJECTION_TYPE);
	wireframeTileShader.loadInt("RemoveDistortion", RemoveDistortion);
	wireframeTileShader.loadMat4("canvas_transform", canvas_transform);
	wireframeTileShader.loadMat4("projectionView", camera.getProjectionViewMatrix());
	wireframeTileShader.loadMat4("transform", transform);
	wireframeTileShader.loadVec3("TSDFminCorner", TSDFminCorner);

	{
		//Draw the selected tile
		glLineWidth(8.0f);
		wireframeTileShader.loadVec4("color", glm::vec4(1, 1, 1, 1));
		wireframeTileShader.loadVec3("tile_size", glm::vec3(tile_size));
		RenderingBase::VAO vao;
		vao.bind();
		glm::ivec4 coord_index = glm::ivec4(glm::ivec3(params.getVec3("Tile")), 0);
		vao.createIntAttribute(0, (const int32_t*)&coord_index.x, 4, 4, 0, GL_STATIC_DRAW);
		vao.bindAttributes({0});
		glDrawArrays(GL_POINTS, 0, 1);
		vao.unbindAttributes({0});
		vao.unbind();
	}
	{
		//Draw the selected voxel
		wireframeTileShader.loadVec3("TSDFminCorner", TSDFminCorner + params.getVec3("Tile") * tile_size);
		glLineWidth(3.0f);
		wireframeTileShader.loadVec4("color", glm::vec4(1, 0, 0, 1));
		wireframeTileShader.loadVec3("tile_size", glm::vec3(tile_size / VOXELS_PER_SIDE));
		RenderingBase::VAO sparseGridVolume;
		sparseGridVolume.bind();
		glm::ivec4 coord_index = glm::ivec4(glm::ivec3(params.getVec3("Voxel")), 0);
		sparseGridVolume.createIntAttribute(0, &coord_index.x, 4, 4, 0, GL_STATIC_DRAW, 0);
		sparseGridVolume.bindAttribute(0);
		glDrawArrays(GL_POINTS, 0, 1);
		sparseGridVolume.unbindAttribute(0);
		sparseGridVolume.unbind();
	}
	{
		RenderingBase::VAO sparseGridVolume;
		sparseGridVolume.bind();
		glm::ivec4 coord_index = glm::ivec4(0, 0, 0, 0);
		sparseGridVolume.createIntAttribute(0, &coord_index.x, 4, 4, 0, GL_STATIC_DRAW, 0);
		sparseGridVolume.bindAttribute(0);

		//Draw the tile frustum
		glLineWidth(3.0f);
		wireframeTileShader.loadInt("USE_FRUSTUM_MODE", 1);
		wireframeTileShader.loadInt("frustumCamIndex", params.getInt("Sequence#SelectedCamera"));

		const int VOXELS_PER_SIDE = 4;
		const float tile_size_inv = 1.0f / tile_size;
		const float scaling = float(VOXELS_PER_SIDE) / STEP_SIZE;

		for(int i=0; i<(int)gpuIntervals.size(); i++){
			const Interval interval = gpuIntervals[i];
			float start = (interval.start-0.5f) / (tile_size_inv * scaling);
			float stop = (interval.end-0.5f) / (tile_size_inv * scaling);

			vec3 frustumMinCorner = vec3(vec2(depthTileCoords*DEPTHMAP_TILE_WIDTH), start);
			vec3 frustumMaxCorner = vec3(vec2(depthTileCoords*DEPTHMAP_TILE_WIDTH)+vec2(DEPTHMAP_TILE_WIDTH), stop);

			wireframeTileShader.loadVec4("color", glm::vec4(1, 1, 1, 1));
			wireframeTileShader.loadVec3("frustumMinCorner", frustumMinCorner);
			wireframeTileShader.loadVec3("frustumMaxCorner", frustumMaxCorner);
			glDrawArrays(GL_POINTS, 0, 1);


			if(i == params.getInt("Interval")){
				//Draw the froxel
				wireframeTileShader.loadVec4("color", glm::vec4(1, 0, 0, 1));
				vec3 frustumDims = vec3(DEPTHMAP_TILE_WIDTH, DEPTHMAP_TILE_WIDTH, interval.end-interval.start);
				vec3 froxelStartCoords = params.getVec3("Froxel");
				vec3 froxelEndCoords = (params.getVec3("Froxel")+vec3(1.0f));
				vec3 frustumSize = (frustumMaxCorner - frustumMinCorner) / frustumDims;
				wireframeTileShader.loadVec3("frustumMinCorner", frustumMinCorner + frustumSize * froxelStartCoords);
				wireframeTileShader.loadVec3("frustumMaxCorner", frustumMinCorner + frustumSize * froxelEndCoords);
				glDrawArrays(GL_POINTS, 0, 1);
			}


		}



		wireframeTileShader.loadInt("USE_FRUSTUM_MODE", 0);

		sparseGridVolume.unbindAttribute(0);
		sparseGridVolume.unbind();
	}

	wireframeTileShader.stop();



	rayShader.start();
	rayShader.loadMat4("projectionView", camera.getProjectionViewMatrix());
	rayShader.loadMat4("transform", transform);
	if(!inSamplePoints.empty() && PROJECTION_TYPE == RENDER_PROJECTION_TYPE::PROJECT_ON_3D_CANVAS){
		RenderingBase::VAO vao;
		vao.bind();
		vao.createFloatAttribute(0, (const float*)inSamplePoints.data(), inSamplePoints.size()*4, 4, 0, GL_STATIC_DRAW);
		vao.bindAttributes({0});
		glPointSize(6.0f);
		rayShader.loadVec4("color", vec4(0, 1, 0, 1));
		glDrawArrays(GL_POINTS, 0, inSamplePoints.size());
		vao.unbindAttributes({0});
		vao.unbind();
	}
	if(!inSamplePoints.empty() && PROJECTION_TYPE == RENDER_PROJECTION_TYPE::PROJECT_ON_3D_CANVAS){
		RenderingBase::VAO vao;
		vao.bind();
		vao.createFloatAttribute(0, (const float*)outSamplePoints.data(), outSamplePoints.size()*4, 4, 0, GL_STATIC_DRAW);
		vao.bindAttributes({0});
		glPointSize(6.0f);
		rayShader.loadVec4("color", vec4(1, 0, 0.4, 1));
		glDrawArrays(GL_POINTS, 0, outSamplePoints.size());
		vao.unbindAttributes({0});
		vao.unbind();
	}
	rayShader.stop();

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
}


vec3 DDATest::project(vec3 position, int camIndex){
	vec4 p = cameraData[camIndex].cameraTransform * vec4(position, 1.0);
	return vec3(p.x, p.y, p.z);
}

vec3 DDATest::unproject(vec2 coords, int camIndex){
	vec4 p = cameraData[camIndex].cameraTransformInv * vec4(coords, 1.0, 0.0);
	return normalize(vec3(p.x, p.y, p.z));
}


vec3 DDATest::unproject(vec2 coords, float depth, int camIndex){
	vec4 p = cameraData[camIndex].cameraTransformInv * vec4(coords*depth, depth, 1.0);
	return vec3(p.x, p.y, p.z);
}

vec3 DDATest::getCamCenter(int camIndex){
	return vec3(cameraData[camIndex].camCenter);
}

vec2 DDATest::getImageSize(int camIndex){
	vec4 distortion2 = cameraData[camIndex].distortion2;
	vec2 image_size = vec2(distortion2.z, distortion2.w);
	return image_size;
}

vec2 DDATest::toPixels(vec2 NDCPixel, int camIndex) {
	vec4 K = cameraData[camIndex].K;
	vec4 distortion1 = cameraData[camIndex].distortion1;

	float fx = K.x;
	float fy = K.y;
	float cx = K.z;
	float cy = K.w;
	float k1 = distortion1.x;
	float k2 = distortion1.y;

	float r2 = NDCPixel.x * NDCPixel.x + NDCPixel.y * NDCPixel.y;
	float l = 1.0 + r2 * (k1 + r2 * k2);

	NDCPixel *= l;
	NDCPixel = vec2(fx * NDCPixel.x + cx, fy * NDCPixel.y + cy);
	return NDCPixel;
}


vec2 DDATest::toNDC(vec2 Pixel, int camIndex) {
	vec4 K = cameraData[camIndex].K;
	vec4 distortion1 = cameraData[camIndex].distortion1;

	float fx = K.x;
	float fy = K.y;
	float cx = K.z;
	float cy = K.w;
	float k1 = distortion1.x;
	float k2 = distortion1.y;

	float x_distorted = (Pixel.x - cx) / fx;
	float y_distorted = (Pixel.y - cy) / fy;

	float rDist = sqrt(x_distorted*x_distorted + y_distorted*y_distorted);

	float r = rDist;
	for(int i=0; i<5; i++){//Newton's method
		float r2 = r*r;
		float f = r * ( 1.0 + r2 * (k1 + r2 * k2)  ) - rDist;
		float fprime = 1.0 + r2 * (3.0 * k1 + r2 * 5.0 * k2);
		r -= f / fprime;
	}

	float s = r / max(rDist, 1.0E-6f);
	vec2 coordsNDC = vec2(x_distorted, y_distorted) * s;
	return coordsNDC;

}




//https://tavianator.com/cgit/dimension.git/tree/libdimension/bvh/bvh.c#n196
bool DDATest::rayVSbox(vec3 base, vec3 ray_inv, vec3 minCorner, vec3 maxCorner, float& tmin, float& tmax) {
	float tx1 = (minCorner.x - base.x) * ray_inv.x;
	float tx2 = (maxCorner.x - base.x) * ray_inv.x;

	tmin = min(tx1, tx2);
	tmax = max(tx1, tx2);

	float ty1 = (minCorner.y - base.y) * ray_inv.y;
	float ty2 = (maxCorner.y - base.y) * ray_inv.y;

	tmin = max(tmin, min(ty1, ty2));
	tmax = min(tmax, max(ty1, ty2));

	float tz1 = (minCorner.z - base.z) * ray_inv.z;
	float tz2 = (maxCorner.z - base.z) * ray_inv.z;

	tmin = max(tmin, min(tz1, tz2));
	tmax = min(tmax, max(tz1, tz2));

 	return tmax >= max(0.0f, tmin);
}

ivec2 DDATest::safeCeil(vec2 coords){
	return ivec2(coords) + mix(ivec2(0), ivec2(1), greaterThan(fract(coords), vec2(0.5f)));
}

ivec2 DDATest::safeFloor(vec2 coords){
	return ivec2(coords) - mix(ivec2(0), ivec2(1), lessThan(fract(coords), vec2(0.5f)));
}

void DDATest::computeTileFrustum(glm::ivec3 selectedTile, int camIndex) {

	const int VOXELS_PER_SIDE = 4;
	const float tile_size_inv = 1.0f / tile_size;
	const float scaling = float(VOXELS_PER_SIDE) / STEP_SIZE;


//	samplePoints.clear();
//	ivec2 pixelCoords = ivec2(params.getVec2("Pixel"));
//	vec2 uv = vec2(pixelCoords) + vec2(0.5f);
//	vec2 undistorted_uv = toNDC(uv, camIndex);
//	vec3 camCenter = getCamCenter(camIndex);
//	vec3 ray = unproject(undistorted_uv, camIndex);
//	ray /= dot(cameraData[camIndex].cameraTransformInv[2], vec4(ray, 0.0));
//	vec3 ray_inv = 1.0f / ray;
//	float tmin, tmax;
//	if(rayVSbox(camCenter, ray_inv, TSDFminCorner, TSDFmaxCorner, tmin, tmax)){
//		for(int k = int(tmin * tile_size_inv * scaling); k < int(tmax * tile_size_inv * scaling); k++){
//			float t = k / (tile_size_inv * scaling);
//			vec4 p = vec4(camCenter + ray * t, 1.0f);
//			samplePoints.push_back(p);
//		}
//	}




	vec3 minTileCorner = TSDFminCorner + vec3(selectedTile) * tile_size;
	vec3 maxTileCorner = minTileCorner + tile_size;
	minTileCorner += 0.5f * tile_size/VOXELS_PER_SIDE;
	maxTileCorner -= 0.5f * tile_size/VOXELS_PER_SIDE;


	const auto proj = [&](vec3 corner){
		vec3 proj = project(corner, camIndex);
		vec2 NDCCoords = vec2(proj) / proj.z;
		vec2 PixelCoords = toPixels(NDCCoords, camIndex);
		return vec3(PixelCoords, proj.z);
	};

	vec3 corners[8] = {
			proj(vec3(minTileCorner.x, minTileCorner.y, minTileCorner.z)),
			proj(vec3(maxTileCorner.x, minTileCorner.y, minTileCorner.z)),
			proj(vec3(minTileCorner.x, maxTileCorner.y, minTileCorner.z)),
			proj(vec3(maxTileCorner.x, maxTileCorner.y, minTileCorner.z)),
			proj(vec3(minTileCorner.x, minTileCorner.y, maxTileCorner.z)),
			proj(vec3(maxTileCorner.x, minTileCorner.y, maxTileCorner.z)),
			proj(vec3(minTileCorner.x, maxTileCorner.y, maxTileCorner.z)),
			proj(vec3(maxTileCorner.x, maxTileCorner.y, maxTileCorner.z))
	};

	vec3 minProj = corners[0];
	vec3 maxProj = corners[0];
	for(int i=1; i<8; i++){
		minProj = min(minProj, corners[i]);
		maxProj = max(maxProj, corners[i]);
	}

	vec2 image_size = getImageSize(camIndex);
	ivec2 pixelMax = min(safeCeil(vec2(maxProj)), ivec2(image_size)-1);
	ivec2 pixelMin = max(safeFloor(vec2(minProj)), ivec2(0));

	ivec2 dims = pixelMax - pixelMin + 1;
	dims = max(dims, ivec2(0));


	float frustumStart = minProj.z;
	float frustumStop = maxProj.z;

	float a = frustumStart * tile_size_inv * scaling;
	float b = frustumStop * tile_size_inv * scaling;
	int stepsBeginFrustum = int(a);
	int stepsEndFrustum = int(b)+1;

	frustumStart = stepsBeginFrustum / (tile_size_inv * scaling);
	frustumStop = stepsEndFrustum / (tile_size_inv * scaling);

	tileProjection = CubicTileProjection{pixelMin, ivec3(dims, stepsEndFrustum - stepsBeginFrustum + 1), stepsBeginFrustum, stepsEndFrustum, frustumStart, frustumStop};

	computeSamples(selectedTile, camIndex);
}

vec3 safeMod(vec3 v, vec3& intPart){
	//works with negative v
	intPart = floor(v);
	return v - intPart;
}


void DDATest::computeSamples(glm::ivec3 selectedTile, int camIndex) {
	const int VOXELS_PER_SIDE = 4;
	const float tile_size_inv = 1.0f / tile_size;

	const ivec2 sliceDims = ivec2(tileProjection.dims);
	const int sliceSize = sliceDims.x * sliceDims.y;

	sampleDistances = ivec4(0);
	for(int sliceLocalIndex=0; sliceLocalIndex < sliceSize; sliceLocalIndex++){

		int local_y = sliceLocalIndex / sliceDims.x;
		int local_x = sliceLocalIndex % sliceDims.x;
		ivec2 coordsPixel = ivec2(tileProjection.pixelMinCorner) + ivec2(local_x, local_y);

		const float frustumStart = tileProjection.minDepth;
		const float frustumStop = tileProjection.maxDepth;
		const float scaling = float(VOXELS_PER_SIDE) / STEP_SIZE;
		const int stepsBeginFrustum = int(round(frustumStart * tile_size_inv * scaling));
		const int stepsEndFrustum = int(round(frustumStop * tile_size_inv * scaling));

		const vec2 coordsNDC = toNDC(vec2(coordsPixel)+vec2(0.5f), camIndex);
		vec3 ray = unproject(coordsNDC, camIndex);
		ray /= dot(cameraData[camIndex].cameraTransformInv[2], vec4(ray, 0.0));

		const vec3 startPoint = unproject(coordsNDC, frustumStart, camIndex);
		const ivec3 tile = ivec3(floor((startPoint - TSDFminCorner) * tile_size_inv));

		const vec3 minTileCorner = vec3(tile);
		const vec3 maxTileCorner = minTileCorner + vec3(1.0f);
		const vec3 camCenter = (getCamCenter(camIndex) - TSDFminCorner) * tile_size_inv;


		float tmin, tmax;// must have tmin < t < tmax
		if(!rayVSbox(camCenter, 1.0f/ray, minTileCorner-vec3(0.001f), maxTileCorner+vec3(0.001f), tmin, tmax)) {
			//TODO //should not happen, but maybe it can ???
			continue;
		}

		tmin *= scaling;
		int stepsStart = min(int(tmin)+1, stepsBeginFrustum);

		ray *= STEP_SIZE;


		for(int step = stepsStart; step <= stepsEndFrustum; step++){
			if(step >= stepsBeginFrustum){
				vec3 p = camCenter * float(VOXELS_PER_SIDE) + ray * float(step);
//				samplePoints.push_back(vec4(p / float(VOXELS_PER_SIDE) / tile_size_inv + TSDFminCorner, 1.0f));

				vec3 intTile;
				p = safeMod(p * (1.0f / float(VOXELS_PER_SIDE)), intTile) * float(VOXELS_PER_SIDE);

				const ivec3 toCenterTile = abs(selectedTile - ivec3(intTile));
				const int d = max(toCenterTile.x, max(toCenterTile.y, toCenterTile.z));
				sampleDistances[min(d, 3)]++;
			}


		}
	}

	computeFrustumAspectRatio(selectedTile, camIndex);

}

void DDATest::computeFrustumAspectRatio(glm::ivec3 selectedTile, int camIndex) {
	const int VOXELS_PER_SIDE = 4;
	const float tile_size_inv = 1.0f / tile_size;
	const float scaling = float(VOXELS_PER_SIDE) / STEP_SIZE;
	vec3 frustumMinCorner = vec3(vec2(tileProjection.pixelMinCorner), tileProjection.minDepth - 0.5f / (tile_size_inv * scaling));
	vec3 frustumMaxCorner = vec3(vec2(tileProjection.pixelMinCorner)+vec2(tileProjection.dims), tileProjection.maxDepth + 0.5f / (tile_size_inv * scaling));

	vec3 froxelStartCoords = params.getVec3("Froxel") / vec3(tileProjection.dims);
	vec3 froxelEndCoords = (params.getVec3("Froxel")+vec3(1.0f)) / vec3(tileProjection.dims);
	vec3 frustumSize = frustumMaxCorner - frustumMinCorner;
	vec3 froxelMinCorner = frustumMinCorner + frustumSize * froxelStartCoords;
	vec3 froxelMaxCorner = frustumMinCorner + frustumSize * froxelEndCoords;


	const auto& toWorld = [&](vec3 p){
		vec2 ndc = toNDC(vec2(p), camIndex);
		return unproject(ndc, p.z, camIndex);
	};

	float dx = distance(
			toWorld(vec3(froxelMinCorner.x, froxelMinCorner.y, froxelMinCorner.z)),
			toWorld(vec3(froxelMaxCorner.x, froxelMinCorner.y, froxelMinCorner.z))
		);
	float dy = distance(
			toWorld(vec3(froxelMinCorner.x, froxelMinCorner.y, froxelMinCorner.z)),
			toWorld(vec3(froxelMinCorner.x, froxelMaxCorner.y, froxelMinCorner.z))
		);
	float dz = distance(
			toWorld(vec3(froxelMinCorner.x, froxelMinCorner.y, froxelMinCorner.z)),
			toWorld(vec3(froxelMinCorner.x, froxelMinCorner.y, froxelMaxCorner.z))
		);

	froxelRatio = vec3(dx, dy, dz) / dz;

}
