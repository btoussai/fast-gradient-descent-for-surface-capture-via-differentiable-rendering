/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * MarchingCubes.cpp
 *
 *  Created on: 7 janv. 2022
 *      Author: Briac
 */

#include "MarchingCubes.h"

#include "MarchingCubesLUT.h"
#include "CommonTypes.h"

MarchingCubes::MarchingCubes() {
	marchingCubesShader.finishInit();
	marchingCubesShader.init_uniforms({"TSDFminCorner", "TilesTotal", "tile_size", "SDF_OFFSET", "transform"});

	//Make a bigger buffer whose every 16 element is the number of triangles to generate
	GLint buff[256 + 3 * 5 * 256];
	for (int i = 0; i < 256; i++) {
		int triangles = 0;
		for (int j = 0; j < 5; j++) {
			if (::EdgesLUT[i][j][0] == -1) {
				break;
			}
			triangles++;
		}

		//std::cout <<i <<": " <<triangles <<" triangles";

		for (int j = 0; j < 5; j++) {
			//std::cout <<" [ ";
			for (int k = 0; k < 3; k++) {
				buff[16 * i + 3 * j + k + 1] = ::EdgesLUT[i][j][k];
				//std::cout <<::EdgesLUT[i][j][k] <<" ";
			}
			//std::cout <<"] ";
		}
		buff[i * 16] = triangles;

		//std::cout <<std::endl;
	}


	LUT.bind();
	LUT.storeData(&buff, sizeof(buff), GL_STATIC_DRAW);
	LUT.unbind();
}

MarchingCubes::~MarchingCubes() {
	// TODO Auto-generated destructor stub
}

int MarchingCubes::extractMesh(glm::vec3 TSDFminCorner, float tile_size, glm::ivec3 TilesTotal,
		int TilesCounter, float SDF_OFFSET,
		const std::unique_ptr<Texture3D>& tileOccupancy,
		RenderingBase::VBO& TilesDescriptorBlock,
		GLuint voxelTextures,
		RenderingBase::VBO& VerticesBlock,
		glm::mat4 transform) {
	marchingCubesShader.start();
	marchingCubesShader.loadVec3("TSDFminCorner", TSDFminCorner);
	marchingCubesShader.loadiVec3("TilesTotal", TilesTotal);
	marchingCubesShader.loadFloat("tile_size", tile_size);
	marchingCubesShader.loadFloat("SDF_OFFSET", SDF_OFFSET);
	marchingCubesShader.loadMat4("transform", transform);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, VerticesBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, TilesDescriptorBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, LUT.getID());
	glBindImageTexture(0, voxelTextures, 0, GL_FALSE, 0, GL_READ_ONLY, voxelInternalFormat);
	tileOccupancy->bindToTextureUnit(0);

	RenderingBase::VBO GlobalVertexCounter = RenderingBase::VBO(GL_ATOMIC_COUNTER_BUFFER);
	const int zero = 0;
	GlobalVertexCounter.bind();
	GlobalVertexCounter.storeData(&zero, sizeof(int), GL_STATIC_DRAW);
	GlobalVertexCounter.unbind();

	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, GlobalVertexCounter.getID());

	glDispatchCompute(TilesCounter, 1, 1);

//	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT | GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);
	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, 0);
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, 0);
	glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, voxelInternalFormat);
	tileOccupancy->unbindFromTextureUnit(0);

	marchingCubesShader.stop();

	int* ptr = (int*)glMapNamedBuffer(GlobalVertexCounter.getID(), GL_READ_ONLY);
	int TotalVertices = ptr[0];
	glUnmapNamedBuffer(GlobalVertexCounter.getID());

	return TotalVertices;
}
