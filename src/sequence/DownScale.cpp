/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * DownScale.cpp
 *
 *  Created on: 16 févr. 2022
 *      Author: Briac
 */

#include "DownScale.h"

DownScale::DownScale() {
	downscaleShader.finishInit();
	downscaleVisualHullShader.finishInit();
	upscaleVisualHullShader.finishInit();
}

DownScale::~DownScale() {
}

void DownScale::apply(Texture2DArray& src, Texture2DArray& dest, int layer) const {

	downscaleShader.start();
	const GLenum FORMAT = GL_RGBA8;
	const int width = dest.getWidth();
	const int height = dest.getHeight();

	glm::ivec2 groups = glm::ivec2(width, height) / 8;
	glBindImageTexture(0, src.getID(), 0, GL_FALSE, layer, GL_READ_ONLY, FORMAT);
	glBindImageTexture(1, dest.getID(), 0, GL_FALSE, layer, GL_WRITE_ONLY, FORMAT);
	glDispatchCompute(groups.x, groups.y, 1);
	glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
	glBindImageTexture(0, 0, 0, GL_FALSE, layer, GL_READ_ONLY, FORMAT);
	glBindImageTexture(1, 0, 0, GL_FALSE, layer, GL_WRITE_ONLY, FORMAT);
	downscaleShader.stop();

	RenderingBase::Texture2D::TextureData texData = RenderingBase::Texture2D::TextureData { src.getImagePath(layer), width,
		height, FORMAT, FORMAT, GL_UNSIGNED_BYTE, RenderingBase::Texture2D::TextureData::data_ptr()};
	dest.validateLayer(layer, std::move(texData));
}

void DownScale::apply(Texture2DArray& src, Texture2DArray& dest) const {
	const GLenum FORMAT = GL_RGBA8;
	const int width = dest.getWidth();
	const int height = dest.getHeight();
	glm::ivec2 groups = (glm::ivec2(width, height) + DEPTHMAP_TILE_WIDTH-1) / DEPTHMAP_TILE_WIDTH;

	downscaleShader.start();
	glBindImageTexture(0, src.getID(), 0, GL_TRUE, 0, GL_READ_ONLY, FORMAT);
	glBindImageTexture(1, dest.getID(), 0, GL_TRUE, 0, GL_WRITE_ONLY, FORMAT);
	glDispatchCompute(groups.x, groups.y, src.getLayers());
	glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
	glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_READ_ONLY, FORMAT);
	glBindImageTexture(1, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, FORMAT);
	downscaleShader.stop();

	for(int layer = 0; layer < src.getLayers(); layer++){
		RenderingBase::Texture2D::TextureData texData = RenderingBase::Texture2D::TextureData { src.getImagePath(layer), width,
			height, FORMAT, FORMAT, GL_UNSIGNED_BYTE, RenderingBase::Texture2D::TextureData::data_ptr()};
		dest.validateLayer(layer, std::move(texData));
	}

}

int DownScale::downscaleVisualHull(Texture2DArray &src,
		Texture2DArray &dest,
		Texture2DArray* occupancyTexture,
		RenderingBase::VBO &occupancyTiles,
		RenderingBase::VBO &tiles) const {
	const GLenum FORMAT = GL_RGBA8;
	const int width = dest.getWidth();
	const int height = dest.getHeight();
	glm::ivec2 groups = (glm::ivec2(width, height) + DEPTHMAP_TILE_WIDTH-1) / DEPTHMAP_TILE_WIDTH;

	const int zero = 0;
	RenderingBase::VBO AtomicBlock = RenderingBase::VBO(GL_ATOMIC_COUNTER_BUFFER);
	AtomicBlock.bind();
	AtomicBlock.storeData(&zero, sizeof(uint32_t), GL_STATIC_DRAW);
	AtomicBlock.unbind();

	downscaleVisualHullShader.start();
	glBindImageTexture(0, src.getID(), 0, GL_TRUE, 0, GL_READ_ONLY, FORMAT);
	glBindImageTexture(1, dest.getID(), 0, GL_TRUE, 0, GL_WRITE_ONLY, FORMAT);
	if(occupancyTexture){
		glBindImageTexture(2, occupancyTexture->getID(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_R32I);
	}
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, occupancyTiles.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, tiles.getID());
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, AtomicBlock.getID());
	glDispatchCompute(groups.x, groups.y, src.getLayers());
	glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
	glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_READ_ONLY, FORMAT);
	glBindImageTexture(1, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, FORMAT);
	glBindImageTexture(2, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, 0);
	downscaleVisualHullShader.stop();

	int* counter = (int*)glMapNamedBuffer(AtomicBlock.getID(), GL_READ_ONLY);
	int n = counter[0];
	glUnmapNamedBuffer(AtomicBlock.getID());
	return n;
}

int DownScale::upscaleVisualHull(Texture2DArray &src, Texture2DArray &dest,
		Texture2DArray* occupancyTexture,
		RenderingBase::VBO &occupancyTiles,
		RenderingBase::VBO& tiles) const {
	const GLenum FORMAT = GL_RGBA8;
	const int width = dest.getWidth();
	const int height = dest.getHeight();
	glm::ivec2 groups = (glm::ivec2(width, height) + DEPTHMAP_TILE_WIDTH-1) / DEPTHMAP_TILE_WIDTH;

	const int zero = 0;
	RenderingBase::VBO AtomicBlock = RenderingBase::VBO(GL_ATOMIC_COUNTER_BUFFER);
	AtomicBlock.bind();
	AtomicBlock.storeData(&zero, sizeof(uint32_t), GL_STATIC_DRAW);
	AtomicBlock.unbind();

	upscaleVisualHullShader.start();
	glBindImageTexture(0, src.getID(), 0, GL_TRUE, 0, GL_READ_ONLY, FORMAT);
	glBindImageTexture(1, dest.getID(), 0, GL_TRUE, 0, GL_READ_WRITE, FORMAT);
	if(occupancyTexture){
		glBindImageTexture(2, occupancyTexture->getID(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_R32I);
	}
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, occupancyTiles.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, tiles.getID());
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, AtomicBlock.getID());
	glDispatchCompute(groups.x, groups.y, src.getLayers());
	glMemoryBarrier(GL_TEXTURE_FETCH_BARRIER_BIT);
	glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_READ_ONLY, FORMAT);
	glBindImageTexture(1, 0, 0, GL_FALSE, 0, GL_READ_WRITE, FORMAT);
	glBindImageTexture(2, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32I);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, 0);
	upscaleVisualHullShader.stop();

	int* counter = (int*)glMapNamedBuffer(AtomicBlock.getID(), GL_READ_ONLY);
	int n = counter[0];
	glUnmapNamedBuffer(AtomicBlock.getID());
	return n;
}
