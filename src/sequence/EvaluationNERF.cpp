/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Evaluation.cpp
 *
 *  Created on: 16 mai 2022
 *      Author: Briac
 */

#include "../imgui/imgui.h"
#include "../tinyxml2/tinyxml2.h"

#include <filesystem>
#include <string>
#include "CommonTypes.h"
#include "EvaluationNERF.h"
#include "SceneReferenceFrame.h"

#include "../rendering_base/openglObjects.h"
#include "../stb/stb_image_write.h"

#include "../renderdoc/InitRenderDoc.h"

using namespace tinyxml2;


static inline glm::vec3 readVec3(const char *str) {
	std::stringstream ss(str);
	glm::vec3 v;
	ss >> v.x >> v.y >> v.z;
	return v;
}
static inline glm::mat3 readMat3(const char *str) {
	std::stringstream ss(str);
	glm::mat3 m;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			ss >> m[j][i];
		}
	}
	return m;
}

std::vector<CameraInfo> readCams(std::string calibTestPath){
	std::vector<CameraInfo> cameras;

	XMLDocument doc;
	doc.LoadFile(calibTestPath.c_str());
	const XMLElement *CalibrationResult = doc.RootElement();
	if (!CalibrationResult) {
		throw "Couldn't read " + calibTestPath;
	}

	const XMLElement *information = CalibrationResult->FirstChildElement(
			"Information");

	int nbCameras = std::atoi(information->Attribute("nbCamera"));
	cameras = std::vector<CameraInfo>(nbCameras);

	int n = 0;
	const XMLElement *camera = information->NextSiblingElement("Camera");
	while (camera) {
		CameraInfo &camInfo = cameras.at(n);

		camInfo.ID = std::atoi(camera->Attribute("id"));
		camInfo.width = std::atoi(camera->Attribute("width"));
		camInfo.height = std::atoi(camera->Attribute("height"));

		const XMLElement *eK = camera->FirstChildElement("K");
		const XMLElement *eDistortion = camera->FirstChildElement(
				"Distortion");
		const XMLElement *eR = camera->FirstChildElement("R");
		const XMLElement *eT = camera->FirstChildElement("T");

		const XMLAttribute* attribModel = eDistortion->FindAttribute("model");
		if(attribModel == nullptr){
//				std::cout <<"No projection model given, defaulting to 4dview" <<std::endl;
			camInfo.projection_model = PROJECTION_TYPE::PROJECTION_FOURDVIEW;
		}else{
			std::string distortionModel = attribModel->Value();
//				std::cout <<distortionModel <<std::endl;

			if(distortionModel == "opencv"){
				camInfo.projection_model = PROJECTION_TYPE::PROJECTION_OPENCV;
			}else if(distortionModel == "4dview"){
				camInfo.projection_model = PROJECTION_TYPE::PROJECTION_FOURDVIEW;
			}else{
				throw std::string(
						"Only 'opencv' and '4dview' are accepted distortion models for now");
			}
		}

		camInfo.distortion.push_back(
				std::atof(eDistortion->Attribute("K1")));
		camInfo.distortion.push_back(
				std::atof(eDistortion->Attribute("K2")));

		if(camInfo.projection_model == PROJECTION_TYPE::PROJECTION_FOURDVIEW){
			camInfo.distortion.push_back(
					std::atof(eDistortion->Attribute("Cx")));
			camInfo.distortion.push_back(
					std::atof(eDistortion->Attribute("Cy")));
		}else{
			camInfo.distortion.push_back(0);
			camInfo.distortion.push_back(0);
		}
		//camInfo.distortion.push_back(std::atof(eDistortion->Attribute("K3")));
		//ignore K3
		camInfo.distortion.push_back(0);
		//ignore P1 and P2 as well

		camInfo.K = readMat3(eK->GetText());
		camInfo.R_backup = camInfo.R = readMat3(eR->GetText());
		camInfo.T_backup = camInfo.T = readVec3(eT->GetText());

		camera = camera->NextSiblingElement("Camera");
		n++;
	}

	std::cout << "Read calibration file " << calibTestPath << " with "
			<< nbCameras << " cameras." << std::endl;

	return cameras;
}

RenderingBase::VBO camsToUBO(std::vector<CameraInfo>& cameras){
	RenderingBase::VBO CameraDataBlock = RenderingBase::VBO(GL_UNIFORM_BUFFER);
	struct CameraData {
		glm::vec4 K;				// vec4(cx, cy, fx, fy)
		glm::vec4 distortion1;
		glm::vec4 distortion2;
		glm::vec4 camCenter;
		glm::mat4 cameraTransform;
		glm::mat4 cameraTransformInv;
	};

	std::vector<CameraData> v;
	v.reserve(cameras.size());

	for (const CameraInfo &cam : cameras) {
		CameraData data;
		data.K = glm::vec4(cam.K[0][0], cam.K[1][1], cam.K[2][0], cam.K[2][1]);
		data.distortion1 = glm::vec4(cam.distortion[0], cam.distortion[1],
				cam.distortion[2], cam.distortion[3]);
		data.distortion2 = glm::vec4(cam.distortion[4], 0.0, cam.width,
				cam.height);
		data.camCenter = glm::vec4(-glm::inverse(cam.R) * cam.T, 1.0);
		glm::mat4 cameraTransform = glm::mat4(cam.R);
		cameraTransform[3] = glm::vec4(cam.T, 1.0);
		data.cameraTransform = cameraTransform;
		data.cameraTransformInv = glm::inverse(cameraTransform);
		v.push_back(data);
	}

	CameraDataBlock.bind();
	CameraDataBlock.storeData(v.data(), v.size() * sizeof(CameraData),
			GL_STATIC_DRAW);
	CameraDataBlock.unbind();


	return CameraDataBlock;
}

EvaluationNERF::EvaluationNERF() {
	// TODO Auto-generated constructor stub

}

EvaluationNERF::~EvaluationNERF() {
	// TODO Auto-generated destructor stub
}

void EvaluationNERF::updateGUIs(RenderingBase::Camera& camera,
		Backgrounds& backgrounds,
		SparseGrid& sparseGrid,
		DifferentialRendering& differentialRendering,
		AsyncLoader<ImageLoadingTask>& imageLoader) {

	ImVec2 buttonSize =  ImVec2(ImGui::GetContentRegionAvailWidth(), ImGui::GetTextLineHeight()*1.5f);
	if(ImGui::Button("Evaluate", buttonSize)){
		eval(camera, backgrounds, sparseGrid, differentialRendering, imageLoader);
	}

}

void EvaluationNERF::eval(RenderingBase::Camera &camera,
		Backgrounds &backgrounds, SparseGrid &sparseGrid,
		DifferentialRendering &differentialRendering,
		AsyncLoader<ImageLoadingTask> &imageLoader) {

	std::string exportPath0 = params.getString("Window#ExportDir") + "/eval_mesh";
	std::filesystem::create_directories(exportPath0);
	std::string exportPath1 = params.getString("Window#ExportDir") + "/eval_rasterization";
	std::filesystem::create_directories(exportPath1);
	std::string exportPath2 = params.getString("Window#ExportDir") + "/eval_volumeRendering";
	std::filesystem::create_directories(exportPath2);

	std::string calibTestPath = params.getString("Window#ExportDir") + "/calib_test.xml";
	std::vector<CameraInfo> cameras = readCams(calibTestPath);

	for (CameraInfo &cam : cameras) {
		cam.applyTransform(SceneReferenceFrame::getSceneRotation(), SceneReferenceFrame::getSceneTranslation(), SceneReferenceFrame::getSceneScale());
	}

	RenderingBase::VBO CameraDataBlock = camsToUBO(cameras);

	RenderingBase::Shader simpleShader = RenderingBase::Shader(
			"simple2/simple2.vs", "simple2/simple2.fs");
	simpleShader.bindVertexAttribute(0, "position");
	simpleShader.finishInit();
	simpleShader.init_uniforms( { "transform" });

	RenderingBase::VAO quad;
	quad.bind();
	float positions[] = { -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f };
	quad.createFloatAttribute(0, positions, 8, 2, 0, GL_STATIC_DRAW);
	quad.unbind();

	RenderingBase::FBO fbo = RenderingBase::FBO("evalFBO", 0);
	fbo.resize(cameras.at(0).width, cameras.at(0).height);
	fbo.addTextureAttachment("DEPTH", RenderingBase::AttachmentFormat_DEPTH_COMPONENT);
	fbo.addTextureAttachment("COLOR", RenderingBase::AttachmentFormat_RGBA);
	fbo.finish();

	stbi_flip_vertically_on_write(true);

	fbo.bind();
	fbo.bindColorAttachments({"COLOR"});
	fbo.setViewport();
	for(int i=0; i<(int)cameras.size(); i++){
		fbo.clearDepthAttachment(1.0f);
		fbo.clearColorAttachment("COLOR", glm::vec4(0.0));

		glm::mat4 canvas_transform = glm::mat4(1.0);
		sparseGrid.renderTestCamera(i, canvas_transform, cameras, CameraDataBlock, false);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		char* pixels = new char[fbo.getWidth() * fbo.getHeight() * 4];
		glReadPixels(0, 0, fbo.getWidth(), fbo.getHeight(), GL_RGBA, GL_UNSIGNED_BYTE, pixels);


		imageLoader.exec([pixels, exportPath0, i, width = fbo.getWidth(), height = fbo.getHeight()](){
			std::string path = exportPath0 + "/r_" + std::to_string(i) + ".png";

			std::cout <<"Writing " <<path <<std::endl;
			stbi_write_png(path.c_str(), width, height, 4, pixels, 0);
			delete[] pixels;
		});
	}
	for(int i=0; i<(int)cameras.size(); i++){
		fbo.clearDepthAttachment(1.0f);
		fbo.clearColorAttachment("COLOR", glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

		glm::mat4 canvas_transform = glm::mat4(1.0);
//
//		GLuint bgID = backgrounds.getImages(0).getID();
//
//		simpleShader.start();
//		simpleShader.loadMat4("transform", canvas_transform);
//		quad.bind();
//		quad.bindAttribute(0);
//		glBindImageTexture(0, bgID, 0, GL_FALSE, i, GL_READ_ONLY, GL_RGBA8);
//
//		glDisable(GL_DEPTH_TEST);
//		glDisable(GL_CULL_FACE);
//		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
//		glEnable(GL_DEPTH_TEST);
//		glEnable(GL_CULL_FACE);
//
//		quad.unbindAttribute(0);
//		quad.unbind();
//		simpleShader.stop();


//		GLuint dstName = fbo.getAttachment("COLOR")->getID();
//
//		glCopyImageSubData(srcName, GL_TEXTURE_2D_ARRAY, 0, 0, 0, i,
//				dstName, GL_TEXTURE_2D, 0, 0, 0, 0, cameras.at(0).width, cameras.at(0).height, 1);

		sparseGrid.renderTestCamera(i, canvas_transform, cameras, CameraDataBlock, true);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		char* pixels = new char[fbo.getWidth() * fbo.getHeight() * 4];
		glReadPixels(0, 0, fbo.getWidth(), fbo.getHeight(), GL_RGBA, GL_UNSIGNED_BYTE, pixels);


		imageLoader.exec([pixels, exportPath1, i, width = fbo.getWidth(), height = fbo.getHeight()](){
			std::string path = exportPath1 + "/r_" + std::to_string(i) + ".png";

			std::cout <<"Writing " <<path <<std::endl;
			stbi_write_png(path.c_str(), width, height, 4, pixels, 0);
			delete[] pixels;
		});
	}
	fbo.unbind();


	RenderingBase::Shader shader = RenderingBase::Shader("differentialRendering/volumeRendering.cp");
	shader.finishInit();
	shader.init_uniforms({"TSDFminCorner", "TSDFmaxCorner",
		"TSDFTilesCount", "tile_size", "STEP_SIZE", "NumCameras", "NumTiles",
		"camIndex"});

	std::vector<std::string> samplerNames(sparseGrid.getVoxelsTexturesCount());
	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		samplerNames.push_back("VoxelsTexture[" + std::to_string(i) + "]");
	}
	shader.init_uniforms(samplerNames);
	shader.start();
	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		shader.connectTextureUnit(samplerNames[i], i);
	}
	shader.stop();

	shader.start();
	shader.loadVec3("TSDFminCorner", sparseGrid.getTSDFMinCorner());
	shader.loadVec3("TSDFmaxCorner", sparseGrid.getTSDFMaxCorner());
	shader.loadiVec3("TSDFTilesCount", sparseGrid.getTilesDim());
	shader.loadFloat("tile_size", sparseGrid.getTileSize());
	shader.loadFloat("STEP_SIZE", sparseGrid.getStepSize());
	shader.loadInt("NumCameras", sparseGrid.getNbCameras());
	shader.loadInt("NumTiles", sparseGrid.getTotalTiles());

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, CameraDataBlock.getID());

	for(int i=0; i<sparseGrid.getVoxelsTexturesCount(); i++){
		sparseGrid.getVoxelsTextures()[i]->bindToTextureUnit(i);
	}
	sparseGrid.getCompressedTileOccupancy()->bindToTextureUnit(7);
	sparseGrid.getTileOccupancy()->bindToTextureUnit(8);
	backgrounds.getImages(sparseGrid.getCurrentLOD()).bindToTextureUnit(9);
	sparseGrid.getImageLoDs().at(sparseGrid.getCurrentLOD()).ExposureLUT->bindToTextureUnit(10);

	for(int i=0; i<(int)cameras.size(); i++){
		shader.loadInt("camIndex", i);
		glBindImageTexture(0, fbo.getAttachment("COLOR")->getID(), 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
		glDispatchCompute((fbo.getWidth() + 7) / 8, (fbo.getHeight() + 7) / 8, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		char* pixels = new char[fbo.getWidth() * fbo.getHeight() * 4];
		glGetTextureSubImage(fbo.getAttachment("COLOR")->getID(), 0, 0, 0, 0, fbo.getWidth(), fbo.getHeight(), 1, GL_RGBA, GL_UNSIGNED_BYTE, fbo.getWidth() * fbo.getHeight() * 4, pixels);


		imageLoader.exec([pixels, exportPath2, i, width = fbo.getWidth(), height = fbo.getHeight()](){
			std::string path = exportPath2 + "/r_" + std::to_string(i) + ".png";

			std::cout <<"Writing " <<path <<std::endl;
			stbi_write_png(path.c_str(), width, height, 4, pixels, 0);
			delete[] pixels;
		});
	}

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);


	shader.stop();

	glViewport(0, 0, camera.getWindowSize().x, camera.getWindowSize().y);
}
