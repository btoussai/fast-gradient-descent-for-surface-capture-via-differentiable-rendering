/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Sequence.h
 *
 *  Created on: 6 oct. 2021
 *      Author: Briac
 */

#ifndef SEQUENCE_SEQUENCE_H_
#define SEQUENCE_SEQUENCE_H_

#include <string>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>

#include "AsyncLoader.h"

#include "../glm/vec3.hpp"
#include "../glm/mat3x3.hpp"
#include "../glm/mat4x4.hpp"
#include "../glm/gtc/matrix_transform.hpp"

#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Parameters.h"
#include "../rendering_base/Camera.h"
#include "../rendering_base/Texture2D.h"
#include "SequenceLoader.h"
#include "AsyncTextureLoader.h"
#include "Backgrounds.h"
#include "CommonTypes.h"
#include "Texture2DArray.h"
#include "Depthmap.h"
#include "DifferentialRendering.h"
#include "DDATest.h"
#include "DownScale.h"
#include "EnvMap.h"
#include "EvaluationNERF.h"
#include "MeshSequence.h"
#include "SparseGrid.h"
#include "VisualHull.h"
#include "SharedContext.h"

class Sequence {
public:
	Sequence(SequenceInfo sequenceInfo, GLFWwindow* w);
	virtual ~Sequence();

	void buildImagePyramid();
	void prepareVolume();
	void exportMesh();
	void updateUI(RenderingBase::Camera &camera);
	void render(RenderingBase::Camera &camera);
	void generateBackgrounds(const std::string &filtersDir);
	void loadBackgrounds(const std::string &filtersDir);
	void memUsage(bool showGui);


private:
	RenderingBase::Parameters params = RenderingBase::Parameters("Sequence");
	const SequenceInfo sequenceInfo;

	RenderingBase::ShaderPreprocessor preprocessor = SequenceLoader::buildShaderPreprocessorReplacements(params, sequenceInfo);

	std::unique_ptr<Texture1D> ExposureLUT;
	std::vector<ImageLOD> imageLODs = std::vector<ImageLOD>(imagePyramidLevels);

	RenderingBase::VAO gridFloor = RenderingBase::VAO();
	RenderingBase::Shader simpleShader = RenderingBase::Shader(
			"simple/simple.vs", "simple/simple.fs");

	std::vector<glm::vec3> camera_frustum_points;
	std::unique_ptr<RenderingBase::VAO> cameraFrustumsVAO = nullptr;
	RenderingBase::Shader cameraFrustumsShader = RenderingBase::Shader(
			"cameraFrustums/cameraFrustums.vs",
			"cameraFrustums/cameraFrustums.fs");

	RenderingBase::VAO imageBorderVAO = RenderingBase::VAO();
	RenderingBase::VAO quad = RenderingBase::VAO();
	RenderingBase::Shader quadShader = RenderingBase::Shader("quad/drawQuad.vs",
			"quad/drawQuad.fs");

	DownScale downscale = DownScale();

	Backgrounds backgrounds = Backgrounds(imageLODs);

	AsyncLoader<ImageLoadingTask> imageLoader = AsyncLoader<ImageLoadingTask>();
	std::unique_ptr<SharedContext> sharedContext;

	VisualHull visualHull = VisualHull(imageLODs);
	SparseGrid sparseGrid = SparseGrid(imageLODs);
	AllDepthmaps depthmaps = AllDepthmaps(imageLODs, imageLoader);
	DifferentialRendering differentialRendering = DifferentialRendering(imageLODs, sparseGrid);
	DDATest ddaTest = DDATest();
	EnvMap envMap = EnvMap(imageLODs);
	MeshSequence meshSequence = MeshSequence();
	EvaluationNERF eval = EvaluationNERF();

	const int singleViewPrefetchCount = 5;
	const int multiViewPrefetchCount = 2;
	AsyncTextureLoader asyncTexLoader = AsyncTextureLoader(imageLODs, imageLoader, sharedContext, backgrounds);

	float volumePreparationTime = 0;



	glm::ivec2 selectedPixel = glm::ivec2();
	glm::ivec2 selectedDepthTile = glm::ivec2();

	void renderGridFloor(RenderingBase::Camera &camera);
	void renderCameraFrustums(RenderingBase::Camera &camera);
	void renderCameraFrustums2D(glm::mat4 canvas_transform);
	void drawMousePixelCoords(RenderingBase::Camera &camera,
			glm::mat4 canvas_transform);
	void drawCameraIDs(RenderingBase::Camera &camera);
	void drawCameraIDs2D(RenderingBase::Camera &camera,
			glm::mat4 canvas_transform, int selectedCameraIndex);
	void drawImageBorder(glm::mat4 canvas_transform);
	void drawCornerText(RenderingBase::Camera &camera);

	void drawImages(const std::vector<glm::mat4> &transforms, int viewpoint);
	void prepareCameras();
};

#endif /* SEQUENCE_SEQUENCE_H_ */
