/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * TextureArray.cpp
 *
 *  Created on: 25 oct. 2021
 *      Author: Briac
 */

#include "Texture2DArray.h"

#include "../stb/stb_image_write.h"
#include <sstream>
#include <assert.h>
#include <iostream>

Texture2DArray::Texture2DArray() :
		width(0), height(0), layers(0), internalformat(0), ID(0), occupancy(0) {

}

Texture2DArray::~Texture2DArray() {
	glDeleteTextures(1, &ID);
	ID = 0;
}

void Texture2DArray::init(int width, int height, int layers,
		GLenum internalformat) {
	assert(layers > 0);
	glDeleteTextures(1, &ID);
	ID = 0;

	this->width = width;
	this->height = height;
	this->layers = layers;
	this->internalformat = internalformat;
	this->occupancy = std::vector<bool>(layers);
	this->dataInfo = std::vector<RenderingBase::Texture2D::TextureData>(layers);

	GLsizei mipLevelCount = 1;

	glGenTextures(1, &ID);
	glBindTexture(GL_TEXTURE_2D_ARRAY, ID);
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, mipLevelCount, internalformat, width,
			height, layers);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
}


void Texture2DArray::uploadAllLayers(const std::vector<unsigned char>& merged_data) {
	glTextureSubImage3D(ID, 0, 0, 0, 0, width, height, layers, GL_RGBA, GL_UNSIGNED_BYTE, merged_data.data());
}

void Texture2DArray::createLayer(int layer,
		const RenderingBase::Texture2D::TextureData &data, bool clear, bool PBO) {
	assert(layer < layers);

//	std::cout <<"Creating layer " <<layer <<" of " <<data.path <<std::endl;

	if(PBO || clear){
		glTextureSubImage3D(ID, 0, 0, 0, layer, data.width, data.height, 1,
		data.format, data.type, nullptr);
	}else if(data.data){
		glTextureSubImage3D(ID, 0, 0, 0, layer, data.width, data.height, 1,
				data.format, data.type, data.data.get());
	}
	if (clear) {
		glClearTexSubImage(ID, 0, 0, 0, layer, width, height, 1, data.format,
		data.type, data.data.get());
	}

	occupancy[layer] = true;
	dataInfo[layer] = RenderingBase::Texture2D::TextureData { data.path, width,
			height, data.internalFormat, data.format, data.type, RenderingBase::Texture2D::TextureData::data_ptr()};
}

/**
 * Super slow for some reason !!!
 */
void Texture2DArray::createLayerFromTexture(int layer, RenderingBase::Texture2D& texture){
	assert(layer < layers);

	GLuint fbo;
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
			texture.getID(), 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		throw "Error while creating copy framebuffer";
	}

	glCopyTextureSubImage3D(ID, 0, 0, 0, layer, 0, 0, texture.getWidth(), texture.getHeight());


	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDeleteFramebuffers(1, &fbo);

	occupancy[layer] = true;
	dataInfo[layer] = RenderingBase::Texture2D::TextureData { texture.getPath(), width,
				height, texture.getData().internalFormat, texture.getData().format, texture.getData().type, RenderingBase::Texture2D::TextureData::data_ptr() };
}

unsigned char* Texture2DArray::getTextureData(const std::string &path,
		int layer, int nrChannels) const {
	assert(occupancy[layer]);
	const auto &data = dataInfo.at(layer);

	std::cout << "Reading texture data from gpu mem: " << path << " "
			<< data.width << "x" << data.height << std::endl;

	int buff_size = data.width * data.height * nrChannels;
	unsigned char *buff = new unsigned char[buff_size];
	glGetTextureSubImage(ID, 0, 0, 0, layer, width, height, 1, data.format,
			data.type, buff_size, buff);

	return buff;
}

std::function<void()> Texture2DArray::saveToDiskLater(const std::string &path,
		int layer, int nrChannels) const {
	assert(occupancy[layer]);
	const auto &data = dataInfo.at(layer);

	unsigned char *buff = getTextureData(path, layer, nrChannels);
	return [&, buff=buff, path=path, nrChannels=nrChannels]() {
		std::stringstream ss;
		ss << "Writing png to ";
		ss << path;
		std::cout << ss.str() << std::endl;
		stbi_write_png(path.c_str(), data.width, data.height, nrChannels,
				buff, 0);
		delete buff;
	};
}

void Texture2DArray::saveToDisk(const std::string &path,
		int layer, int nrChannels) const{
	assert(occupancy[layer]);
		const auto &data = dataInfo.at(layer);

	unsigned char *buff = getTextureData(path, layer, nrChannels);
	std::stringstream ss;
	ss << "Writing png to ";
	ss << path;
	std::cout << ss.str() << std::endl;
	stbi_write_png(path.c_str(), data.width, data.height, nrChannels,
			buff, 0);
	delete buff;
}
