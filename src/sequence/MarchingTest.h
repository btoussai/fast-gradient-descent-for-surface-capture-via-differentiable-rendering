/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * MarchingTest.h
 *
 *  Created on: 9 mars 2022
 *      Author: btoussai
 */

#ifndef SEQUENCE_MARCHINGTEST_H_
#define SEQUENCE_MARCHINGTEST_H_

#include "CommonTypes.h"

#include "../glm/glm.hpp"
#include "../rendering_base/openglObjects.h"

#include "../renderdoc/InitRenderDoc.h"
#include "Texture3D.h"
#include "Texture2DArray.h"

using namespace glm;

void MarchingTest(){


	InitRenderDoc::startFrame();
	RenderingBase::VAO quad;
	quad.bind();
	float positions[] = { -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f };
	quad.createFloatAttribute(0, positions, 8, 2, 0, GL_STATIC_DRAW);
	quad.unbind();

	RenderingBase::Shader shader = RenderingBase::Shader("copyBindlessTexture/copyBindlessTexture.vs",
			"copyBindlessTexture/copyBindlessTexture.fs");
	shader.bindVertexAttribute(0, "position");
	shader.finishInit();
	shader.start();

	RenderingBase::FBO fbo = RenderingBase::FBO("testFBO", 0);
	fbo.resize(2048, 2048);
	fbo.addTextureAttachment("DEPTH", RenderingBase::AttachmentFormat_DEPTH_COMPONENT);
	fbo.addTextureAttachment("COLOR", RenderingBase::AttachmentFormat_RGBA);
	fbo.finish();

	stbi_flip_vertically_on_write(true);
	fbo.bind();
	fbo.bindColorAttachments({"COLOR"});
	fbo.setViewport();

	for(int i=0; i<1; i++){
		std::cout <<"Reading layer " <<i <<" of ray_texels" <<std::endl;

		fbo.clearDepthAttachment(1.0f);
		fbo.clearColorAttachment("COLOR", glm::vec4(0.0, 1.0, 0.0, 1.0));

		quad.bind();
		quad.bindAttribute(0);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		quad.unbindAttribute(0);
		quad.unbind();


//		std::string exportPath = "/home/btoussai/Desktop/briac/test/r_" + std::to_string(i) + ".png";
//		char* pixels = new char[fbo.getWidth() * fbo.getHeight() * 4];
//		glReadPixels(0, 0, fbo.getWidth(), fbo.getHeight(), GL_RGBA, GL_UNSIGNED_BYTE, pixels);
//		stbi_write_png(exportPath.c_str(), width, height, 4, pixels, 0);
//		delete pixels;
	}
	shader.stop();

	fbo.unbind();

	InitRenderDoc::stopFrame();

}


#endif /* SEQUENCE_MARCHINGTEST_H_ */
