/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * MarchingCubes.h
 *
 *  Created on: 7 janv. 2022
 *      Author: Briac
 */

#ifndef SRC_SEQUENCE_MARCHINGCUBES_H_
#define SRC_SEQUENCE_MARCHINGCUBES_H_

#include <memory>

#include "../rendering_base/openglObjects.h"
#include "Texture3D.h"

class MarchingCubes {
public:
	MarchingCubes();
	virtual ~MarchingCubes();

	int extractMesh(glm::vec3 TSDFminCorner, float tile_size, glm::ivec3 TilesTotal, int TilesCounter, float SDF_OFFSET,
			const std::unique_ptr<Texture3D>& tileOccupancy,
			RenderingBase::VBO& TilesDescriptorBlock,
			GLuint voxelTextures,
			RenderingBase::VBO& VerticesBlock,
			glm::mat4 transform);
private:
	RenderingBase::Shader marchingCubesShader = RenderingBase::Shader("sparseGrid/generateVertices.cp");

	RenderingBase::VBO LUT = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
};

#endif /* SRC_SEQUENCE_MARCHINGCUBES_H_ */
