/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * CubeMap.h
 *
 *  Created on: 18 mars 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_CUBEMAP_H_
#define SEQUENCE_CUBEMAP_H_

#include "../rendering_base/openglObjects.h"

class CubeMap{
public:
	CubeMap(int size, GLenum internalFormat) :
		size(size), internalFormat(internalFormat)
	{
		glGenTextures(1, &ID);
		glBindTexture(GL_TEXTURE_CUBE_MAP, ID);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexStorage2D(GL_TEXTURE_CUBE_MAP, 1, internalFormat, size, size);
		glBindTexture(GL_TEXTURE_3D, 0);
	}
	~CubeMap(){
		glDeleteTextures(1, &ID);
	}
	CubeMap(const CubeMap&) = delete;
	CubeMap& operator=(const CubeMap&) = delete;


	void bind() const{
		glBindTexture(GL_TEXTURE_CUBE_MAP, ID);
	}

	void unbind() const{
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	}

	void bindToTextureUnit(int unit) const{
		glActiveTexture(GL_TEXTURE0 + unit);
		glBindTexture(GL_TEXTURE_CUBE_MAP, ID);
	}

	void unbindFromTextureUnit(int unit) const{
		glActiveTexture(GL_TEXTURE0 + unit);
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	}

	GLuint getID() const{
		return ID;
	}

	int getSize() const {
		return size;
	}

	GLenum getInternalFormat() const {
		return internalFormat;
	}

private:
	GLuint ID;
	const int size;
	const GLenum internalFormat;

};



#endif /* SEQUENCE_CUBEMAP_H_ */
