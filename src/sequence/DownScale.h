/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * DownScale.h
 *
 *  Created on: 16 févr. 2022
 *      Author: Briac
 */

#ifndef SRC_SEQUENCE_DOWNSCALE_H_
#define SRC_SEQUENCE_DOWNSCALE_H_

#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Texture2D.h"
#include "Texture2DArray.h"
#include <memory>

#include "CommonTypes.h"

class DownScale {
public:
	DownScale();
	virtual ~DownScale();

	void apply(Texture2DArray& src, Texture2DArray& dest, int layer) const;
	void apply(Texture2DArray& src, Texture2DArray& dest) const;
	int downscaleVisualHull(Texture2DArray& src, Texture2DArray& dest,
			Texture2DArray* occupancyTexture,
			RenderingBase::VBO& occupancyTiles,
			RenderingBase::VBO& tiles) const;
	int upscaleVisualHull(Texture2DArray& src, Texture2DArray& dest,
			Texture2DArray* occupancyTexture,
			RenderingBase::VBO& occupancyTiles,
			RenderingBase::VBO& tiles) const;

private:
	RenderingBase::Shader downscaleLayerShader = RenderingBase::Shader("downscale/downscaleLayer.cp");
	RenderingBase::Shader downscaleShader = RenderingBase::Shader("downscale/downscaleArray.cp");
	RenderingBase::Shader downscaleVisualHullShader = RenderingBase::Shader("downscale/downscaleVisualHull.cp");
	RenderingBase::Shader upscaleVisualHullShader = RenderingBase::Shader("downscale/upscaleVisualHull.cp");
};

#endif /* SRC_SEQUENCE_DOWNSCALE_H_ */
