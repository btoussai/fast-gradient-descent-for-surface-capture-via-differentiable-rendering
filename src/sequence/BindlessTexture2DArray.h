/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * BindlessTexture2DArray.h
 *
 *  Created on: 17 mai 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_BINDLESSTEXTURE2DARRAY_H_
#define SEQUENCE_BINDLESSTEXTURE2DARRAY_H_

#include "../rendering_base/openglObjects.h"

class BindlessTexture2DArray {
public:
	BindlessTexture2DArray();
	virtual ~BindlessTexture2DArray();

	void init(int width, int height, int layers, GLenum internalformat);
	void makeTextureHandlesResident(bool resident);
	void makeImageHandlesResident(bool resident, GLenum access);

	size_t memUsage(int bytesPerPixel) const {
		return size_t(width) * size_t(height) * size_t(layers) * bytesPerPixel;
	}

	const RenderingBase::VBO& getImageHandlesVBO() const {
		return imageHandlesVBO;
	}

	const RenderingBase::VBO& getTexHandlesVBO() const {
		return texHandlesVBO;
	}

	int getHeight() const {
		return height;
	}

	GLenum getInternalformat() const {
		return internalformat;
	}

	int getLayers() const {
		return layers;
	}

	int getWidth() const {
		return width;
	}

	const std::vector<GLuint>& getIDs() const {
		return IDs;
	}

	const std::vector<uint64_t>& getImageHandles() const {
		return image_handles;
	}

	const std::vector<uint64_t>& getTextureHandles() const {
		return texture_handles;
	}

private:
	int width;
	int height;
	int layers;
	GLenum internalformat;
	std::vector<GLuint> IDs;
	std::vector<uint64_t> texture_handles;
	std::vector<uint64_t> image_handles;

	RenderingBase::VBO texHandlesVBO = RenderingBase::VBO(GL_UNIFORM_BUFFER);
	RenderingBase::VBO imageHandlesVBO = RenderingBase::VBO(GL_UNIFORM_BUFFER);
};

#endif /* SEQUENCE_BINDLESSTEXTURE2DARRAY_H_ */
