/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * AsyncTextureUploader.cpp
 *
 *  Created on: 4 mai 2022
 *      Author: Briac
 */

#include "AsyncTextureLoader.h"

#include "../rendering_base/Texture2D.h"
#include <chrono>

bool FrameRequests::exists(int timestamp) {
	return requests.find(timestamp) != requests.end();
}

std::shared_ptr<Frame>& FrameRequests::get(int timestamp) {
	return requests.find(timestamp)->second;
}

std::shared_ptr<Frame>& FrameRequests::add(int timestamp, int count, bool hasSilhouettes) {
//	std::cout << "Requesting timestamp " << timestamp << " with " << count
//			<< " images." << std::endl;

	return requests.try_emplace(timestamp,
			std::make_shared<Frame>(timestamp, count, hasSilhouettes)).first->second;
}

void FrameRequests::remove(int timestamp) {
//	std::cout << "Deleting timestamp " << timestamp << std::endl;

	requests.erase(requests.find(timestamp));
}

void FrameRequests::removeOldRequests(const std::vector<int> &timestamps) {
	for (auto it = requests.cbegin(), next_it = it; it != requests.cend(); it = next_it) {
		++next_it;
		if (std::find(timestamps.begin(), timestamps.end(), it->first)
				== timestamps.end()) {
//			std::cout << "Deleting timestamp " << it->second->timestamp << std::endl;
			requests.erase(it);
		}
	}
}

AsyncTextureLoader::AsyncTextureLoader(std::vector<ImageLOD>& imageLODs,
		AsyncLoader<ImageLoadingTask>& imageLoader,
		std::unique_ptr<SharedContext>& sharedContext,
		Backgrounds& backgrounds) :
		imageLODs(imageLODs),
		imageLoader(imageLoader),
		sharedContext(sharedContext),
		backgrounds(backgrounds){

}

AsyncTextureLoader::~AsyncTextureLoader() {
}

void AsyncTextureLoader::update(std::vector<int> timestamps, int selectedTimeStamp,
		int selectedCameraIndex, bool loadAllViewPoints,
		int nbCameras,
		const std::string& sequenceDir,
		const std::string& silhouettesDir,
		bool loadSilhouettes) {

	FrameRequests& requests = loadAllViewPoints ? multiviewLoadingRequests : singleviewLoadingRequests;

	const auto& load = [&](int timestamp, std::shared_ptr<Frame> &frame, int camIndex){
		const auto &camData = imageLODs.at(0).cameras.at(camIndex);
		std::string path = camData.pathOfNthImage(sequenceDir, timestamp, false);
		std::string silhouettePath = camData.pathOfNthImage(silhouettesDir, timestamp, true);
		imageLoader.exec([camIndex, timestamp, path, silhouettePath, frame]() {
			frame->appendImage(ImageLoadingTask { camIndex, timestamp,
					RenderingBase::Texture2D::readTextureData(path) });
			if(frame->hasSilhouettes){
				frame->appendSilhouette(ImageLoadingTask { camIndex, timestamp,
						RenderingBase::Texture2D::readTextureData(silhouettePath) });
			}
		});
	};

	requests.removeOldRequests(timestamps);
	for(int timestamp : timestamps){
		if (requests.exists(timestamp)) {
			std::shared_ptr<Frame> & frame = requests.get(timestamp);
			if (frame->isCompleted()) {
				if(timestamp != selectedTimeStamp){
					frame->setUploaded(false);
				}else if(!frame->isUploaded() && allTransferBuffersAvailable){
					async_upload(frame);
				}

				if(!loadAllViewPoints && frame->images.front().camIndex != selectedCameraIndex){
					requests.remove(timestamp);
					std::shared_ptr<Frame> &frame = requests.add(timestamp, 1, loadSilhouettes);
					load(timestamp, frame, selectedCameraIndex);
				}
			}

		} else {

			if(loadAllViewPoints){
				std::shared_ptr<Frame> &frame = requests.add(timestamp, nbCameras, loadSilhouettes);
				for(int i=0; i<nbCameras; i++){
					load(timestamp, frame, i);
				}
			}else{
				std::shared_ptr<Frame> &frame = requests.add(timestamp, 1, loadSilhouettes);
				load(timestamp, frame, selectedCameraIndex);
			}
		}
	}

}

void AsyncTextureLoader::async_upload(std::shared_ptr<Frame>& frame) {
	allTransferBuffersAvailable = false;

	uint32_t width = frame->images.front().result.width;
	uint32_t height = frame->images.front().result.height;
	uint32_t data_size = (width * height * sizeof(uint32_t));

	for(TransferBuffer& buff : transfer_buffers){
		buff.realloc(data_size);
	}

	sharedContext->scheduleTask([this, data_size, frame](){
		auto t0 = std::chrono::system_clock::now();

		int k = 0;
		for (auto &t : frame->images) {
			const unsigned char* ptr = t.result.data.get();

			transfer_buffers[k].waitForSync();
			memcpy(transfer_buffers[k].mappedPtr, ptr, data_size);
			transfer_buffers[k].buffer.bind();
			imageLODs.at(0).allViewpoints.createLayer(t.camIndex, t.result, false, true);
			transfer_buffers[k].buffer.unbind();
			transfer_buffers[k].sync();
			k = (k+1) % transfer_buffers.size();
		}

		if(frame->hasSilhouettes){
			for (auto &t : frame->silhouettes) {
				const unsigned char* ptr = t.result.data.get();

				transfer_buffers[k].waitForSync();
				memcpy(transfer_buffers[k].mappedPtr, ptr, data_size);
				transfer_buffers[k].buffer.bind();
				backgrounds.getSilhouettes()->createLayer(t.camIndex, t.result, false, true);
				transfer_buffers[k].buffer.unbind();
				transfer_buffers[k].sync();
				k = (k+1) % transfer_buffers.size();
			}
		}

		for(TransferBuffer& buff : transfer_buffers){
			buff.waitForSync();
		}

		if(frame->hasSilhouettes){
			backgrounds.recombineSilhouettes(imageLODs.at(0).allViewpoints);
			glFlush();
		}

		allTransferBuffersAvailable = true;
		frame->setUploaded(true);



		auto t1 = std::chrono::system_clock::now();
		auto time = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
		std::cout <<"Uploaded "<<frame->count <<" image(s) in " <<time.count() <<"ms for timestamp " <<frame->timestamp <<std::endl;

	});

}
