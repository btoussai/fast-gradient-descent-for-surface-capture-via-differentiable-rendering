/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * SharedContext.h
 *
 *  Created on: 4 mai 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_SHAREDCONTEXT_H_
#define SEQUENCE_SHAREDCONTEXT_H_

#include "../opengl_loader/glad/glad.h"
#include <GLFW/glfw3.h>

#include <thread>
#include <functional>

#include "AsyncLoader.h"

/**
 * A background thread holding a second OpenGL context
 */
class SharedContext {
public:
	SharedContext(GLFWwindow* mainWindow);
	virtual ~SharedContext();

	void scheduleTask(std::function<void()>&& task);

private:
	GLFWwindow* w;

	std::atomic_bool should_exit = false;
	std::unique_ptr<std::thread> thread;
	ThreadSafeQueue<std::function<void()>> tasks;

	void thread_loop();
};

#endif /* SEQUENCE_SHAREDCONTEXT_H_ */
