/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Morton.h
 *
 *  Created on: 14 mars 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_MORTON_H_
#define SEQUENCE_MORTON_H_


/**
 * This is a subset of https://github.com/Forceflow/libmorton

MIT License

Copyright (c) 2016 Jeroen Baert

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

 */

// Magicbits masks (3D encode)
static uint32_t magicbit3D_masks32_encode[6] = { 0x000003ff, 0, 0x30000ff, 0x0300f00f, 0x30c30c3, 0x9249249 }; // we add a 0 on position 1 in this array to use same code for 32-bit and 64-bit cases
static uint64_t magicbit3D_masks64_encode[6] = { 0x1fffff, 0x1f00000000ffff, 0x1f0000ff0000ff, 0x100f00f00f00f00f, 0x10c30c30c30c30c3, 0x1249249249249249 };

// HELPER METHOD: Magic bits encoding (helper method)
template<typename morton, typename coord>
static inline morton morton3D_SplitBy3bits(const coord a) {
	const morton* masks = (sizeof(morton) <= 4) ? reinterpret_cast<const morton*>(magicbit3D_masks32_encode) : reinterpret_cast<const morton*>(magicbit3D_masks64_encode);
	morton x = ((morton)a) & masks[0];
	if (sizeof(morton) == 8) { x = (x | (uint64_t)x << 32) & masks[1]; } // for 64-bit case
	x = (x | x << 16) & masks[2];
	x = (x | x << 8)  & masks[3];
	x = (x | x << 4)  & masks[4];
	x = (x | x << 2)  & masks[5];
	return x;
}

// ENCODE 3D Morton code : Magic bits method
// This method uses certain bit patterns (magic bits) to split bits in the coordinates
template<typename morton, typename coord>
inline morton m3D_e_magicbits(const coord x, const coord y, const coord z) {
	return morton3D_SplitBy3bits<morton, coord>(x) | (morton3D_SplitBy3bits<morton, coord>(y) << 1) | (morton3D_SplitBy3bits<morton, coord>(z) << 2);
}


// Magicbits masks (2D encode)
constexpr uint32_t magicbit2D_masks32[6] = { 0xFFFFFFFF, 0x0000FFFF,
		0x00FF00FF, 0x0F0F0F0F, 0x33333333, 0x55555555 };
constexpr uint64_t magicbit2D_masks64[6] = { 0x00000000FFFFFFFF,
		0x0000FFFF0000FFFF, 0x00FF00FF00FF00FF, 0x0F0F0F0F0F0F0F0F,
		0x3333333333333333, 0x5555555555555555 };

template<typename morton, typename coord>
inline morton morton2D_SplitBy2Bits(const coord a) {
	const morton *masks =
			(sizeof(morton) <= 4) ?
					(const morton*)magicbit2D_masks32 : (const morton*)magicbit2D_masks64;
	morton x = a;
	if (sizeof(morton) > 4) {
		x = (x | (uint64_t) x << 32) & masks[0];
	}
	x = (x | x << 16) & masks[1];
	x = (x | x << 8) & masks[2];
	x = (x | x << 4) & masks[3];
	x = (x | x << 2) & masks[4];
	x = (x | x << 1) & masks[5];
	return x;
}

// ENCODE 2D Morton code : Magic bits
template<typename morton, typename coord>
inline morton m2D_e_magicbits(const coord x, const coord y) {
	return morton2D_SplitBy2Bits<morton, coord>(x)
			| (morton2D_SplitBy2Bits<morton, coord>(y) << 1);
}


#endif /* SEQUENCE_MORTON_H_ */
