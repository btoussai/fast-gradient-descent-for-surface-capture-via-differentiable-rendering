/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * AnimatedReconstruction.h
 *
 *  Created on: 15 nov. 2021
 *      Author: Briac
 */

#ifndef SEQUENCE_ANIMATEDRECONSTRUCTION_H_
#define SEQUENCE_ANIMATEDRECONSTRUCTION_H_

#include <string>

#include "../rendering_base/Camera.h"
#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Parameters.h"

#include "EigenPrecompiled.h"

class AnimatedReconstruction {
public:
	AnimatedReconstruction(const std::string& path);
	virtual ~AnimatedReconstruction();
	void updateUI();
	void render(const RenderingBase::Camera& camera);

private:
	void recombine(SVDdata & svd, Eigen::MatrixXd& mesh);

	int N_meshes;
	int N_verts;
	int Timestamp = 0;
	int MaxSingularValues = 0;
	float maxDist = 0;
	RenderingBase::Parameters params = RenderingBase::Parameters("AnimatedReconstruction");
	RenderingBase::VAO vao = RenderingBase::VAO();
	RenderingBase::Shader shader = RenderingBase::Shader("animatedReconstruction/animatedReconstruction.vs", "animatedReconstruction/animatedReconstruction.fs");

	Eigen::MatrixXd mesh_cano;
	Eigen::MatrixXd mesh_posed;
	SVDdata* svd_cano;
	SVDdata* svd_posed;

};

#endif /* SEQUENCE_ANIMATEDRECONSTRUCTION_H_ */
