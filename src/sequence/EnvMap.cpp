/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * EnvMap.cpp
 *
 *  Created on: 18 mars 2022
 *      Author: Briac
 */

#include "EnvMap.h"

#include "../imgui/imgui.h"

#include "CommonTypes.h"
#include "../rendering_base/ModelLoader.h"

EnvMap::EnvMap(const std::vector<ImageLOD>& imageLODs) : imageLODs(imageLODs)
{
	envMapGenShader.finishInit();
	envMapGenShader.init_uniforms({"minCorner", "maxCorner"});

	envMapRenderShader.finishInit();
	envMapRenderShader.init_uniforms({"minCorner", "maxCorner", "transform", "projectionView"});

	RenderingBase::ModelLoader::load(cube, "cube.obj", 0, false);

	const int N = 1;
	timers.reserve(N);
	for(int i=0; i<N; i++){
		timers.emplace_back(GL_TIME_ELAPSED);
	}
}

EnvMap::~EnvMap() {
}

void EnvMap::updateGUI(const Backgrounds &backgrounds) {

	ImGui::Checkbox("Enable EnvMap", &params.getBool("Enable"));

	if(!params.getBool("Enable")){
		return;
	}

	ImGui::SliderFloat3("Min corner", &params.getVec3("MinCorner").x, -8, 0, "%.3f");
	ImGui::SliderFloat3("Max corner", &params.getVec3("MaxCorner").x, 0, 8, "%.3f");

	ImVec2 buttonSize =  ImVec2(ImGui::GetContentRegionAvailWidth(), ImGui::GetTextLineHeight()*1.5f);


	if(ImGui::Button("Generate envMap", buttonSize) || true){
		auto& q = timers.at(0).push_back();
		q.begin();
		genEnvMap(backgrounds);
		q.end();
	}

	ImGui::TextColored(ImVec4(1, 0, 1, 1), "EnvMap generation: %.3f ms", timers.at(0).getLastResult(true)*1.0E-6f);


}

void EnvMap::render(const RenderingBase::Camera &camera, RenderingBase::Shader& wireframeTileShader) {
	if(!params.getBool("Enable")){
		return;
	}

	glm::mat4 transform = glm::mat4(1.0);

	if(cubemap){
		envMapRenderShader.start();
		envMapRenderShader.loadVec3("minCorner", getMinCorner());
		envMapRenderShader.loadVec3("maxCorner", getMaxCorner());
		envMapRenderShader.loadMat4("projectionView", camera.getProjectionViewMatrix());
		envMapRenderShader.loadMat4("transform", transform);

		glEnable(GL_CULL_FACE);
//		glCullFace(GL_FRONT);
		glCullFace(GL_BACK);
		cubemap->bindToTextureUnit(0);
		cube.bind();
		cube.bindAttribute(0);
		glDrawElements(GL_TRIANGLES, cube.getIndexCount(), GL_UNSIGNED_INT, 0);
		cube.unbindAttribute(0);
		cube.unbind();
		cubemap->unbindFromTextureUnit(0);

		envMapRenderShader.stop();
	}
}

void EnvMap::genEnvMap(const Backgrounds &backgrounds) {
	cubemap = std::make_unique<CubeMap>(backgrounds.getImages(envMapPyramidLevel).getWidth(), GL_RGBA8);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLODs.at(envMapPyramidLevel).CameraDataBlock.getID());
	envMapGenShader.start();
	envMapGenShader.loadVec3("minCorner", getMinCorner());
	envMapGenShader.loadVec3("maxCorner", getMaxCorner());

	backgrounds.getImages(envMapPyramidLevel).bindToTextureUnit(0);
	glBindImageTexture(0, cubemap->getID(), 0, GL_TRUE, 0, GL_WRITE_ONLY, cubemap->getInternalFormat());

	glm::ivec3 groups = glm::ivec3(cubemap->getSize() / 8, cubemap->getSize() / 8, 6);
	glDispatchCompute(groups.x, groups.y, groups.z);

	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	glBindImageTexture(0, 0, 0, GL_TRUE, 0, GL_WRITE_ONLY, cubemap->getInternalFormat());
	backgrounds.getImages(envMapPyramidLevel).unbindFromTextureUnit(0);

	envMapGenShader.stop();
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
}






