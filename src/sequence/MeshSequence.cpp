/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * MeshSequence.cpp
 *
 *  Created on: 23 mars 2022
 *      Author: Briac
 */

#include "MeshSequence.h"

#include <assimp/postprocess.h>
#include <string>
#include "../rendering_base/ModelLoader.h"
#include "../sequence/SceneReferenceFrame.h"

#include "../imgui/imgui.h"

#include "CommonTypes.h"
#include<fstream>

#include "../stb/stb_image_write.h"

MeshSequence::MeshSequence() {
	meshShader.bindVertexAttribute(0, "position");
	meshShader.bindVertexAttribute(1, "normal");
	meshShader.finishInit();
	meshShader.init_uniforms({"projection", "view", "transform", "camPos", "useColors", "PROJECTION_TYPE", "useVolumeColors"});

	texturedMeshShader.bindVertexAttribute(0, "position");
	texturedMeshShader.bindVertexAttribute(1, "normal");
	texturedMeshShader.bindVertexAttribute(2, "uv");
	texturedMeshShader.finishInit();
	texturedMeshShader.init_uniforms({"projection", "view", "transform", "camPos", "useColors", "PROJECTION_TYPE"});
}

MeshSequence::~MeshSequence() {
}

void MeshSequence::updateGUI(const std::function<void(std::function<void()>&&)>& loadMesh, int beginTimestamp, int endTimestamp) {
	const int N = 1;//prefetch

	static bool animate = false;
	static float animateTimer = 0;
	ImGui::Checkbox("Animate", &animate);
	ImGui::SameLine();
	if(ImGui::Checkbox("Use Vincent's Models", &modelesVincent)){
		//delete everything
		meshes = std::unordered_map<int, std::shared_ptr<Mesh>>();
	}
	ImGui::Checkbox("Record", &record);
	if(record){
		ImGui::SameLine();
		ImGui::Checkbox("Record Rotation", &recordRotation);
	}
	ImGui::SliderFloat("Speed", &params.getFloat("Speed"), 0.0f, 1.0f);


	int& SelectedLOD = params.getInt("SelectedLOD");
	if(!modelesVincent){
		ImGui::SliderInt("Selected LOD", &SelectedLOD, 0, imagePyramidLevels-1, "%d", ImGuiSliderFlags_AlwaysClamp);
	}

	ImGui::Checkbox("Use colors", &params.getBool("UseColors"));

	if (animate) {
		animateTimer += (1.0f / 2.0f) * params.getFloat("Speed");
		bool currentOk = meshes[timestamp] && meshes[timestamp]->isFinishedLoaded;
		bool nextOk = meshes[timestamp+1] && meshes[timestamp+1]->isFinishedLoaded;
		if ((nextOk || (N == 0 && currentOk)) && animateTimer > 1.0f) {
			animateTimer = 0.0f;
			timestamp++;
			frameWritten = false;
		}
		if (animateTimer < 0.0f) {
			animateTimer += 1.0f;
			timestamp--;
		}
	}

	const float spacing = ImGui::GetStyle().ItemInnerSpacing.x;
	ImGui::PushButtonRepeat(true);
	if (ImGui::ArrowButton("##timestampleft", ImGuiDir_Left)) {
		timestamp--;
	}
	ImGui::SameLine(0.0f, spacing);
	if (ImGui::ArrowButton("##timestampright", ImGuiDir_Right)) {
		timestamp++;
	}
	ImGui::PopButtonRepeat();
	ImGui::SameLine();
	ImGui::SliderInt("Timestamp", &timestamp, beginTimestamp, endTimestamp, "%d",ImGuiSliderFlags_AlwaysClamp);
	if(timestamp < beginTimestamp){
		timestamp = endTimestamp;
	}
	if(timestamp > endTimestamp){
		timestamp = beginTimestamp;
	}

	for(int i=timestamp; i<=timestamp+N; i++){
		if(!meshes[i]){
			meshes[i] = std::make_shared<Mesh>();
		}
		auto& m = meshes[i];

		if(!m->hasBeenInstantiated && isTransferBufferAvailable){
			m->hasBeenInstantiated = true;
			m->LOD = SelectedLOD;

			std::string fullpath;
			std::string texPath;
			if(modelesVincent){
				fullpath = "/home/btoussai/Desktop/briac/2022-05_datacube_INTERNAL_ONLY_NOT_FOR_PUBLICATIONS/leo-jea-dance/leo-jea-dance_OBJ_4DCVT_65k_110_629/OBJ_4DCVT_65k";
				texPath = fullpath + string_format("/model-%05d.png", i);
				fullpath = fullpath + string_format("/model-%05d.obj", i);
			}else{
				fullpath = params.getString("Window#ExportDir") +
						"/LOD" + std::to_string(m->LOD) +
						"/mesh_" + std::to_string(i) + ".bin";
			}

			if(std::filesystem::exists(fullpath)){
				if(modelesVincent){
					m->vao = std::make_unique<RenderingBase::VAO>();
					RenderingBase::ModelLoader::load(*m->vao, fullpath, aiProcess_GenNormals, false, "");
					m->texture = std::make_unique<RenderingBase::Texture2D>(texPath);
					m->isBeingLoaded = false;
					m->isFinishedLoaded = true;
					m->exists = true;
				}else{

					m->fileReading = true;
					m->data_size = std::filesystem::file_size(fullpath);
					m->isBeingLoaded = true;

					if(TransferBuffer.getDatasize() < m->data_size){
						std::cout <<"Re-allocating transfer buffer !" <<std::endl;
						//reallocate the vbo
						uint32_t buffsize = (uint32_t)(m->data_size * 1.3f);
						TransferBuffer = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
						glNamedBufferStorage(TransferBuffer.getID(), buffsize, nullptr, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT);
						TransferBuffer.setDataSize(buffsize);
						mappedPtr = (char*)glMapNamedBufferRange(TransferBuffer.getID(), 0, buffsize, GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_UNSYNCHRONIZED_BIT | GL_MAP_FLUSH_EXPLICIT_BIT);
					}
					isTransferBufferAvailable = false;

					if(availableVertexBuffers.empty()){
						m->VerticesBuffer = std::make_unique<RenderingBase::VBO>(GL_SHADER_STORAGE_BUFFER);
					}else{
						m->VerticesBuffer = std::move(availableVertexBuffers.front());
						availableVertexBuffers.pop_front();
					}

					if(m->VerticesBuffer->getDatasize() < m->data_size){
						std::cout <<"Re-allocating vertex buffer !" <<std::endl;
						uint32_t buffsize = (uint32_t)(m->data_size * 1.3f);
						m->VerticesBuffer->bind();
						m->VerticesBuffer->storeData(nullptr, buffsize, GL_STATIC_DRAW);
						m->VerticesBuffer->unbind();
					}


					loadMesh([this, fullpath, m](){
						auto startTime = std::chrono::system_clock::now();

						std::ifstream f(fullpath, std::ios::in | std::ios::binary);
						assert(m->data_size % sizeof(float) == 0);

						f.read(mappedPtr, m->data_size);

						auto endTime = std::chrono::system_clock::now();
						auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);
						std::cout <<"File read mesh: " <<elapsedTime.count() <<"ms" <<std::endl;

						m->fileReading = false;
						m->copying_stage = 0;

					});

				}
			}else{
				m->exists = false;
				m->isFinishedLoaded = true;
			}

		}
	}

	//erase old ones
	for (auto it = meshes.cbegin(), next_it = it; it != meshes.cend(); it = next_it) {
		++next_it;
		auto& m = it->second;

		if(m && m->isBeingLoaded){

			if(m->copying_stage == 0){
				glFlushMappedNamedBufferRange(TransferBuffer.getID(), 0, TransferBuffer.getDatasize());

				m->TotalVertices = m->data_size / (8*sizeof(float));
				glCopyNamedBufferSubData(TransferBuffer.getID(), m->VerticesBuffer->getID(), 0, 0, m->data_size);

				copySync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
				m->copying_stage++;
			}else if(m->copying_stage == 1){
				if(glClientWaitSync(copySync, GL_SYNC_FLUSH_COMMANDS_BIT, 0) == GL_ALREADY_SIGNALED){
					glDeleteSync(copySync);
					copySync = nullptr;

					m->isBeingLoaded = false;
					m->isFinishedLoaded = true;
					m->exists = true;
					m->TotalVertices = m->data_size / (8*sizeof(float));

					m->copying_stage++;
					isTransferBufferAvailable = true;
				}
			}


		}

		bool wrongTimestamp = it->first < timestamp || it->first > timestamp + N;
		bool wrongLOD = it->second && it->second->LOD != SelectedLOD;

		if (wrongTimestamp || wrongLOD) {

			if(!m || m->isFinishedLoaded){
				if(m && m->VerticesBuffer){
					availableVertexBuffers.emplace_back(std::move(it->second->VerticesBuffer));
				}
				meshes.erase(it);

			}
		}
	}

	const auto& m = meshes[timestamp];
	if(m && m->fileReading){
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Mesh %d is being loaded...", timestamp);
	}else if(!m || !m->exists){
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Mesh %d does not exist.", timestamp);
	}else if(m && m->isFinishedLoaded){
		ImGui::TextColored(ImVec4(0, 1, 0, 1), "Mesh %d has %d triangles.", timestamp, m->TotalVertices / 3);
	}
}


void MeshSequence::render(const RenderingBase::Camera &camera, const std::function<void(std::function<void()>&&)>& saveFrame) {

	auto& m = meshes[timestamp];
	if(!m || !m->exists){
		return;
	}

	glm::mat4 transform = SceneReferenceFrame::getSceneTransform();
	transform = glm::inverse(transform);

	if(modelesVincent){

		texturedMeshShader.start();
		texturedMeshShader.loadMat4("projection", camera.getProjectionMatrix());
		texturedMeshShader.loadMat4("view", camera.getViewMatrix());
		texturedMeshShader.loadMat4("transform", transform);
		texturedMeshShader.loadVec3("camPos", camera.getCameraPos());
		texturedMeshShader.loadInt("useColors", params.getBool("UseColors"));
		texturedMeshShader.loadInt("PROJECTION_TYPE", RENDER_PROJECTION_TYPE::PROJECT_ON_3D_CANVAS);

		m->texture->bindToTextureUnit(0);
		m->vao->bind();
		m->vao->bindAttributes({0, 1, 2});
		glDisable(GL_CULL_FACE);
		glDrawElements(GL_TRIANGLES, m->vao->getIndexCount(), GL_UNSIGNED_INT, nullptr);
		glEnable(GL_CULL_FACE);
		m->vao->unbindAttributes({0, 1, 2});
		m->vao->unbind();
		m->texture->unbindFromTextureUnit(0);

		texturedMeshShader.stop();
	}else{
		meshShader.start();
		meshShader.loadMat4("projection", camera.getProjectionMatrix());
		meshShader.loadMat4("view", camera.getViewMatrix());
		meshShader.loadMat4("transform", transform);
		meshShader.loadVec3("camPos", camera.getCameraPos());
		meshShader.loadInt("useColors", params.getBool("UseColors"));
		meshShader.loadInt("useVolumeColors", 0);
		meshShader.loadInt("PROJECTION_TYPE", RENDER_PROJECTION_TYPE::PROJECT_ON_3D_CANVAS);

		RenderingBase::VAO vao;
		vao.bind();
		m->VerticesBuffer->bindAs(GL_ARRAY_BUFFER);
		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)16);
		m->VerticesBuffer->unbindAs(GL_ARRAY_BUFFER);
		vao.bindAttributes({0, 1});

		glDisable(GL_CULL_FACE);
		glDrawArrays(GL_TRIANGLES, 0, m->TotalVertices);
		glEnable(GL_CULL_FACE);

		vao.unbindAttributes({0, 1});
		vao.unbind();

		meshShader.stop();
	}

	if(recordRotation){
		frameWritten = false;
	}
	if(!record){
		recordRotation = false;
	}
	if(record && !frameWritten){
		int w = camera.getWindowSize().x;
		int h = camera.getWindowSize().y;
		unsigned char* ptr = new unsigned char[4 * w * h];
		glFinish();
		glReadPixels(0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, ptr);
		std::string path;
		if(recordRotation){
			frameWritten = false;
			path = params.getString("Window#ExportDir") + "/rasterVideoRotate" + (modelesVincent?"Vincent":"Ours");
		}else{
			path = params.getString("Window#ExportDir") + "/rasterVideo" + (modelesVincent?"Vincent":"Ours");
		}
		int k = (int)round(camera.getTheta() / (2.0*M_PI) * camera.getThetaRotationSteps()) % camera.getThetaRotationSteps();

		std::string imgPath;
		if(recordRotation){
			imgPath = path + string_format("/frame-%05d.png", k);
		}else{
			imgPath = path + string_format("/frame-%05d.png", timestamp);
		}

		saveFrame([this, ptr, path, imgPath, w, h, k](){
			std::filesystem::create_directories(path);
			std::cout <<"Writting " <<imgPath <<std::endl;
			stbi_flip_vertically_on_write(true);
			stbi_write_png(imgPath.c_str(), w, h, 4, (void*)ptr, 0);
			delete[] ptr;
		});
		frameWritten = true;
	}


}

