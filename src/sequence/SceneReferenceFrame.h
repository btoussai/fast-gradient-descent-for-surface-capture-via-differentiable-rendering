/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * SceneReferenceFrame.h
 *
 *  Created on: 6 avr. 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_SCENEREFERENCEFRAME_H_
#define SEQUENCE_SCENEREFERENCEFRAME_H_

#include "../glm/mat4x4.hpp"

class SceneReferenceFrame {
public:
	static void updateMainMenu();

	static float getSceneScale();
	static glm::mat4 getScaleMatrix();
	static glm::mat3 getSceneRotation();
	static glm::vec3 getSceneTranslation();
	static float getFrustumsLength();
	static glm::mat4 getSceneTransform();
};

#endif /* SEQUENCE_SCENEREFERENCEFRAME_H_ */
