/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * PointCloud.cpp
 *
 *  Created on: 23 oct. 2021
 *      Author: Briac
 */

#include <vector>

#include "../glm/vec4.hpp"
#include "../glm/gtx/norm.hpp"
#include "../glm/gtx/string_cast.hpp"
#include "../glm/mat4x4.hpp"

#include <numbers>
#include <algorithm>

#include "SceneReferenceFrame.h"
#include "CommonTypes.h"
#include "VisualHull.h"


VisualHull::VisualHull(std::vector<ImageLOD>& imageLODs) : imageLODs(imageLODs){

	shader.bindVertexAttribute(0, "depth");
	shader.finishInit();
	shader.init_uniforms({"projectionView", "transform", "PROJECTION_TYPE",
		"canvas_transform", "selectedCamIndex", "RemoveDistortion", "PixelOffset",
		"frustumsLength"});

	borderShader.bindVertexAttribute(0, "uv");
	borderShader.finishInit();
	borderShader.init_uniforms({"canvas_transform", "RemoveDistortion", "selectedCamIndex", "viewpoint"});

	bvhShader.bindVertexAttribute(0, "Corners");
	bvhShader.bindVertexAttribute(1, "Data0");
	bvhShader.bindVertexAttribute(2, "Data1");
	bvhShader.finishInit();
	bvhShader.init_uniforms({"canvas_transform", "RemoveDistortion", "selectedCamIndex", "viewpoint", "selectedLevel"});

	raytracingShader.finishInit();
	raytracingShader.init_uniforms({"ViewCount", "MaxIterations", "PixelOffset", "TSDFminCorner", "TSDFmaxCorner"});

	writeVisualHullMaskShader.finishInit();

	const int N = 3;
	timers.reserve(N);
	for(int i=0; i<N; i++){
		timers.emplace_back(GL_TIME_ELAPSED);
	}
}

VisualHull::~VisualHull() {
}


bool VisualHull::displayStatusMsg(){

	if(ImGui::TreeNode("VisualHull status")){
		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Raytracing on borders: %.3f ms", timers.at(0).getLastResult(true)*1.0E-6);
		ImGui::TextColored(ImVec4(1, 0, 1, 1), "Build occupancy textures: %.3f ms", timers.at(1).getLastResult(true)*1.0E-6);
		ImGui::TreePop();
	}

	return false;
}

void VisualHull::updateGUI(const RenderingBase::Camera &camera, Backgrounds& backgrounds, AllDepthmaps& depthmaps, const DownScale& downscale, const SparseGrid& sparseGrid, int selectedCameraIndex) {

	ImVec2 buttonSize =  ImVec2(ImGui::GetContentRegionAvailWidth(), ImGui::GetTextLineHeight()*1.5f);

	//ImGui::Checkbox("Use shader emulator", &params.getBool("UseShaderEmulator"));
	ImGui::Checkbox("Render Points", &params.getBool("RenderPoints"));
	ImGui::SliderInt("Depthmap Selection", &params.getInt("DepthmapSelection"), -1, depthmaps.getNbCameras()-1);
	ImGui::SliderInt("BVH level", &params.getInt("BVH_level"), -1, 30);
	ImGui::SliderInt("MaxIterations", &params.getInt("MaxIterations"), 0, 15);


	const int viewpoint = params.getInt("DepthmapSelection");

	bool f12 = glfwGetKey(camera.getWindow(), GLFW_KEY_F12);

	if(ImGui::Button("Raytracing on borders", buttonSize) || f12){
		backgrounds.applySegmentation(depthmaps, -1);
		raytracingOnBorders(depthmaps, sparseGrid);
		build2DOccupancyTextures(depthmaps, downscale);
	}


	if(viewpoint != -1){
		std::stringstream ss;
		const auto& depthmap = depthmaps.depthmaps.at(viewpoint);
		ss <<"Foreground pixels tiles: " <<depthmap.pixelData.ForegroundPixels / DEPTHMAP_TILE_SIZE <<std::endl;
		ss <<"Border pixels: " <<depthmap.borderData.BorderPixels <<std::endl;
		std::string statusMsg = ss.str();
		ImGui::TextColored(ImVec4(1, 0, 1, 1), statusMsg.c_str(), "");
	}


	if(ImGui::TreeNode("Enable/Disable cameras for visual hull computation")){
		const int nCams = depthmaps.getNbCameras();
		glm::ivec4& activeCameras = params.getiVec4("activeCameras");
		static std::array<bool, MAX_CAMERAS> states = {};
		for(int i=0; i<nCams; i++){
			states[i] = isCameraActive(activeCameras, i);
		}

		ImVec2 halfButtonSize =  ImVec2(ImGui::GetContentRegionAvailWidth() / 2, ImGui::GetTextLineHeight()*1.5f);

		if(ImGui::Button("Select all", halfButtonSize)){
			for(int i=0; i<nCams; i++){
				states[i] = true;
			}
		}
		ImGui::SameLine();
		if(ImGui::Button("De-select all", halfButtonSize)){
			for(int i=0; i<nCams; i++){
				states[i] = false;
			}
		}

		const int COLUMNS_COUNT = 5;
		if (ImGui::BeginTable("table_activeCameras", COLUMNS_COUNT, ImGuiTableFlags_Borders))
		{

		int i = 0;
		for (int row = 0; row < (nCams + COLUMNS_COUNT-1) / COLUMNS_COUNT; row++)
		{
			ImGui::TableNextRow();
			for (int column = 0; column < COLUMNS_COUNT; column++)
			{
				ImGui::TableSetColumnIndex(column);
				if(i < nCams){
					char buf[32];
					sprintf(buf, "Cam %d   ", i);
					ImGui::Checkbox(buf, &states[i]);
				}
				i++;
			}
		}
		ImGui::EndTable();
		}
		for(int i=0; i<nCams; i++){
			setCameraActive(activeCameras, i, states[i]);
		}


		ImGui::TreePop();
	}


	displayStatusMsg();

}

void VisualHull::render(const RenderingBase::Camera &camera, const AllDepthmaps& depthmaps) {


	glm::mat4 transform = glm::mat4(1.0);

	if(params.getBool("RenderPoints")){
		shader.start();
		shader.loadMat4("projectionView", camera.getProjectionViewMatrix());
		shader.loadMat4("transform", transform);
		shader.loadInt("PROJECTION_TYPE", RENDER_PROJECTION_TYPE::PROJECT_ON_3D_CANVAS);
		shader.loadFloat("frustumsLength", SceneReferenceFrame::getFrustumsLength());

		glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLODs.at(segmentationPyramidLevel).CameraDataBlock.getID());
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, depthmaps.depthTileDescriptorsBlock.getID());
		imageLODs.at(segmentationPyramidLevel).allViewpoints.bindToTextureUnit(0);
		glPointSize(3.0f);

		for(int i=0; i<(int)depthmaps.depthmaps.size(); i++){
			if(params.getInt("DepthmapSelection") >= 0 && params.getInt("DepthmapSelection") != i){
				continue;
			}
			const auto& depthmap = depthmaps.depthmaps.at(i);
			const auto& pixelData = depthmap.pixelData;
			if(pixelData.ForegroundPixels <= 0){
				continue;
			}
			shader.loadInt("PixelOffset", pixelData.ForegroundPixelsOffset);
			depthmap.foregroundPixelsVAO.bind();
			depthmap.foregroundPixelsVAO.bindAttributes({0});
			glDrawArrays(GL_POINTS, 0, pixelData.ForegroundPixels);
			depthmap.foregroundPixelsVAO.unbindAttributes({0});
			depthmap.foregroundPixelsVAO.unbind();

		}

		imageLODs.at(segmentationPyramidLevel).allViewpoints.unbindFromTextureUnit(0);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
		glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
		shader.stop();
	}

}

void VisualHull::render(int selectedCameraIndex, const glm::mat4& canvas_transform, const AllDepthmaps& depthmaps, bool RemoveDistortion, bool DrawBorder, bool DrawBorderBVH){

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLODs.at(segmentationPyramidLevel).CameraDataBlock.getID());
	glPointSize(3.0f);
	glLineWidth(4.0f);

	if(params.getBool("RenderPoints")){
		shader.start();
		shader.loadInt("PROJECTION_TYPE", RENDER_PROJECTION_TYPE::PROJECT_ON_2D_CANVAS);
		shader.loadMat4("canvas_transform", canvas_transform);
		shader.loadInt("selectedCamIndex", selectedCameraIndex);
		shader.loadInt("RemoveDistortion", RemoveDistortion);
		shader.loadFloat("frustumsLength", SceneReferenceFrame::getFrustumsLength());


		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, depthmaps.depthTileDescriptorsBlock.getID());
		imageLODs.at(segmentationPyramidLevel).allViewpoints.bindToTextureUnit(0);
		for(int i=0; i<(int)depthmaps.depthmaps.size(); i++){
			if(params.getInt("DepthmapSelection") >= 0 && params.getInt("DepthmapSelection") != i){
				continue;
			}
			const auto& depthmap = depthmaps.depthmaps.at(i);
			const auto& pixelData = depthmap.pixelData;
			if(pixelData.ForegroundPixels <= 0){
				continue;
			}
			shader.loadInt("PixelOffset", pixelData.ForegroundPixelsOffset);
			depthmap.foregroundPixelsVAO.bind();
			depthmap.foregroundPixelsVAO.bindAttributes({0});
			glDrawArrays(GL_POINTS, 0, pixelData.ForegroundPixels);
			depthmap.foregroundPixelsVAO.unbindAttributes({0});
			depthmap.foregroundPixelsVAO.unbind();

		}
		imageLODs.at(segmentationPyramidLevel).allViewpoints.unbindFromTextureUnit(0);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);

		shader.stop();
	}



	const auto& depthmap = depthmaps.depthmaps.at(selectedCameraIndex);
	if(depthmap.finished_bvh){
		if(DrawBorder && depthmap.borderData.SegmentCount > 0){
			borderShader.start();
			borderShader.loadMat4("canvas_transform", canvas_transform);
			borderShader.loadInt("selectedCamIndex", selectedCameraIndex);
			borderShader.loadInt("RemoveDistortion", RemoveDistortion);
			borderShader.loadInt("viewpoint", selectedCameraIndex);
			depthmap.borderSegmentsVAO.bind();
			depthmap.borderSegmentsVAO.bindAttributes({0});
			glDrawArrays(GL_POINTS, 0, depthmap.borderData.SegmentCount);
			depthmap.borderSegmentsVAO.unbindAttributes({0});
			depthmap.borderSegmentsVAO.unbind();
			borderShader.stop();
		}

		if(DrawBorderBVH && depthmap.borderData.NodeCount > 0){
			bvhShader.start();
			bvhShader.loadMat4("canvas_transform", canvas_transform);
			bvhShader.loadInt("selectedCamIndex", selectedCameraIndex);
			bvhShader.loadInt("RemoveDistortion", RemoveDistortion);
			bvhShader.loadInt("viewpoint", selectedCameraIndex);
			bvhShader.loadInt("selectedLevel", params.getInt("BVH_level"));

			depthmap.bvhNodesVAO.bind();
			depthmap.bvhNodesVAO.bindAttributes({0, 1, 2});
			glLineWidth(1.0f);
			glDrawArrays(GL_POINTS, 0, depthmap.borderData.NodeCount);
			depthmap.bvhNodesVAO.unbindAttributes({0, 1, 2});
			depthmap.bvhNodesVAO.unbind();

			bvhShader.stop();
		}
	}

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
}


float computeUndistortedRadius(const CameraInfo& camData, glm::vec2 uv) {
	float cx = camData.K[2][0];
	float cy = camData.K[2][1];
	float fx = camData.K[0][0];
	float fy = camData.K[1][1];
	// we assume p1 = p2 = 0
	float k1 = camData.distortion.at(0);
	float k2 = camData.distortion.at(1);
	float k3 = camData.distortion.at(4);

	uv.x = (uv.x - cx) / fx;
	uv.y = (uv.y - cy) / fy;

	float rDist = glm::length(uv);

	float r = rDist;
	for(int i=0; i<10; i++){//Newton's method
		float r2 = r*r;
		float f = r * ( 1.0 + r2 * (k1 + r2 * (k2 + r2 * k3))  ) - rDist;
		float fprime = 1.0 + r2 * (3.0 * k1 + r2 * (5.0 * k2 + r2 * 7.0 * k3));
		r -= f / fprime;
	}
	return r;
};

std::vector<VisualHull::UniformData> VisualHull::buildUniformsData(const std::vector<CameraInfo> &cameras, const AllDepthmaps& depthmaps, int i) {
	const glm::vec3 camCenter_i = cameras.at(i).center();
	std::vector<UniformData> uniformsData;
	uniformsData.reserve(cameras.size());
	for(int j=0; j<(int)cameras.size(); j++){
		const auto& camData_j = cameras.at(j);

		glm::vec3 center_proj_j = camData_j.R * camCenter_i + camData_j.T;

		if(j==i){
			continue;
		}

		if(!isCameraActive(params.getiVec4("activeCameras"), j)){
			continue;
		}

		center_proj_j.x /= center_proj_j.z;
		center_proj_j.y /= center_proj_j.z;

		float w = camData_j.width;
		float h = camData_j.height;
		std::vector<glm::vec2> corners = {glm::vec2(0, 0), glm::vec2(w, 0), glm::vec2(0, h), glm::vec2(w, h)};
		float rMax = 0.0f;
		for(const glm::vec2& c : corners){
			float r = computeUndistortedRadius(camData_j, c);
			rMax = std::max(rMax, r);
		}

		float centerLength = std::hypot(center_proj_j.x, center_proj_j.y);
		float d = centerLength / rMax;

		const auto& borderData = depthmaps.depthmaps.at(j).borderData;
		uniformsData.emplace_back(UniformData{j, d, borderData.NodeOffset, borderData.SegmentOffset});
	}

	//sort the views by increasing radius
	std::sort(uniformsData.begin(), uniformsData.end(), [](const UniformData& a, const UniformData& b){
		return a.centerProjDist < b.centerProjDist;
	});

//	for(const UniformData& p : uniformsData){
//		std::cout <<p.viewIndex <<std::endl;
//	}

	return uniformsData;
}


/**
 * Computes the visual hull with 2D raytracing.
 * Each foreground pixel will have its own depth value afterwards (or zero if it is not part of the visual hull).
 * The buffer containing the depths values is depthmaps.depthPixelsBlock.
 *
 */
void VisualHull::raytracingOnBorders(const AllDepthmaps& depthmaps, const SparseGrid& sparseGrid) {

	const int viewpoint = params.getInt("DepthmapSelection");
	const int zero = 0;
	glm::uvec2 zero2 = glm::uvec2(0);
	const int negative_one = -1;
	const auto& allViewpoints = imageLODs.at(segmentationPyramidLevel).allViewpoints;
	const int w = (allViewpoints.getWidth() + DEPTHMAP_TILE_WIDTH-1) / DEPTHMAP_TILE_WIDTH;
	const int h = (allViewpoints.getHeight() + DEPTHMAP_TILE_WIDTH-1) / DEPTHMAP_TILE_WIDTH;

	auto& q0 = timers.at(0).push_back();
	q0.begin();

	const auto& segmentsVBO = depthmaps.segmentsVBO;
	const auto& bvhVBO = depthmaps.nodesVBO;

	Texture2DArray CompressedOccupancy = Texture2DArray();
	CompressedOccupancy.init(w, h, depthmaps.getNbCameras(), GL_RG32UI);
	glClearTexImage(CompressedOccupancy.getID(), 0, GL_RG_INTEGER, GL_UNSIGNED_INT, &zero2.x);


	glBindImageTexture(0, imageLODs.at(segmentationPyramidLevel).allViewpoints.getID(), 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA8);
	glBindImageTexture(1, CompressedOccupancy.getID(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_RG32UI);
	glBindBufferBase(GL_UNIFORM_BUFFER, 0, imageLODs.at(segmentationPyramidLevel).CameraDataBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, depthmaps.depthPixelsBlock.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, bvhVBO.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, segmentsVBO.getID());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, depthmaps.depthTileDescriptorsBlock.getID());

	raytracingShader.start();
	raytracingShader.loadInt("MaxIterations", params.getInt("MaxIterations"));
	raytracingShader.loadVec3("TSDFminCorner", sparseGrid.getTSDFMinCorner(false));
	raytracingShader.loadVec3("TSDFmaxCorner", sparseGrid.getTSDFMaxCorner(false));


	if(viewpoint != -1){
		raytracingOnBorders(depthmaps, viewpoint);
	}else{
		for(int i=0; i<depthmaps.getNbCameras(); i++){
			raytracingOnBorders(depthmaps, i);
		}
	}

	raytracingShader.stop();

	glMemoryBarrier(GL_ALL_BARRIER_BITS);

	glBindBufferBase(GL_UNIFORM_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, 0);
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, 0);
	glBindImageTexture(0, 0, 0, GL_TRUE, 0, GL_READ_ONLY, GL_RGBA8);
	glBindImageTexture(1, 0, 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_R32UI);



	auto& lod = imageLODs.at(segmentationPyramidLevel);
	auto& RemainingDescriptors = lod.depthTileDescriptorsBlock;
	auto& tiles = lod.depthTilesBlock;

	lod.TotalDepthTiles = (int)(1.1f * depthmaps.TotalForegroundPixelTiles);
	bool overflow = false;
	do{
		RemainingDescriptors.bind();
		RemainingDescriptors.storeData(nullptr, lod.TotalDepthTiles * sizeof(DepthTileDescriptor), GL_STATIC_DRAW);
		RemainingDescriptors.unbind();

		tiles.bind();
		tiles.storeData(nullptr, lod.TotalDepthTiles * DEPTHMAP_TILE_SIZE * sizeof(uint32_t), GL_STATIC_DRAW);
		tiles.unbind();

		auto& occupancyTexture = lod.occupancyTexture;
		occupancyTexture.init(w, h, depthmaps.getNbCameras(), GL_R32I);
		glClearTexImage(occupancyTexture.getID(), 0, GL_RED_INTEGER, GL_INT, &negative_one);

		RenderingBase::VBO AtomicBlock = RenderingBase::VBO(GL_ATOMIC_COUNTER_BUFFER);
		AtomicBlock.bind();
		AtomicBlock.storeData(&zero, sizeof(uint32_t), GL_STATIC_DRAW);
		AtomicBlock.unbind();

		writeVisualHullMaskShader.start();
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, RemainingDescriptors.getID());
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, tiles.getID());
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, AtomicBlock.getID());
		glBindImageTexture(0, allViewpoints.getID(), 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA8);
		glBindImageTexture(1, occupancyTexture.getID(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_R32I);
		glBindImageTexture(2, CompressedOccupancy.getID(), 0, GL_TRUE, 0, GL_READ_ONLY, GL_RG32UI);

		glDispatchCompute(w, h, depthmaps.getNbCameras());
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);
		glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, 0);
		glBindImageTexture(0, 0, 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA8);
		glBindImageTexture(1, 0, 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_R32I);
		glBindImageTexture(2, 0, 0, GL_TRUE, 0, GL_READ_ONLY, GL_RG32UI);
		writeVisualHullMaskShader.stop();

		int* counter = (int*)glMapNamedBuffer(AtomicBlock.getID(), GL_READ_ONLY);
		int n = counter[0];
		glUnmapNamedBuffer(AtomicBlock.getID());

		overflow = lod.TotalDepthTiles < n;
		if(overflow){
			std::cout <<"Overflow: " <<lod.TotalDepthTiles <<" allocated 2D tiles vs " << n <<" needed. Retrying..." <<std::endl;
		}
		lod.TotalDepthTiles = n;
	} while(overflow);

	q0.end();

}

void VisualHull::raytracingOnBorders(const AllDepthmaps& depthmaps, int i) {

	const int GROUP_SIZE = DEPTHMAP_TILE_SIZE;
	const auto& depthmap = depthmaps.depthmaps.at(i);

//	std::cout <<"Raytracing depthmap " <<i <<" onto borders " <<std::endl;

	std::vector<UniformData> uniformsData = buildUniformsData(imageLODs.at(segmentationPyramidLevel).cameras, depthmaps, i);


//	if(uniformsData.empty()){
//		return;
//	}

	RenderingBase::VBO UniformDataBlock = RenderingBase::VBO(GL_UNIFORM_BUFFER);
	UniformDataBlock.bind();
	UniformDataBlock.storeData(uniformsData.data(), uniformsData.size()*sizeof(glm::vec4), GL_STATIC_DRAW);
	UniformDataBlock.unbind();

	const auto& pixelData = depthmap.pixelData;
	const int pixels = pixelData.ForegroundPixels;
	assert(pixels % GROUP_SIZE == 0);
	const int groups = pixels / GROUP_SIZE;

	raytracingShader.loadInt("ViewCount", (int)uniformsData.size());
	raytracingShader.loadInt("PixelOffset", pixelData.ForegroundPixelsOffset);

	glBindBufferBase(GL_UNIFORM_BUFFER, 1, UniformDataBlock.getID());
	glDispatchCompute(groups, 1, 1);
	glBindBufferBase(GL_UNIFORM_BUFFER, 1, 0);

}

/**
 * Downscale and upscale the visual hull as necessary to establish robust occupancy accross the views.
 * After this, the depthmaps are no longer useful.
 *
 */
void VisualHull::build2DOccupancyTextures(const AllDepthmaps& depthmaps, const DownScale& downscale){

	auto& q1 = timers.at(1).push_back();
	q1.begin();


	const int negative_one = -1;

	for(int i=segmentationPyramidLevel+1; i<imagePyramidLevels; i++){
		auto& lod = imageLODs.at(i);
		//Downscale
		auto& src = imageLODs.at(i-1).allViewpoints;
		auto& dest = lod.allViewpoints;
		const int w = (dest.getWidth() + DEPTHMAP_TILE_WIDTH-1) / DEPTHMAP_TILE_WIDTH;
		const int h = (dest.getHeight() + DEPTHMAP_TILE_WIDTH-1) / DEPTHMAP_TILE_WIDTH;

		auto& occupancyTexture = lod.occupancyTexture;
		occupancyTexture.init(w, h, depthmaps.getNbCameras(), GL_R32I);
		glClearTexImage(occupancyTexture.getID(), 0, GL_RED_INTEGER, GL_INT, &negative_one);

		lod.TotalDepthTiles = (int)(1.4f * imageLODs.at(i-1).TotalDepthTiles / 4);
		bool overflow = false;
		do{
			auto& occupancyTiles = lod.depthTileDescriptorsBlock;
			auto& tiles = lod.depthTilesBlock;
			occupancyTiles.bind();
			occupancyTiles.storeData(nullptr, lod.TotalDepthTiles * sizeof(DepthTileDescriptor), GL_STATIC_DRAW);
			occupancyTiles.unbind();

			tiles.bind();
			tiles.storeData(nullptr, lod.TotalDepthTiles * DEPTHMAP_TILE_SIZE * sizeof(uint32_t), GL_STATIC_DRAW);
			tiles.unbind();

			int n = downscale.downscaleVisualHull(src, dest, &occupancyTexture, occupancyTiles, tiles);

			overflow = lod.TotalDepthTiles < n;
			if(overflow){
				std::cout <<"Overflow: " <<lod.TotalDepthTiles <<" allocated 2D tiles vs " << n <<" needed. Retrying..." <<std::endl;
			}
			lod.TotalDepthTiles = n;
		}while(overflow);

	}

	for(int i=segmentationPyramidLevel-1; i>=0; i--){
		auto& lod = imageLODs.at(i);
		//Upscale
		auto& src = imageLODs.at(i+1).allViewpoints;
		auto& dest = lod.allViewpoints;
		const int w = (dest.getWidth() + DEPTHMAP_TILE_WIDTH-1) / DEPTHMAP_TILE_WIDTH;
		const int h = (dest.getHeight() + DEPTHMAP_TILE_WIDTH-1) / DEPTHMAP_TILE_WIDTH;

		auto& occupancyTexture = lod.occupancyTexture;
		occupancyTexture.init(w, h, depthmaps.getNbCameras(), GL_R32I);
		glClearTexImage(occupancyTexture.getID(), 0, GL_RED_INTEGER, GL_INT, &negative_one);

		auto& occupancyTiles = lod.depthTileDescriptorsBlock;
		auto& tiles = lod.depthTilesBlock;

		occupancyTiles.bind();
		occupancyTiles.storeData(nullptr, 4 * imageLODs.at(i+1).TotalDepthTiles * sizeof(DepthTileDescriptor), GL_STATIC_DRAW);
		occupancyTiles.unbind();

		tiles.bind();
		tiles.storeData(nullptr, 4 * imageLODs.at(i+1).TotalDepthTiles * DEPTHMAP_TILE_SIZE * sizeof(uint32_t), GL_STATIC_DRAW);
		tiles.unbind();

		lod.TotalDepthTiles = downscale.upscaleVisualHull(src, dest, &occupancyTexture, occupancyTiles, tiles);
	}

	q1.end();
}


