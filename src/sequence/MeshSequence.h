/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * MeshSequence.h
 *
 *  Created on: 23 mars 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_MESHSEQUENCE_H_
#define SEQUENCE_MESHSEQUENCE_H_

#include "../rendering_base/Camera.h"
#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Parameters.h"
#include "../rendering_base/ModelLoader.h"
#include "../rendering_base/Texture2D.h"

#include <vector>
#include <unordered_map>
#include <memory>
#include <atomic>
#include <functional>
#include <list>

class MeshSequence {
public:
	MeshSequence();
	~MeshSequence();

	void updateGUI(const std::function<void(std::function<void()>&&)>& loadMesh, int beginTimestamp, int endTimestamp);
	void render(const RenderingBase::Camera& camera, const std::function<void(std::function<void()>&&)>& saveFrame);

private:
	RenderingBase::Parameters params = RenderingBase::Parameters("MeshSequence");

	struct Mesh{
		std::atomic_bool exists = false;
		bool hasBeenInstantiated = false;
		std::atomic_bool fileReading = false;
		std::atomic_int copying_stage = -1;

		bool isBeingLoaded = false;
		bool isFinishedLoaded = false;

		std::unique_ptr<RenderingBase::VBO> VerticesBuffer;
		uint32_t data_size = 0;
		int TotalVertices = 0;
		int LOD = 0;

		//if vincent's meshes
		std::unique_ptr<RenderingBase::VAO> vao;
		std::unique_ptr<RenderingBase::Texture2D> texture;
	};

	std::unordered_map<int, std::shared_ptr<Mesh>> meshes;

	bool isTransferBufferAvailable = true;
	RenderingBase::VBO TransferBuffer = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
	char* mappedPtr = nullptr;
	GLsync copySync = {};

	std::list<std::unique_ptr<RenderingBase::VBO>> availableVertexBuffers;


	int timestamp = 0;
	RenderingBase::Shader meshShader = RenderingBase::Shader("mesh/mesh.vs", "mesh/mesh.fs");
	RenderingBase::Shader texturedMeshShader = RenderingBase::Shader("mesh/texturedMesh.vs", "mesh/texturedMesh.fs");

	bool modelesVincent = false;
	bool record = false;
	bool recordRotation = false;
	bool frameWritten = false;
};

#endif /* SEQUENCE_MESHSEQUENCE_H_ */
