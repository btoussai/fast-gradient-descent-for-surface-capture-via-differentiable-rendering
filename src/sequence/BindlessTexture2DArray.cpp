/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * BindlessTexture2DArray.cpp
 *
 *  Created on: 17 mai 2022
 *      Author: Briac
 */

#include "BindlessTexture2DArray.h"

BindlessTexture2DArray::BindlessTexture2DArray() : width(0), height(0), layers(0), internalformat(0) {

}

BindlessTexture2DArray::~BindlessTexture2DArray() {
	glDeleteTextures(IDs.size(), IDs.data());
}

void BindlessTexture2DArray::init(int width, int height, int layers,
		GLenum internalformat) {
	assert(layers > 0);
	glDeleteTextures(IDs.size(), IDs.data());
	IDs = std::vector<GLuint>(layers);
	texture_handles = std::vector<uint64_t>();
	image_handles = std::vector<uint64_t>();

	this->width = width;
	this->height = height;
	this->layers = layers;
	this->internalformat = internalformat;

	std::vector<uint64_t> aligned_TexHandles;
	aligned_TexHandles.reserve(2 * layers);
	std::vector<uint64_t> aligned_ImgHandles;
	aligned_ImgHandles.reserve(2 * layers);

	for(int i=0; i<layers; i++){
		glGenTextures(1, &IDs.at(i));
		glBindTexture(GL_TEXTURE_2D, IDs.at(i));
		glTexStorage2D(GL_TEXTURE_2D, 1, internalformat, width, height);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_2D, 0);

		uint64_t texHandle = glGetTextureHandleARB(IDs.at(i));
		uint64_t imgHandle = glGetImageHandleARB(IDs.at(i), 0, false, 0, internalformat);

		aligned_TexHandles.push_back(texHandle);
		aligned_TexHandles.push_back(0);

		aligned_ImgHandles.push_back(imgHandle);
		aligned_ImgHandles.push_back(0);

		texture_handles.push_back(texHandle);
		image_handles.push_back(imgHandle);
	}

	texHandlesVBO.bind();
	texHandlesVBO.storeData(aligned_TexHandles.data(), aligned_TexHandles.size() * sizeof(uint64_t), GL_STATIC_DRAW);
	texHandlesVBO.unbind();

	imageHandlesVBO.bind();
	imageHandlesVBO.storeData(aligned_ImgHandles.data(), aligned_ImgHandles.size() * sizeof(uint64_t), GL_STATIC_DRAW);
	imageHandlesVBO.unbind();


}

void BindlessTexture2DArray::makeTextureHandlesResident(bool resident) {
	for(int i=0; i<layers; i++){
		if(resident){
			glMakeTextureHandleResidentARB(texture_handles.at(i));
		}else{
			glMakeTextureHandleNonResidentARB(texture_handles.at(i));
		}
	}
}

void BindlessTexture2DArray::makeImageHandlesResident(bool resident, GLenum access) {
	for(int i=0; i<layers; i++){
		if(resident){
			glMakeImageHandleResidentARB(image_handles.at(i), access);
		}else{
			glMakeImageHandleNonResidentARB(image_handles.at(i));
		}
	}
}



