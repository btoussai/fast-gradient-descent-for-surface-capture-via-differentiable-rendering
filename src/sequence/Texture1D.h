/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * Texture3D.h
 *
 *  Created on: 30 mars 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_TEXTURE1D_H_
#define SEQUENCE_TEXTURE1D_H_

#include "../rendering_base/openglObjects.h"

class Texture1D {
public:
	Texture1D(int width, GLenum internalFormat) :
		width(width), internalFormat(internalFormat)
	{
		glGenTextures(1, &ID);
		glBindTexture(GL_TEXTURE_1D, ID);
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexStorage1D(GL_TEXTURE_1D, 1, internalFormat, width);
		glBindTexture(GL_TEXTURE_1D, 0);
	}
	~Texture1D(){
		glDeleteTextures(1, &ID);
	}
	Texture1D(const Texture1D&) = delete;
	Texture1D& operator=(const Texture1D&) = delete;

	void bind() const{
		glBindTexture(GL_TEXTURE_1D, ID);
	}

	void unbind() const{
		glBindTexture(GL_TEXTURE_1D, 0);
	}

	void bindToTextureUnit(int unit) const{
		glActiveTexture(GL_TEXTURE0 + unit);
		glBindTexture(GL_TEXTURE_1D, ID);
	}

	void unbindFromTextureUnit(int unit) const{
		glActiveTexture(GL_TEXTURE0 + unit);
		glBindTexture(GL_TEXTURE_1D, 0);
	}

	GLuint getID() const{
		return ID;
	}

	int getWidth() const {
		return width;
	}

private:
	GLuint ID = 0;
	int width;
	GLenum internalFormat;
};

#endif /* SEQUENCE_TEXTURE1D_H_ */
