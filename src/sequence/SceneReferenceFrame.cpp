/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * SceneReferenceFrame.cpp
 *
 *  Created on: 6 avr. 2022
 *      Author: Briac
 */

#include "SceneReferenceFrame.h"

#include "../imgui/imgui.h"

#include "../rendering_base/Parameters.h"
#include "../glm/mat3x3.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtx/euler_angles.hpp"

RenderingBase::Parameters params = RenderingBase::Parameters("SceneReferenceFrame");

static float& sceneScale = params.getFloat("SceneScale");
static float& frustumsLength = params.getFloat("FrustumsLength");

static glm::vec3& euler_angles = params.getVec3("Rotation");
static glm::vec3& translation = params.getVec3("Translation");

static bool sceneScaleUpdated = true;
static bool frustumsLengthUpdated = true;
static bool frameTransformUpdated = true;

static float toRads(float angle){
	return angle / 180.0 * (float)std::numbers::pi;
}

void SceneReferenceFrame::updateMainMenu() {

	if (ImGui::BeginMenu("Scene reference frame")) {

		sceneScaleUpdated = ImGui::SliderFloat("Scene scale", &sceneScale, 1.0E-3f, 10.0f, "%.4f", ImGuiSliderFlags_Logarithmic);
		frustumsLengthUpdated = ImGui::SliderFloat("Frustums length", &frustumsLength, 0, 1.0f);

		frameTransformUpdated |= ImGui::SliderFloat("Scene rotation X", &euler_angles.x, -180.0f, 180.0f);
		frameTransformUpdated |= ImGui::SliderFloat("Scene rotation Z", &euler_angles.z, -180.0f, 180.0f);
		frameTransformUpdated |= ImGui::SliderFloat("Scene rotation Y", &euler_angles.y, -180.0f, 180.0f);
		frameTransformUpdated |= ImGui::SliderFloat("Scene translation X", &translation.x, -10, 10);
		frameTransformUpdated |= ImGui::SliderFloat("Scene translation Y", &translation.y, -10, 10);
		frameTransformUpdated |= ImGui::SliderFloat("Scene translation Z", &translation.z, -10, 10);


		ImGui::EndMenu();
	}

}

glm::mat4 SceneReferenceFrame::getScaleMatrix(){
	//Hack for dtu
	glm::mat4 m = glm::mat4(1.0);
	m[0] = params.getVec4("ScaleMatrixRow0");
	m[1] = params.getVec4("ScaleMatrixRow1");
	m[2] = params.getVec4("ScaleMatrixRow2");
	m[3] = params.getVec4("ScaleMatrixRow3");
	m = glm::transpose(m);
	return m;
}

glm::mat3 SceneReferenceFrame::getSceneRotation() {
	return glm::eulerAngleXZY(toRads(euler_angles.x), toRads(euler_angles.z), toRads(euler_angles.y));
}

glm::vec3 SceneReferenceFrame::getSceneTranslation() {
	return translation;
}

float SceneReferenceFrame::getSceneScale() {
	return sceneScale;
}

float SceneReferenceFrame::getFrustumsLength() {
	return frustumsLength;
}

glm::mat4 SceneReferenceFrame::getSceneTransform() {
	glm::mat4 transform = glm::mat4(1.0);
	glm::mat3 R = SceneReferenceFrame::getSceneRotation();
	glm::vec3 T = SceneReferenceFrame::getSceneTranslation();
	float s = SceneReferenceFrame::getSceneScale();

	transform = glm::mat4(R * s);
	transform[3] = glm::vec4(T, 1.0);
	return transform;
}
