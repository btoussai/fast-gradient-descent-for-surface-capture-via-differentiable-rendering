/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * SequenceLoader.h
 *
 *  Created on: 20 mai 2022
 *      Author: Briac
 */

#ifndef SEQUENCE_SEQUENCELOADER_H_
#define SEQUENCE_SEQUENCELOADER_H_

#include "CommonTypes.h"
#include "../rendering_base/Parameters.h"

class SequenceLoader {
public:
	static SequenceInfo load(
			const std::string &sequencefilepath,
			const std::string &calibrationfilepath);

	static RenderingBase::ShaderPreprocessor buildShaderPreprocessorReplacements(
			RenderingBase::Parameters& params,
			const SequenceInfo& sequenceInfo);
};

#endif /* SEQUENCE_SEQUENCELOADER_H_ */
