/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * CommonTypes.h
 *
 *  Created on: 7 oct. 2021
 *      Author: Briac
 */

#ifndef SEQUENCE_COMMONTYPES_H_
#define SEQUENCE_COMMONTYPES_H_

#include <string>
#include <vector>
#include <memory>
#include <assert.h>
#include <numbers>
#include <mutex>
#include <filesystem>

#include "../glm/vec3.hpp"
#include "../glm/vec4.hpp"
#include "../glm/mat3x3.hpp"
#include "../glm/gtc/matrix_transform.hpp"
#include "../glm/gtc/type_precision.hpp"
#include "../glm/detail/type_half.hpp"

#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Texture2D.h"
#include "Texture2DArray.h"
#include "Texture1D.h"

#include "../imgui/imgui.h"

static inline void HelpMarker(const char *desc) {
	ImGui::SameLine();
	ImGui::TextDisabled("(?)");
	if (ImGui::IsItemHovered()) {
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
		ImGui::TextUnformatted(desc);
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
}

static inline std::string prettyPrintMem(size_t bytes){
	std::stringstream ss;
	ss <<std::fixed <<std::setprecision(2);

	int n = log10(bytes) / 3;

	if(n > 0 && n <= 3){
		ss <<bytes * pow(10, -n * 3);
	}else{
		ss <<bytes;
	}

	switch(n){
	case 1:
		ss <<" kB";
		break;
	case 2:
		ss <<" MB";
		break;
	case 3:
		ss <<" GB";
		break;
	default:
		ss <<" bytes";
	}

	return ss.str();
}

static inline std::string prettyPrintQuantity(size_t count){
	std::stringstream ss;
	ss <<std::fixed <<std::setprecision(2);

	int n = log10(count) / 3;

	if(n > 0 && n <= 3){
		ss <<count * pow(10, -n * 3);
	}else{
		ss <<count;
	}

	switch(n){
	case 1:
		ss <<" k";
		break;
	case 2:
		ss <<" M";
		break;
	case 3:
		ss <<" G";
		break;
	default:
		ss <<" ";
	}

	return ss.str();
}

typedef std::unique_ptr<RenderingBase::Texture2D> TexturePtr;
typedef std::vector<std::unique_ptr<RenderingBase::Texture2D>> TextureVector;

//https://stackoverflow.com/questions/2342162/stdstring-formatting-like-sprintf
template<typename ... Args>
static inline std::string string_format( const std::string& format, Args ... args )
{
    int size_s = std::snprintf( nullptr, 0, format.c_str(), args ... ) + 1; // Extra space for '\0'
    if( size_s <= 0 ){
    	throw std::string( "Error during formatting." );
    }
    auto size = static_cast<size_t>( size_s );
    std::unique_ptr<char[]> buf = std::make_unique<char[]>( size );
    std::snprintf( buf.get(), size, format.c_str(), args ... );

    return std::string(buf.get());
}

enum PROJECTION_TYPE{
	UNKNOWN = 0,
	PROJECTION_OPENCV = 1,
	PROJECTION_FOURDVIEW = 2
};


struct CameraInfo{
	int ID;
	std::string path;
	std::string silhouettePath;
	std::string fileformat;
	int beginId = 0;
	int endId = 0;
	int width = 0;
	int height = 0;
	glm::mat3 K;
	glm::mat3 R;
	glm::vec3 T;
	glm::mat3 R_backup;
	glm::vec3 T_backup;
	PROJECTION_TYPE projection_model = UNKNOWN;
	std::vector<float> distortion;

	std::string pathOfNthImage(const std::string& sequenceDir, int t, bool silhouette) const{
		assert(t >= beginId && t <= endId);

		std::string imgPath = (silhouette ? silhouettePath : path) + "/" + string_format(fileformat, t);

		std::filesystem::path path(imgPath); // Construct the path from a string.
		if (path.is_absolute()) {
		    return imgPath;
		}else {
			return sequenceDir + "/" + imgPath;
		}

	}

	void applyTransform(glm::mat3 rotation, glm::vec3 translation, float scale){
		R = R_backup * rotation;
		T = (T_backup + R_backup * translation) / scale;
	}

	glm::vec3 center() const{
		return -glm::transpose(R) * T;
	}

	glm::ivec2 size() const{
		return glm::ivec2(width, height);
	}


	/**
	 * coords in [0, image_size] returns in [-1, 1]
	 */
	glm::vec2 toNDC(glm::vec2 Pixel, bool undistort=true) const{
		glm::vec4 distortion1 = glm::vec4(distortion.at(0),distortion.at(1), distortion.at(2), distortion.at(3));
		[[maybe_unused]]glm::vec4 distortion2 = glm::vec4(distortion.at(4), 0.0f, width, height);
		glm::vec4 K = glm::vec4(this->K[0][0], this->K[1][1], this->K[2][0], this->K[2][1]);

		switch(projection_model){
		case PROJECTION_FOURDVIEW:
		{
			float fx = K.x;
			float fy = K.y;
			float cx = K.z;
			float cy = K.w;
			float k1 = distortion1.x;
			float k2 = distortion1.y;
			float cx_dist = distortion1.z;
			float cy_dist = distortion1.w;

			if(undistort){
				float dxl = Pixel.x - cx_dist;
				float dyl = Pixel.y - cy_dist;
				// rd = ru * (1 + k1 * ru2 + k2 * ru4)
				float rd = sqrt(dxl*dxl + dyl*dyl) / 1000.0;
				float ru = rd;
				for(int i=0; i<5; i++){//Newton's method
					float ru2 = ru*ru;
					float ru3 = ru*ru2;
					float ru4 = ru2*ru2;
					float f = ru * (1.0 + k1*ru2 + k2*ru4) - rd;
					float fprime = 1.0 + 3.0*k1*ru2 + 4*k2*ru3;
					ru -= f / fprime;;
				}

				Pixel = glm::vec2(cx_dist, cy_dist) + glm::vec2(dxl, dyl) * ru / std::max(rd, 1.0E-6f);//Avoid division by zero
			}

			return glm::vec2(Pixel.x - cx, Pixel.y - cy) / glm::vec2(fx, fy);
		}
		case PROJECTION_OPENCV:
		{
			float fx = K.x;
			float fy = K.y;
			float cx = K.z;
			float cy = K.w;
			float k1 = distortion1.x;
			float k2 = distortion1.y;

			float x_distorted = (Pixel.x - cx) / fx;
			float y_distorted = (Pixel.y - cy) / fy;
			glm::vec2 coordsNDC = glm::vec2(x_distorted, y_distorted);

			if(undistort){
				float rDist = sqrt(x_distorted*x_distorted + y_distorted*y_distorted);
				float r = rDist;
				for(int i=0; i<5; i++){//Newton's method
					float r2 = r*r;
					float f = r * ( 1.0 + r2 * (k1 + r2 * k2)  ) - rDist;
					float fprime = 1.0 + r2 * (3.0 * k1 + r2 * 5.0 * k2);
					r -= f / fprime;
				}
				float s = r / std::max(rDist, 1.0E-6f);
				coordsNDC *= s;
			}

			return coordsNDC;
		}
		default:
			throw "unknown projection model";
		}

	}

	glm::vec2 toPixels(glm::vec2 NDCPixel, bool distort=true) const {
		glm::vec4 distortion1 = glm::vec4(distortion.at(0),distortion.at(1), distortion.at(2), distortion.at(3));
		[[maybe_unused]]glm::vec4 distortion2 = glm::vec4(distortion.at(4), 0.0f, width, height);
		glm::vec4 K = glm::vec4(this->K[0][0], this->K[1][1], this->K[2][0], this->K[2][1]);

		switch(projection_model){
			case PROJECTION_FOURDVIEW:
			{
				float fx = K.x;
				float fy = K.y;
				float cx = K.z;
				float cy = K.w;
				float k1 = distortion1.x;
				float k2 = distortion1.y;
				float cx_dist = distortion1.z;
				float cy_dist = distortion1.w;

				NDCPixel = glm::vec2(fx * NDCPixel.x + cx, fy * NDCPixel.y + cy);

				if(distort){
					float dx = (NDCPixel.x - cx_dist) / 1000.0;
					float dy = (NDCPixel.y - cy_dist) / 1000.0;

					float r2 = dx*dx + dy*dy;
					float l = 1.0 + r2 * (k1 + r2 * k2);

					//Distorted coordinates
					float x_predicted = cx_dist + dx * l * 1000.0;
					float y_predicted = cy_dist + dy * l * 1000.0;

					NDCPixel = glm::vec2(x_predicted, y_predicted);
				}

				return NDCPixel;
			}
			case PROJECTION_OPENCV: {
				float fx = K.x;
				float fy = K.y;
				float cx = K.z;
				float cy = K.w;
				float k1 = distortion1.x;
				float k2 = distortion1.y;

				if(distort){
					float r2 = NDCPixel.x * NDCPixel.x + NDCPixel.y * NDCPixel.y;
					float l = 1.0 + r2 * (k1 + r2 * k2);
					NDCPixel *= l;
				}

				NDCPixel = glm::vec2(fx * NDCPixel.x + cx, fy * NDCPixel.y + cy);
				return NDCPixel;
			}
			default:
			{
				throw "unknown projection model";
			}
		}
	}


	glm::vec3 project(glm::vec3 position) const{
		return R * position + T;
	}

	glm::vec3 unproject(glm::vec2 coords, float depth) const{
		glm::mat4 cameraTransform = glm::mat4(R);
		cameraTransform[3] = glm::vec4(T, 1.0);

		glm::vec4 p = glm::inverse(cameraTransform) * glm::vec4(coords * depth, depth, 1.0);
		return glm::vec3(p.x, p.y, p.z);
	}

	glm::vec3 unproject(glm::vec2 coords) const{
		glm::mat4 cameraTransform = glm::mat4(R);
		cameraTransform[3] = glm::vec4(T, 1.0);

		glm::vec4 p = glm::inverse(cameraTransform) * glm::vec4(coords, 1.0, 0.0);
		return glm::normalize(glm::vec3(p.x, p.y, p.z));
	}

	glm::vec3 getCamCenter() const{
		return glm::vec3(-glm::inverse(R) * T);
	}

	glm::vec2 getImageSize() const{
		glm::vec2 image_size = glm::vec2(width, height);
		return image_size;
	}

	bool isInsideImageRange(glm::vec2 pixelUV, glm::vec2 image_size) {
		return pixelUV.x > 0.0f && pixelUV.x < image_size.x && pixelUV.y > 0.0f && pixelUV.y < image_size.y;
	}
	bool isInsideImageRange(glm::vec2 pixelUV, glm::vec2 image_size, float margin) {
		return pixelUV.x > margin && pixelUV.x < image_size.x-margin && pixelUV.y > margin && pixelUV.y < image_size.y - margin;
	}

};

struct SequenceInfo{
	std::vector<CameraInfo> cameras;
	int nbCameras;
	float record_fps;
	int beginId;
	int endId;
	std::string sequenceDir;
	std::string silhouettesDir;
	bool hasSilhouettes;
	PROJECTION_TYPE projection_model;
};

struct ImageLOD{
	std::vector<CameraInfo> cameras;
	RenderingBase::VBO CameraDataBlock = RenderingBase::VBO(GL_UNIFORM_BUFFER);
	Texture2DArray allViewpoints; //All the viewpoints at a given timestamp and a given resolution
	Texture2DArray occupancyTexture;//Each texel is a pointer to the depth tile
	int TotalDepthTiles=0;
	RenderingBase::VBO depthTileDescriptorsBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);//An array of DepthTileDescriptors
	RenderingBase::VBO depthTilesBlock = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);//The foreground pixels
	Texture1D* ExposureLUT = nullptr;
};

struct ImageLoadingTask{
	int camIndex;
	int timestamp;
	RenderingBase::Texture2D::TextureData result;
};

struct AABB{
	glm::vec2 minCorner = glm::vec2(0.0f);
	glm::vec2 maxCorner = glm::vec2(0.0f);

	bool contains(const AABB& other) const {
		if(minCorner.x <= other.minCorner.x && maxCorner.x >= other.maxCorner.x){
			if(minCorner.y <= other.minCorner.y && maxCorner.y >= other.maxCorner.y){
				return true;
			}
		}
		return false;
	}
	bool intersects(const AABB &other) const {
		if(maxCorner.x > other.minCorner.x && minCorner.x < other.maxCorner.x){
			if(maxCorner.y > other.minCorner.y && minCorner.y < other.maxCorner.y){
				return true;
			}
		}
		return false;
	}

	float surfaceArea() const{
		float dx = maxCorner.x - minCorner.x;
		float dy = maxCorner.y - minCorner.y;
		return dx + dy;
	}

	static AABB unionAABB(AABB& box1, AABB& box2){
		glm::vec2 minCorner = glm::min(box1.minCorner, box2.minCorner);
		glm::vec2 maxCorner = glm::max(box1.maxCorner, box2.maxCorner);
		return AABB{minCorner, maxCorner};
	}
};

struct segment {
	glm::vec2 a, b;
};

struct BVHNode{
	AABB bounds;
	int childL;
	int childR;
	int parent;
	int handle;
};

enum RENDER_PROJECTION_TYPE{
	PROJECT_ON_2D_CANVAS = 0,
	PROJECT_ON_3D_CANVAS = 1
};

struct f16vec2{
	glm::detail::hdata x, y;
	f16vec2() : f16vec2(glm::vec2(0.0f)){
	}
	f16vec2(glm::vec2 v){
		x = glm::detail::toFloat16(v.x);
		y = glm::detail::toFloat16(v.y);
	}
	f16vec2(float a, float b){
		x = glm::detail::toFloat16(a);
		y = glm::detail::toFloat16(b);
	}
	f16vec2(float a){
		x = glm::detail::toFloat16(a);
		y = glm::detail::toFloat16(a);
	}
	operator glm::vec2() const{
		return glm::vec2(glm::detail::toFloat32(x), glm::detail::toFloat32(y));
	}
};


struct f16vec4{
	glm::detail::hdata x, y, z, w;
	f16vec4() : f16vec4(glm::vec4(0.0f)){

	}
	f16vec4(glm::vec4 v){
		x = glm::detail::toFloat16(v.x);
		y = glm::detail::toFloat16(v.y);
		z = glm::detail::toFloat16(v.z);
		w = glm::detail::toFloat16(v.w);
	}
	f16vec4(glm::vec3 v, float vw){
		x = glm::detail::toFloat16(v.x);
		y = glm::detail::toFloat16(v.y);
		z = glm::detail::toFloat16(v.z);
		w = glm::detail::toFloat16(vw);
	}
	f16vec4(float a, float b, float c, float d){
		x = glm::detail::toFloat16(a);
		y = glm::detail::toFloat16(b);
		z = glm::detail::toFloat16(c);
		w = glm::detail::toFloat16(d);
	}
	f16vec4(float a){
		x = glm::detail::toFloat16(a);
		y = glm::detail::toFloat16(a);
		z = glm::detail::toFloat16(a);
		w = glm::detail::toFloat16(a);
	}
	operator glm::vec4() const{
		return glm::vec4(glm::detail::toFloat32(x), glm::detail::toFloat32(y), glm::detail::toFloat32(z), glm::detail::toFloat32(w));
	}
};

struct float16_t{
	glm::detail::hdata f;
	float16_t(float f){
		this->f = glm::detail::toFloat16(f);
	}

	operator float() const{
		return glm::detail::toFloat32(f);
	}
};

const int MAX_CAMERAS = 128;//This is a hard limit

static inline bool isCameraActive(glm::ivec4 activeCameras, int camIndex){
	return (activeCameras[camIndex/32] & (1<<(camIndex%32))) != 0;
}
static inline void setCameraActive(glm::ivec4& activeCameras, int camIndex, bool b){
	activeCameras[camIndex/32] ^= (-b ^ activeCameras[camIndex/32]) & (1UL << (camIndex % 32));
}

const std::string exportFileFormat = "ply";

const bool SortDepthTilesByMortonOrder = false;
const int envMapPyramidLevel = 1;
extern int imagePyramidLevels;
extern int segmentationPyramidLevel;

const int DEPTHMAP_TILE_WIDTH = 8;
const int DEPTHMAP_TILE_SIZE = DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH;

const int VOXELS_PER_SIDE = 4;
const int VOXELS_PER_TILE = VOXELS_PER_SIDE * VOXELS_PER_SIDE * VOXELS_PER_SIDE;

/** Each depthmap is cut in DEPTHMAP_TILE_WIDTH * DEPTHMAP_TILE_WIDTH tiles
 *	Describes the contents of a tile
 */
struct DepthTileDescriptor{
	glm::ivec2 tileCoords;//coordinates of the tile in the image
	int foregroundPixelCount;//number of actual foreground pixels in the tile
	int camIndex;//index of the camera
};

/**
 * 	Each element of a depth tile is a DepthPixel
 */
struct DepthPixel{
	float depth;
};

/**
 * Each foreground point having at least one background neighbor is a BorderPoint
 */
struct BorderPoint{
	glm::vec4 uv_viewpoint_type;
};

/**
 * A descriptor of a 3D tile
 */
struct TileDesc{
	glm::ivec4 coords_index;
	glm::ivec4 activeCameras;
};

struct CameraData{
	glm::vec4 K;				// vec4(cx, cy, fx, fy)
	glm::vec4 distortion1;   // vec4(k1, k2, p1, p2)
	glm::vec4 distortion2;   // vec4(k3, 0, w, h)
	glm::vec4 camCenter;
	glm::mat4 cameraTransform;
	glm::mat4 cameraTransformInv;
};


struct GlobalIntegrationTexel{
	f16vec4 transmittance_rgb;
};

struct TileTextureTexel{
	//f16vec4 transmittance_rgb;
	glm::u8vec4 transmittance_rgb;
};

struct TileTextureTexel2{
	uint16_t v;//actually a half float
	//uint8_t v;//actually a truncated half float
};

struct RayDescriptor{
	int intervalsPtr;
	int intervalsCount;
	int texelsPtr;
	int texelsCount;
	int depth_tile_x;
	int depth_tile_y;
};

struct Interval{
	int16_t start;//inclusive
	int16_t end;//exclusive
};

struct Vertex{
	glm::vec4 p;
	glm::vec4 n;
};

struct Gradient{
	glm::u8vec4 grad;
};

const GLenum voxelInternalFormat = GL_RGBA8;
const GLenum voxelGradsInternalFormat = GL_RGBA8;

static inline uint16_t float16BitsToUint16(float16_t v) {
	uint16_t r;
	memcpy(&r, &v, sizeof(uint16_t));
	return r;
}

static inline float16_t uint16BitsToFloat16(uint16_t v) {
	float16_t r(0.0f);
	memcpy(&r.f, &v, sizeof(uint16_t));
	return r;
}

static const float encodeScaling = 2048;
static inline glm::u8vec4 encodeSignedFloat8(glm::vec4 v){
	v *= encodeScaling;
	f16vec4 v2 = f16vec4(v);
	glm::uvec4 r = glm::uvec4(
			float16BitsToUint16(v2.x),
			float16BitsToUint16(v2.y),
			float16BitsToUint16(v2.z),
			float16BitsToUint16(v2.w)
		);
	return glm::u8vec4(r >> 8u);
}

static inline glm::vec4 decodeSignedFloat8(glm::u8vec4 r0){
	glm::u16vec4 r = glm::u16vec4(glm::uvec4(r0) << 8u);
	f16vec4 v2 = f16vec4(
			uint16BitsToFloat16(r.x),
			uint16BitsToFloat16(r.y),
			uint16BitsToFloat16(r.z),
			uint16BitsToFloat16(r.w)
		);
	return glm::vec4(v2) / encodeScaling;
}

struct Stats{
	int iterations;
	double elapsedTime;
	double vramUsage;


};

#endif /* SEQUENCE_COMMONTYPES_H_ */
