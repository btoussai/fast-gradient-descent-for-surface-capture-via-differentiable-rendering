/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * DDATest.h
 *
 *  Created on: 2 févr. 2022
 *      Author: Briac
 */

#ifndef SRC_SEQUENCE_DDATEST_H_
#define SRC_SEQUENCE_DDATEST_H_

#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Parameters.h"
#include "Backgrounds.h"
#include "Depthmap.h"
#include "DifferentialRendering.h"
#include "emulator/ShaderEmulator.h"
#include "CommonTypes.h"
#include "SparseGrid.h"

#include "../renderdoc/InitRenderDoc.h"
#include "../stb/stb_image_write.h"

class DDATest {
public:
	DDATest();
	virtual ~DDATest();

	void updateGUIs(const RenderingBase::Camera& camera, std::vector<ImageLOD>& imageLODs, AllDepthmaps& depthmaps,
			SparseGrid& sparseGrid, DifferentialRendering& differentialRendering, Backgrounds& backgrounds,
			glm::ivec2 selectedPixel, glm::ivec2 selectedDepthTile);
	void render(const RenderingBase::Camera& camera, SparseGrid &sparseGrid, const std::vector<ImageLOD>& imageLODs,
			RENDER_PROJECTION_TYPE PROJECTION_TYPE, glm::mat4 canvas_transform, bool RemoveDistortion);

private:
	RenderingBase::Parameters params = RenderingBase::Parameters("DDATest");
	RenderingBase::Shader rayShader = RenderingBase::Shader("ddaTest/ray.vs", "ddaTest/ray.fs");

	int nonEmptyTiles = 0;
	int tilesIntersected = 0;
	int intervalsCount = 0;
	int maxIntervalLength = 0;
	int totalIntervalLengths = 0;


	glm::vec4 final_transmittance_rgb;
	glm::vec4 global_transmittance_rgb;
	glm::vec4 emulated_transmittance_rgb;


	glm::ivec2 depthTileCoords;
	int depthTileIndex = 0;
	RayDescriptor rayDescriptor;
	std::vector<Interval> gpuIntervals;
	std::vector<glm::vec4> outSamplePoints;
	std::vector<glm::vec4> inSamplePoints;
	std::vector<glm::vec4> accumulatedTransmittanceColor;
	std::vector<int> samplesLocation;

	struct buffers{
		GLEmulator::GL_PTR occupancyDescriptorsPTR;
		GLEmulator::GL_PTR cameraDataPTR;
		GLEmulator::GL_PTR ray_descriptorsPTR;
		GLEmulator::GL_PTR intervalsPTR;
		std::vector<GLEmulator::uimage2D> RayTexels;
		GLEmulator::GL_PTR global_integration_texturePTR;
		GLEmulator::usampler3D CompressedTileOccupancy;
		GLEmulator::isampler3D TileOccupancy;
		GLEmulator::isampler2DArray occupancyImageSampler;

		buffers(std::vector<ImageLOD>& imageLODs, AllDepthmaps &depthmaps, SparseGrid &sparseGrid, Backgrounds& backgrounds,
				DifferentialRendering &differentialRendering) :
			CompressedTileOccupancy(sparseGrid.getCompressedTileOccupancy()->getID(), 2, 4,
					sparseGrid.getCompressedTileOccupancy()->getWidth(),
					sparseGrid.getCompressedTileOccupancy()->getHeight(),
					sparseGrid.getCompressedTileOccupancy()->getDepth(),
					GL_RG_INTEGER, GL_UNSIGNED_INT),
			TileOccupancy(sparseGrid.getTileOccupancy()->getID(), 1, 4,
					sparseGrid.getTileOccupancy()->getWidth(),
					sparseGrid.getTileOccupancy()->getHeight(),
					sparseGrid.getTileOccupancy()->getDepth(),
					GL_RED_INTEGER, GL_INT),
			occupancyImageSampler(imageLODs.at(sparseGrid.getCurrentLOD()).occupancyTexture, 1, 4, GL_RED_INTEGER, GL_INT)
		{
			occupancyDescriptorsPTR.set(imageLODs.at(sparseGrid.getCurrentLOD()).depthTileDescriptorsBlock, true);
			cameraDataPTR.set(imageLODs.at(sparseGrid.getCurrentLOD()).CameraDataBlock, true);
			ray_descriptorsPTR.set(differentialRendering.getRayDescriptorsBlock(), true);
			intervalsPTR.set(differentialRendering.getIntervalsBlock(), true);

			InitRenderDoc::startFrame();
			differentialRendering.rayIntegration(backgrounds, sparseGrid);
//			differentialRendering.writeAccumulatedTransmittance(backgrounds, sparseGrid);
			InitRenderDoc::stopFrame();

			[[maybe_unused]]int width = differentialRendering.getRayTexels().getWidth();
			[[maybe_unused]]int height = differentialRendering.getRayTexels().getHeight();

			RenderingBase::VAO quad;
			quad.bind();
			float positions[] = { -1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f };
			quad.createFloatAttribute(0, positions, 8, 2, 0, GL_STATIC_DRAW);
			quad.unbind();

			RenderingBase::Shader shader = RenderingBase::Shader("copyBindlessTexture/copyBindlessTexture.cp");
			shader.finishInit();
			shader.init_uniforms({"size", "layer"});
			shader.start();

			RayTexels.reserve(differentialRendering.getRayTexels().getLayers());
			glBindBufferBase(GL_UNIFORM_BUFFER, 1, differentialRendering.getRayTexels().getImageHandlesVBO().getID());
			differentialRendering.getRayTexels().makeImageHandlesResident(true, GL_READ_ONLY);
			for(int i=0; i<differentialRendering.getRayTexels().getLayers(); i++){
				shader.loadInt("layer", i);
				shader.loadiVec2("size", glm::ivec2(width, height));

				RenderingBase::VBO buff = RenderingBase::VBO(GL_SHADER_STORAGE_BUFFER);
				buff.bind();
				buff.storeData(nullptr, width * height * 4, GL_STATIC_DRAW);
				buff.unbind();

				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, buff.getID());
				glDispatchCompute(width * height / 64, 1, 1);

				unsigned char* pixels = new unsigned char[width * height * 4];

				unsigned char* ptr = (unsigned char*)glMapNamedBuffer(buff.getID(), GL_READ_ONLY);
				memcpy(pixels, ptr, width * height * 4);
				glUnmapNamedBuffer(buff.getID());

				RayTexels.emplace_back(4, 1, width, height, pixels);

			}
			glBindBufferBase(GL_UNIFORM_BUFFER, 1, 0);
			differentialRendering.getRayTexels().makeImageHandlesResident(false, GL_READ_ONLY);
			shader.stop();

			global_integration_texturePTR.set(differentialRendering.getGlobalIntegrationTextureBlock(), true);

		}
	};

	std::unique_ptr<buffers> buffs;

	void main_shader();
	void searchIntersectionWithSelectedTile(glm::ivec3 selectedTile);
	void globalIntegration();

	struct CubicTileProjection{
		glm::ivec2 pixelMinCorner;
		glm::ivec3 dims;
		int stepsBeginFrustum;
		int stepsEndFrustum;
		float minDepth;
		float maxDepth;
	};
	CubicTileProjection tileProjection = {};
	glm::ivec4 sampleDistances;
	glm::vec3 froxelRatio;
	void computeTileFrustum(glm::ivec3 selectedTile, int camIndex);
	void computeSamples(glm::ivec3 selectedTile, int camIndex);
	void computeFrustumAspectRatio(glm::ivec3 selectedTile, int camIndex);


	glm::vec3 project(glm::vec3 position, int camIndex);
	glm::vec3 unproject(glm::vec2 coords, int camIndex);
	glm::vec3 unproject(glm::vec2 coords, float depth, int camIndex);
	glm::vec3 getCamCenter(int camIndex);
	glm::vec2 getImageSize(int camIndex);
	glm::vec2 toPixels(glm::vec2 NDCPixel, int camIndex);
	glm::vec2 toNDC(glm::vec2 Pixel, int camIndex);
	bool rayVSbox(glm::vec3 base, glm::vec3 ray_inv, glm::vec3 minCorner, glm::vec3 maxCorner, float& tmin, float& tmax);
	glm::ivec2 safeCeil(glm::vec2 coords);
	glm::ivec2 safeFloor(glm::vec2 coords);






};

#endif /* SRC_SEQUENCE_DDATEST_H_ */
