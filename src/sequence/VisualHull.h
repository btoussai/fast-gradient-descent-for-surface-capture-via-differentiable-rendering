/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * PointCloud.h
 *
 *  Created on: 23 oct. 2021
 *      Author: Briac
 */

#ifndef SEQUENCE_VISUALHULL_H_
#define SEQUENCE_VISUALHULL_H_

#include "../rendering_base/openglObjects.h"
#include "../rendering_base/Camera.h"
#include "../rendering_base/Parameters.h"

#include <vector>

#include "CommonTypes.h"
#include "Texture2DArray.h"
#include "Depthmap.h"
#include "DownScale.h"
#include "Backgrounds.h"
#include "SparseGrid.h"

class VisualHull {
public:
	VisualHull(std::vector<ImageLOD>& imageLODs);
	virtual ~VisualHull();

	bool displayStatusMsg();
	void updateGUI(const RenderingBase::Camera &camera, Backgrounds& backgrounds, AllDepthmaps& depthmaps, const DownScale& downscale, const SparseGrid& sparseGrid, int selectedCameraIndex);
	void render(const RenderingBase::Camera &camera, const AllDepthmaps& depthmaps);
	void render(int selectedCameraIndex, const glm::mat4 &canvas_transform,
			const AllDepthmaps& depthmaps, bool RemoveDistortion, bool DrawBorder,
			bool DrawBorderBVH);

	void raytracingOnBorders(const AllDepthmaps& depthmaps, const SparseGrid& sparseGrid);
	void build2DOccupancyTextures(const AllDepthmaps& depthmaps, const DownScale& downscale);
private:

	struct UniformData{
		int viewIndex;
		float centerProjDist;
		int NodeOffset;
		int SegmentOffset;
	};

	std::vector<UniformData> buildUniformsData(const std::vector<CameraInfo> &cameras, const AllDepthmaps& depthmaps, int i);
	void raytracingOnBorders(const AllDepthmaps& depthmaps, int i);

	RenderingBase::Parameters params = RenderingBase::Parameters("PointCloud");
	RenderingBase::Shader shader = RenderingBase::Shader(
			"visual_hull/point_cloud.vs", "visual_hull/point_cloud.fs");

	RenderingBase::Shader borderShader = RenderingBase::Shader(
			"visual_hull/border.vs", "visual_hull/border.gs", "visual_hull/border.fs");
	RenderingBase::Shader bvhShader = RenderingBase::Shader("visual_hull/bvh.vs", "visual_hull/bvh.gs", "visual_hull/bvh.fs");

	RenderingBase::Shader raytracingShader = RenderingBase::Shader("visual_hull/raytracingShader.cp");
	RenderingBase::Shader writeVisualHullMaskShader = RenderingBase::Shader("visual_hull/writeVisualHullMask.cp");

	std::vector<ImageLOD>& imageLODs;
	std::vector<RenderingBase::QueryBuffer> timers;

};

#endif /* SEQUENCE_VISUALHULL_H_ */
