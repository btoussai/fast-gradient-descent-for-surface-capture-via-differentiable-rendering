/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * SequenceLoader.cpp
 *
 *  Created on: 20 mai 2022
 *      Author: Briac
 */

#include "SequenceLoader.h"

#include "../tinyxml2/tinyxml2.h"
using namespace tinyxml2;


static inline glm::vec3 readVec3(const char *str) {
	std::stringstream ss(str);
	glm::vec3 v;
	ss >> v.x >> v.y >> v.z;
	return v;
}
static inline glm::mat3 readMat3(const char *str) {
	std::stringstream ss(str);
	glm::mat3 m;
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			ss >> m[j][i];
		}
	}
	return m;
}

SequenceInfo SequenceLoader::load(
		const std::string &sequencefilepath,
		const std::string &calibrationfilepath) {

	SequenceInfo sequenceInfo;

	{
		XMLDocument doc;
		doc.LoadFile(sequencefilepath.c_str());

		const XMLElement *video = doc.RootElement();

		if (!video) {
			throw "Couldn't read " + sequencefilepath;
		}

		const XMLElement *information = video->FirstChildElement("Information");

		sequenceInfo.nbCameras = std::atoi(information->Attribute("nbCamera"));
		sequenceInfo.record_fps = std::atof(information->Attribute("fps"));
		sequenceInfo.beginId = std::atoi(information->Attribute("beginId"));
		sequenceInfo.endId = std::atoi(information->Attribute("endId"));

		sequenceInfo.cameras.reserve(sequenceInfo.nbCameras);

		const XMLElement *camera = information->NextSiblingElement("Camera");
		while (camera) {
			int id = std::atoi(camera->Attribute("id"));
			std::string path = camera->Attribute("path");
			std::string fileNameFormat = camera->Attribute("fileNameFormat");
			std::string silhouettePath = path;

			sequenceInfo.cameras.emplace_back(CameraInfo{id, path, silhouettePath, fileNameFormat,
				sequenceInfo.beginId, sequenceInfo.endId});
			camera = camera->NextSiblingElement("Camera");
		}

		std::cout << "Read sequence file " << sequencefilepath << " with "
				<< sequenceInfo.nbCameras << " cameras." << std::endl;

		if(sequenceInfo.nbCameras > MAX_CAMERAS){
			throw "Error, at most " + std::to_string(MAX_CAMERAS) + " are supported.";
		}

		sequenceInfo.sequenceDir =
				std::filesystem::path(sequencefilepath).parent_path().string();
		sequenceInfo.silhouettesDir = std::filesystem::path(sequenceInfo.sequenceDir).parent_path().string() + "/MaskedSilhouettes";

		sequenceInfo.hasSilhouettes = std::filesystem::exists(sequenceInfo.silhouettesDir);
		if(sequenceInfo.hasSilhouettes){
			std::cout <<"Silhouettes dir detected: " <<sequenceInfo.silhouettesDir <<std::endl;
		}

	}

	{
		XMLDocument doc;
		doc.LoadFile(calibrationfilepath.c_str());
		const XMLElement *CalibrationResult = doc.RootElement();
		if (!CalibrationResult) {
			throw "Couldn't read " + sequencefilepath;
		}

		const XMLElement *information = CalibrationResult->FirstChildElement(
				"Information");

		bool error = false;
		error |= sequenceInfo.nbCameras != std::atoi(information->Attribute("nbCamera"));
//		if (information->FindAttribute("units") != nullptr) {
//			error |= std::string("mm") != information->Attribute("units");
//		} else {
//			std::cout << "No units were specified, assuming mm" << std::endl;
//		}
//		if (information->FindAttribute("up") != nullptr) {
//			error |= std::string("+z") != information->Attribute("up");
//		} else {
//			std::cout << "No up vector given, assuming up == +z." << std::endl;
//		}
		if (error) {
			throw std::string("Error while reading ") + sequencefilepath;
		}
		const XMLElement *camera = information->NextSiblingElement("Camera");
		int n = 0;
		while (camera) {
			CameraInfo &camInfo = sequenceInfo.cameras.at(n);

			int id = std::atoi(camera->Attribute("id"));
			if (id != camInfo.ID) {
				throw std::string("Error while reading ") + sequencefilepath;
			}

			camInfo.width = std::atoi(camera->Attribute("width"));
			camInfo.height = std::atoi(camera->Attribute("height"));

			const XMLElement *eK = camera->FirstChildElement("K");
			const XMLElement *eDistortion = camera->FirstChildElement(
					"Distortion");
			const XMLElement *eR = camera->FirstChildElement("R");
			const XMLElement *eT = camera->FirstChildElement("T");

			const XMLAttribute* attribModel = eDistortion->FindAttribute("model");
			if(attribModel == nullptr){
//				std::cout <<"No projection model given, defaulting to 4dview" <<std::endl;
				camInfo.projection_model = PROJECTION_TYPE::PROJECTION_FOURDVIEW;
			}else{
				std::string distortionModel = attribModel->Value();
//				std::cout <<distortionModel <<std::endl;

				if(distortionModel == "opencv"){
					camInfo.projection_model = PROJECTION_TYPE::PROJECTION_OPENCV;
				}else if(distortionModel == "4dview"){
					camInfo.projection_model = PROJECTION_TYPE::PROJECTION_FOURDVIEW;
				}else{
					throw std::string(
							"Only 'opencv' and '4dview' are accepted distortion models for now");
				}
			}

			camInfo.distortion.push_back(
					std::atof(eDistortion->Attribute("K1")));
			camInfo.distortion.push_back(
					std::atof(eDistortion->Attribute("K2")));

			if(camInfo.projection_model == PROJECTION_TYPE::PROJECTION_FOURDVIEW){
				camInfo.distortion.push_back(
						std::atof(eDistortion->Attribute("Cx")));
				camInfo.distortion.push_back(
						std::atof(eDistortion->Attribute("Cy")));
			}else{
				camInfo.distortion.push_back(0);
				camInfo.distortion.push_back(0);
			}
			//camInfo.distortion.push_back(std::atof(eDistortion->Attribute("K3")));
			//ignore K3
			camInfo.distortion.push_back(0);
			//ignore P1 and P2 as well

			camInfo.K = readMat3(eK->GetText());
			camInfo.R_backup = camInfo.R = readMat3(eR->GetText());
			camInfo.T_backup = camInfo.T = readVec3(eT->GetText());

			camera = camera->NextSiblingElement("Camera");
			n++;
		}

		std::cout << "Read calibration file " << calibrationfilepath << " with "
				<< sequenceInfo.nbCameras << " cameras." << std::endl;
	}

	sequenceInfo.projection_model = sequenceInfo.cameras.at(0).projection_model;
	for(CameraInfo& cam : sequenceInfo.cameras){
		if(cam.projection_model != sequenceInfo.projection_model){
			throw "All cameras must use the same projection model";
		}
	}

	return sequenceInfo;
}

RenderingBase::ShaderPreprocessor SequenceLoader::buildShaderPreprocessorReplacements(
		RenderingBase::Parameters& params,
		const SequenceInfo& sequenceInfo) {
	std::unordered_map<std::string, std::string> m;

	int SHBands = params.getInt("Window#SHBands");
	int paramsPerVoxel = 1 + 3*(SHBands+1)*(SHBands+1);
	int VoxelsTexturesCount = (int)ceil(paramsPerVoxel / 4.0);

	m["#define #SHBands"] = "#define SHBands " + std::to_string(SHBands);
	m["#define #VoxelsTexturesCount"] = "#define VoxelsTexturesCount " + std::to_string(VoxelsTexturesCount);

	m["#PROJECTION_MODEL"] = std::to_string(sequenceInfo.projection_model);

	RenderingBase::ShaderPreprocessor preprocessor = RenderingBase::ShaderPreprocessor(m);

	RenderingBase::Shader::buildNamedString("common/sampling3D.txt", "/common/sampling3D.txt");
	RenderingBase::Shader::buildNamedString("common/projection.txt", "/common/projection.txt");
	RenderingBase::Shader::buildNamedString("common/rayVSBox.txt", "/common/rayVSBox.txt");
	RenderingBase::Shader::buildNamedString("common/robust_norm.txt", "/common/robust_norm.txt");
	RenderingBase::Shader::buildNamedString("common/float_encoding.txt", "/common/float_encoding.txt");

	return preprocessor;
}
