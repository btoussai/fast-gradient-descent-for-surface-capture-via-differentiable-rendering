/*
*    	Software KinovisReconstruction
*	Copyright © by Briac Toussaint and Jean-Sébastien Franco
*	FREE SOFTWARE LICENSE AGREEMENT FOR NON-COMMERCIAL PURPOSES – 2022
*
*	Contact : jean-sebastien.franco@inria.fr  
*
*/

/*
 * KinovisReconstructionMain.cpp
 *
 *  Created on: 5 oct. 2021
 *      Author: Briac
 */

#include <string>
#include <iostream>

#include <exception>
#include <signal.h>
#include <assert.h>

#include "rendering_base/Window.h"

void terminate_handler() {
	std::cout << "Unhandled exception" << std::endl;
	std::abort();
}


extern "C" void handle_aborts(int signal_number)
{
	switch(signal_number){
	case SIGABRT:
		std::cerr <<"SIGABRT";
		break;
	case SIGSEGV:
		std::cerr <<"SIGSEGV";
		break;
	case SIGTERM:
		std::cerr <<"SIGTERM";
		break;
	case SIGFPE:
		std::cerr <<"SIGFPE";
		break;
	default:
		std::cerr <<signal_number;
		break;
	}
    std::cerr << " signal received, shutting down." <<std::endl;
    std::flush(std::cerr);
    std::flush(std::cout);
}

#include <barrier>
#include <iostream>
#include <string>
#include <thread>

#include "renderdoc/InitRenderDoc.h"

int main(int argc, char* argv[]) {

	std::set_terminate(terminate_handler);
	signal(SIGABRT, &handle_aborts);
	signal(SIGSEGV, &handle_aborts);
	signal(SIGTERM, &handle_aborts);
	signal(SIGFPE, &handle_aborts);

	InitRenderDoc::init();

	int samples = 4; //multisampling

	try {
		RenderingBase::Window w(std::string("Kinovis Reconstruction"), samples);

		w.mainloop(argc, argv);
	} catch (const char* msg) {
		std::cerr << msg << std::endl;
	} catch (const std::string& msg) {
		std::cerr << msg << std::endl;
	} catch (const std::bad_cast& e) {
		std::cerr << e.what() << std::endl;
	}
    std::flush(std::cerr);


	return 0;
}





