/*
 * progress_bar.h
 *
 *  Created on: 23 mars 2022
 *      Author: Briac
 *
 *      Taken from https://github.com/ocornut/imgui/issues/1901
 *
 */

#ifndef IMGUI_PROGRESS_BAR_H_
#define IMGUI_PROGRESS_BAR_H_

#include "imgui.h"

namespace ImGui {
    bool BufferingBar(const char* label, float value,  const ImVec2& size_arg, const ImU32& bg_col, const ImU32& fg_col);
    bool Spinner(const char* label, float radius, int thickness, const ImU32& color);

}



#endif /* IMGUI_PROGRESS_BAR_H_ */
