DifferentialRendering#IterationsPerLod ivec4 10 40 100 400
DifferentialRendering#MaskReg float 0.027390
DifferentialRendering#SHLaplacianReg float 0.002960
DifferentialRendering#SHParcimonyReg float 0.002960
DifferentialRendering#backgroundEntropyReg float 0.006800
DifferentialRendering#ballooningTerm float 0.000000
DifferentialRendering#baseColorLaplacianReg float 0.002630
DifferentialRendering#colorDiscrepancyReg float 0.153940
DifferentialRendering#opacityLaplacianReg float 0.001930
SceneReferenceFrame#FrustumsLength float 1.000000
SceneReferenceFrame#Rotation vec3 -114.690002 0.000000 0.000000
SceneReferenceFrame#SceneScale float 0.500000
SceneReferenceFrame#Translation vec3 0.000000 0.000000 0.000000
SparseGrid#TSDFTilesDim vec3 377.000000 286.000000 323.000000
SparseGrid#VolumeMinCorner vec3 -1.828000 -1.077000 -1.450000
SparseGrid#VoxelSize float 2.000000
Window#BackgroundsDir string unknown
Window#CalibrationFile string unknown/calib.xml
Window#ExportDir string unknown
Window#SequenceFile string unknown/sequence.xml
Window#imagePyramidLevels int 4
Window#segmentationPyramidLevel int 1
SceneReferenceFrame#ScaleMatrixRow0 vec4 225.08636474609375 0.0 0.0 -86.59539794921875
SceneReferenceFrame#ScaleMatrixRow1 vec4 0.0 225.08636474609375 0.0 -40.850154876708984
SceneReferenceFrame#ScaleMatrixRow2 vec4 0.0 0.0 225.08636474609375 708.9678344726562
SceneReferenceFrame#ScaleMatrixRow3 vec4 0.0 0.0 0.0 1.0
SceneReferenceFrame#ScaleMatrixRow0 vec4 225.08636474609375 0.0 0.0 -86.59539794921875
SceneReferenceFrame#ScaleMatrixRow1 vec4 0.0 225.08636474609375 0.0 -40.850154876708984
SceneReferenceFrame#ScaleMatrixRow2 vec4 0.0 0.0 225.08636474609375 708.9678344726562
SceneReferenceFrame#ScaleMatrixRow3 vec4 0.0 0.0 0.0 1.0
