DifferentialRendering#MaskReg float 0.023280
SceneReferenceFrame#FrustumsLength float 0.500000
SceneReferenceFrame#Rotation vec3 -114.690002 0.000000 0.000000
SceneReferenceFrame#SceneScale float 0.500000
SceneReferenceFrame#Translation vec3 0.000000 0.000000 0.000000
SparseGrid#TSDFTilesDim vec3 177.000000 249.000000 155.000000
SparseGrid#VolumeMinCorner vec3 -0.600000 -0.952000 -0.693000
SparseGrid#VoxelSize float 2.000000
Window#BackgroundsDir string unknown
Window#CalibrationFile string unknown/calib.xml
Window#ExportDir string unknown
Window#SequenceFile string unknown/sequence.xml
Window#imagePyramidLevels int 4
Window#segmentationPyramidLevel int 1
SceneReferenceFrame#ScaleMatrixRow0 vec4 302.9843444824219 0.0 0.0 -41.97808074951172
SceneReferenceFrame#ScaleMatrixRow1 vec4 0.0 302.9843444824219 0.0 -19.879968643188477
SceneReferenceFrame#ScaleMatrixRow2 vec4 0.0 0.0 302.9843444824219 632.1859130859375
SceneReferenceFrame#ScaleMatrixRow3 vec4 0.0 0.0 0.0 1.0
SceneReferenceFrame#ScaleMatrixRow0 vec4 302.9843444824219 0.0 0.0 -41.97808074951172
SceneReferenceFrame#ScaleMatrixRow1 vec4 0.0 302.9843444824219 0.0 -19.879968643188477
SceneReferenceFrame#ScaleMatrixRow2 vec4 0.0 0.0 302.9843444824219 632.1859130859375
SceneReferenceFrame#ScaleMatrixRow3 vec4 0.0 0.0 0.0 1.0
