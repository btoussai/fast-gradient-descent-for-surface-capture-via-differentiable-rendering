DifferentialRendering#IterationsPerLod ivec4 10 40 100 400
DifferentialRendering#MaskReg float 0.016940
DifferentialRendering#SHLaplacianReg float 0.002960
DifferentialRendering#SHParcimonyReg float 0.002960
DifferentialRendering#backgroundEntropyReg float 0.006800
DifferentialRendering#ballooningTerm float 0.000020
DifferentialRendering#baseColorLaplacianReg float 0.002630
DifferentialRendering#colorDiscrepancyReg float 0.153940
DifferentialRendering#opacityLaplacianReg float 0.001930
SceneReferenceFrame#FrustumsLength float 0.500000
SceneReferenceFrame#Rotation vec3 -114.690002 0.000000 0.000000
SceneReferenceFrame#SceneScale float 0.500000
SceneReferenceFrame#Translation vec3 0.000000 0.000000 0.000000
SparseGrid#TSDFTilesDim vec3 279.000000 286.000000 323.000000
SparseGrid#VolumeMinCorner vec3 -0.923000 -1.077000 -1.450000
SparseGrid#VoxelSize float 2.000000
Window#BackgroundsDir string unknown
Window#CalibrationFile string unknown/calib.xml
Window#ExportDir string unknown
Window#SequenceFile string unknown/sequence.xml
Window#imagePyramidLevels int 4
Window#segmentationPyramidLevel int 1
SceneReferenceFrame#ScaleMatrixRow0 vec4 336.7069091796875 0.0 0.0 -16.939083099365234
SceneReferenceFrame#ScaleMatrixRow1 vec4 0.0 336.7069091796875 0.0 -31.074146270751953
SceneReferenceFrame#ScaleMatrixRow2 vec4 0.0 0.0 336.7069091796875 652.7977294921875
SceneReferenceFrame#ScaleMatrixRow3 vec4 0.0 0.0 0.0 1.0
SceneReferenceFrame#ScaleMatrixRow0 vec4 336.7069091796875 0.0 0.0 -16.939083099365234
SceneReferenceFrame#ScaleMatrixRow1 vec4 0.0 336.7069091796875 0.0 -31.074146270751953
SceneReferenceFrame#ScaleMatrixRow2 vec4 0.0 0.0 336.7069091796875 652.7977294921875
SceneReferenceFrame#ScaleMatrixRow3 vec4 0.0 0.0 0.0 1.0
