DifferentialRendering#activeCameras ivec4 -1 -1 -1 15
DifferentialRendering#activeCameras vec4 -1.000000 -1.000000 15.000000 0.000000
SceneReferenceFrame#FrustumsLength float 0.465000
SceneReferenceFrame#Rotation vec3 90.000000 0.000000 0.000000
SceneReferenceFrame#ScaleMatrixRow0 vec4 270.711731 0.000000 0.000000 -15.406829
SceneReferenceFrame#ScaleMatrixRow1 vec4 0.000000 270.711731 0.000000 -4.745273
SceneReferenceFrame#ScaleMatrixRow2 vec4 0.000000 0.000000 270.711731 646.610779
SceneReferenceFrame#ScaleMatrixRow3 vec4 0.000000 0.000000 0.000000 1.000000
SceneReferenceFrame#SceneScale float 0.679000
SceneReferenceFrame#Translation vec3 -0.619000 -0.531000 -1.150000
SparseGrid#TSDFTilesDim vec3 349.000000 157.000000 317.000000
SparseGrid#VolumeMinCorner vec3 -2.520000 0.551000 -3.701000
SparseGrid#VoxelSize float 5.000000
Window#BackgroundsDir string unknown
Window#CalibrationFile string unknown/calib.xml
Window#ExportDir string unknown
Window#SequenceFile string unknown/sequence.xml
Window#imagePyramidLevels int 4
Window#segmentationPyramidLevel int 1
